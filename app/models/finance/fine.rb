class Fine < ActiveRecord::Base
  belongs_to :user
  has_many :finance_fee_collections
  has_many :fine_rules
  validates_presence_of :name
  validates_uniqueness_of :name, :scope => [:is_deleted], :if => 'is_deleted == false'
  validates_format_of :name, :with => /^\S.*\S$/i, :message => :should_not_contain_white_spaces_at_the_beginning_and_end
  validate :uniqueness_fine_days

  accepts_nested_attributes_for :fine_rules, :allow_destroy => true

  named_scope :active, {:conditions => {:is_deleted => false}}
  # validates_associated :fine_rules
  validate :mark_fine_slabs_for_removal

  def mark_fine_slabs_for_removal
    fine_rules.each do |fine_rule|
      if (fine_rule._destroy == true)
        if finance_fee_collections.empty? or (finance_fee_collections.all( :conditions => "finance_fee_collections.created_at < '#{fine_rule.created_at}'")).present?
          fine_rule.mark_for_destruction
        else
          errors.add_to_base(t('cant_delete_fee_collection_assigned'))
          false
        end
      end
    end
  end


  def uniqueness_fine_days
    hash = {}
    fine_rules.each do |child|
      if hash[child.fine_days]
        errors.add(:fine_days, :taken) if errors[:fine_days].blank?
      end
      hash[child.fine_days]=true
    end
  end
end
