class FinanceTransactionLedger < ActiveRecord::Base
  has_many :finance_transactions, :foreign_key => 'transaction_ledger_id'
  has_many :cancelled_finance_transactions, :foreign_key => 'transaction_ledger_id'
  belongs_to :payee, :polymorphic => true
  accepts_nested_attributes_for :finance_transactions
  
  before_validation :set_transaction_mode, :set_transaction_type
  validates_presence_of :transaction_type, :transaction_mode
  before_create :generate_and_set_receipt_no #generate_receipt_no
  attr_accessor :category_is_income
  
  named_scope :active_transactions, {:conditions => "status = 'ACTIVE' or status = 'PARTIAL'" }
  named_scope :cancelled_transactions, {:conditions => {:status => 'CANCELLED'}}
  #  named_scope :cancelled_transactions
  #  Various codes for transaction ledger
  # modes for transaction receipt generation
  #  0  =>  SINGLE
  #  1  =>  MULTIPLE
  # transaction types
  #  0  =>  SINGLE
  #  1  =>  MULTIPLE
  # transaction status
  #  0  =>  CANCELLED
  #  1  =>  ACTIVE 
  #  2  =>  PARTIAL   # some transactions is reverted under a ledger record
  
  def self.receipt_number_settings
    # 0 : single receipt number for multi
    # 1 : individual receipt number for respective finance transactions
    Configuration.get_config_value('SingleReceiptNumber').try(:to_i) || 0
  end

  def set_transaction_mode
    # set mode of receipt generation while transaction execution was recorded
    # 0 : Multiple receipt 
    # 1 : Single receipt
    self.transaction_mode = FinanceTransactionLedger.receipt_number_settings.zero? ? "MULTIPLE" : "SINGLE" if new_record?
  end
    
  def set_transaction_type    
    self.transaction_type = 'SINGLE' unless transaction_type.present?
  end
  
  def mark_cancelled(reason=nil, revert_mode = "FULL")
    if revert_mode == "PARTIAL"
      self.status = self.finance_transactions.count > 0 ? "PARTIAL" : "CANCELLED"
      self.save
    else
      self.status = "CANCELLED" #"CANCELLED"
      self.save
      self.destroy_finance_transactions reason if finance_transactions.present?
    end
  end

  def destroy_finance_transactions reason
    ActiveRecord::Base.transaction do
      #      ft= FinanceTransaction.find(params[:transaction_id])
      self.finance_transactions.each do |ft|
        ft.cancel_reason = reason
        if FedenaPlugin.can_access_plugin?("fedena_pay")
          finance_payment = ft.finance_payment
          unless finance_payment.nil?
            status = Payment.payment_status_mapping[:reverted]
            finance_payment.payment.update_attributes(:status_description => status)
            #            payment.save
          end
        end
        raise ActiveRecord::Rollback unless ft.destroy        
      end
    end
  end
  
  #####
  # TO DO: If needed to added check to identify if transaction mode and transaction type is a valid value
  #####   either MULTIPLE or SINGLE
  
  def generate_and_set_receipt_no    
    if transaction_mode == "SINGLE" and category_is_income.present? and category_is_income
        self.receipt_no = FinanceTransactionLedger.generate_receipt_no        
    end
  end  
  
  def send_sms
    recipients = []
    #    payee=student
    payee_name=payee.first_name
    sms_setting = SmsSetting.new()
    if sms_setting.application_sms_active and sms_setting.fee_submission_sms_active and payee.present?
      if payee.is_a? Student and payee.is_sms_enabled
        if sms_setting.parent_sms_active
          guardian = payee.immediate_contact if payee.immediate_contact.present?
          recipients.push guardian.mobile_phone if (guardian.present? and guardian.mobile_phone.present?)
        end
        recipients.push payee.phone2 if (sms_setting.student_sms_active and payee.phone2.present?)
      end
      message = "#{t('multi_fee_sms_message_body', :payee_name => payee_name, 
      :currency_name => Configuration.currency, :payment_date => format_date(transaction_date),
      :amount => FedenaPrecision.set_and_modify_precision(amount.to_f), 
      :collection_name => finance_transactions.collect(&:finance_name).join(','))}"
      Delayed::Job.enqueue(SmsManager.new(message, recipients),{:queue => 'sms'}) if recipients.present?
    end
  end
  
  private 
  
  #####
  #TO DO : Apply transaction_mode & transaction_type to receipt_no search logic
  #####
  class << self
    def generate_receipt_no
      next_receipt_number=''
      if is_available_in_cache?
        next_receipt_number = calculate_receipt_number
      else
        next_receipt_number = FeeReceiptLock.receipt_number_in_cache
      end
      # checking new receipt_no is present in cancel_transaction ,Else it will make duplicates while reverting last transaction
      check_receipt_number_existance(next_receipt_number)      
    end
    
    def calculate_receipt_number
      config_receipt_no_format = Configuration.get_config_value('FeeReceiptNo').nil? ? "" : Configuration.get_config_value('FeeReceiptNo').delete(' ')
      config_receipt_number = /(.*?)(\d*)$/.match(config_receipt_no_format)
      config_receipt_num_prefix = config_receipt_number[1] =~ /^\d+$/ ? "" : config_receipt_number[1]
      config_receipt_num_sufix = config_receipt_number[2].to_i
      if config_receipt_num_prefix.present?
        finance_transaction_receipt_nos = transactions_with_similar_receipt_number(config_receipt_num_prefix)
        if finance_transaction_receipt_nos.present?
          last_receipt_number = finance_transaction_receipt_nos.map { |k| k.scan(/\d+$/i).last.to_i }.max
          next_receipt_no_sufix = last_receipt_number > config_receipt_num_sufix ? last_receipt_number : config_receipt_num_sufix
          next_receipt_number = config_receipt_num_prefix + next_receipt_no_sufix.next.to_s
        else
          next_prefix = config_receipt_num_sufix.present? ? config_receipt_num_sufix : 0
          next_receipt_number = config_receipt_num_prefix + next_prefix.to_s
        end
      else
        #code for manage no prefix(string) condition
        finance_transaction_receipts = []
        finance_transaction_receipts += FinanceTransactionLedger.search(
          :receipt_no_not_like => "refund", 
          :receipt_no_greater_than => config_receipt_num_sufix.to_i).all.map(&:receipt_no)
        finance_transaction_receipts += FinanceTransaction.search(
          :receipt_no_not_like => "refund", 
          :receipt_no_greater_than => config_receipt_num_sufix.to_i).all.map(&:receipt_no)
        if finance_transaction_receipts.present?
          last_receipt_number = finance_transaction_receipts.map { |k| k.to_i }.max
          # to find maximum value of receipt no
          next_receipt_number = last_receipt_number.next
        else
          # for the first transaction it will count from 1  else it will count from suffix.
          next_receipt_number = config_receipt_num_sufix.present? ? config_receipt_num_sufix.next : 1
        end
      end
      next_receipt_number
    end
    
    def is_available_in_cache?
      !FeeReceiptLock.cache_has_receipt_no?
    end
    
    def transactions_with_similar_receipt_number(config_receipt_num_prefix)
      transaction_receipt_nos = []
      transaction_receipt_nos += (FinanceTransactionLedger.all(:conditions => "receipt_no IS NOT NULL and 
                                                                        receipt_no REGEXP '(#{config_receipt_num_prefix})\d*' and 
                                                                        receipt_no NOT LIKE 'refund%'")).map(&:receipt_no)
      transaction_receipt_nos += (FinanceTransaction.all(:conditions => "receipt_no IS NOT NULL and 
                                                              receipt_no REGEXP '(#{config_receipt_num_prefix})\d*' and 
                                                              receipt_no NOT LIKE 'refund%'")).map(&:receipt_no)
      transaction_receipt_nos.compact!
      return transaction_receipt_nos if transaction_receipt_nos.present?    
    end
    
    def check_receipt_number_existance(next_receipt_number)
      updated_receipt_number = check_receipt_number_in_cancel_transaction(next_receipt_number)
      FeeReceiptLock.receipt_no(updated_receipt_number)
    end  
    
    def check_receipt_number_in_cancel_transaction(receipt_number)
      cancel_transaction = CancelledFinanceTransaction.find_last_by_receipt_no(receipt_number)
      if cancel_transaction.nil?
        return receipt_number
      else
        data = /(.*?)(\d*)$/.match(receipt_number.to_s)
        receipt_number = data[1].to_s + data[2].next.to_s
        check_receipt_number_in_cancel_transaction(receipt_number)
      end
    end

    # creates safely in case of db duplication issues
    def safely_create (*args)
      begin
        retries ||= 0
        create(*args)
      rescue ActiveRecord::StatementInvalid => ex
        retry if (retries += 1) < 2
        raise ex
      end
    end

  end  
end
