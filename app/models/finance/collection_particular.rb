class CollectionParticular < ActiveRecord::Base
  belongs_to :finance_fee_particular
  belongs_to :finance_fee_collection

  validates_uniqueness_of :finance_fee_collection_id,:scope=>:finance_fee_particular_id
end
