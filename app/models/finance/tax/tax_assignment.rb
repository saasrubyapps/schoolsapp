class TaxAssignment < ActiveRecord::Base

  belongs_to :taxable, :polymorphic => true, :dependent => :destroy
  belongs_to :tax_slab
  
end