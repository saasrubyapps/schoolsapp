#Fedena
#Copyright 2011 Foradian Technologies Private Limited
#
#This product includes software developed at
#Project Fedena - http://www.projectfedena.org/
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.
class FinanceFee < ActiveRecord::Base

  belongs_to :finance_fee_collection, :foreign_key => 'fee_collection_id'
  delegate :name,:to=>:finance_fee_collection,:allow_nil=>true
  has_many :finance_transactions, :as => :finance
  has_many :cancelled_finance_transactions, :as => :finance
  has_many :components, :class_name => 'FinanceFeeComponent', :foreign_key => 'fee_id'
  belongs_to :student
  belongs_to :batch
  belongs_to :student_category
  has_many :finance_transactions, :through => :fee_transactions, :dependent => :destroy, :order => "finance_transactions.id DESC"
  has_many :fee_transactions
  has_one :fee_refund
  has_many :particular_payments, :dependent => :destroy
  has_many :finance_fee_particulars,:finder_sql=>'select finance_fee_particulars.* from finance_fee_particulars inner join collection_particulars cp on cp.finance_fee_collection_id=#{self.fee_collection_id} and cp.finance_fee_particular_id=finance_fee_particulars.id where finance_fee_particulars.batch_id=#{self.batch_id || 0} and ((finance_fee_particulars.receiver_type="Batch" and finance_fee_particulars.receiver_id=#{self.batch_id || 0}) or (finance_fee_particulars.receiver_type="StudentCategory" and finance_fee_particulars.receiver_id=#{self.student_category_id|| 0}) or (finance_fee_particulars.receiver_type="Student" and finance_fee_particulars.receiver_id=#{self.student_id}))'
  has_many :fee_discounts,:finder_sql=>'select fee_discounts.* from fee_discounts inner join collection_discounts cd on cd.finance_fee_collection_id=#{self.fee_collection_id} and cd.fee_discount_id=fee_discounts.id where fee_discounts.batch_id=#{self.batch_id || 0} and ((fee_discounts.receiver_type="Batch" and fee_discounts.receiver_id=#{self.batch_id || 0}) or (fee_discounts.receiver_type="StudentCategory" and fee_discounts.receiver_id=#{self.student_category_id || 0}) or (fee_discounts.receiver_type="Student" and fee_discounts.receiver_id=#{self.student_id || 0}))'

  #invoice associations
  has_many :fee_invoices, :as => :fee
  #discount associations
  has_many :finance_fee_discounts, :dependent => :destroy
  #tax associations
  has_many :tax_payments, :as => :taxed_fee, :dependent => :destroy
  has_many :taxed_particulars, :through => :tax_payments, :source => :taxed_entity, :source_type => "FinanceFeeParticular"
  
  has_many :tax_collections, :as => :taxable_fee, :dependent => :destroy
  has_many :tax_particulars, :through => :tax_collections, :source => :taxable_entity, :source_type => "FinanceFeeParticular"
  
  accepts_nested_attributes_for :particular_payments
  named_scope :active, :joins => [:finance_fee_collection], :conditions => {:finance_fee_collections => {:is_deleted => false}}
  cattr_accessor :invoice_number_enabled
  after_create :add_invoice_number, :if => Proc.new { |fee| fee.invoice_number_enabled.present? }
  before_destroy :mark_invoice_number_deleted
  
  validates_uniqueness_of :fee_collection_id,:scope=>:student_id

  def check_transaction_done
    unless self.transaction_id.nil?
      return true
    else
      return false
    end
  end

  def invoice_no
    fee_invoices.present? ? fee_invoices.try(:first).try(:invoice_number) : ""
  end
  
  def add_invoice_number
    fee_invoices.create
  end
  
  def mark_invoice_number_deleted
    fee_invoice = fee_invoices.try(:last)
    fee_invoice.mark_deleted if fee_invoice.present?
  end
  
  def former_student
    ArchivedStudent.find_by_former_id(self.student_id)
  end

  def due_date
    format_date(finance_fee_collection.due_date, :format => :long)
  end

  def payee_name
    if student
      "#{student.full_name} - #{student.admission_no}"
    elsif former_student
      "#{former_student.full_name} - #{former_student.admission_no}"
    else
      "#{t('user_deleted')}"
    end
  end

  def school_discount_mode
    new_discount_modes = ["NEW_DISCOUNT", "NEW_DISCOUNT_MODE"]
    old_discount_modes = ["OLD_DISCOUNT"]
    valid_discount_modes = old_discount_modes + new_discount_modes
    
    return "OLD" if old_discount_modes.include?(self.finance_fee_collection.discount_mode) 
    return "NEW" if new_discount_modes.include?(self.finance_fee_collection.discount_mode) 
  end
  
  def self.new_student_fee(date, student)
    tax_enabled = date.tax_enabled?
    self.invoice_number_enabled = date.invoice_enabled #? 1 : 0
    if tax_enabled
      particular_tax_slab = {}
      collection_tax_slabs = date.collectible_tax_slabs.all(:include => [:collection_tax_slab, :collectible_entity])
      collection_tax_slabs.each {|cts| particular_tax_slab[cts.collectible_entity_id] = cts.collection_tax_slab }      
    end
    #    include_particular_associations = date.tax_enabled? ? [:collection_tax_slabs] : []
    include_particular_associations = []
    fee_particulars = date.finance_fee_particulars.all(:include => include_particular_associations,
      :conditions => "batch_id=#{student.batch_id}").select { |par| (par.receiver.present?) and 
        (par.receiver==student or par.receiver==student.student_category or 
          par.receiver==student.batch) }
    discounts=date.fee_discounts.all(:conditions => "batch_id=#{student.batch_id}").
      select { |par| (par.receiver.present?) and (
        (par.receiver== student or par.receiver == student.student_category or 
            par.receiver== student.batch) and 
          (par.master_receiver_type!='FinanceFeeParticular' or 
            (par.master_receiver_type=='FinanceFeeParticular' and 
              (par.master_receiver.receiver.present? and par.master_receiver.is_deleted==false) and 
              (par.master_receiver.receiver== student or 
                par.master_receiver.receiver == student.student_category or 
                par.master_receiver.receiver== student.batch)))) }
    
    finance_fee = FinanceFee.new(:student_id => student.id, :fee_collection_id => date.id,  
      :batch_id => student.batch_id, :student_category_id => student.student_category_id, 
      :tax_enabled => date.tax_enabled, :invoice_number_enabled => invoice_number_enabled
    )
    
    total_discount = 0
    total_tax = 0 if tax_enabled
    tax_arr = [] if tax_enabled
    total_payable =fee_particulars.map { |l| l.amount }.sum.to_f
    
    particular_discounts = {}
    common_discounts = []
    #discount mode
    discount_mode = finance_fee.school_discount_mode
    
    total_discount_amount = discounts.map { |d| d.master_receiver_type=='FinanceFeeParticular' ? 
        (d.master_receiver.amount * d.discount.to_f/(d.is_amount? ? d.master_receiver.amount : 100)) : 
        total_payable * d.discount.to_f/(d.is_amount? ? total_payable : 100) 
    }.sum.to_f if discount_mode == "OLD" and discounts.present?
    
    # extract discounts for batch/student/student_category or particular level
    if fee_particulars.present? and discount_mode == "NEW"      
      discounts.map do |d|
        if d.master_receiver_type=='FinanceFeeParticular'
          # discount_amount = d.master_receiver.amount * d.discount.to_f/(d.is_amount? ? d.master_receiver.amount : 100)
          #          discount_amount = d.master_receiver.amount * d.discount.to_f/(d.is_amount? ? fee_particulars.length : 100)
          p_amount = d.master_receiver.amount
          #          discount_amount = d.is_amount? ? (d.discount.to_f / fee_particulars.length.to_f)  : (p_amount * d.discount.to_f * 0.01)
          discount_amount = d.is_amount? ? d.discount.to_f  : (p_amount * d.discount.to_f * 0.01)
          finance_fee.finance_fee_discounts.build({ :discount_amount => discount_amount, 
              :fee_discount_id => d.id, :finance_fee_particular_id => d.master_receiver_id })
          particular_discounts[d.master_receiver_id] = particular_discounts[d.master_receiver_id].to_f + 
            discount_amount        
          total_discount += discount_amount
        else
          common_discounts << d
        end
      end
    end
    
    # build applicable discount & tax amount records        
    fixed_discounts = []    
    no_of_parts = fee_particulars.length
    fee_particulars.each_with_index do |particular,pi|
      p_amount = particular.amount.to_f
      taxable_particular_amount = nil
      if discount_mode == "OLD"
        discount_amount = nil
        if total_discount_amount.to_f > 0
          discount_amount = (total_discount_amount >= p_amount) ? p_amount : total_discount_amount
          total_discount_amount -= discount_amount.to_f
          taxable_particular_amount = p_amount - discount_amount.to_f
          finance_fee.finance_fee_discounts.build({
              :discount_amount => discount_amount,
              :finance_fee_particular_id => particular.id
            })
          total_discount += discount_amount.to_f
        else
          taxable_particular_amount = p_amount
        end
        
      elsif discount_mode == "NEW"
        common_discounts.each do |disc|        
          if disc.is_amount?
            fixed_discounts[disc.id] ||= {:real_amount => disc.discount.to_f, 
              :applied_sum => 0}
            discount_ratio_amt = (disc.discount.to_f / total_payable.to_f) * p_amount
            fixed_discounts[disc.id][:applied_sum] += discount_ratio_amt            
            if no_of_parts == (pi+1)
              disc_diff = fixed_discounts[disc.id][:real_amount] - fixed_discounts[disc.id][:applied_sum]
              discount_ratio_amt += disc_diff if disc_diff > 0
            end
          end
          discount_amount = disc.is_amount? ? discount_ratio_amt  : (p_amount * disc.discount.to_f * 0.01)
          finance_fee.finance_fee_discounts.build({
              :discount_amount => discount_amount, :fee_discount_id => disc.id,
              :finance_fee_particular_id => particular.id
            })
          total_discount += discount_amount
          particular_discounts[particular.id] = particular_discounts[particular.id].to_f + discount_amount                        
        end
        taxable_particular_amount = p_amount.to_f - particular_discounts[particular.id].to_f
      end
      if tax_enabled
        #        tax_slab = particular.collection_tax_slabs.try(:last)
        tax_slab = particular_tax_slab[particular.id]
        if tax_slab.present?
          tax_amount = taxable_particular_amount.to_f > 0 ? (taxable_particular_amount.to_f *  tax_slab.rate * 0.01).to_f  : 0.0          
          tax_collection = finance_fee.tax_collections.build({:tax_amount => tax_amount})
          tax_collection.taxable_entity = particular
          tax_collection.slab_id = tax_slab.id
          total_tax += tax_amount
          tax_arr << tax_amount
        end
      end
    end
    precisioned_total_tax = tax_arr.map { |x| FedenaPrecision.set_and_modify_precision(x).to_f }.
      sum.to_f if tax_enabled
    balance= FedenaPrecision.set_and_modify_precision(total_payable-total_discount + 
        (tax_enabled ? precisioned_total_tax : 0.0))
    finance_fee.balance = balance
    finance_fee.particular_total = total_payable
    finance_fee.discount_amount = total_discount
    finance_fee.tax_amount = precisioned_total_tax
    finance_fee.is_paid = (balance.to_f<=0)
    finance_fee.save
    finance_fee
  end
  
  def self.csv_batch_fees_head_wise_report(parameters)
    batch_ids=parameters[:batch_ids] if parameters[:batch_ids].present?
    if batch_ids.present?
      students=Student.all(:select => "DISTINCT students.*", 
        :conditions => ["finance_fees.batch_id IN (?)", batch_ids], 
        :joins => [{:finance_fees=>{:finance_fee_collection=>:fee_collection_batches}}], 
        :include => [{:finance_fees => [{:finance_fee_collection => [{:collection_particulars => 
                      {:finance_fee_particular => :receiver}}, {:collection_discounts => 
                      {:fee_discount=>:receiver}}]}, :finance_transactions, { :batch=> :course}, :student_category, 
              {:tax_collections => :tax_slab}]}], :order => 'first_name ASC')
    else
      students=Student.all(:select => "DISTINCT students.*", 
        :joins => [{:finance_fees=>{:finance_fee_collection=> :fee_collection_batches}}], 
        :include => [{:finance_fees => [{:finance_fee_collection => [{:collection_particulars => 
                      {:finance_fee_particular => :receiver }}, {:collection_discounts => 
                      {:fee_discount => :receiver }}]}, :finance_transactions, { :batch=> :course}, :student_category, 
              {:tax_collections => :tax_slab}]}], :order => 'first_name ASC')
    end
    
    all_fees = students.map {|s| s.finance_fees.select{|ff| !ff.finance_fee_collection.is_deleted} }.flatten
    student_fees = all_fees.group_by { |f| f.student_id }
    is_tax_enabled = all_fees.map {|ff| ff.tax_enabled and ff.tax_collections.present? }.uniq.include?(true)      
    col_heads=["#{t('no_text')}", "#{t('student_name')}", "#{t('batch_name')}", 
      "#{t('fee_collection')} #{t('name')}", "#{t('particulars')}", "#{t('discount')}"]
    col_heads << "#{t('tax_text')}" if is_tax_enabled
    col_heads += ["#{t('amount_to_pay')}(#{Configuration.currency})", 
      "#{t('paid')} #{t('amount')}(#{Configuration.currency})"]
    return FinanceFee.csv_make_data(students,col_heads, student_fees, is_tax_enabled, parameters)
  end


  def self.csv_fee_collection_fees_head_wise_report(parameters)
    students=Student.all(:select => "DISTINCT students.*", 
      :conditions => ["finance_fees.fee_collection_id=? and finance_fees.batch_id = ?", 
        parameters[:fee_collection_id], parameters[:batch_id]], 
      :joins => :finance_fees, :include => {:batch => :course}, :order => 'first_name ASC')
    
    all_fees = FinanceFee.all(:conditions => {:student_id => students.map(&:id),
        :fee_collection_id => parameters[:fee_collection_id] }, :include => [ :finance_transactions,
        {:finance_fee_collection => [{:collection_particulars => {:finance_fee_particular =>:receiver}}, 
            {:collection_discounts => {:fee_discount=>:receiver}}]}, {:tax_collections => :tax_slab}, 
        :batch,:student_category])
    is_tax_enabled = all_fees.map {|x| x.tax_enabled? and x.tax_collections.present? }.uniq.include?(true)    
    student_fees = all_fees.group_by {|ff| ff.student_id }      
    col_heads = ["#{t('no_text')}", "#{t('student_name')}", "#{t('batch_name')}", "#{t('particulars')}", 
      "#{t('discount')}"]
    col_heads << "#{t('tax_text')}" if is_tax_enabled
    col_heads += ["#{t('amount_to_pay')}(#{Configuration.currency})", 
      "#{t('paid')} #{t('amount')}(#{Configuration.currency})"]      
    
    #    col_heads=["#{t('no_text')}", "#{t('student_name')}", "#{t('batch_name')}", "#{t('particulars')}", 
    #      "#{t('discount')}", "#{t('amount_to_pay')}(#{Configuration.currency})", 
    #      "#{t('paid')} #{t('amount')}(#{Configuration.currency})",]
    return FinanceFee.csv_make_data(students,col_heads, student_fees, is_tax_enabled, parameters)
    #    return FinanceFee.csv_make_data(students,col_heads,parameters)
  end


  def self.csv_make_data(students, col_heads, students_fees, is_tax_enabled, parameters)
    data=[]
    data << col_heads
    students.each_with_index do |student, i|
      student_fees = students_fees[student.id]
      total_bal= student_fees.present? ? student_fees.map {|x| x.balance.to_f }.sum.to_f : 0
      total_paid=0
      #      if col_heads.include? "#{t('fee_collection')} #{t('name')}"
      #        finance_fees=student.finance_fees.select{|ff| !ff.finance_fee_collection.is_deleted}
      #      else
      #        finance_fees = student_fees[student.id]
      #        finance_fees=student.finance_fees.find(:all, :conditions => "fee_collection_id='#{parameters[:fee_collection_id]}'").select{|ff| !ff.finance_fee_collection.is_deleted}
      #      end
      j = 0
      student_fees.each do |finance_fee|
        total_paid=total_paid+finance_fee.finance_transactions.map(&:amount).sum.to_f
        ffc=finance_fee.finance_fee_collection
        particulars=ffc.collection_particulars.select { |cp| cp.finance_fee_particular.present? and 
            ((cp.finance_fee_particular.batch_id==finance_fee.batch_id and 
                cp.finance_fee_particular.receiver.present?) and 
              (cp.finance_fee_particular.receiver==finance_fee.student_category or 
                cp.finance_fee_particular.receiver==finance_fee.batch or 
                cp.finance_fee_particular.receiver==student)) }
        discounts=ffc.collection_discounts.select { |cd| cd.fee_discount.present? and 
            ((cd.fee_discount.batch_id==finance_fee.batch_id and cd.fee_discount.receiver.present?) and 
              (cd.fee_discount.receiver==finance_fee.student_category or 
                cd.fee_discount.receiver==finance_fee.batch or cd.fee_discount.receiver==student)) }
        
        tax_slabs = finance_fee.tax_collections.map {|tc| tc.tax_slab }.uniq if is_tax_enabled
        
        count= is_tax_enabled ? [particulars.length, discounts.length, tax_slabs.length].max : 
          [particulars.length, discounts.length].max
        k=0
        while k<count
          col=[]
          if j == 0
            col<< "#{i+1}"
            student_no = "(#{student.admission_no})"
            col<< "#{student.full_name}#{student_no}"
            col<< "#{student.batch.full_name}"
          else
            col<< ""
            col<< ""
            col<< ""
          end
          col<< "#{ffc.name}" if col_heads.include? "#{t('fee_collection')} #{t('name')}"
          col<< "#{(particulars[k].present?) ? (particulars[k].finance_fee_particular.name+':'+
          FedenaPrecision.set_and_modify_precision(particulars[k].finance_fee_particular.amount.to_f)) : '-' }"
          col<< "#{(discounts[k].present?) ? (discounts[k].fee_discount.name+':'+
          FedenaPrecision.set_and_modify_precision(discounts[k].fee_discount.discount.to_f)+
          (discounts[k].fee_discount.is_amount? ? '' : '%')) : '-' }"
          if is_tax_enabled
            col << "#{tax_slabs[k].present? ? (tax_slabs[k].name + ':' + 
            FedenaPrecision.set_and_modify_precision(tax_slabs[k].rate)) : '-' }"
          end
          if k==0
            col<< "#{finance_fee.balance.nil? ? FedenaPrecision.set_and_modify_precision(0) : 
            FedenaPrecision.set_and_modify_precision(finance_fee.balance)}"
            col<< "#{FedenaPrecision.set_and_modify_precision(finance_fee.finance_transactions.
            map(&:amount).sum.to_f)}"
          else
            col<<""
            col<<""
          end
          col=col.flatten
          data<< col
          k=k+1
          j = j+1
        end
      end
      if col_heads.include? "#{t('fee_collection')} #{t('name')}" #batch wise report
        d_col = ["", "", "", "TOTAL", "", ""]
        d_col << "" if is_tax_enabled
        d_col += [FedenaPrecision.set_and_modify_precision(total_bal), 
          FedenaPrecision.set_and_modify_precision(total_paid)]
        data<< d_col
      else #fee collection wise report
        # data<< ["", "", "TOTAL", FedenaPrecision.set_and_modify_precision(total_bal), FedenaPrecision.set_and_modify_precision(total_paid), "", ""]
      end

    end
    return data
  end
  # currently is_paid flag is not properly used in the case of fees with fine, this is a workaround for that
  def is_paid_with_fine?
    return true if is_paid?
    return false if finance_transactions.empty?
    last_payment_date=finance_transactions.last.transaction_date
    days=(last_payment_date-finance_fee_collection.due_date.to_date).to_i
    auto_fine=finance_fee_collection.fine
    if days > 0 && auto_fine.present?
      fine_rule=auto_fine.fine_rules.find(:last, :conditions => 
          ["fine_days <= '#{days}' and created_at <= '#{finance_fee_collection.created_at}'"], 
        :order => 'fine_days ASC')
      fine_amount=fine_rule.is_amount ? fine_rule.fine_amount : 
        (_amount*fine_rule.fine_amount)/100 if fine_rule.present?
    end
    fine_amount ||= 0
    FedenaPrecision.set_and_modify_precision(balance.to_f + 
        fine_amount)==FedenaPrecision.set_and_modify_precision(0)
  end
  #FIXME use sql instead of ruby select
  #TODO tempoaray hack using underscore to make backward compatiable
  def _total_payable
    fee_particulars = finance_fee_collection.finance_fee_particulars.all(
      :conditions=>"batch_id=#{batch_id}").select{|par|  (par.receiver.present?) and 
        (par.receiver==student or par.receiver==student_category or par.receiver==batch) }
    total_payable=fee_particulars.map{|s| s.amount}.sum.to_f
  end
  def _total_discount
    discounts=finance_fee_collection.fee_discounts.all(:conditions => "batch_id=#{batch_id}").
      select { |par|
      (par.receiver.present?) and (
        (par.receiver==student or par.receiver==student_category or par.receiver==self.batch) and
          (par.master_receiver_type!='FinanceFeeParticular' or
            (par.master_receiver_type=='FinanceFeeParticular' and
              (
              par.master_receiver.receiver==self.student or
                par.master_receiver.receiver==self.student_category or
                par.master_receiver.receiver==self.batch
            )
          )
        )
      )
    }
    total_discount = discounts.map { |d|
      d.master_receiver_type=='FinanceFeeParticular' ? 
        (d.master_receiver.amount * d.discount.to_f/(d.is_amount? ? d.master_receiver.amount : 100)) :
        _total_payable * d.discount.to_f/(d.is_amount? ? _total_payable : 100) }.sum.to_f unless discounts.nil?
  end
  def _amount
    _total_payable-_total_discount
  end

  def _paid_amount
    finance_transactions.sum(:amount).to_f
  end
end
