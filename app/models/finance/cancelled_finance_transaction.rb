class CancelledFinanceTransaction < ActiveRecord::Base
  belongs_to :category, :class_name => 'FinanceTransactionCategory', :foreign_key => 'category_id'
  belongs_to :student ,:primary_key => 'payee_id'
  belongs_to :employee ,:primary_key => 'payee_id'
  belongs_to :instant_fee,:foreign_key=>'finance_id',:conditions=>'payee_id is NULL'
  belongs_to :finance, :polymorphic => true
  belongs_to :payee, :polymorphic => true
  belongs_to :master_transaction,:class_name => "FinanceTransaction"
  belongs_to :transaction_ledger, :class_name => "FinanceTransactionLedger", :foreign_key => 'transaction_ledger_id'
  belongs_to :user
  serialize  :other_details, Hash
  include CsvExportMod

  def receipt_number
    receipt_no.present? ? receipt_no : (
      (transaction_ledger.present? and 
          transaction_ledger.transaction_mode == 'SINGLE') ? transaction_ledger.receipt_no : "")    
  end

  def get_archieved_payee_name
    if payee_type=='Student' or payee_type=='Employee'
      archived_payee=("Archived"+payee_type).constantize.find_by_former_id(payee_id)
      if archived_payee.present?
        return "#{archived_payee.full_name}-&#x200E; (#{payee_type=="Student" ? archived_payee.admission_no : archived_payee.employee_number })&#x200E;"
      else
        return "#{t('user_deleted')}"
      end
    else
      return nil
    end
  end
  
  def get_archieved_payee_name_csv
    if payee_type=='Student' or payee_type=='Employee'
      archived_payee=("Archived"+payee_type).constantize.find_by_former_id(payee_id)
      if archived_payee.present?
        return "#{archived_payee.full_name} - (#{payee_type=="Student" ? archived_payee.admission_no : archived_payee.employee_number })"
      else
        return "#{t('user_deleted')}"
      end
    else
      return nil
    end
  end

  def payee_name
    if payee_type.present? and payee_id.present?
      if payee.present?
        "#{payee.full_name}-&#x200E; (#{payee_type=="Student" ? payee.admission_no : payee.employee_number })&#x200E;"
      else
        get_archieved_payee_name
      end
    elsif finance_type.present? and !(finance_type.constantize.present? rescue false)
      return nil
    elsif payee.nil? and finance.nil?
      return "#{t('user_deleted')}"
    else payee.nil?
      finance.payee_name
    end
  end
  
  def payee_name_for_csv
    if payee_type.present? and payee_id.present?
      if payee.present?
        "#{payee.full_name} - (#{payee_type=="Student" ? payee.admission_no : payee.employee_number })"
      else
        get_archieved_payee_name_csv
      end
    elsif finance_type.present? and !(finance_type.constantize.present? rescue false)
      return nil
    elsif payee.nil? and finance.nil?
      return "#{t('user_deleted')}"
    else payee.nil?
      finance.payee_name
    end
  end
  
  def self.fetch_cancelled_transactions_advance_search_result(params)
    cancelled_transactions_advance_search params
  end
  
  def self.generate_cancelled_transactions_csv(params,transactions)
    csv_string=FasterCSV.generate do |csv|
      cols=[]
      cols << "Cancelled Transactions"
      csv << cols
      cols = []
      cols << t('sl_no')
      unless params[:transaction_type] == t('payslips')
        cols << t('payee_name')
        cols << t('receipt_no')
      else
        cols << t('employee_name')
      end
      cols << t('amount')
      cols << t('cancelled_by')
      cols << t('reason')
      cols << t('date_text')
      if (params['transaction_type'].nil? or params['transaction_type'] == "" or params['transaction_type']==t('fees_text'))
        cols << t('fee_collection_name')
      end
      unless params[:transaction_type] == t('payslips')
        cols << t('finance_type')
      end
      csv << cols
      cols = []
      i=0
      transactions.each do |f|
        cols <<  (i +=1)
        cols << f.payee_name_for_csv
        unless params[:transaction_type] == t('payslips')
          cols << f.receipt_number
        end
        cols << (precision_label(f.amount))
        cols << (f.user.present?? f.user.full_name  : t('user_deleted'))
        cols << (f.cancel_reason.present? ? f.cancel_reason :  "-")
        cols << (format_date(f.created_at,:format=>:short_date))
        if (params['transaction_type'].nil? or params['transaction_type'] == "" or params['transaction_type']==t('fees_text'))
          cols << f.collection_name
        end
        unless params[:transaction_type] == t('payslips')
          cols << f.finance_type.underscore.humanize()
        end
        csv << cols
        cols =[]
      end
    end
    return csv_string
  end
  
end
