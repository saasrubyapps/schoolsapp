class FeeInvoice < ActiveRecord::Base
  belongs_to :fee, :polymorphic => true
  
  before_create :generate_invoice_no
  serialize :invoice_data
  include FeeReceiptMod
  
  # check if collection has existing fee invoices
  # method can fetch only if collection has atleast 1 fee record present
  def self.is_generated_for_collection?(collection,is_active=nil)
    collection_name = collection.class.name    
    fee_model_name = collection_name.gsub("Collection","")
    fee_name = fee_model_name.underscore
    fee_type = fee_name.pluralize    
    fee_name = "fee" if fee_name == 'finance_fee'
    conditions = {:fee_type => fee_model_name}
    conditions = conditions.merge({:is_active => is_active}) unless is_active.nil?
    invoice = FeeInvoice.last(:conditions => conditions,
      :joins => "INNER JOIN #{fee_type} ff 
                                  ON ff.#{fee_name}_collection_id = #{collection.id} and 
                                        ff.id = fee_invoices.fee_id")
    invoice.present?
  end
  
  # marks fee invoice record as deleted and stores last state of fee structure
  def mark_deleted    
    get_student_invoice(self.fee,self.fee_type.underscore.pluralize)
    self.update_attributes({:is_active => false, :invoice_data => @inv_data})
  end
  
  # generates new invoice number
  def generate_invoice_no
    r = 0
    while r < 2
      next_invoice_number = calculate_invoice_number          
      check_invoice_number_existance(next_invoice_number) ? false : next_invoice_number
      break if next_invoice_number
      r = r.next
    end
    self.invoice_number = next_invoice_number if next_invoice_number.present?
  end
    
  # checks if an invoice number is already used
  def check_invoice_number_existance(next_invoice_number)
    FeeInvoice.last(:conditions => {:invoice_number => next_invoice_number})
  end
  
  # finds new invoice number based on invoice number configuration and last generated invoice numbers
  def calculate_invoice_number
    config_invoice_no_format = Configuration.get_config_value('FeeInvoiceNo').nil? ? "" : 
      Configuration.get_config_value('FeeInvoiceNo').delete(' ')
    config_invoice_number = /(.*?)(\d*)$/.match(config_invoice_no_format)
    config_invoice_num_prefix = config_invoice_number[1] =~ /^\d+$/ ? "" : config_invoice_number[1]
    config_invoice_num_sufix = config_invoice_number[2].to_i
    if config_invoice_num_prefix.present?
      fee_invoice_nos = invoices_with_similar_invoice_number(config_invoice_num_prefix)
      if fee_invoice_nos.present?
        last_invoice_number = fee_invoice_nos.map { |k| k.scan(/\d+$/i).last.to_i }.max
        next_invoice_no_sufix = last_invoice_number > config_invoice_num_sufix ? last_invoice_number : 
          config_invoice_num_sufix
        next_invoice_number = config_invoice_num_prefix + next_invoice_no_sufix.next.to_s
      else
        next_prefix = config_invoice_num_sufix.present? ? (config_invoice_num_sufix > 0 ? 
            config_invoice_num_sufix : 1) : 0
        next_invoice_number = config_invoice_num_prefix + next_prefix.to_s
      end
    else
      #code for manage no prefix(string) condition
      fee_invoices = []
      fee_invoices += FeeInvoice.search(
        :invoice_number_greater_than => config_invoice_num_sufix.to_i).all.
        map(&:invoice_number)
      if fee_invoices.present?
        last_invoice_number = fee_invoices.map { |k| k.to_i }.max
        # to find maximum value of invoice no
        next_invoice_number = last_invoice_number.next
      else
        # for the first transaction it will count from 1  else it will count from suffix.
        next_invoice_number = config_invoice_num_sufix.present? ? config_invoice_num_sufix.next : 1
      end
    end
    next_invoice_number
  end
  
  # checks invoice numbers with current invoice number configuration for similar invoice number records
  def invoices_with_similar_invoice_number(config_invoice_num_prefix)
    fee_invoice_nos = []
    fee_invoice_nos += (
      FeeInvoice.all(:conditions => "invoice_number IS NOT NULL and 
                                     invoice_number REGEXP '(#{config_invoice_num_prefix})\d*'")).
      map(&:invoice_number)    
    fee_invoice_nos.compact!
    return fee_invoice_nos if fee_invoice_nos.present?    
  end
end
