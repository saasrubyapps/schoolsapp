#Fedena
#Copyright 2011 Foradian Technologies Private Limited
#
#This product includes software developed at
#Project Fedena - http://www.projectfedena.org/
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

class FinanceTransaction < ActiveRecord::Base
  belongs_to :category, :class_name => 'FinanceTransactionCategory', :foreign_key => 'category_id'
  delegate :name, :to => :category, :allow_nil => true, :prefix => true
  belongs_to :student
  belongs_to :finance, :polymorphic => true
  delegate :name, :to => :finance, :allow_nil => true, :prefix => true
  belongs_to :payee, :polymorphic => true
  belongs_to :master_transaction, :class_name => "FinanceTransaction"
  belongs_to :user
  belongs_to :batch
  belongs_to :transaction_ledger, :class_name => "FinanceTransactionLedger"
  has_many :finance_fees, :through => :fee_transactions
  has_many :fee_transactions
  has_one :fee_refund, :dependent => :destroy
  has_one :finance_donation, :foreign_key => 'transaction_id', :dependent => :destroy
  cattr_reader :per_page
  attr_accessor :cancel_reason
  attr_accessor :transaction_type, :transaction_mode
  attr_accessor_with_default :full_fine_paid, false
  validates_presence_of :title, :amount, :transaction_date
  validates_uniqueness_of :receipt_no, :scope => :school_id, :allow_blank => true, :allow_nil => true
  validates_presence_of :category, :message => :not_specified
  validates_numericality_of :amount, :greater_than_or_equal_to => 0, :message => :must_be_positive, :allow_blank => true

  before_save :verify_fine_amount
  before_create :make_transaction_ledger
  before_create :add_voucher_or_receipt_number
  before_save :verify_precision, :set_fine, :set_transaction_stamp
  after_create :add_user
  before_destroy :refund_check
  #  before_save :check_data_correctnes
  after_destroy :create_cancelled_transaction
  has_many :monthly_payslips
  has_many :employee_payslips
  has_many :particular_payments, :dependent => :destroy
  #tax associations
  has_many :tax_payments, :dependent => :destroy
  
  has_and_belongs_to_many :multi_fees_transactions, :join_table => "multi_fees_transactions_finance_transactions"
  has_many :successor_transactions, :foreign_key => 'finance_id', :primary_key => 'finance_id', 
    :class_name => 'FinanceTransaction', 
    :conditions => 'id > #{self.id} and finance_type="#{self.finance_type}"'
  after_create :verify_and_send_sms, :if => Proc.new { FinanceTransaction.send_sms }
  # update fee payment records for core fees
  after_create :add_particular_amount, :if => Proc.new { |ft| !FinanceTransaction.particular_wise_pay_lock and 
      ft.finance_type=='FinanceFee' and (ft.amount > ft.fine_amount) }
  eligible_finance_types=["TransportFee", "HostelFee", "FinanceFee", "InstantFee"]
  named_scope :eligible, :conditions => ["finance_type IN (?)", eligible_finance_types]
  #  named_scope :receipt_no_as, lambda { |query| {:conditions => ["receipt_no LIKE ?", "%#{query}%"]} }
  named_scope :receipt_no_equals, lambda { |query| {:joins => :transaction_ledger, :include => :transaction_ledger,
      :conditions => ["finance_transactions.receipt_no = ? or finance_transaction_ledgers.receipt_no = ?",
        "#{query}", "#{query}"]} }
  named_scope :receipt_no_as, lambda { |query| {:conditions => 
        ["finance_transactions.receipt_no LIKE ? or 
          finance_transaction_ledgers.receipt_no LIKE ?", "%#{query}%", "%#{query}%"]} }
  named_scope :reference_no_like, lambda { |query| {:conditions => 
        ["finance_transactions.reference_no LIKE ? or
          finance_transaction_ledgers.reference_no LIKE ?", "%#{query}%", "%#{query}%"]} }
  named_scope :start_date_as, lambda { |query| {:conditions => ["finance_transactions.transaction_date >= ?", "#{query}"]} }
  named_scope :end_date_as, lambda { |query| {:conditions => ["finance_transactions.transaction_date <= ?", "#{query}"]} }
  named_scope :payment_mode_equals, lambda { |query| {:conditions => ["finance_transactions.payment_mode = ?", "#{query}"]} }
  named_scope :user_name_equals, lambda { |user_id| {:joins => :user, :conditions => ["finance_transactions.user_id = ?", user_id]} }
  named_scope :collection_id_equals, lambda { |ftid| fid, ftype, fname = ftid.split(':'); 
    {:joins => "LEFT OUTER JOIN finance_fees ON finance_fees.id=finance_transactions.finance_id 
                      LEFT OUTER JOIN transport_fees ON transport_fees.id=finance_transactions.finance_id 
                      LEFT OUTER JOIN hostel_fees ON hostel_fees.id=finance_transactions.finance_id 
                      LEFT OUTER JOIN instant_fees on instant_fees.id=finance_transactions.finance_id", 
      :conditions => ["(finance_fees.fee_collection_id = ? and finance_transactions.finance_type = ?) or 
                                 (transport_fees.transport_fee_collection_id = ? and finance_transactions.finance_type = ?) or 
                                 (hostel_fees.hostel_fee_collection_id = ? and finance_transactions.finance_type = ?) or 
                                 (instant_fees.instant_fee_category_id = ? and finance_transactions.finance_type = ?) or 
                                 (instant_fees.instant_fee_category_id IS NULL and finance_transactions.finance_type = ? and 
                                  instant_fees.custom_category=?)", fid, ftype, fid, ftype, fid, ftype, fid, ftype, ftype, fname]} }
  
  named_scope :collection_name_type_equals, lambda { |ftid| fname, ftype = ftid.split(/\ - ([^ - ]*)$/); 
    feec_ids = (ftype=='InstantFee') ? InstantFeeCategory.find_all_by_name(fname).collect(&:id): 
      "#{ftype}Collection".constantize.find_all_by_name(fname).collect(&:id) ; 
    {:joins => "LEFT OUTER JOIN finance_fees ON finance_fees.id=finance_transactions.finance_id 
                       LEFT OUTER JOIN transport_fees ON transport_fees.id=finance_transactions.finance_id 
                       LEFT OUTER JOIN hostel_fees ON hostel_fees.id=finance_transactions.finance_id 
                       LEFT OUTER JOIN instant_fees ON instant_fees.id=finance_transactions.finance_id", 
      :conditions => ["(finance_fees.fee_collection_id IN (?) and finance_transactions.finance_type IN (?)) or 
                                 (transport_fees.transport_fee_collection_id IN (?) and finance_transactions.finance_type IN (?)) or 
                                 (hostel_fees.hostel_fee_collection_id IN (?) and finance_transactions.finance_type = ?) or 
                                 (instant_fees.instant_fee_category_id IN (?) and finance_transactions.finance_type = ?) or 
                                 (instant_fees.instant_fee_category_id IS NULL and finance_transactions.finance_type = ? and 
                                  instant_fees.custom_category=?)", 
        feec_ids, ftype, feec_ids, ftype, feec_ids, ftype, feec_ids, ftype, ftype, fname]} }
  
  named_scope :employee_info_like, lambda { |search_string| {
      :joins => "LEFT OUTER JOIN employees es 
                                           ON es.id = finance_transactions.payee_id AND 
                                                 finance_transactions.payee_type='Employee' 
                      LEFT OUTER JOIN archived_employees ars 
                                           ON finance_transactions.payee_type='Employee' AND 
                                                 ars.former_id = finance_transactions.payee_id",
      :conditions => ["finance_transactions.payee_type = 'Employee' and 
                                (finance_transactions.payee_id IN (?) OR finance_transactions.payee_id in (?))", 
        Employee.find(:all, :conditions => ["(LTRIM(first_name) LIKE ? OR LTRIM(middle_name) LIKE ? OR 
                                                                  LTRIM(last_name) LIKE ? OR employee_number = ? OR 
                                                                  (CONCAT(TRIM(first_name), \" \", TRIM(last_name)) LIKE ? ) OR 
                                                                  (CONCAT(TRIM(first_name), \" \", TRIM(middle_name), \" \", 
                                                                                 TRIM(last_name)) LIKE ? ))", "%#{search_string}%", 
            "%#{search_string}%", "%#{search_string}%", "#{search_string}", "%#{search_string}%", 
            "%#{search_string}%"]).collect(&:id).uniq.join(','), 
        ArchivedEmployee.find(:all, 
          :conditions => ["(LTRIM(first_name) LIKE ? OR LTRIM(middle_name) LIKE ? OR 
                                      LTRIM(last_name) LIKE ? OR employee_number = ? OR 
                                      (CONCAT(trim(first_name), \" \", TRIM(last_name)) LIKE ? ) OR 
                                      (CONCAT(TRIM(first_name), \" \", TRIM(middle_name), \" \", 
                                      TRIM(last_name)) LIKE ? ))", "%#{search_string}%", "%#{search_string}%", 
            "%#{search_string}%", "#{search_string}", "%#{search_string}%", 
            "%#{search_string}%"]).collect(&:former_id).uniq.join(',')]} }
  
  named_scope :student_info_like, lambda { |search_string| {
      :joins => "LEFT OUTER JOIN students ss 
                                            ON ss.id = finance_transactions.payee_id and 
                                                  finance_transactions.payee_type='Student'
                      LEFT OUTER JOIN archived_students ars 
                                            ON finance_transactions.payee_type='Student' and 
                                                  ars.former_id = finance_transactions.payee_id", 
      :conditions => ["finance_transactions.payee_type = 'Student' and 
                                (finance_transactions.payee_id in (?) or finance_transactions.payee_id in (?))", 
        Student.find(:all, 
          :conditions => ["(LTRIM(first_name) LIKE ? OR LTRIM(middle_name) LIKE ? OR 
                                      LTRIM(last_name) LIKE ? OR admission_no = ? OR 
                                      (CONCAT(TRIM(first_name), \" \", TRIM(last_name)) LIKE ? ) OR 
                                      (CONCAT(TRIM(first_name), \" \", TRIM(middle_name), \" \", 
                                                     TRIM(last_name)) LIKE ? ))", "%#{search_string}%",
            "%#{search_string}%", "%#{search_string}%", "#{search_string}", 
            "%#{search_string}%", "%#{search_string}%"]).collect(&:id).uniq.join(','), 
        ArchivedStudent.find(:all, 
          :conditions => ["(LTRIM(first_name) LIKE ? OR LTRIM(middle_name) LIKE ? OR 
                                      LTRIM(last_name) LIKE ? OR admission_no = ? OR 
                                      (CONCAT(TRIM(first_name), \" \", TRIM(last_name)) LIKE ? ) OR 
                                      (CONCAT(TRIM(first_name), \" \", TRIM(middle_name), \" \", 
                                                     TRIM(last_name)) LIKE ? ))", "%#{search_string}%", 
            "%#{search_string}%", "%#{search_string}%", "#{search_string}", "%#{search_string}%", 
            "%#{search_string}%"]).collect(&:former_id).uniq.join(',')]} }
  
  include CsvExportMod


  class << self
    attr_accessor_with_default :send_sms, true
    attr_accessor_with_default :particular_wise_pay_lock, false    
  end

  def make_transaction_ledger
    if !transaction_type.present? || (transaction_type.present? and transaction_type == 'SINGLE')            
      self.transaction_ledger = FinanceTransactionLedger.create({
          :payment_note => self.payment_note,
          :payment_mode => self.payment_mode,
          :transaction_date => self.transaction_date,
          :transaction_type => transaction_type || 'SINGLE',
          :payee_id => self.payee_id,
          :payee_type => self.payee_type,
          :reference_no => self.reference_no,
          :amount => self.amount,
          :category_is_income => self.category.is_income
        })
    end
  end
  
  def verify_and_send_sms
    recipients = []
    models = {'FinanceFee' => 'finance_fee_collection', 'HostelFee' => 'hostel_fee_collection', 'TransportFee' => 'transport_fee_collection',
      'InstantFee' => 'instant_fee_category', 'RegistrationCourse' => ''}
    sms_setting = SmsSetting.new()
    if sms_setting.application_sms_active and models.keys.include? finance_type and sms_setting.fee_submission_sms_active and payee.present?
      payee_name = self.payee.first_name
      case finance_type
      when 'InstantFee'
        collection_name = self.finance.instant_fee_category.present? ? self.finance.instant_fee_category.name : self.finance.custom_category
      when 'RegistrationCourse'
        collection_name = t('application_fees')
      else
        collection = self.finance.send(models[finance_type])
        collection_name = collection.name if collection.present?
      end
      if payee.is_a? Student and payee.is_sms_enabled
        if sms_setting.parent_sms_active
          guardian = payee.immediate_contact if payee.immediate_contact.present?
          recipients.push guardian.mobile_phone if (guardian.present? and guardian.mobile_phone.present?)
        end
        recipients.push payee.phone2 if (sms_setting.student_sms_active and payee.phone2.present?)
      end
      if payee.is_a? Employee and sms_setting.employee_sms_active
        recipients.push payee.mobile_phone if payee.mobile_phone.present?
      end
      if payee.class.name == "Applicant"
        recipients.push payee.phone2 if payee.phone2.present?
      end
    end
    if recipients.present? and collection_name.present?
      recipients = recipients.collect { |x| x.split(',') }
      recipients.flatten!
      recipients.uniq!
      message = "#{t('fee_sms_message_body', :payee_name => payee_name, :currency_name => currency_name, :amount => FedenaPrecision.set_and_modify_precision(amount.to_f), :collection_name => collection_name, :payment_date => format_date(transaction_date))}"

      Delayed::Job.enqueue(SmsManager.new(message, recipients), {:queue => 'sms'})
    end
  end

  def set_transaction_stamp
    self.transaction_stamp = Time.now.to_i
  end

  def verify_precision
    unless finance_type == 'EmployeePayslip'
      self.amount = FedenaPrecision.set_and_modify_precision self.amount
      self.fine_amount = FedenaPrecision.set_and_modify_precision self.fine_amount
    end
  end

  def self.report(start_date, end_date, page)
    cat_names = ['Fee', 'Salary', 'Donation']
    FedenaPlugin::FINANCE_CATEGORY.each do |category|
      cat_names << "#{category[:category_name]}"
    end
    fixed_cat_ids = FinanceTransactionCategory.find(:all, :conditions => {:name => cat_names}).collect(&:id)
    self.find(:all,
      :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and category_id NOT IN (#{fixed_cat_ids.join(",")})"],
      :order => 'transaction_date')
  end

  def self.grand_total(start_date, end_date)
    fee_id = FinanceTransactionCategory.find_by_name("Fee").id
    donation_id = FinanceTransactionCategory.find_by_name("Donation").id
    cat_names = ['Fee', 'Salary', 'Donation']
    plugin_name = []
    FedenaPlugin::FINANCE_CATEGORY.each do |category|
      cat_names << "#{category[:category_name]}"
      plugin_name << "#{category[:category_name]}"
    end
    fixed_categories = FinanceTransactionCategory.find(:all, :conditions => {:name => cat_names})
    fixed_cat_ids = fixed_categories.collect(&:id)
    fixed_transactions = FinanceTransaction.find(:all,
      :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and category_id IN (#{fixed_cat_ids.join(",")})"])
    other_transactions = FinanceTransaction.find(:all,
      :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and category_id NOT IN (#{fixed_cat_ids.join(",")})"])
    #    transactions_fees = FinanceTransaction.find(:all,
    #      :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and category_id ='#{fee_id}'"])
    #    donations = FinanceTransaction.find(:all,
    #      :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and category_id ='#{donation_id}'"])
    trigger = FinanceTransactionTrigger.find(:all)
    hr = Configuration.find_by_config_value("HR")
    income_total = 0
    expenses_total = 0
    fees_total =0
    salary = 0

    unless hr.nil?
      salary = FinanceTransaction.sum('amount', :conditions => {:title => "Monthly Salary", :transaction_date => start_date..end_date}).to_f
      expenses_total += salary
    end

    transactions_fees = fixed_transactions.reject { |tr| tr.category_id != fee_id }
    donations = fixed_transactions.reject { |tr| tr.category_id != donation_id }

    donations.each do |d|
      if d.master_transaction_id == 0
        income_total +=d.amount
      else
        expenses_total +=d.amount
      end

    end
    transactions_fees.each do |fees|
      income_total +=fees.amount
      fees_total += fees.amount
    end

    # plugin transactions
    plugin_name.each do |p|
      category = fixed_categories.reject { |cat| cat.name.downcase != p.downcase }
      unless category.blank?
        cat_id = category.first.id
        transactions_plugin = fixed_transactions.reject { |tr| tr.category_id != cat_id }
        transactions_plugin.each do |t|
          if t.category.is_income?
            income_total +=t.amount
          else
            expenses_total +=t.amount
          end
        end
      end
    end
    other_transactions.each do |t|
      if t.category.is_income? and t.master_transaction_id == 0
        income_total +=t.amount
      else
        expenses_total +=t.amount
      end
    end
    income_total-expenses_total

  end


  def fetch_finance_batch
    batch = case self.finance_type
    when "TransportFee" then
      self.finance.groupable
    when "HostelFee" then
      self.finance.batch
    when "FinanceFee" then
      self.finance.batch
    when "InstantFee" then
      self.finance.groupable
    end
    batch
  end


  def get_collection
    collection_type = finance_type.underscore + "_collection"
    finance.send(collection_type)
  end

  def self.total_fees(start_date, end_date)
    fee_id = FinanceTransactionCategory.find_by_name("Fee").id
    fees =[]
    fees = FinanceTransaction.find(:all,
      :joins => "INNER JOIN batches on batches.id=finance_transactions.batch_id
              INNER JOIN finance_fees on finance_fees.id=finance_transactions.finance_id and finance_transactions.finance_type='FinanceFee'",
      :conditions => ["finance_transactions.transaction_date >= '#{start_date}' and finance_transactions.transaction_date <= '#{end_date}' and finance_transactions.category_id='#{fee_id}'"],
      :group => ["finance_fees.fee_collection_id,finance_transactions.batch_id"],
      :select => ["batches.*,SUM(finance_transactions.amount) as transaction_total,finance_fees.fee_collection_id as collection_id ,batches.id as batch_id"])
    return fees
  end

  def self.total_other_trans(start_date, end_date)
    cat_names = ['Fee', 'Salary', 'Donation']
    FedenaPlugin::FINANCE_CATEGORY.each do |category|
      cat_names << "#{category[:category_name]}"
    end
    fixed_cat_ids = FinanceTransactionCategory.find(:all, :conditions => {:name => cat_names}).collect(&:id)
    fees = 0
    transactions = FinanceTransaction.find(:all, :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and category_id NOT IN (#{fixed_cat_ids.join(",")})"])
    transactions_income = transactions.reject { |x| !x.category.is_income? }.compact
    transactions_expense = transactions.reject { |x| x.category.is_income? }.compact
    income = 0
    expense = 0
    transactions_income.each do |f|
      income += f.amount
    end
    transactions_expense.each do |f|
      expense += f.amount
    end
    [income, expense]
  end

  def self.donations_triggers(start_date, end_date)
    donation_id = FinanceTransactionCategory.find_by_name("Donation").id
    donations_income =0
    donations_expenses =0
    donations_income=FinanceTransaction.sum(:amount, :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'  and category_id ='#{donation_id}'"]).to_f
    ##    trigger = FinanceTransaction.find(:all, :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}' and master_transaction_id != 0 and category_id ='#{donation_id}'"])
    #    donations.each do |d|
    #      if d.category.is_income?
    #        donations_income+=d.amount
    #      else
    #        donations_expenses+=d.amount
    #      end
    #    end
    #    trigger.each do |t|
    #unless t.finance_category.id.nil?
    # if d.category_id == t.finance_category.id
    #      donations_expenses += t.amount
    #end
    #end
    #    end
    donations_income
  end


  def self.expenses(start_date, end_date)
    expenses = FinanceTransaction.find(:all, :select => 'finance_transactions.*', :joins => ' INNER JOIN finance_transaction_categories ON finance_transaction_categories.id = finance_transactions.category_id', \
        :conditions => ["finance_transaction_categories.is_income = 0 and finance_transaction_categories.id != 1 and transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'"])
    expenses=expenses.reject { |exp| (exp.category.is_fixed or exp.master_transaction_id != 0) }
  end

  def self.incomes(start_date, end_date)
    incomes = FinanceTransaction.find(:all, 
      :select => 'finance_transactions.*', 
      :joins => ' INNER JOIN finance_transaction_categories 
                                 ON finance_transaction_categories.id = finance_transactions.category_id',
      :conditions => ["finance_transaction_categories.is_income = 1 and 
                               transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}' "],
      :include => [:transaction_ledger,:category] )
    incomes = incomes.reject { |income| (income.category.is_fixed or income.master_transaction_id != 0) }
    incomes
  end


  def student_payee
    stu = self.payee
    stu ||= ArchivedStudent.find_by_former_id(self.payee_id)
  end

  def employee_payee
    stu = self.payee
    stu ||= ArchivedEmployee.find_by_former_id(self.payee_id)
  end

  def fetch_payee
    record = self.payee
    record ||= self.payee_type == "Employee" ? self.employee_payee : self.payee_type == "Student" ? self.student_payee : self.payee
  end

  def amount_with_precision
    return FedenaPrecision.set_and_modify_precision(self.amount)
  end


  def self.total_transaction_amount(transaction_category, start_date, end_date)
    amount = 0
    finance_transaction_category = FinanceTransactionCategory.find_by_name("#{transaction_category}")
    category_type = finance_transaction_category.is_income ? "income" : "expense"
    transactions = FinanceTransaction.find(:all,
      :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and category_id ='#{finance_transaction_category.id}'"], :include => :category)
    transactions.each { |transaction| amount += transaction.amount }
    return {:amount => amount, :category_type => category_type}
  end

  def self.get_total_amount(category_name, date_range_1, date_range_2)
    cat_id=get_transaction_category(category_name)
    fees=FinanceTransaction.find(:first,
      :conditions => {:category_id => cat_id},
      :select => "ifnull(sum(case when transaction_date >= '#{date_range_1.first}' and transaction_date <= '#{date_range_1.last}' then finance_transactions.amount end),0)  as first,ifnull(sum(case when transaction_date >= '#{date_range_2.first}' and transaction_date <= '#{date_range_2.last}' then finance_transactions.amount end),0) as second")
    return fees
  end

  def self.get_transaction_category(category_type)
    cat_id=FinanceTransactionCategory.find_by_name(category_type).id
    return cat_id
  end

  #  def receipt_number_settings
  #    # 1 : single receipt number for multi
  #    # 0 : individual receipt number for respective finance transactions
  #    @receipt_number_settings ||= (Configuration.find_by_config_key 'SingleReceiptNumber').try(:to_i) || 1
  #  end

  #  def receipt_number
  #    if !FinanceTransactionReceipt.receipt_number_settings.zero? and multi_fees_transactions.present?
  #      multi_fees_transactions.last.finance_transaction_receipt.try(:receipt_number)
  #    else
  #      finance_transaction_receipt.try(:receipt_number)      
  #    end
  #  end

  def receipt_number
    receipt_no.present? ? receipt_no : (
      (transaction_ledger.present? and 
          transaction_ledger.transaction_mode == 'SINGLE') ? transaction_ledger.receipt_no : "")    
  end
  
  def add_voucher_or_receipt_number
    if self.category.is_income and self.master_transaction_id == 0      
      if FinanceTransactionLedger.receipt_number_settings.zero?
        self.receipt_no = FinanceTransactionLedger.generate_receipt_no
      end
    else
      last_transaction = FinanceTransaction.last(:conditions => "voucher_no IS NOT NULL and TRIM(voucher_no) not like ''")
      last_voucher_no = last_transaction.voucher_no unless last_transaction.nil?
      if last_voucher_no.present?
        voucher_split = last_voucher_no.to_s.scan(/[A-Z]+|\d+/i)
        if voucher_split[1].blank?
          voucher_number = voucher_split[0].next
        else
          voucher_number = voucher_split[0]+voucher_split[1].next
        end
      else
        voucher_number = "1"
      end
      self.voucher_no = voucher_number
    end    
  end

  def refund_receipt_no
    receipt_numbers = FinanceTransaction.search(:receipt_no_not_like => "refund").map { |f| f.receipt_no }
    last_no = receipt_numbers.map { |k| k.scan(/\d+$/i).last.to_i }.max
    last_transaction = FinanceTransaction.last(:conditions => ["receipt_no NOT LIKE '%refund%' and receipt_no LIKE ?", "%#{last_no}"])
    last_receipt_no = last_transaction.receipt_no unless last_transaction.nil?
    unless last_receipt_no.nil?
      receipt_split = /(.*?)(\d+)$/.match(last_receipt_no)
      if receipt_split[1].blank?
        receipt_number = receipt_split[2].next
      else
        receipt_number = receipt_split[1]+receipt_split[2].next
      end
    else
      config_receipt_no = Configuration.get_config_value('FeeReceiptNo')
      receipt_number = config_receipt_no.present? ? config_receipt_no : "1"
    end
    return receipt_number
  end

  def set_fine
    # balance=finance.balance+fine_amount-(amount)
    if finance_type=="FinanceFee"
      balance=finance.balance
      manual_fine= fine_amount.present? ? fine_amount.to_f : 0
      fee_balance=balance
      actual_amount=balance+finance.finance_transactions.sum(:amount)-finance.finance_transactions.sum(:fine_amount)
      date=finance.finance_fee_collection
      days=(transaction_date-date.due_date.to_date).to_i
      auto_fine=date.fine
      fine_amount=0
      if auto_fine.present?
        fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{date.created_at}'"], :order => 'fine_days ASC')
        fine_amount=fine_rule.is_amount ? fine_rule.fine_amount : (actual_amount*fine_rule.fine_amount)/100 if fine_rule
        paid_fine=finance.finance_transactions.find(:all, :conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
        fine_amount=fine_amount-paid_fine
      end
      # actual_balance=FedenaPrecision.set_and_modify_precision(finance.balance+fine_amount).to_f
      actual_balance=FedenaPrecision.set_and_modify_precision(finance.balance).to_f+FedenaPrecision.set_and_modify_precision(fine_amount).to_f
      amount_paying=FedenaPrecision.set_and_modify_precision(amount-manual_fine).to_f
      actual_balance=0 if FinanceFee.find(finance.id).is_paid
      if amount_paying > actual_balance and description!='fine_amount_included'
        errors.add_to_base(t('finance.flash19'))
        return false
      end
    end
  end

  def add_user    
    if Fedena.present_user.present?
      update_attributes(:user_id => Fedena.present_user.id)
      if finance_type=="FinanceFee"
        update_attributes(:batch_id => "#{payee.batch_id}")
        FeeTransaction.create(:finance_fee_id => finance.id, :finance_transaction_id => id)
        balance=finance.balance+fine_amount-(amount)
        manual_fine= fine_amount.present? ? fine_amount.to_f : 0
        fee_balance=balance
        actual_amount=balance+finance.finance_transactions.sum(:amount)-
          finance.finance_transactions.sum(:fine_amount)        
        
        actual_amount -= finance.tax_amount.to_f if finance.tax_enabled?
                
        actual_amount += finance.finance_transactions.sum(:tax_amount).to_f if finance.tax_enabled?          
        
        date=finance.finance_fee_collection
        days=(transaction_date-date.due_date.to_date).to_i
        auto_fine=date.fine
        fine_amount=0
        if auto_fine.present?
          fine_rule=auto_fine.fine_rules.find(:last, :order => 'fine_days ASC', 
            :conditions => ["fine_days <= '#{days}' and created_at <= '#{date.created_at}'"])
          fine_amount=fine_rule.is_amount ? fine_rule.fine_amount : 
            (actual_amount*fine_rule.fine_amount)/100 if fine_rule
          auto_fine_amount=fine_amount
          
          paid_fine=finance.finance_transactions.find(:all, 
            :conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
          fine_amount=fine_amount-paid_fine
        end
        is_paid=false
        balance=FedenaPrecision.set_and_modify_precision(balance).to_f
        fine_amount= FedenaPrecision.set_and_modify_precision(fine_amount).to_f
        
        if (balance <= 0)
          fee_balance=0
          is_paid=(-(balance)==fine_amount)
          self.full_fine_paid = (fine_amount <= 0) 
          if -(balance)>0
            # self.fine_amount=-(balance)+manual_fine
            # self.fine_included=true
            # self.description="fine_amount_included"
            auto_fine_amount=nil unless is_paid            
            sql="UPDATE `finance_transactions` 
                           SET `fine_amount` = '#{-(balance)+manual_fine}',
                                  `auto_fine`='#{auto_fine_amount}', 
                                  `fine_included` = 1, 
                                  `description` = 'fine_amount_included' 
                      WHERE `id` = #{id}"
            # self.save(false)
            ActiveRecord::Base.connection.execute(sql)
          end
        end
        # is_paid= ((balance.to_f==0.0) and (finance.finance_transactions.sum(:fine_amount).to_f) >= (fine_amount.to_f))
        # finance.update_attributes(:balance => fee_balance, :is_paid => is_paid)
        finance_fee_sql="UPDATE `finance_fees` 
                                          SET `balance` = '#{fee_balance}', 
                                                 `is_paid` = #{is_paid} 
                                     WHERE `id` = '#{finance.id}'"
        
        ActiveRecord::Base.connection.execute(finance_fee_sql)
      elsif finance_type=="HostelFee"
        finance.update_attributes(:finance_transaction_id => id)
      elsif finance_type=="TransportFee"
        finance.update_attributes(:transaction_id => id)
      end

    end
  end

  def self.total(trans_id, fees)
    paid_fees = FinanceTransaction.find(:all, :conditions => "FIND_IN_SET(id,\"#{trans_id}\")", :order => "created_at ASC")
    total_fees=fees
    paid=0
    fine=0
    paid_fees.each do |p|
      paid += p.amount.to_f
      fine += p.fine_amount.to_f
    end
    total_fees =total_fees-paid
    total_fees =total_fees+fine
    #return @total_fees
  end

  def currency_name
    Configuration.currency
  end

  def date_of_transaction
    format_date(self.transaction_date, :format => :long)
  end

  def self.fetch_finance_payslip_data(params)
    finance_payslip_data(params)
  end

  def self.fetch_finance_transaction_data(params)
    finance_transaction_data(params)
  end

  def self.fetch_finance_batch_fee_transaction_data(params)
    finance_batch_fees_transaction_data(params)
  end

  def self.fetch_compare_finance_transactions_date(params)
    compare_finance_transactions_date(params)
  end

  def self.fetch_salary_with_department_data(params)
    salary_with_department_data(params)
  end

  def self.fetch_income_data(params)
    income_details_csv(params)
  end

  def create_cancelled_transaction
    transaction_ledger.mark_cancelled(self.cancel_reason, "PARTIAL")
    finance_transaction_attributes=self.attributes.except('id','created_at', 'updated_at','multi_fees_transaction_id')
    finance_transaction_attributes.merge!(:cancel_reason => self.cancel_reason)
    if finance_type=='FinanceFee'
      balance=finance.balance+(amount-fine_amount)
      finance.update_attributes(:is_paid => false, :balance => balance)

      FeeTransaction.destroy_all({:finance_transaction_id => id})
    end
    if finance.present? and ["FinanceFee", "HostelFee", "TransportFee", "InstantFee"].include? finance_type
      collection_name=finance.name
      finance_type_name=finance_type
    else
      if category_name=='Refund' and fee_refund.present? and fee_refund.finance_fee.present?
        collection_name=fee_refund.finance_fee.name
      else
        collection_name=nil
      end
      finance_type_name=category_name
    end
    finance_transaction_attributes.merge!(:user_id => Fedena.present_user.id, :finance_type => finance_type_name, :collection_name => collection_name)
    #    finance_transaction_attributes.delete "id"
    #    finance_transaction_attributes.delete "created_at"
    #    finance_transaction_attributes.delete "updated_at"
    #    finance_transaction_attributes.delete "multi_fees_transaction_id"
    finance_transaction_attributes.delete "finance_transaction_id"
    dependend_destroy_models=FinanceTransaction.reflect_on_all_associations.select { |a| a.options[:dependent]==:destroy }.map { |d| d.name }
    other_details={}
    dependend_destroy_models.each do |ddm|
      if instance_eval(ddm.to_s).respond_to? 'fetch_other_details_for_cancelled_transaction'
        other_details=instance_eval(ddm.to_s).fetch_other_details_for_cancelled_transaction
      end
    end
    finance_transaction_attributes.merge!(:other_details => other_details, :finance_transaction_id => id)
    if FedenaPlugin.can_access_plugin? :fedena_tally
      finance_transaction_attributes.merge!(:lastvchid => -(lastvchid.to_i.abs))
    end    
    cancelled_transaction=CancelledFinanceTransaction.new(finance_transaction_attributes)
    #    cancelled_transaction.build_finance_transaction_receipt(finance_transaction_receipt_attributes.
    #        except('id','created_at','deleted_at').
    #        merge({:is_cancelled_transaction => true})) if finance_transaction_receipt_attributes.present?
    cancelled_transaction.save
  end

  def refund_check
    if finance_type=='FinanceFee'
      return finance.fee_refund.blank?
    elsif finance_type=="TransportFee"
      finance.update_attributes(:transaction_id => nil)
    elsif finance_type=="HostelFee"
      finance.update_attributes(:finance_transaction_id => nil)
    end
  end

  def cashier_name
    user.present? ? user.full_name : "#{t('deleted_user')}"
  end
  
  def get_cashier_name
    user.present? ? ((user.user_type == "Parent" or user.user_type == "Student") ? '': user.full_name ) : " "
  end

  def particular_wise?
    trans_type=="particular_wise"
  end


  private

  def check_receipt_number_in_cancel_transaction(receipt_number)
    cancel_transaction = CancelledFinanceTransaction.find_last_by_receipt_no(receipt_number)
    if cancel_transaction.nil?
      return receipt_number
    else
      data = /(.*?)(\d*)$/.match(receipt_number.to_s)
      receipt_number = data[1].to_s + data[2].next.to_s
      check_receipt_number_in_cancel_transaction(receipt_number)
    end
  end

  def check_data_correctnes
    self.finance.present?
  end

  def transactions_with_similar_receipt_number(config_receipt_num_prefix)
    FinanceTransaction.all(:conditions => " receipt_no IS NOT NULL and receipt_no REGEXP '(#{config_receipt_num_prefix})\d*' and receipt_no NOT LIKE 'refund%'")
  end

  def check_receipt_number_existance(next_receipt_number)
    updated_receipt_number = check_receipt_number_in_cancel_transaction(next_receipt_number)
    FeeReceiptLock.receipt_no(updated_receipt_number)
  end

  def is_available_in_cache?
    !FeeReceiptLock.cache_has_receipt_no?
  end

  def calculate_receipt_number
    config_receipt_no_format = Configuration.get_config_value('FeeReceiptNo').nil? ? "" : Configuration.get_config_value('FeeReceiptNo').delete(' ')
    config_receipt_number = /(.*?)(\d*)$/.match(config_receipt_no_format)
    config_receipt_num_prefix = config_receipt_number[1] =~ /^\d+$/ ? "" : config_receipt_number[1]
    config_receipt_num_sufix = config_receipt_number[2].to_i
    if config_receipt_num_prefix.present?
      finance_transactions = transactions_with_similar_receipt_number(config_receipt_num_prefix)
      if finance_transactions.present?
        last_receipt_number = finance_transactions.map { |k| k.receipt_no.scan(/\d+$/i).last.to_i }.max
        next_receipt_no_sufix = last_receipt_number > config_receipt_num_sufix ? last_receipt_number : config_receipt_num_sufix
        next_receipt_number = config_receipt_num_prefix + next_receipt_no_sufix.next.to_s
      else
        next_prefix = config_receipt_num_sufix.present? ? config_receipt_num_sufix : 0
        next_receipt_number = config_receipt_num_prefix + next_prefix.to_s
      end
    else
      #code for manage no prefix(string) condition
      finance_transactions = FinanceTransaction.search(:receipt_no_not_like => "refund", :receipt_no_greater_than => config_receipt_num_sufix.to_i).all
      if finance_transactions.present?
        last_receipt_number = finance_transactions.map { |k| k.receipt_no.to_i }.max
        # to find maximum value of receipt no
        next_receipt_number = last_receipt_number.next
      else
        # for the first transaction it will count from 1  else it will count from suffix.
        next_receipt_number = config_receipt_num_sufix.present? ? config_receipt_num_sufix.next : 1
      end
    end
    next_receipt_number
  end

  def add_particular_amount
    allocate_amount_to_particulars=AllocateAmountToParticulars.new(self, self.finance)
    allocate_amount_to_particulars.save_allocation
  end

  def verify_fine_amount
    self.fine_amount = (fine_amount > amount) ? amount : fine_amount
  end
  
end


