class StudentRecord < ActiveRecord::Base
  #  VALID_IMAGE_TYPES = ['image/gif', 'image/png','image/jpeg', 'image/jpg']
  #  validates_attachment_content_type :photo, :content_type =>VALID_IMAGE_TYPES,
  #    :message=>'Image can only be GIF, PNG, JPG',:if=> Proc.new { |p| !p.photo_file_name.blank? }
  #  validates_attachment_size :photo, :less_than => 512000,\
  #    :message=>'must be less than 500 KB.',:if=> Proc.new { |p| p.photo_file_name_changed? }

  has_many :record_addl_attachments,:dependent=>:destroy
  belongs_to :record,:foreign_key=>'additional_field_id'
  belongs_to :student
  belongs_to :batch
  validates_uniqueness_of :additional_info,:scope=>[:student_id,:batch_id,:additional_field_id]
  accepts_nested_attributes_for :record_addl_attachments, :allow_destroy => true , :reject_if => lambda { |a| a.values.all?(&:blank?) }




  def self.get_records(student_id,batch_id,records_ids)
    StudentRecord.find_all_by_student_id_and_batch_id_and_additional_field_id(student_id,batch_id,records_ids)
  end
end
