class ConvertedAssessmentMark < ActiveRecord::Base
  
  serialize :actual_mark, Hash
  
  belongs_to :markable, :polymorphic => true
  belongs_to :assessment_group_batch
  belongs_to :assessment_group
  belongs_to :student
  
  def mark_with_grade
    if mark.present? 
    "#{mark.to_f.round(2)} #{overrided_max_mark}#{grade.present? ? "&#x200E;(#{grade})&#x200E;" : ""}"
    else
      grade
    end 
  end
  
  def mark_with_omm
    "#{mark.to_f}#{overrided_max_mark}"
  end
  
  def overrided_max_mark
    if self.markable_type != "Activity"
      self.assessment_group.overrided_mark(self.markable,self.assessment_group_batch.batch.course_id)
    else
      ""
    end
  end
  
end
