class AssessmentPlan < ActiveRecord::Base
  
  belongs_to :academic_year
  has_many :assessment_terms, :dependent => :destroy
  accepts_nested_attributes_for :assessment_terms
  has_many :assessment_plans_courses
  has_many :courses, :through => :assessment_plans_courses
  has_many :assessment_groups, :as => :parent
  has_many :generated_reports, :as => :report
  has_many :individual_reports, :as => :reportable

  accepts_nested_attributes_for :assessment_plans_courses, :allow_destroy => true, :reject_if => lambda { |l| l[:selected] == "0" }
  
  validates_presence_of :name
  validates_uniqueness_of :name, :case_sensitive => false, :scope => :academic_year_id
  
  validate :check_term_date_ranges
  before_save :check_courses
  
  def build_terms(count = nil)
    ac_year = academic_year
    if count
      count.times do |i|
        self.assessment_terms.build(:start_date => (i==0 ? ac_year.try(:start_date) : nil), :end_date => (i==(count-1) ? ac_year.try(:end_date) : nil))
      end
    else
      self.assessment_terms.build(:start_date => ac_year.try(:start_date), :end_date => ac_year.try(:end_date))
    end
  end
  
  def assessment_plan_id
    self.id
  end
  
  def has_dependencies?
    if has_terms?
      assessment_terms.all(:joins => :assessment_groups).count > 0
    else
      assessment_groups.present?
    end
  end
  
  def has_dependency_for_course(course)
    ass_ids  = get_assessment_groups.collect(&:id).uniq
    agbs = course.assessment_group_batches.all(:conditions => ["assessment_group_id in (?)", ass_ids])
    has_associated_records  = agbs.map {|agb| agb.children?}
    has_associated_records.include?(true)
  end
  
  def check_term_date_ranges
    previous = nil
    assessment_terms.sort{|a, b| (a.start_date && b.start_date) ? a.start_date <=> b.start_date : (a.start_date ? -1 : 1)}.each_with_index do |term, index|
      if index > 0 and term.start_date.present? and previous.end_date.present? and term.start_date < previous.end_date
        term.errors.add(:start_date, :overlaps_with_an_term) 
        errors.add(:base, :dependencies_exist)
      end
      previous = term
    end
  end
  
  def build_courses
    courses = Course.active.all(:select => "courses.*, group_concat(if(ap.academic_year_id = #{academic_year_id}, ap.id, null)) AS planner_id, 
      group_concat(if(ap.academic_year_id = #{academic_year_id}, ap.name, null)) AS planner_name",
      :joins => "LEFT OUTER JOIN assessment_plans_courses AS apc ON apc.course_id = courses.id 
      LEFT OUTER JOIN assessment_plans AS ap ON ap.id = apc.assessment_plan_id AND ap.academic_year_id = #{academic_year_id}",
      :include => :batches, :group => "courses.id")
    course_ids = assessment_plans_courses.collect(&:course_id) 
    groups_present = has_dependencies?
    courses.each do |c|
      unless course_ids.include? c.id
        assessment_plans_courses.build(:course_id => c.id, :selected => false, 
          :name => c.full_name, :batches_count => c.batches.length, :disable => c.planner_id.present?, :planner_name => c.planner_name)
      else
        course = assessment_plans_courses.detect{|pc| pc.course_id == c.id}
        course.attributes = {:selected => true, :name => c.full_name, 
          :batches_count => c.batches.length, :disable => groups_present, :planner_name => name}
      end
    end
  end
  
  def check_courses
    assessment_plans_courses.each do |course|
      course.mark_for_destruction if (!course.new_record? and course.selected == "0")
    end
  end
  
  def assessment_groups_count
    get_assessment_groups.count
  end
  
  def get_assessment_groups
    if terms_count > 0
      assessment_terms.all(:select=>'assessment_groups.*',:joins=>:assessment_groups)
    else
      assessment_groups
    end
  end
  
  def subject_exams
    if terms_count > 0
      assessment_terms.all(:select => 'assessment_groups.*', :conditions => ['assessment_groups.type = ? and assessment_groups.is_single_mark_entry = ?','SubjectAssessmentGroup',true] , :joins => :assessment_groups)
    else
      assessment_groups.all(:conditions => ['type = ? and is_single_mark_entry = ?','SubjectAssessmentGroup', true])
    end
  end
  
  def attribute_exams
    if terms_count > 0
      assessment_terms.all(:select => 'assessment_groups.*', :conditions => ['assessment_groups.type = ? and assessment_groups.is_single_mark_entry = ?','SubjectAssessmentGroup', false] , :joins => :assessment_groups)
    else
      assessment_groups.all(:conditions => ['type = ? and is_single_mark_entry = ?','SubjectAssessmentGroup', false])
    end
  end
  
  def activity_exams
    if terms_count > 0
      assessment_terms.all(:select => 'assessment_groups.*', :conditions => ['assessment_groups.type = ?','ActivityAssessmentGroup'] , :joins => :assessment_groups)
    else
      assessment_groups.all(:conditions => ['type = ?','ActivityAssessmentGroup'])
    end
  end
  
  def derived_exams
    if terms_count > 0
      assessment_terms.all(:select => 'assessment_groups.*', :conditions => ['assessment_groups.type = ? and assessment_groups.is_final_term = ? and parent_type = ?','DerivedAssessmentGroup', false, 'AssessmentTerm'] , :joins => :assessment_groups)
    else
      assessment_groups.all(:conditions => ['type = ? and is_final_term = ? and parent_type = ?','DerivedAssessmentGroup', false, 'AssessmentPlan'])
    end
  end
  
  def has_terms?
    terms_count > 0
  end
  
  def course_students
    courses.all(:joins => {:batches => :students}, :select => 'DISTINCT students.*', :conditions => ['batches.is_active = ? and students.is_active = ?',true, true])
  end
  
  def final_assessment
    final = self.assessment_groups.first(:conditions => {:is_final_term => true}) || DerivedAssessmentGroup.new(
      :parent => self,
      :scoring_type => 1,
      :assessment_plan_id => self.id,
      :academic_year_id => self.academic_year_id,
      :is_final_term => true
    )
    
    final.connectable_assessments = connectable_assessments
    final
  end
  
  def connectable_assessments
    AssessmentGroup.all(:conditions => ['parent_id in (?) AND parent_type = ? AND
      ((assessment_groups.type = ?) OR (assessment_groups.type = ? 
      AND assessment_groups.scoring_type in (?)))',assessment_term_ids,
        'AssessmentTerm','DerivedAssessmentGroup','SubjectAssessmentGroup',[1,3]], :include => :parent)
  end
  
  def connectable_assessments_without_derived
    AssessmentGroup.all(:conditions => ['parent_id in (?) AND parent_type = ? AND
      (assessment_groups.type = ? AND assessment_groups.scoring_type in (?))',assessment_term_ids,
      'AssessmentTerm','SubjectAssessmentGroup',[1,3]], :include => {:assessment_group_batches => 
      [:subject_assessments, :attribute_assessments, :activity_assessments]})
  end
  
  def subject_and_derived_assessments
     AssessmentGroup.all(:conditions => ['parent_id in (?) AND parent_type = ? AND
      (assessment_groups.type <> ?)',assessment_term_ids,
      'AssessmentTerm','ActivityAssessmentGroup'])
  end
  
  
  def connectable_derived_assessments
      DerivedAssessmentGroup.all(:conditions => ['parent_id in (?) AND parent_type = ? AND
      (scoring_type in (?))',assessment_term_ids,
      'AssessmentTerm',[1,3]], :include => :derived_assessment_group_setting)
  end
  
  def plan_assessment_groups
    AssessmentGroup.all(:conditions => ['parent_id in (?) AND parent_type = ?',assessment_term_ids, 'AssessmentTerm'])
  end
  
  def course_report(course_id)
    generated_reports.first(:conditions => {:course_id => course_id})
  end
  
  def get_assessment_groups_for_report(term_ids)
    black_list_group_ids = []
    connectable_derived_assessments.each do |dag|
      black_list_group_ids += dag.assessment_group_ids  if !dag.is_final_term and !dag.show_child_in_term_report? 
    end
    black_list_group_ids << '' if black_list_group_ids.blank?
    AssessmentGroup.all(:conditions => ['parent_id in (?) AND parent_type = ? AND id NOT IN (?) AND
      (assessment_groups.type <> ?)',term_ids,'AssessmentTerm',black_list_group_ids,'ActivityAssessmentGroup'])
  end
  
  def activity_assessments(term_ids)
    ActivityAssessmentGroup.all(:conditions => ['parent_id in (?) AND parent_type = ?',term_ids, 'AssessmentTerm'],
      :include => {:assessment_group_batches => {:activity_assessments => :assessment_activity}})
  end
end
