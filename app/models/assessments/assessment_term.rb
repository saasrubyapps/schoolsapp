class AssessmentTerm < ActiveRecord::Base
  belongs_to :assessment_plan
  has_many :assessment_groups, :as => :parent
  has_many :generated_reports, :as => :report
  has_many :individual_reports, :as => :reportable
  attr_accessor :academic_year_id
  
  validates_presence_of :start_date, :end_date, :name
  validate :check_date_range
  
  def validate
    unless start_date.nil? or end_date.nil?
      errors.add(:end_date,:end_date_cant_before_start_date) if self.end_date < self.start_date
    end
  end
  
  def has_employee_privilege
    true #Todo: Change in phase 2
  end
  
  
  def check_date_range
    unless start_date.nil? or end_date.nil?
      ay = AcademicYear.find academic_year_id
      error1 = self.start_date < ay.start_date
      error2 = self.end_date > ay.end_date
      errors.add(:start_date,"#{t('should_be_after_ay_start')}") if error1
      errors.add(:end_date,"#{t('should_be_before_ay_end')}") if error2
      unless error1 or error2
        
      end
    end
  end
  
  def final_assessment_added?
    final_assessment.present?
  end
  
  def final_assessment
    self.assessment_groups.first(:conditions => ['assessment_groups.type = ? and assessment_groups.is_final_term = ?', 'DerivedAssessmentGroup', true])
  end
  
  def term_name_with_span
    "#{self.name} <span>&#x200E;(#{term_span})&#x200E;</span>"
  end
  
  def term_span
    "#{format_date(start_date,:format => :month_year)} - #{format_date(end_date,:format => :month_year)}"
  end
  
  def term_name_with_max_marks
    final_assessment_added? ? "#{self.name} &#x200E;(#{self.final_assessment.maximum_marks})&#x200E;" : self.name
  end
  
  def get_assessment_groups_for_term_report
    derived_report_assessment_groups = self.assessment_groups.all(:include => :derived_assessment_group_setting,
      :conditions => {:type => 'DerivedAssessmentGroup', :is_final_term => false})
    black_list_group_ids = []
    derived_report_assessment_groups.each do |dag|
      black_list_group_ids += dag.assessment_group_ids  unless dag.show_child_in_term_report? 
    end
    black_list_group_ids << '' if black_list_group_ids.blank?
    self.assessment_groups.all(:conditions => ['assessment_groups.id NOT IN (?) AND assessment_groups.is_final_term = ? AND assessment_groups.type <> ?', 
        black_list_group_ids.compact.flatten.uniq , false, 'ActivityAssessmentGroup'])
  end
  
  def has_report?(course_id)
    report = course_report(course_id)
    report.present? and report.generated_report_batches.completed_batches.present?
  end
  
  def course_report(course_id)
    generated_reports.first(:conditions => {:course_id => course_id})
  end
  
  def subject_and_derived_assessments
    assessment_groups.all(:conditions => ["type <> ?", 'ActivityAssessmentGroup'])
  end
  
  
end
