class SubjectAssessment < ActiveRecord::Base

  attr_accessor :assessment_form_field_id, :batch_id, :course_id, :subject_list
  
  belongs_to :assessment_group_batch
  belongs_to :subject
  belongs_to :batch
  has_many :assessment_marks, :as => :assessment, :dependent => :destroy
  has_one :event, :as => :origin, :dependent => :destroy
  attr_accessor :assessment_form_field_id, :batch_id, :course_id, :subject_list
  before_save :check_marks
  after_create :create_assessment_event, :change_agb_status
  after_update :update_assessment_event
  after_update :check_submitted_marks
  after_destroy :destroy_marks, :change_agb_status
  
  named_scope :marks_added_assessments, {:conditions => {:marks_added => true}}
  
  accepts_nested_attributes_for :assessment_marks, :allow_destroy => true, :reject_if => lambda{|a| ((a[:is_absent] == "false") and a[:id].blank? and a[:grade_id].blank? and a[:marks].blank?) }
  before_save :check_status_change, :if=> Proc.new{|as| as.submission_status_changed? and as.submission_status.nil? }
  
  SUBMISSION_STATUS = {1 => t('marks_submitting'), 2 => t('marks_submitted'), 3 => t('marks_submission_failed')}
  
  def validate
    all_marks = assessment_marks.group_by(&:student_id)
    all_marks.each do |student_id, marks|
      if marks.length > 1
        old_data = marks.detect{|m| !m.new_record?}
        new_data = marks.select{|m| m.new_record?}
        old_data.attributes = {:marks => new_data.first.marks, :grade => new_data.first.grade, 
          :grade_id => new_data.first.grade_id, :is_absent => new_data.first.is_absent} if new_data.present?
        new_data.each{|m| m.mark_for_destruction}
      end
    end
  end
  
  def destroy_marks
    ConvertedAssessmentMark.delete_all(["assessment_group_batch_id = ? AND markable_id = ? AND markable_type = ?", assessment_group_batch_id, subject_id, 'Subject'])
  end
  
  def change_agb_status
    agb = self.assessment_group_batch
    if agb.subject_assessments.all(:conditions => {:marks_added => false}).blank?
      change_marks_added_status(agb, true)
    end
  end
  
  def change_marks_added_status(group_batch, status)
    if group_batch
      group_batch.reload
      group_batch.marks_added = status
      group_batch.send(:update_without_callbacks)
    end
  end
  
  def check_submitted_marks
    if self.changes.present? and self.changes.include? 'subject_id'
      ConvertedAssessmentMark.delete_all(["assessment_group_batch_id = ? AND markable_id = ? AND markable_type = ?", assessment_group_batch_id, self.subject_id_was, 'Subject'])
    end
  end
  
  def create_assessment_event
    if self.event.blank?
      batch = assessment_group_batch.batch
      group = assessment_group_batch.assessment_group
      params = {:event => {:title => "#{t('exam_text')}", :description => "#{group.name} #{t('for')} #{batch.full_name} - #{self.subject.name}",
          :is_common => false, :start_date => start_date_time, :end_date => end_date_time, :is_exam => true, 
          :origin => self, :batch_events_attributes =>{1=>{:batch_id => batch.id, :selected => "1"}}}}
      new_event = Event.new(params[:event])
      new_event.save
    end
  end
  
  def  update_assessment_event
    batch = assessment_group_batch.batch
    group = assessment_group_batch.assessment_group
    self.event.update_attributes(:start_date => start_date_time, :end_date => end_date_time, 
      :description => "#{group.name} #{t('for')} #{batch.full_name} - #{self.subject.name}") unless self.event.blank?
  end
  
  def start_date_time
    DateTime.new(exam_date.year, exam_date.month,
      exam_date.day, start_time.hour,
      start_time.min, start_time.sec)
  end
  
  def end_date_time
    DateTime.new(exam_date.year, exam_date.month,
      exam_date.day, end_time.hour,
      end_time.min, end_time.sec)
  end
  
  def submission_status_text
    submission_status.present? ? SUBMISSION_STATUS[submission_status] : t('no_marks_entered')
  end
  
  def build_student_marks(students)
    marks = self.assessment_marks.all(:include => :grade_details)
    assessment_scores = []
    students.each_with_index do |student, idx|
      student_mark = marks.detect{|m| m.student_id == student.id}
      assessment_scores << if student_mark.present?
        student_mark.attributes = {:sl_no => (idx+1),
          :student_name => student.full_name, 
          :student_roll_no => student.roll_number,
          :student_admission_no => student.admission_no}
        student_mark
      else
        self.assessment_marks.build(:student_id => student.id, 
          :sl_no => (idx+1),
          :student_name => student.full_name, 
          :student_roll_no => student.roll_number,
          :student_admission_no => student.admission_no)
      end
    end
    assessment_scores
  end
  
  def check_marks
    assessment_marks.each do |am|
      am.mark_for_destruction if !am.new_record? and !am.is_absent and am.marks.blank? and am.grade_id.blank?
    end
  end
  
  def maximum_marks_text
    if maximum_marks.present? and minimum_marks.present?
      "#{maximum_marks} &#x200E;(#{t('pass_text')} - #{minimum_marks})&#x200E;"
    elsif maximum_marks.present?
      "#{maximum_marks}"
    else
      "-"
    end
  end
  
  def submit_marks(students)
    self.reload
    valid = true
    student_ids = assessment_marks.collect(&:student_id)
    students.each{ |st| valid = false unless student_ids.include? st.id }
    if valid
      valid = self.update_attributes(:submission_status => 1)
      Delayed::Job.enqueue(DelayedAssessmentMarksSubmission.new(id, self.class.to_s),{:queue => "gradebook"}) if valid
    end
    return valid
  end
  
  def check_status_change
    agb = self.assessment_group_batch
    if agb.submission_status == 2 or agb.marks_added == true
      agb.submission_status = nil
      agb.marks_added = false
    end
    agb.send(:update_without_callbacks)
  end
  
end
