class IndividualReport < ActiveRecord::Base
  
  Report = Struct.new(:profile, :main_header, :header, :marks, :activities) 
  
  serialize :report, Report
  
  belongs_to :reportable, :polymorphic => true
  belongs_to :student
  belongs_to :generated_report_batch
  
end
