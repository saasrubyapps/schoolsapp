class AssessmentMark < ActiveRecord::Base
  
  attr_accessor :sl_no, :student_name, :student_roll_no, :student_admission_no
  
  belongs_to :student
  belongs_to :grade_details, :class_name => "Grade", :foreign_key => "grade_id"
  belongs_to :assessment, :polymorphic => true
  belongs_to :subject_assessment
  belongs_to :attribute_assessment
  belongs_to :activity_assessment
  
  before_save :reset_for_absentee
  before_save :check_for_mark_change , :if => :marks_changes
  after_destroy :check_for_mark_change
  

  def reset_for_absentee
    self.attributes = {:marks => '', :grade => '', :grade_id => ''} if self.is_absent
  end
  
  def check_for_mark_change
    assess = self.assessment
    assess.update_attributes(:submission_status => nil) if assess.submission_status == 2
  end
  
  def marks_changes
    self.marks_changed? or self.grade_changed? or self.grade_id_changed? or self.is_absent_changed?
  end
  
end
