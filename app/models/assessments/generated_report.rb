class GeneratedReport < ActiveRecord::Base
  
  #  serialize :batch_ids, Array
  
  belongs_to :report, :polymorphic => true
  belongs_to :course
  has_many :generated_report_batches, :dependent => :destroy
  has_many :batches, :through => :generated_report_batches
  
  after_save :generate_report
  
  def get_batches(group, course)
    batches = group.assessment_group_batches.select{|a| a.course_id == course.id}.compact
    all_batches = course.batches_in_academic_year(group.academic_year_id)
    batches_list = {}
    report_batches = generated_report_batches
    all_batches.each do |b|
      status = []
      group_batch = batches.detect{|agb| agb.batch_id == b.id}
      if group_batch.present? and  group_batch.childrens_present?
        status << "#{t('no_marks_entered')}" unless group_batch.marks_added
      else
        status << "#{t('not_scheduled')}"
      end
      generated_batch = report_batches.detect{|agb| agb.batch_id == b.id}
      batches_list[b.id] = {:selected => generated_batch.present?, :name => b.name, 
        :can_generate => status.empty?, :students => b.students.length,  :status_no => generated_batch.try(:generation_status),
        :status => generated_batch.try(:status_text), :publish_status => generated_batch.try(:publish_status), 
        :generate_status => status, :report_published => generated_batch.try(:report_published), :last_error => generated_batch.try(:last_error)}
    end
    batches_list
  end
  
  def get_term_batches(course)
    all_batches = course.batches_in_academic_year(report.assessment_plan.academic_year_id)
    batches_list = {}
    report_batches = generated_report_batches
    final_assessment = report.final_assessment
    term_groups = if final_assessment.present?
      final_assessment.all_assessment_groups
    else
      report.assessment_groups.without_derived.all(:include => {:assessment_group_batches => 
            [:subject_assessments, :attribute_assessments, :activity_assessments]})
    end
    derived_groups = if final_assessment.present?
      final_assessment.assessment_groups.derived_groups
    else
      report.assessment_groups.derived_groups
    end
    #    derived_groups << final_assessment if final_assessment.present?
    all_batches.each do |b|
      status = []
      term_groups.each do |group|
        group_batch = group.assessment_group_batches.detect{|agb| (agb.assessment_group_id == group.id and agb.batch_id == b.id)}
        if group_batch.present? and  group_batch.childrens_present?
          status << "#{group.name} - #{t('no_marks_entered')}" unless group_batch.marks_added
        else
          status << "#{group.name} - #{t('not_scheduled')}"
        end
      end
      derived_groups.each do |d_group|
        group_batch = d_group.assessment_group_batches.detect{|agb| (agb.assessment_group_id == d_group.id and agb.batch_id == b.id)}
        unless group_batch.present? and group_batch.try(:marks_added)
          status << ("#{d_group.name} - #{t('marks_not_calculated')}")
        end
      end
      status << "#{t('no_examgroups_present')}" if term_groups.length == 0
      generated_batch = report_batches.detect{|agb| agb.batch_id == b.id}
      batches_list[b.id] = {:selected => generated_batch.present?, :name => b.name, 
        :students => b.students.length, :status => generated_batch.try(:status_text), :status_no => generated_batch.try(:generation_status),
        :publish_status => generated_batch.try(:publish_status), :can_generate => status.empty?, :generate_status => status,
        :report_published => generated_batch.try(:report_published), :last_error => generated_batch.try(:last_error)
      }
    end
    batches_list
  end
  
  def get_plan_batches(all_batches, report)
    batches_list = {}
    report_batches = generated_report_batches
    final_assessment = report.final_assessment
    
    unless final_assessment.new_record?
      term_groups = final_assessment.all_assessment_groups
      derived_groups = final_assessment.assessment_groups.derived_groups
    end
    
    all_batches.each do |b|
      status = []
      unless final_assessment.new_record?
        term_groups.each do |group|
          group_batch = group.assessment_group_batches.detect{|agb| (agb.assessment_group_id == group.id and agb.batch_id == b.id)}
          if group_batch.present? and  group_batch.childrens_present?
            status << "#{group.name} - #{t('no_marks_entered')}" unless group_batch.marks_added
          else
            status << "#{group.name} - #{t('not_scheduled')}"
          end
        end
        derived_groups.each do |d_group|
          group_batch = d_group.assessment_group_batches.detect{|agb| (agb.assessment_group_id == d_group.id and agb.batch_id == b.id)}
          unless group_batch.present? and group_batch.try(:marks_added)
            status << ("#{d_group.name} - #{t('marks_not_calculated')}")
          end
        end
        status << "#{t('no_examgroups_present')}" if term_groups.length == 0
      else
        status << "#{t('final_plan_not_configured')}"
      end
      
      generated_batch = report_batches.detect{|agb| agb.batch_id == b.id}
      batches_list[b.id] = {:selected => generated_batch.present?, :name => b.name, 
        :students => b.students.length, :status => generated_batch.try(:status_text), :status_no => generated_batch.try(:generation_status),
        :publish_status => generated_batch.try(:publish_status), :can_generate => status.empty?, :generate_status => status,
        :report_published => generated_batch.try(:report_published), :last_error => generated_batch.try(:last_error)
      }
    end
    batches_list
  end
  
  def generate_report
    Delayed::Job.enqueue(DelayedGenerateAssessmentReport.new(id),{:queue => "gradebook"})
  end
  
  def publish_reports(course_id, batch_id = nil)
    conditions = (batch_id.present? ? {:batch_id => batch_id} : {})
    report_batches = generated_report_batches.all(:conditions => conditions)
    report_batches.each{|rb| rb.update_attribute(:report_published, true)}
    if report_type == 'AssessmentGroup'
      conditions[:course_id] = course_id
      conditions[:batch_id] = report_batches.collect(&:batch_id) unless batch_id.present?
      group_batches = report.assessment_group_batches.all(:conditions => conditions)
      group_batches.each do |rb|
        rb.result_published = true
        rb.send(:update_without_callbacks)
      end
    end
  end
  
  def fetch_students(batch_id)
    report_batch = generated_report_batches.first(:conditions => {:batch_id => batch_id}, :include => :individual_reports)
    student_ids = report_batch.individual_reports.collect(&:student_id)
    Student.all(:conditions => {:batch_id => batch_id, :id => student_ids}, :order => Student.sort_order)
  end
  
  def fetch_status(batch_id)
    generated_report_batches.first(:conditions => {:batch_id => batch_id}).report_published
  end
  
end
