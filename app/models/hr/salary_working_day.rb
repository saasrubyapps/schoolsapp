class SalaryWorkingDay < ActiveRecord::Base
  xss_terminate
  
  validates_presence_of :payment_period
  validates_presence_of :working_days, :message => :enter_the_number_of_working_days
  validates_numericality_of :working_days, :message => :working_days_should_be_a_number, :if => lambda{|d| d.working_days.present?}
  DEFAULT_VALUES = {2 => 7, 3 => 14, 4 => 15, 5 => 30}
  MAX_VALUES = {2 => 7, 3 => 14, 4 => 15, 5 => 31}

  def validate
    errors.add(:payment_period, :invalid) unless DEFAULT_VALUES.keys.include? payment_period
    if working_days.present? and is_number?(working_days)
      max_value = MAX_VALUES[payment_period.to_i]
      errors.add(:working_days, :working_days_limit_message, {:max_count => max_value}) unless (1..max_value).include? working_days.to_i
    end
    unless (working_days.to_f % 0.5 == 0.0)
      errors.add(:working_days, :working_days_as_whole_numbers)
    end
  end

  def self.get_working_days(payment_period)
    salary_working_day = find_by_payment_period(payment_period)
    working_days =
      if salary_working_day.present? and salary_working_day.working_days.present?
      salary_working_day.working_days
    else
      DEFAULT_VALUES[payment_period] || 1
    end
    return working_days
  end

  private
  def is_number?(num)
    /^(\+|-){0,1}[\d]+(\.[\d]+){0,1}$/ === num.to_s
  end
end







