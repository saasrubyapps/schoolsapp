class LeaveGroupEmployee < ActiveRecord::Base
  attr_accessor :selected, :name, :department, :position, :grade
  
  belongs_to :employee, :polymorphic => true
  belongs_to :leave_group
  
  validates_presence_of :employee_id
end
