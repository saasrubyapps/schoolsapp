class LeaveGroupLeaveType < ActiveRecord::Base
  attr_accessor :selected, :type_name
  
  belongs_to :employee_leave_type
  belongs_to :leave_group
  
  validates_presence_of :employee_leave_type_id, :leave_count
  
  def validate
    unless leave_count.to_f%0.5 == 0.0 
      errors.add(:leave_count, :leave_count_as_whole_numbers)
    end
  end
  
  def display_leave_count
    ("%g" % ("%.2f" % leave_count)) if leave_count.present?
  end
end
