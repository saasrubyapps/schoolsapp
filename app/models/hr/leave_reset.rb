class LeaveReset < ActiveRecord::Base
  xss_terminate
  
  has_many :leave_reset_logs

  RESET_TYPE = {1 => "all", 2 => "department_wise", 3 => "employee_wise"}

  RESET_STATUS = { 1 => "resetting", 2 => "completed", 3 => "failed"}

  validate :valid_reset_date
  validates_presence_of :reset_remark, :message => :please_enter_some_remarks
  validates_length_of :reset_remark, :maximum => 250

  def valid_reset_date
    unless reset_date.present?
      errors.add(:reset_date, :please_select_a_date)
    else
      if reset_date > Date.today
        errors.add(:reset_date, :reset_date_cannot_be_future_date)
      end
    end
  end

  def reset_msg
     case self.reset_type
     when 1
       return "#{t('all_employee')} (#{self.employee_count})&#x200E;"
     when 2
       return "#{self.employee_count} of #{department_name(self.reset_value)}"
     when 3
       e = Employee.find(self.reset_value) rescue nil
       e.present? ? "#{e.full_name} (#{e.employee_number})&#x200E;" : "#{t('deleted_user')}"
     end
  end

  def resetted_user
    user = User.find(self.resetted_by) rescue nil
    user.present? ? user.full_name : "#{t('user_deleted')}"
  end
  
  def department_name(dpt_id)
    department = EmployeeDepartment.find_by_id(dpt_id)
    dname = department.present? ? department.name : "#{t('deleted')} #{t('department')}"
    return dname
  end
end