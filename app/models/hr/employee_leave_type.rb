#Fedena
#Copyright 2011 Foradian Technologies Private Limited
#
#This product includes software developed at
#Project Fedena - http://www.projectfedena.org/
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

class EmployeeLeaveType < ActiveRecord::Base
  xss_terminate
  
  has_many :employee_leaves, :dependent => :destroy
  has_many :employee_attendances
  has_many :leave_group_leave_types
  has_many :leave_groups, :through => :leave_group_leave_types

  validates_presence_of :name,:message => :enter_a_leave_name
  validates_presence_of :code,:message => :enter_a_leave_code
  validates_presence_of :creation_status
  validates_uniqueness_of :name,:case_sensitive => false, :message => :leave_name_already_in_use
  validates_uniqueness_of :code,:case_sensitive => false, :message => :leave_code_already_in_use
  validates_length_of :name, :maximum => 80
  validates_length_of :code, :maximum => 20

  validates_numericality_of :max_leave_count, :greater_than_or_equal_to => 0,  :message => :leave_count_must_be_a_number
  validates_numericality_of :max_carry_forward_leaves, :greater_than => 0, :if => "carry_forward_type == 2 && carry_forward", :message => :enter_maximum_leaves_carry_forwarded, :allow_blank => false

  
  # validates_presence_of :max_carry_forward_leaves, :if => "carry_forward_type == 2 && carry_forward"

  before_validation :strip_leading_spaces
  validate :valid_reset_date

  named_scope :active,:conditions => {:is_active => true, :creation_status => 2}
  named_scope :inactive,:conditions => {:is_active=> false, :creation_status => 2}
  named_scope :all_leave_types, :conditions => {:creation_status => 2}

  def strip_leading_spaces
    self.name = self.name.strip
    self.code = self.code.strip
  end

  def valid_reset_date
    if reset_date > Date.today
      errors.add(:reset_date, :reset_date_cannot_be_future_date)
    end

    unless max_leave_count.to_f%0.5 == 0.0 
      errors.add(:max_leave_count, :leave_count_as_whole_numbers)
    end

    unless max_carry_forward_leaves.to_f%0.5 == 0.0
      errors.add(:max_carry_forward_leaves, :leave_count_as_whole_numbers)
    end
  end
  
  def name_with_code
    "#{name} &#x200E;(#{code})&#x200E;"
  end
  
  LEAVE_STATUS = {1 => t('creating_leave_type'), 2 => "success", 3 => t('leave_creation_failed') }

  CARRY_FORWARD_TYPE = {1 => :any_count , 2=> :specific_count }
end
