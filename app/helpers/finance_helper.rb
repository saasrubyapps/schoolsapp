module FinanceHelper
  # include ActionView
  # include Helpers
  # include TagHelper
  include ApplicationHelper
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::CaptureHelper

  def transaction_date_field(transaction_date=Date.today_with_timezone.to_date, attrs={})
    "<div class='label-field-pair3 special_case' style='height: auto; margin-top:-17px;'>
      <label>#{t('payment_date') }</label>
      <div class='date-input-bg'>
        #{calendar_date_select_tag 'transaction_date', I18n.l(transaction_date, :format => :default), {:popup => 'force', :class => 'start_date'}.merge(attrs) }
      </div>
      </div>".html_safe
  end

  def transaction_date_field_with_ajax(payment_date=I18n.l(FedenaTimeSet.current_time_to_local_time(Time.now).to_date, :format => :default), collection_id=nil, batch_id=nil, student_id=nil)
    "<div class='label-field-pair3 special_case' style='height: auto; margin-top:-17px;'>
      <label>#{t('payment_date') }</label>
      <div class='date-input-bg'>
        #{calendar_date_select_tag 'transaction_date', payment_date,
                                   :popup => 'force',
                                   :class => 'start_date',
                                   :onchange => "j.get('/finance/load_fees_submission_batch?date=#{collection_id}&batch_id=#{batch_id}&student=#{student_id}&payment_date='+j('#transaction_date').val()+'&fine=#{@fine}&reference_no='+j('#fees_reference_no').val()+'&payment_note='+j('#fees_payment_note').val()+'&payment_mode='+j('#fees_payment_mode').val()+'&others_payment_mode='+j('.others_payment_mode').val());" }
      </div>
      </div>".html_safe
  end

  def receipt_buttons(transaction_ids)
    result=""
    #FIXME following code not working due to a bug in link privilege
    # result+= link_to({:controller=>:finance,:action => "generate_fee_receipt_pdf",:transaction_id=>transaction_ids},:target =>'_blank')  do
    #   '<span class="hover-message">pdf</span>'
    # end
    pdf_link_text=content_tag(:span, "", :class => "pdf_icon_img") #+content_tag(:span,"pdf receipt",:class=>"hover-message")
    print_link_text=content_tag(:span, "", :class => "print_icon_img") #+content_tag(:span,"print receipt",:class=>"hover-message")
    result+=link_to(pdf_link_text, {:controller => :finance, :action => "generate_fee_receipt_pdf", :transaction_id => transaction_ids}, {:target => '_blank', :tooltip => I18n.t('view_pdf_receipt')})
    result+=link_to_function print_link_text, "show_print_dialog(#{transaction_ids.to_json})", :tooltip => I18n.t('print_receipt')
  end

  def receipt_buttons_for_pay_all_fees(student_id, transaction_ids, batch_id)
    result=""
    #FIXME following code not working due to a bug in link privilege
    # result+= link_to({:controller=>:finance,:action => "generate_fee_receipt_pdf",:transaction_id=>transaction_ids},:target =>'_blank')  do
    #   '<span class="hover-message">pdf</span>'
    # end
    pdf_link_text=content_tag(:span, "", :class => "pdf_icon_img") #+content_tag(:span,"pdf receipt",:class=>"hover-message")
    print_link_text=content_tag(:span, "", :class => "print_icon_img") #+content_tag(:span,"print receipt",:class=>"hover-message")
    result+=link_to_function(pdf_link_text, "show_receipts(this);", :class => "receipts")
    result+=link_to_function print_link_text, "show_print_dialog(#{transaction_ids.to_json})", :tooltip => I18n.t('print_receipt')
    show_receipts(result, student_id, transaction_ids, batch_id)

  end

  def show_receipts(result, student_id, transaction_ids, batch_id)
    result+=content_tag(:div, :class => "receipt-box") do
      concat content_tag(:div, (content_tag(:div, "", :class => "arrow_box")), :class => "arrow_dominate")
      concat content_tag(:li, (link_to I18n.t("overall_receipt"), {:controller => :finance_extensions, :action => "generate_overall_fee_receipt_pdf", :student_id => student_id, :transaction_id => transaction_ids, :batch_id => batch_id}, {:target => '_blank'}), :class => "overall-receipt")
      concat content_tag(:li, (link_to I18n.t("detailed_receipt"), {:controller => :finance, :action => "generate_fee_receipt_pdf", :transaction_id => transaction_ids}, {:target => '_blank'}), :class => "detailed-receipt")
    end
    javascript_block(result)
  end

  def javascript_block(result)
    result+=javascript_tag do
      <<-EOT



   j(function () {
        j('.receipts').hover(
                function () {
                    j(".receipt-box").hide();
                    j(this).parent().children('.receipt-box').show();
                },
                j('.receipt-box').mouseleave(function (e) {
                    j(".receipt-box").hide();
                })
        )
    });
    j(document).on('mouseover', 'div', function (e) {
        class_array = ['receipts', 'pdf_icon_img', 'detailed-receipt', 'overall-receipt', 'arrow_dominate', undefined]
        hover_class = (j(e.target).attr('class'))
        if (j.inArray(hover_class, class_array) == -1) {
            j(".receipt-box").hide();
        }
    });
   j('.receipts').click(function(e)
     {
       e.preventDefault();
     });

      EOT
    end
    result
  end


  def show_more_in_paid_fees(column,separator=',',regex=/.*?,/)
    result=""
    if column.present?
      first_element= (column.slice! regex)
      column_elements=column.split(separator)
      elements_count=column_elements.count
      column_elements=show_with_index(column_elements)
      column_elements=column_elements.join("<div class='label-underline'></div>")
      if !first_element.nil?

        result+=content_tag(:td, :class => "col-3 left_align min-width150") do
          concat content_tag(:div, "#{first_element.gsub(separator, '')}"+(content_tag(:c, "+#{(elements_count)} #{t('more').downcase}", :class => "collection_column", :tooltip => column_elements, :delay => "10")), :class => "colln_or_recp_name")
        end
      else
        content_tag(:td, column_elements.gsub('1. ', ''), :class => "col-3 left_align min-width150")
      end
    end
  end


  def show_with_index(elements)
    elements.inject(nil) { |a, b| elements[elements.index(b)]="#{elements.index(b)+1}. #{b}" } if elements.count >1
    return elements
  end


  def get_payment_mode_text(mode)
    case mode
      when 'Cash'
        return I18n.t('reference_no')
      when 'Cheque'
        return I18n.t('cheque_no')
      when 'Online Payment'
        return I18n.t('transaction_id')
      when 'DD'
        return I18n.t('dd_no')
      when 'Others'
        return I18n.t('others')
    end
  end
  
  def f_cashier_name(userid)
    user = User.find_by_id(userid)
    if user.present?
       cashier = (user.user_type == "Parent" or user.user_type == "Student") ? '': user.full_name 
       return cashier
    else
      return ''
    end
  end
end
