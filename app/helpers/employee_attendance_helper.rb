module EmployeeAttendanceHelper

  def total_leave_balance(employee,lt=nil)
    leave_type_status = true
    leave_type_status = lt.is_active if lt
    if leave_type_status 
      if lt
        emp_leaves =  employee.employee_leaves.select{|el| el.employee_leave_type_id  == lt.id}
      else
        emp_leaves =  employee.employee_leaves.all(:joins => :employee_leave_type, :conditions => ["employee_leave_types.is_active = ?",true])
      end
      leave_count = emp_leaves.sum(&:leave_count)
      balance_leave = (emp_leaves.sum(&:leave_count) - emp_leaves.sum(&:leave_taken)).to_f
      balance_leave < 0 ? "0/#{sprintf("%g",leave_count)}" : "#{sprintf("%g",balance_leave)}/#{sprintf("%g",leave_count)}"
    else
      return "<i/>#{t(:inactive)}"
    end
  end

  def total_additional_leave_count(employee,start_date =nil, end_date=nil,lt_id=nil)
    conditions = ""
    if start_date && end_date
      conditions += ((" && " if conditions.present?).to_s + "al.attendance_date.to_date <= end_date.to_date && al.attendance_date.to_date >= start_date.to_date")
    else
      conditions += ((" && " if conditions.present?).to_s + "al.attendance_date.to_date >= employee.last_reset_date.to_date")
    end

    if lt_id
      conditions += ((" && " if conditions.present?).to_s + "al.employee_leave_type_id == lt_id")
    end
    count = employee.employee_additional_leaves.collect{|al| (al.is_half_day ? 0.5 : 1) if eval(conditions)}.compact.sum
    count.zero? ? "-" : count
  end

  def total_lop_count(employee, start_date=nil, end_date=nil, lt_id=nil)
    conditions = "al.is_deductable"
    if start_date && end_date
      conditions += " && " + "al.attendance_date.to_date <= end_date.to_date && al.attendance_date.to_date >= start_date.to_date"
    else
      conditions += " && " + "al.attendance_date.to_date >= employee.last_reset_date.to_date"
    end

    if lt_id
      conditions += " && " + "al.employee_leave_type_id == lt_id"
    end
    count = employee.employee_additional_leaves.collect{|al| (al.is_half_day ? 0.5 : 1) if eval(conditions)}.compact.sum
    count.zero? ? "-" : count
  end

  def emp_leave_count(employee, start_date=nil, end_date=nil, lt_id=nil)
    conditions = ""
    if start_date && end_date
      conditions += (" && " if conditions.present?).to_s + "ea.attendance_date.to_date <= end_date.to_date && ea.attendance_date.to_date >= start_date.to_date"
    else
      conditions += (" && " if conditions.present?).to_s + "ea.attendance_date.to_date >= employee.last_reset_date.to_date"
    end

    if lt_id
      conditions += (" && " if conditions.present?).to_s + "ea.employee_leave_type_id == lt_id"
    end
    count = employee.employee_attendances.collect{|ea| (ea.is_half_day ? 0.5 : 1) if eval(conditions)}.compact.sum
    count.zero? ? "-" : count
  end

  def application_date_range(applied_leave)
    start_date = applied_leave.start_date
    end_date = applied_leave.end_date
    
    if start_date == end_date
      return format_date(start_date, :format => :short)
    else
      return format_date(start_date, :format => :short) + "#{t('to')}&nbsp" + format_date(end_date, :format => :short)
    end
  end

  def days_count(applied_leave)
    return 0.5 if applied_leave.is_half_day
    applied_leave.end_date - applied_leave.start_date + 1
  end

  def additional_leave_count(emp_leave, applied_leave)
    leave_count = days_count(applied_leave)
    status = application_status(applied_leave)
    if status == "pending" or status == 'denied'
      if emp_leave.leave_taken > emp_leave.leave_count
        if applied_leave.is_half_day
          return 0.5
        else
          return applied_leave.end_date - applied_leave.start_date + 1
        end
      else
        add_leave_count = 0
        new_count = emp_leave.leave_taken.to_f
        (1..(leave_count)/0.5).each do |l|
          new_count+= 0.5
          if new_count > emp_leave.leave_count
            add_leave_count += 0.5
          end
        end
        return add_leave_count
      end
    else
      start_date = applied_leave.start_date
      end_date = applied_leave.end_date
      employee = emp_leave.employee
      additional_leaves = employee.employee_additional_leaves.select{|l| l.employee_leave_type_id == emp_leave.employee_leave_type_id && l.attendance_date >= start_date && l.attendance_date <= end_date}
      return additional_leaves.collect{|ea| (ea.is_half_day ? 0.5 : 1)}.compact.sum
    end
  end

  def application_status(applied_leave)
    return 'approved' if applied_leave.approved and applied_leave.viewed_by_manager
    return 'denied' if !applied_leave.approved and applied_leave.viewed_by_manager
    return 'pending' if (applied_leave.approved.nil? or !applied_leave.approved) and !applied_leave.viewed_by_manager
  end


  def recent_leave(leaves)
    if leaves.present?
      no_of_days = leaves.inject(0){|sum,x| sum + (x.is_half_day ? 0.5 : 1.0) }
      dates = leaves.collect(&:attendance_date)
      if dates.min == dates.max
        return "#{sprintf("%g",no_of_days)} (#{t('day')}) , #{format_date(dates.min)}"
      end
      return "#{sprintf("%g",no_of_days)} (#{t('days')}) , #{format_date(dates.min)} #{t('to')} #{format_date(dates.max)}"
    else
      return "-"
    end
  end
end