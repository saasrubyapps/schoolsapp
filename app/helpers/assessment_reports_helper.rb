module AssessmentReportsHelper
  def fetch_report_generation_path(report, course)
    
    case report.class.to_s
    when 'AssessmentPlan'
      generate_planner_reports_assessment_reports_path(:assessment_plan_id => report.id, :course_id => course.id)
    when 'AssessmentTerm'
      generate_term_reports_assessment_reports_path(:term_id => report.id, :course_id => course.id)
    else
      generate_exam_reports_assessment_reports_path(:group_id => report.id, :course_id => course.id)
    end
  end
end
