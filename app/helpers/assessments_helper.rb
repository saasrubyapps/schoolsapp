module AssessmentsHelper
  def attribute_linking_link_name(batch,ag)
    attribute_assessment_present?(batch,ag) ? t('edit_attributes') : t('link_attributes')
  end
  
  def assessment_activated?(batch,ag,type)
    if type.subject_attribute or type.subject_wise_attribute
      AssessmentGroupBatch.batch_attribute_assessments(batch,ag).count > 0
    elsif type.subject
      AssessmentGroupBatch.batch_subject_assessments(batch,ag).count > 0
    else
      AssessmentGroupBatch.batch_actvity_assessments(batch,ag).count > 0
    end
  end
  
  def marks_entered?(batch,ag,type)
    if type.subject_attribute or type.subject_wise_attribute
      AssessmentGroupBatch.batch_attribute_assessments_with_marks(batch,ag).count > 0
    elsif type.subject
      AssessmentGroupBatch.batch_subject_assessments_with_marks(batch,ag).count > 0
    else
      AssessmentGroupBatch.batch_actvity_assessments_with_marks(batch,ag).count > 0
    end
  end
  
  def list_assessments(type,assessments, batch, assessment_group, inactive_subjects)
    if type.activity
      render :partial => "activity_assessments_list", :locals=>{:assessments=>assessments, :batch => batch , :assessment_group => assessment_group}
    elsif type.subject_attribute or type.subject_wise_attribute
      render :partial => "attribute_assessments_list", :locals=>{:assessments=>assessments, :batch => batch, 
        :assessment_group => assessment_group, :inactive_subjects => inactive_subjects}
    elsif type.subject
      render :partial => "subject_assessments_list", :locals=>{:assessments=>assessments, :batch => batch, :assessment_group => assessment_group}
    end
  end
  
  def link_to_add_assessment_fields(name, c, association, partial)
    new_object = c.object.class.reflect_on_association(association).klass.new
    fields = c.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(partial + "_fields", :c => builder)
    end
    link_to_function(name, h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")"),{:class=>"add_button_img"})
  end
end
