class GradebooksController < ApplicationController
  before_filter :login_required
  filter_access_to :all
  
  before_filter :find_academic_year, :only=>[:exam_management]
  
  def index
  end
  
  def settings
  end
  
  def exam_management
#    @courses = Course.active.paginate(:per_page=>10,:page=>params[:page])
    @courses = Course.active.paginate(:per_page=>10,:page=>params[:page],:joins => "LEFT OUTER JOIN assessment_schedules ON courses.id = assessment_schedules.course_id AND assessment_schedules.start_date >= NOW()",
      :select => 'courses.*, assessment_schedules.start_date as uc_start_date, assessment_schedules.end_date as uc_end_date, DATEDIFF(assessment_schedules.start_date, NOW()) AS date_diff', 
      :order => "courses.course_name asc, assessment_schedules.start_date asc", :group => "courses.id")
    @academic_years = AcademicYear.all
    if request.xhr?
      render(:update) do |page|
        page.replace_html 'exam_management_box', :partial=>'list_courses'
      end
    end
  end
  
  def course_assessment_groups
    @course = Course.find params[:id]
    @academic_year = AcademicYear.find params[:academic_year_id]
    @batch_ids = @course.batches_in_academic_year(@academic_year)
    @plan = @course.assessment_plans.last(:conditions=>{:academic_year_id=>@academic_year.id}, :include=>:assessment_terms)
    if request.xhr?
      render(:update) do |page|
        page.replace_html 'right-panel', :partial=>'list_course_plan_details'
      end
    end
  end
  
  def list_course_exam_groups
    @course = Course.find params[:course_id]
    @academic_year = AcademicYear.find params[:academic_year_id]
    @assessment_groups = @course.assessment_groups.without_derived.all(:conditions=>{:academic_year_id=>@academic_year.id})
    render(:update) do |page|
      page.replace_html 'right-panel', :partial=>'list_course_ag_details'
    end
  end
  
  def list_course_plan_details
    render(:update) do |page|
      page.replace_html 'right-panel', :partial=>'list_course_plan_details'
    end
  end
  
  def change_academic_year
    @academic_year = AcademicYear.find params[:id]
    @academic_years = AcademicYear.all
    @courses = Course.active.paginate(:per_page=>10,:page=>params[:page],:joins => "LEFT OUTER JOIN assessment_schedules ON courses.id = assessment_schedules.course_id AND assessment_schedules.start_date >= NOW()",
      :select => 'courses.*, assessment_schedules.start_date as uc_start_date, assessment_schedules.end_date as uc_end_date, DATEDIFF(assessment_schedules.start_date, NOW()) AS date_diff', 
      :order => "courses.course_name asc, assessment_schedules.start_date asc", :group => "courses.id", :include => :batches)
    render :update do |page|
      page.replace_html 'exam_management_box', :partial=>'list_courses', :object => [@academic_year, @courses]
      page.replace_html 'course_assessment_link', :partial=>'course_assessment_create_link', :object => @academic_year if can_access_request? :new_course_exam,:assessment_groups
    end
  end
  
  private
  
  def find_academic_year
    @academic_year = AcademicYear.active.first
    if @academic_year.nil?
      if can_access_request? :index,:academic_years
        flash[:notice] = "#{t('set_up_academic_year')}"
        redirect_to :controller=>:academic_years ,:action=>:index and return
      else
        flash[:notice] = "#{t('set_up_academic_year_with_admin')}"
        redirect_to :controller => 'user', :action => 'dashboard' and return
      end
    end
  end
end