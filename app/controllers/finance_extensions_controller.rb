class FinanceExtensionsController < FinanceController
  filter_access_to [:generate_overall_fee_receipt_pdf], :attribute_check => true, :load_method => lambda { FinanceTransaction.find_by_id_and_payee_id(params[:transaction_id], params[:student_id]) }
  check_request_fingerprint :particular_wise_fee_payment, :create_instant_particular, :create_instant_discount, :pay_defall_fees, :update_collection_discount,
    :update_collection_particular, :delete_student_particular, :pay_all_fees
  around_filter :lock_particular_wise_auto_creation, :only => :particular_wise_fee_payment
  before_filter :invoice_number_enabled?, :only => [ :view_fees_structure, :fees_structure_for_student, 
    :generate_overall_fee_receipt_pdf, :fee_structure_pdf]
  
  def pay_fees_in_particular_wise
    @student = Student.find(params[:id])
    @dates=FinanceFeeCollection.find(:all, 
      :joins => "INNER JOIN collection_particulars 
                                 ON collection_particulars.finance_fee_collection_id=finance_fee_collections.id 
                      INNER JOIN finance_fee_particulars 
                                 ON finance_fee_particulars.id=collection_particulars.finance_fee_particular_id 
                      INNER JOIN finance_fees 
                                 ON finance_fees.fee_collection_id=finance_fee_collections.id", 
      :conditions => "finance_fees.student_id='#{@student.id}' and 
                              finance_fee_collections.is_deleted=#{false} and 
                            ((finance_fee_particulars.receiver_type='Batch' and 
                              finance_fee_particulars.receiver_id=finance_fees.batch_id) or 
                             (finance_fee_particulars.receiver_type='Student' and 
                              finance_fee_particulars.receiver_id='#{@student.id}') or 
                             (finance_fee_particulars.receiver_type='StudentCategory' and 
                              finance_fee_particulars.receiver_id='#{@student.student_category_id}'))").uniq
  end

  def particular_wise_fee_payment
    @target_action='particular_wise_fee_payment'
    @target_controller='finance_extensions'
    @transaction_date=params[:transaction_date].present? ? Date.parse(params[:transaction_date]) : Date.today_with_timezone
    if params[:date].present?
      @student = Student.find(params[:id])
      @date = @fee_collection = FinanceFeeCollection.find(params[:date], 
        :include => [:fee_category, 
          {:finance_fee_particulars => :particular_payments}, :fee_discounts])
      @financefee =FinanceFee.find_by_fee_collection_id_and_student_id(@date.id, @student.id, 
        :include => [:finance_transactions, 
          {:particular_payments => [:particular_discounts, :finance_fee_particular]}])
      @collection_wise_paid = @financefee.finance_transactions.map(&:trans_type).include?("collection_wise")
      if @financefee.tax_enabled?
        @error = true
        flash.now[:notice]="#{t('particular_wise_payment_disabled')}"
      elsif @collection_wise_paid
        @error = true
        flash.now[:notice]="#{t('collection_wise_paid_fee_payment_disabled')}"        
      else
        @due_date = @fee_collection.due_date
        @fee_category = @fee_collection.fee_category
        @transaction_category_id=FinanceTransactionCategory.find_by_name("Fee").id
        particular_and_discount_details
        bal=(@total_payable-@total_discount).to_f
        @transaction_date = request.post? ? Date.today_with_timezone : @transaction_date
        days=(@transaction_date-@date.due_date.to_date).to_i
        auto_fine=@date.fine
        if days > 0 and auto_fine
          @fine_rule=auto_fine.fine_rules.find(:last, 
            :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], 
            :order => 'fine_days ASC')
          @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule
        end
        @paid_fine=@fine_amount
        @fine_amount=0 if @financefee.is_paid_with_fine?
        if request.post?
          FinanceTransaction.transaction do          
            transaction=FinanceTransaction.new(params[:fees])
            #        check_amount=transaction.amount.to_f-transaction.fine_amount.to_f
            #        transaction.pay_all_fees = false
            if transaction.save
              finance_transaction_hsh={"finance_transaction_id" => transaction.id}
              if params[:particular_payment].present?
                params[:particular_payment][:particular_payments_attributes].values.each { |hsh| hsh.merge!(finance_transaction_hsh) }
                @financefee.update_attributes(params[:particular_payment])
              end
              flash[:notice] = "#{t('finance.flash14')}.  <a href ='#' onclick='show_print_dialog(#{transaction.id})'>#{t('print_receipt')}</a>"
              # flash[:notice]="#{t('finance.flash14')}. <a href ='http://#{request.host_with_port}/finance/generate_fee_receipt_pdf?particular_wise=true&transaction_id=#{transaction.id}' target='_blank'>#{t('print_receipt')}</a>"
              @error=false
            else
              flash[:notice]="#{t('fee_payment_failed')}"
            end
          end
          @transaction_date = Date.today_with_timezone
        end
        @financefee.reload
        @paid_fees=@financefee.finance_transactions.all( :include => [:transaction_ledger, 
            {:particular_payments => [:finance_fee_particular, :particular_discounts]}])
        paid_fine=@paid_fees.select do |fine_transaction| 
          fine_transaction.description=='fine_amount_included' 
        end.sum(&:fine_amount).to_f
        @fine_amount=@fine_amount.to_f-paid_fine
        @fine_amount=0 if @financefee.is_paid
        # @fine_amount=@fine_amount
        # @fine_amount=nil unless @financefee.balance <= @fine_amount
        @applied_discount=FedenaPrecision.set_and_modify_precision(
          ParticularDiscount.find(:all, 
            :joins => [{:particular_payment => :finance_fee}], 
            :conditions => "finance_fees.id=#{@financefee.id}").sum(&:discount)).to_f
      end      
    else
      @error=true
    end
  end

  
  def particular_wise_fee_pay_pdf
    @fine_amount=params[:fine_amount]
    @paid_fine=@fine_amount
    @student = Student.find(params[:id])
    @date = @fee_collection = FinanceFeeCollection.find(params[:date], 
      :include => [:fee_category, {:finance_fee_particulars => :particular_payments}, :fee_discounts])
    @financefee =FinanceFee.find_by_fee_collection_id_and_student_id(@date.id, @student.id, 
      :include => [:finance_transactions, {:particular_payments => [:particular_discounts, :finance_fee_particular]}])
    @due_date = @fee_collection.due_date
    @fee_category = @fee_collection.fee_category
    @transaction_category_id=FinanceTransactionCategory.find_by_name("Fee").id
    particular_and_discount_details
    @paid_fees=@financefee.finance_transactions.all(
      :include => [:transaction_ledger, {:particular_payments => [:finance_fee_particular, :particular_discounts]}])
    @applied_discount=ParticularDiscount.find(:all, 
      :joins => [{:particular_payment => :finance_fee}], 
      :conditions => "finance_fees.id=#{@financefee.id}").sum(&:discount).to_f
    @fine_amount=0 if @financefee.is_paid
    @transaction_date = params[:transaction_date].present? ? Date.parse(params[:transaction_date]) : Date.today_with_timezone
    days=(@transaction_date-@date.due_date.to_date).to_i
    auto_fine=@date.fine
    if days > 0 and auto_fine
      @fine_rule=auto_fine.fine_rules.find(:last, 
        :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], 
        :order => 'fine_days ASC')
    end
    render :pdf => 'particular_wise_fee_pay_pdf'
  end

  def fetch_all_fees
    fee_transaction_category_id=FinanceTransactionCategory.find_by_name("Fee").id
    hostel_transaction_category_id=FinanceTransactionCategory.find_by_name("Hostel").try(:id)
    transport_transaction_category_id=FinanceTransactionCategory.find_by_name("Transport").try(:id)
    precision_count = FedenaPrecision.get_precision_count
    master_fees_sql = <<-SQL
      SELECT distinct finance_fee_collections.name AS collection_name,
                  #{fee_transaction_category_id} as transaction_category_id,
                  finance_fees.is_paid,
                  finance_fees.balance,
                  finance_fees.id AS id,
                  'FinanceFee' as fee_type,
                  (IFNULL((particular_total - discount_amount),
                              finance_fees.balance + 
                              (SELECT IFNULL(SUM(finance_transactions.amount - 
                                                               finance_transactions.fine_amount),
                                                       0)
                                  FROM finance_transactions
                                WHERE finance_transactions.finance_id=finance_fees.id AND 
                                            finance_transactions.finance_type='FinanceFee'
                              ) - 
                              IF(finance_fees.tax_enabled,finance_fees.tax_amount,0)
                    ) 
                  ) AS actual_amount,
                  (SELECT IFNULL(SUM(finance_transactions.fine_amount),0)
                      FROM finance_transactions
                    WHERE finance_transactions.finance_id=finance_fees.id AND 
                                finance_transactions.finance_type='FinanceFee' AND 
                                description= 'fine_amount_included'
                  ) AS paid_fine,
                  fine_rules.fine_amount AS fine_amount,
                  IF(finance_fees.tax_enabled,
                      (SELECT SUM(ROUND(tax_amount,#{precision_count}))
                          FROM tax_collections tc
                        WHERE tc.taxable_fee_type = 'FinanceFee' AND
                                    tc.taxable_fee_id = finance_fees.id),
                     '-'
                  ) AS tax_amount,
                  finance_fees.tax_enabled,
                  fine_rules.is_amount
       FROM `finance_fees`
       INNER JOIN `finance_fee_collections` ON `finance_fee_collections`.id = `finance_fees`.fee_collection_id
       INNER JOIN `fee_collection_batches` ON fee_collection_batches.finance_fee_collection_id = finance_fee_collections.id
       INNER JOIN `collection_particulars` ON (`finance_fee_collections`.`id` = `collection_particulars`.`finance_fee_collection_id`)
       INNER JOIN `finance_fee_particulars` ON (`finance_fee_particulars`.`id` = `collection_particulars`.`finance_fee_particular_id`)
         LEFT JOIN `finance_transactions` ON (`finance_transactions`.`finance_id` = `finance_fees`.`id`)
         LEFT JOIN `fines` ON `fines`.id = `finance_fee_collections`.fine_id AND fines.is_deleted is false
         LEFT JOIN `fine_rules` 
                  ON  fine_rules.fine_id = fines.id  AND 
                        fine_rules.id= (
                           SELECT id 
                              FROM fine_rules ffr 
                            WHERE ffr.fine_id=fines.id AND 
                                        ffr.created_at <= finance_fee_collections.created_at AND 
                                        ffr.fine_days <= DATEDIFF(
                                                                       COALESCE(Date('#{@transaction_date}'),CURDATE()),
                                                                  finance_fee_collections.due_date) 
                             ORDER BY ffr.fine_days DESC LIMIT 1
                         )
    SQL
    (FedenaPlugin.can_access_plugin?("fedena_hostel")) ? hostel_fees_sql="UNION ALL 
                                               (SELECT fc.name as collection_name,
                                                          #{hostel_transaction_category_id} as transaction_category_id,
                                                          if(hf.balance > 0,false,true) is_paid,
                                                          hf.balance balance,
                                                          hf.id as id,
                                                          'HostelFee' as fee_type,
                                                          hf.rent actual_amount,
                                                          0 as paid_fine,
                                                          0 fine_amount,
                                                          IF(hf.tax_enabled,hf.tax_amount,'-') AS tax_amount, 
                                                          hf.tax_enabled, 
                                                          0 is_amount
                                                FROM `hostel_fees` hf 
                                                INNER JOIN `hostel_fee_collections` fc 
                                                           ON `fc`.id = `hf`.hostel_fee_collection_id and 
                                                                  fc.is_deleted=0 and 
                                                                  hf.student_id='#{@student.id}' and 
                                                                  hf.batch_id=#{@current_batch.id} and 
                                                                  hf.balance > 0 and 
                                                                  is_active is true)" : hostel_fees_sql=''
    (FedenaPlugin.can_access_plugin?("fedena_transport")) ? transport_fees_sql="UNION ALL 
                                               (SELECT tc.name as collection_name,
                                                          #{transport_transaction_category_id} as transaction_category_id,
                                                          if(tf.balance > 0,false,true) is_paid,
                                                          tf.balance balance,
                                                          tf.id as id,
                                                          'TransportFee' as fee_type,
                                                          tf.bus_fare actual_amount,
                                                          0 as paid_fine,
                                                          0 fine_amount,
                                                          IF(tf.tax_enabled,tf.tax_amount,'-') AS tax_amount, 
                                                          tf.tax_enabled, 
                                                          0 is_amount 
                                                FROM `transport_fees` tf 
                                                INNER JOIN `transport_fee_collections` tc 
                                                          ON `tc`.id = `tf`.transport_fee_collection_id and 
                                                                tc.is_deleted=0 and 
                                                                tf.receiver_id='#{@student.id}' and 
                                                                tf.groupable_type='Batch' and 
                                                                tf.groupable_id=#{@current_batch.id} and 
                                                                tf.receiver_type='Student' and tf.balance > 0 and 
                                                                is_active is true)" : transport_fees_sql=''
    @finance_fees=FinanceFee.find_by_sql(<<-SQL


#{master_fees_sql} WHERE
      (
      finance_fees.student_id=#{@student.id} and
        finance_fees.is_paid=false and
        finance_fees.batch_id=#{@current_batch.id} and
        finance_fee_collections.is_deleted=0 and
        (
          (
            finance_fee_particulars.receiver_type='Batch' and
            finance_fee_particulars.receiver_id=finance_fees.batch_id
          ) or
          (
            finance_fee_particulars.receiver_type='Student' and
            finance_fee_particulars.receiver_id=finance_fees.student_id
          ) or
          (
            finance_fee_particulars.receiver_type='StudentCategory' and
            finance_fee_particulars.receiver_id=finance_fees.student_category_id
          )
        )
      ) GROUP BY finance_fees.id  #{transport_fees_sql}  #{hostel_fees_sql}
      SQL
    )    
    @disabled_fee_ids = FinanceFee.find_all_by_id(@finance_fees.map(&:id), 
      :joins => "INNER JOIN finance_transactions fts 
                         ON fts.trans_type = 'particular_wise' AND 
                            fts.finance_type = 'FinanceFee' AND 
                            fts.finance_id = finance_fees.id" ).map(&:id)    
  end

  def pay_all_fees
    @student=Student.find(params[:id])
    @transaction_date=params[:transaction_date].present? ? Date.parse(params[:transaction_date]) : 
      Date.today_with_timezone.to_date
    @current_batch= params[:batch_id].present? ? Batch.find(params[:batch_id]) : @student.batch
    @all_batches= (@student.previous_batches+[@student.batch]).uniq
    fetch_all_fees
    @finance_fees = @finance_fees.reject do |finance_fee| 
      (finance_fee.is_paid? ? 0 : 
          (finance_fee.is_amount? ? finance_fee.fine_amount : 
            (finance_fee.actual_amount.to_f)*(finance_fee.fine_amount.to_f/100))).to_f - 
        (finance_fee.paid_fine.to_f) < 0.0
    end
    #To remove paid fee from finance fee list
    @finance_fees = @finance_fees.reject  do |finance_fee|
      ( finance_fee.is_paid? or (finance_fee.balance.to_f + (finance_fee.is_paid? ? 0 : 
              (finance_fee.is_amount? ? finance_fee.fine_amount : 
                (finance_fee.actual_amount.to_f)*(finance_fee.fine_amount.to_f/100))).to_f - 
            (finance_fee.paid_fine.to_f) == 0.0))
    end
    @is_tax_present = @finance_fees.map(&:tax_enabled).include?(true)
    #    @tax_config = Configuration.get_multiple_configs_as_hash(['FinanceTaxIdentificationLabel',
    #        'FinanceTaxIdentificationNumber']) if @is_tax_present
    # validate if particular wise fee attempted to pay    
    particular_paid = false 
    params[:transactions].each do |k,v|
      particular_paid = true if @disabled_fee_ids.include?(v[:finance_id].try(:to_i)) and v[:finance_type] == 'FinanceFee'      
      break if particular_paid
    end if request.post?
    if request.post?
      FinanceTransactionLedger.transaction do        
        if !particular_paid
          status=true
          transaction_ledger = FinanceTransactionLedger.safely_create(params[:multi_fees_transaction].
              merge({:transaction_type => 'MULTIPLE', :category_is_income => true}))        
          finance_transactions = []
          FinanceTransaction.send_sms=false
          params[:transactions].each do |k, v|

            begin
              retries ||= 0
              status = true
              v.merge!({:transaction_type => transaction_ledger.transaction_type, 
                  :transaction_mode => transaction_ledger.transaction_mode, 
                  :transaction_ledger_id => transaction_ledger.id})
              finance_transaction = FinanceTransaction.new(v)              
              finance_transaction.save!
              finance_transactions << finance_transaction
            rescue ActiveRecord::StatementInvalid => er            
              # run code again  to  avoid duplications
              puts er.inspect
              status = false
              retry if (retries += 1) < 2
              # to avoid loop
            rescue Exception => e
              puts e.inspect
              status=false
            end
          end        
          FinanceTransaction.send_sms=true
          if status and (transaction_ledger.valid?)
            tids = transaction_ledger.finance_transactions.collect(&:id)
            trans_code=[]
            tids.each do |tid|
              trans_code << "transaction_id%5B%5D=#{tid}"
            end
            # send sms for a payall transaction
            transaction_ledger.send_sms
            trans_code=trans_code.join('&')
            flash[:notice] = "#{t('finance.flash14')}.  <a href ='#' onclick='show_print_dialog(#{tids.to_json})'>#{t('print_receipt')}</a>"
            # flash[:notice]="#{t('fees_paid')}. <a href ='http://#{request.host_with_port}/finance/generate_fee_receipt_pdf?#{trans_code}' target='_blank'>#{t('print_receipt')}</a>"
          else
            flash[:notice]="#{t('fee_payment_failed')}"
            raise ActiveRecord::Rollback
          end
        else        
          flash[:notice]="#{t('fee_payment_failed')}"
          raise ActiveRecord::Rollback
        end
      end
      redirect_to :controller => 'finance_extensions', :action => 'pay_all_fees', :batch_id => @current_batch.id
    end
    get_paid_fees
  end

  def paginate_paid_fees
    @student=Student.find params[:id]
    @current_batch= params[:batch_id].present? ? Batch.find(params[:batch_id]) : @student.batch
    get_paid_fees
    render :update do |page|
      page.replace_html "pay_fees1", :partial => 'recently_paid_fees'
    end
  end

  def delete_multi_fees_transaction
    @student=Student.find(params[:id])
    @current_batch= params[:batch_id].present? ? Batch.find(params[:batch_id]) : @student.batch
    @transaction_category_id=FinanceTransactionCategory.find_by_name("Fee").id
    @transaction_date=Date.today_with_timezone
    if params[:type]=='multi_fees_transaction'
      ftl=FinanceTransactionLedger.find(params[:transaction_id],:include => :finance_transactions)
      ftl.mark_cancelled(params[:reason])
    else
      ActiveRecord::Base.transaction do
        ft= FinanceTransaction.find(params[:transaction_id])
        ft.cancel_reason = params[:reason]
        if FedenaPlugin.can_access_plugin?("fedena_pay")
          finance_payment = ft.finance_payment
          unless finance_payment.nil?
            status = Payment.payment_status_mapping[:reverted]
            finance_payment.payment.update_attributes(:status_description => status)
          end
        end
        raise ActiveRecord::Rollback unless ft.destroy
      end
    end
    unless params[:si_no].to_i==1
      if params[:si_no].to_i%10==1
        params[:page]=(params[:page].to_i)-1
      end
    end
    get_paid_fees
    fetch_all_fees
    @is_tax_present = @finance_fees.map(&:tax_enabled).include?(true)
    #    @tax_config = Configuration.get_multiple_configs_as_hash(['FinanceTaxIdentificationLabel',
    #        'FinanceTaxIdentificationNumber']) if @is_tax_present
    render :update do |page|
      flash[:notice]="#{t('finance.flash18')}"
      page.replace_html "pay_fees", :partial => 'pay_fees_form'
    end
  end

  def pay_all_fees_receipt_pdf
    @student=Student.find(params[:id])
    @current_batch= Batch.find(params[:batch_id])
    fetch_all_fees    
    fee_types = "'FinanceFee'"
    #    fee_types = ["FinanceFee"]#, "HostelFee", "TransportFee"]    
    fee_types += ", 'HostelFee'" if FedenaPlugin.can_access_plugin?("fedena_hostel")
    fee_types += ", 'TransportFee'" if FedenaPlugin.can_access_plugin?("fedena_transport")    
    @is_tax_present = @finance_fees.map(&:tax_enabled).include?(true)
    @tax_config = Configuration.get_multiple_configs_as_hash(['FinanceTaxIdentificationLabel',
        'FinanceTaxIdentificationNumber']) if @is_tax_present    
    @paid_fees = @student.finance_transaction_ledgers.active_transactions.all(
      :select => "DISTINCT finance_transaction_ledgers.*", :joins => :finance_transactions,
      :conditions => "finance_transactions.finance_type in (#{fee_types})", 
      :include => :finance_transactions)
    @transactions = Hash.new
    @paid_fees.each {|ledger| @transactions[ledger.id] = ledger.
        finance_transactions.select {|x| fee_types.include?(x.finance_type)} }
    #    @paid_fees=@student.multi_fees_transactions
    #    @other_transactions=FinanceTransaction.find(:all, :select => "distinct finance_transactions.*", 
    #      :joins => "LEFT OUTER JOIN `multi_fees_transactions_finance_transactions` ON `multi_fees_transactions_finance_transactions`.finance_transaction_id = `finance_transactions`.id 
    #                      LEFT OUTER JOIN `multi_fees_transactions` ON `multi_fees_transactions`.id = `multi_fees_transactions_finance_transactions`.multi_fees_transaction_id", 
    #      :conditions => "payee_id='#{@student.id}' and multi_fees_transactions.id is NULL and finance_type in (#{fees})")
    
    render :pdf => 'pay_all_fees_receipt_pdf', 
      :margin => { :left => 15, :right => 15}, 
      :show_as_html => params.key?(:debug)
  end

  def discount_particular_allocation
    @batches=Batch.active
    @dates=[]
  end

  def particulars_with_tabs
    if params[:collection_id].present?
      @finance_fee_collection=FinanceFeeCollection.find(params[:collection_id])
      finance_fee_category=@finance_fee_collection.fee_category
      paid_fees=FinanceFee.find(:all, :joins => :finance_transactions, 
        :conditions => "fee_collection_id='#{@finance_fee_collection.id}'")
      # paid_fees=finance_fee_collection.finance_fees.all(:conditions => "is_paid=true")
      paid_student_ids=(paid_fees.collect(&:student_id)<<0).compact.uniq
      paid_student_category_ids=(paid_fees.collect(&:student_category_id)<<0).compact.uniq
      #      @particulars=FinanceFeeParticular.find(:all, :select => "finance_fee_particulars.*,IF(batches.id is null,IF(student_categories.id is NULL,concat(students.first_name,\" (\",students.admission_no,\" )\"),student_categories.name),'') as receiver_name,if(#{paid_fees.present? and params[:type]=='Batch'},true,if(#{paid_fees.present? and params[:type]=='Student'} and finance_fee_particulars.receiver_id in (#{paid_student_ids.join(',')}),true,if(#{paid_fees.present? and params[:type]=='StudentCategory'} and finance_fee_particulars.receiver_id in (#{paid_student_category_ids.join(',')}),true,false))) as disabled", :joins => "LEFT JOIN batches on batches.id=finance_fee_particulars.receiver_id and finance_fee_particulars.receiver_type='Batch' LEFT JOIN students on students.id=finance_fee_particulars.receiver_id and finance_fee_particulars.receiver_type='Student' LEFT JOIN student_categories on student_categories.id=finance_fee_particulars.receiver_id and finance_fee_particulars.receiver_type='StudentCategory'", :conditions => "finance_fee_particulars.is_instant=false and finance_fee_particulars.batch_id='#{params[:batch_id]}' and finance_fee_category_id='#{finance_fee_category.id}' and finance_fee_particulars.receiver_type='#{params[:type]}'")
      if @finance_fee_collection.tax_enabled?
        particular_joins = " LEFT JOIN collectible_tax_slabs cts
                                                ON cts.collectible_entity_id = finance_fee_particulars.id AND
                                                      cts.collectible_entity_type = 'FinanceFeeParticular' AND
                                                      cts.collection_id = #{@finance_fee_collection.id} AND
                                                      cts.collection_type = 'FinanceFeeCollection'
                                      LEFT JOIN tax_slabs ts ON ts.id = cts.tax_slab_id"
        particular_include_associations = [:tax_slabs,:particular_wise_discounts]
        particular_tax_select = ",IFNULL(ts.name,'-') as slab_name"
      else
        particular_joins = ""
        particular_include_associations = [:particular_wise_discounts]
        particular_tax_select = ""
      end
      @particulars=FinanceFeeParticular.find(:all, 
        :select => "DISTINCT finance_fee_particulars.*,
                           IF(batches.id is null,
                               IF(student_categories.id is NULL,
                                   concat(students.first_name,\" (\",
                                               students.admission_no,\" )\"),
                                               student_categories.name
                                ),''
                            ) as receiver_name,
                            IF(#{paid_fees.present? and params[:type]=='Batch'},
                               true,
                               IF(#{paid_fees.present? and params[:type]=='Student'} and 
                                   finance_fee_particulars.receiver_id in (#{paid_student_ids.join(',')}),
                                   true,
                                   IF(#{paid_fees.present? and params[:type]=='StudentCategory'} and 
                                       finance_fee_particulars.receiver_id in (#{paid_student_category_ids.join(',')}),
                                       true,false
                                    )
                                )
                            ) as disabled#{particular_tax_select}",
        :include => particular_include_associations,
        :joins => "LEFT JOIN batches 
                                  ON batches.id=finance_fee_particulars.receiver_id AND 
                                        finance_fee_particulars.receiver_type='Batch' 
                         LEFT JOIN students 
                                   ON students.id=finance_fee_particulars.receiver_id AND 
                                         finance_fee_particulars.receiver_type='Student' 
                         LEFT JOIN student_categories 
                                  ON student_categories.id=finance_fee_particulars.receiver_id AND 
                                        finance_fee_particulars.receiver_type='StudentCategory'
                         #{particular_joins}", 
        :conditions => "finance_fee_particulars.is_instant=false and 
                          finance_fee_particulars.batch_id='#{params[:batch_id]}' and 
                          finance_fee_category_id='#{finance_fee_category.id}' and 
                          finance_fee_particulars.receiver_type='#{params[:type]}'")
      @collection_particular_ids=@finance_fee_collection.collection_particulars.collect(&:finance_fee_particular_id)
      @partial='particulars'
      @particular_details=FinanceFeeParticular.find(:all, 
        :select => "count(distinct finance_fee_particulars.id) as total,
                           count(IF(finance_fee_particulars.receiver_type='Student',1,NULL)) as student_wise,
                           count(IF(finance_fee_particulars.receiver_type='StudentCategory',1,NULL)) as category_wise,
                           count(IF(finance_fee_particulars.receiver_type='Batch',1,NULL)) as batch_wise", 
        :joins => :collection_particulars,
        :conditions => "finance_fee_particulars.is_instant=false and 
                                 collection_particulars.finance_fee_collection_id='#{@finance_fee_collection.id}' and 
                                 finance_fee_particulars.batch_id='#{params[:batch_id]}'").first
      render :update do |page|
        page.replace_html "receivers", :partial => 'batches_and_fee_collections'
        page.replace_html "particular-wise-discount", :text => ""

      end
    else
      render :update do |page|
        page.hide "loader_collection"
        page.replace_html "receivers", :text => ''
        page.replace_html "particular-wise-discount", :text => ""

      end
    end
  end

  def show_discounts
    @finance_fee_collection=FinanceFeeCollection.find(params[:collection_id])
    finance_fee_category=@finance_fee_collection.fee_category
    paid_fees=FinanceFee.find(:all, :joins => :finance_transactions, :conditions => "fee_collection_id='#{@finance_fee_collection.id}'")
    # paid_fees=@finance_fee_collection.finance_fees.all(:conditions => "is_paid=true")
    paid_student_ids=(paid_fees.collect(&:student_id)<<0).compact
    paid_student_category_ids=(paid_fees.collect(&:student_category_id)<<0).compact
    @discounts=FeeDiscount.find(:all, :select => "fee_discounts.*,IF(batches.id is null,IF(student_categories.id is NULL,IF(finance_fee_particulars.id is NULL,IF(students.id is NULL,concat(archived_students.first_name,\" (\",archived_students.admission_no,\" )\"),concat(students.first_name,\" (\",students.admission_no,\" )\")),finance_fee_particulars.name),student_categories.name),'') as receiver_name,if(#{paid_fees.present? } and fee_discounts.receiver_type='Batch',true,if(#{paid_fees.present? } and fee_discounts.receiver_type='Student' and fee_discounts.receiver_id in (#{paid_student_ids.join(',')}),true,if(#{paid_fees.present?} and fee_discounts.receiver_type='StudentCategory' and fee_discounts.receiver_id in (#{paid_student_category_ids.join(',')}),true,false))) as disabled", :joins => "LEFT JOIN batches on batches.id=fee_discounts.master_receiver_id and fee_discounts.master_receiver_type='Batch' LEFT JOIN archived_students on archived_students.former_id=fee_discounts.master_receiver_id and fee_discounts.master_receiver_type='Student' LEFT JOIN students on students.id=fee_discounts.master_receiver_id and fee_discounts.master_receiver_type='Student' LEFT JOIN student_categories on student_categories.id=fee_discounts.master_receiver_id and fee_discounts.master_receiver_type='StudentCategory' LEFT JOIN finance_fee_particulars on finance_fee_particulars.id=fee_discounts.master_receiver_id and fee_discounts.master_receiver_type='FinanceFeeParticular'", :conditions => "fee_discounts.is_instant=false and fee_discounts.batch_id='#{params[:batch_id]}' and fee_discounts.finance_fee_category_id='#{finance_fee_category.id}' and fee_discounts.master_receiver_type='#{params[:type]}'")
    # @discounts=finance_fee_category.fee_discounts.all(:conditions => {:batch_id => params[:batch_id], :receiver_type => params[:type]})
    @collection_discount_ids=@finance_fee_collection.collection_discounts.collect(&:fee_discount_id)
    @discount_details=FeeDiscount.find(:all, :select => "count(distinct fee_discounts.id) as total,count(IF(fee_discounts.master_receiver_type='Student',1,NULL)) as student_wise,count(IF(fee_discounts.master_receiver_type='StudentCategory',1,NULL)) as category_wise,count(IF(fee_discounts.master_receiver_type='Batch',1,NULL)) as batch_wise,count(IF(fee_discounts.master_receiver_type='FinanceFeeParticular',1,NULL)) as particular_wise", :joins => :collection_discounts, :conditions => "fee_discounts.is_instant=false and collection_discounts.finance_fee_collection_id='#{@finance_fee_collection.id}' and fee_discounts.batch_id='#{params[:batch_id]}'").first
    render :update do |page|
      page.replace_html "particular-wise-discount", :text => "#{link_to_function t('particular')+'-'+t('wise'), 'select_tab(this);' }"
      page.replace_html "right-panel", :partial => 'discounts'
    end

  end

  def show_particulars
    @finance_fee_collection=FinanceFeeCollection.find(params[:collection_id])
    if @finance_fee_collection.tax_enabled?
      particular_joins = " LEFT JOIN collectible_tax_slabs cts
                                                ON cts.collectible_entity_id = finance_fee_particulars.id AND
                                                      cts.collectible_entity_type = 'FinanceFeeParticular' AND
                                                      cts.collection_id = #{@finance_fee_collection.id} AND
                                                      cts.collection_type = 'FinanceFeeCollection'
                                      LEFT JOIN tax_slabs ts ON ts.id = cts.tax_slab_id"
      particular_include_associations = [:tax_slabs, :particular_wise_discounts]
      particular_tax_select = ",IFNULL(ts.name,'-') as slab_name"
    else
      particular_joins = ""
      particular_include_associations = [:particular_wise_discounts]
      particular_tax_select = ""
    end
    finance_fee_category=@finance_fee_collection.fee_category
    paid_fees=FinanceFee.find(:all, :joins => :finance_transactions, :conditions => "fee_collection_id='#{@finance_fee_collection.id}'")
    paid_student_ids=(paid_fees.collect(&:student_id)<<0).compact.uniq
    paid_student_category_ids=(paid_fees.collect(&:student_category_id)<<0).compact.uniq
    @particulars=FinanceFeeParticular.find(:all, 
      :select => "DISTINCT finance_fee_particulars.*,
                         IF(batches.id is NULL,
                             IF(student_categories.id IS NULL,
                                 CONCAT(students.first_name,\" (\",students.admission_no,\" )\"),
                                               student_categories.name),'') AS receiver_name,
                         IF(#{paid_fees.present? and params[:type]=='Batch'},
                             true,IF(#{paid_fees.present? and params[:type]=='Student'} and 
                                        finance_fee_particulars.receiver_id IN (#{paid_student_ids.join(',')}),
                                        true,
                                        IF(#{paid_fees.present? and params[:type]=='StudentCategory'} and 
                                            finance_fee_particulars.receiver_id IN (#{paid_student_category_ids.join(',')}),
                                            true,false))) AS disabled#{particular_tax_select}", 
      :include => particular_include_associations,
      :joins => "LEFT JOIN batches 
                                ON batches.id=finance_fee_particulars.receiver_id AND 
                                      finance_fee_particulars.receiver_type='Batch' 
                       LEFT JOIN students 
                                 ON students.id=finance_fee_particulars.receiver_id AND 
                                       finance_fee_particulars.receiver_type='Student' 
                       LEFT JOIN student_categories 
                                 ON student_categories.id=finance_fee_particulars.receiver_id AND 
                                       finance_fee_particulars.receiver_type='StudentCategory'
                       #{particular_joins}", 
      :conditions => "finance_fee_particulars.is_instant=false AND 
                               finance_fee_particulars.batch_id='#{params[:batch_id]}' AND 
                               finance_fee_category_id='#{finance_fee_category.id}' AND 
                               finance_fee_particulars.receiver_type='#{params[:type]}'")
    # @particulars=FinanceFeeParticular.find(:all,:select=>"finance_fee_particulars.*,IF(batches.id is null,IF(student_categories.id is NULL,concat(students.first_name,\" (\",students.admission_no,\" )\"),student_categories.name),'') as receiver_name",:joins=>"LEFT JOIN batches on batches.id=finance_fee_particulars.receiver_id and finance_fee_particulars.receiver_type='Batch' LEFT JOIN students on students.id=finance_fee_particulars.receiver_id and finance_fee_particulars.receiver_type='Student' LEFT JOIN student_categories on student_categories.id=finance_fee_particulars.receiver_id and finance_fee_particulars.receiver_type='StudentCategory'",:conditions=>"finance_fee_particulars.batch_id='#{params[:batch_id]}' and finance_fee_category_id='#{finance_fee_category.id}' and finance_fee_particulars.receiver_type='#{params[:type]}'")
    # @particulars=finance_fee_category.fee_particulars.all(:conditions => {:batch_id => params[:batch_id], :receiver_type => params[:type]})
    @collection_particular_ids=@finance_fee_collection.collection_particulars.
      collect(&:finance_fee_particular_id)
    @particular_details=FinanceFeeParticular.find(:all, 
      :select => "count(distinct finance_fee_particulars.id) as total,
                         count(IF(finance_fee_particulars.receiver_type='Student',1,NULL)) as student_wise,
                         count(IF(finance_fee_particulars.receiver_type='StudentCategory',1,NULL)) as category_wise,
                         count(IF(finance_fee_particulars.receiver_type='Batch',1,NULL)) as batch_wise", 
      :joins => :collection_particulars, 
      :conditions => "finance_fee_particulars.is_instant=false and 
                               collection_particulars.finance_fee_collection_id='#{@finance_fee_collection.id}' and 
                               finance_fee_particulars.batch_id='#{params[:batch_id]}' ").first
    render :update do |page|
      page.replace_html "right-panel", :partial => 'particulars'
      page.replace_html "particular-wise-discount", :text => ''
    end
  end


  def fee_collections_for_batch
    @batch=Batch.find(params[:batch_id])
    @dates=@batch.finance_fee_collections
    render :update do |page|
      page.replace_html "fee_collections", :partial => 'fee_collections'
    end
  end

  def update_collection_discount
    flash_message=''
    finance_fee_collection=FinanceFeeCollection.find(params[:fees_list][:collection_id])
    finance_fee_category=finance_fee_collection.fee_category
    paid_fees=FinanceFee.find(:all, :joins => :finance_transactions, :conditions => "fee_collection_id='#{finance_fee_collection.id}'")
    paid_student_ids= (paid_fees.collect(&:student_id)<<0).compact.uniq
    paid_student_category_ids = (paid_fees.collect(&:student_category_id)<<0).compact.uniq
    collection_discount_ids = finance_fee_collection.collection_discounts.collect(&:fee_discount_id)
    part_ids = finance_fee_collection.finance_fee_particulars.collect(&:id)
    exclude_particular_enabled_discounts = part_ids.present? ? 
      " or (fee_discounts.master_receiver_type='FinanceFeeParticular' and 
              fee_discounts.master_receiver_id NOT IN (#{part_ids.join(',')}))" : ""
    discounts=FeeDiscount.find(:all, 
      :select => "fee_discounts.*,
                         IF(#{paid_fees.present? } and 
                             fee_discounts.receiver_type='Batch',true,
                             IF(#{paid_fees.present? } and fee_discounts.receiver_type='Student' and 
                                 fee_discounts.receiver_id IN (#{paid_student_ids.join(',')}),true,
                                 IF(#{paid_fees.present?} and fee_discounts.receiver_type='StudentCategory' and 
                                     fee_discounts.receiver_id IN (#{paid_student_category_ids.join(',')}),true,false
                                 )
                             ) #{exclude_particular_enabled_discounts}
                         ) AS disabled",
      :joins => "LEFT JOIN batches 
                                ON batches.id=fee_discounts.master_receiver_id and 
                                      fee_discounts.master_receiver_type='Batch' 
                       LEFT JOIN students 
                                ON students.id=fee_discounts.master_receiver_id and 
                                      fee_discounts.master_receiver_type='Student' 
                       LEFT JOIN student_categories 
                                ON student_categories.id=fee_discounts.master_receiver_id and 
                                      fee_discounts.master_receiver_type='StudentCategory' 
                       LEFT JOIN finance_fee_particulars 
                                ON finance_fee_particulars.id=fee_discounts.master_receiver_id and 
                                      fee_discounts.master_receiver_type='FinanceFeeParticular'",
      :conditions => "fee_discounts.is_instant=false and 
                                fee_discounts.batch_id='#{params[:fees_list][:batch_id]}' and 
                                fee_discounts.finance_fee_category_id='#{finance_fee_category.id}' and 
                                fee_discounts.master_receiver_type='#{params[:fees_list][:type]}'")
    
    disabled_discounts=discounts.select { |d| d.disabled? }.collect(&:id)
    disabled_and_assigned=collection_discount_ids&disabled_discounts
    disabled_and_unassigned=disabled_discounts-collection_discount_ids


    existing_discounts=CollectionDiscount.find(:all, :select => "distinct collection_discounts.*",
      :joins => [:finance_fee_collection, :fee_discount],
      :include => :fee_discount,
      :conditions => "fee_discounts.is_instant=false and 
                                finance_fee_collections.id='#{params[:fees_list][:collection_id]}' and 
                                fee_discounts.batch_id='#{params[:fees_list][:batch_id]}' and 
                                fee_discounts.master_receiver_type='#{params[:fees_list][:type]}'")
    existing_discounts_ids=existing_discounts.collect(&:fee_discount_id)
    new_discount_ids=[]
    new_discount_ids=params[:fees_list][:discount_ids].map { |d| d.to_i } if params[:fees_list][:discount_ids].present?
    discounts_to_be_deleted=existing_discounts_ids-new_discount_ids
    discounts_to_be_added=new_discount_ids-existing_discounts_ids

    update_fee = false
    unless (discounts_to_be_deleted&disabled_and_assigned).present? or 
        (discounts_to_be_added&disabled_and_unassigned).present?

      ActiveRecord::Base.transaction do
        begin
          
          all_fees = FinanceFee.all(:conditions => {:fee_collection_id => finance_fee_collection.id, 
              :batch_id => params[:fees_list][:batch_id]})          
          all_fees_ids = all_fees.map(&:id)
          if existing_discounts.present?
            discount_to_unlink_ids = []
            existing_discounts.select { |ed| discounts_to_be_deleted.include? ed.fee_discount_id }.each do |cd|
              discount=cd.fee_discount
              cd.destroy
              discount_to_unlink_ids << discount.id
              update_fee = true
            end
            if discount_to_unlink_ids.present?
              ffd_delete_cond = finance_fee_collection.discount_mode == "OLD_DISCOUNT" ? "" : " and fee_discount_id in (#{discount_to_unlink_ids.join(',')})"
              FinanceFeeDiscount.delete_all("finance_fee_id in (#{all_fees_ids.join(',')}) #{ffd_delete_cond}")
            end
          end

          if discounts_to_be_added.present?
            discs_to_be_added = FeeDiscount.find_all_by_id(discounts_to_be_added)
            
            discs_to_be_added.each do |discount|
              CollectionDiscount.create(:finance_fee_collection_id => finance_fee_collection.id, 
                :fee_discount_id => discount.id)
              update_fee = true            
            end            
          end
          
          if update_fee
            all_fees.each do |fee|
              FinanceFeeParticular.add_or_remove_particular_update_discounts_and_taxes(nil, fee)              
            end            
          end
          
          flash_message="<p class='flash-msg'>#{t('discounts')} #{t('update').downcase} #{t('succesful')}</p>"
        rescue Exception => e          
          puts e.inspect
          flash_message="<p class='flash-msg'>#{t('flash_msg3')}</p>"
          a={"discount" => {"collection-"+finance_fee_collection.id.to_s => e.message}}
          File.open("#{RAILS_ROOT}/log/finance.yml", "a+") { |f| f.write a.to_yaml }
          raise ActiveRecord::Rollback
        end
      end
    else
      fee_part_ids=discounts.select { |fd| fd.master_receiver_type=='FinanceFeeParticular' }.collect(&:master_receiver_id)
      msg=''
      if paid_fees.present?
        msg="<li>
                      #{t('somebody_has_paid_for_the_collection_already')}. 
                      #{t('please_revert_transactions_and_try_again')}
                  </li>"
      end

      if (fee_part_ids.map { |f| f.to_i }-part_ids.map { |f| f.to_i }).present?
        msg+="<li>#{t('please_assign_associated_particular')}</li>"
      end
      flash_message="<div class='errorExplanation'><ul>#{msg}</ul></div>"

    end
    render :update do |page|
      page.replace_html "flash-div", :text => flash_message
    end
  end

  def this_fee_particulars(particulars, fee)
    particulars.select do |particular|
      (particular.is_instant && fee.student_id == particular.receiver_id && particular.receiver_type == "Student") or 
        ((!particular.is_instant) and 
          ((particular.receiver_type == "Batch" and particular.receiver_id == fee.batch_id) or 
            (particular.receiver_type == "StudentCategory" and particular.receiver_id == fee.student_category_id) or 
            (particular.receiver_type == "Student" and particular.receiver_id == fee.student_id)
        ))
    end    
  end
  
  def this_fee_discounts(discounts, fee)
    discounts.select do |discount|
      (discount.is_instant && fee.student_id == discount.master_receiver_id && discount.receiver_type == "Student") or 
        ((!discount.is_instant) and 
          ((discount.master_receiver_type == "Batch" and discount.master_receiver_id == fee.batch_id) or 
            (discount.master_receiver_type == "StudentCategory" and discount.master_receiver_id == fee.student_category_id) or 
            (discount.master_receiver_type == "Student" and discount.master_receiver_id == fee.student_id) or 
            (discount.master_receiver_type == "FinanceFeeParticular" and (
              (discount.receiver_type == "Batch" and fee.batch_id == discount.receiver_id) or
                (discount.receiver_type == "Student" and fee.student_id == discount.receiver_id) or
                (discount.receiver_type == "StudentCategory" and fee.student_category_id == discount.receiver_id)
            ))
        ))
    end
  end
  
  def update_collection_particular
    flash_message=''
    finance_fee_collection=FinanceFeeCollection.find(params[:fees_list][:collection_id])


    finance_fee_category=finance_fee_collection.fee_category
    paid_fees=FinanceFee.find(:all, :joins => :finance_transactions, 
      :conditions => "fee_collection_id='#{finance_fee_collection.id}'")
    paid_student_ids=(paid_fees.collect(&:student_id)<<0).compact
    paid_student_category_ids=(paid_fees.collect(&:student_category_id)<<0).compact
    collection_particular_ids=finance_fee_collection.collection_particulars.collect(&:finance_fee_particular_id)
    particulars=FinanceFeeParticular.find(:all, 
      :select => "finance_fee_particulars.*,if(#{paid_fees.present? and 
      params[:fees_list][:type]=='Batch'},true,
                         IF(#{paid_fees.present? and params[:fees_list][:type]=='Student'} and 
                             finance_fee_particulars.receiver_id IN (#{paid_student_ids.join(',')}),
                             true,
                             IF(#{paid_fees.present? and params[:fees_list][:type]=='StudentCategory'} and 
                                 finance_fee_particulars.receiver_id IN (#{paid_student_category_ids.join(',')}),
                                 true,false))) as disabled", 
      :joins => "LEFT JOIN batches 
                                ON batches.id=finance_fee_particulars.receiver_id and 
                                      finance_fee_particulars.receiver_type='Batch' 
                       LEFT JOIN students 
                                 ON students.id=finance_fee_particulars.receiver_id and 
                                       finance_fee_particulars.receiver_type='Student' 
                       LEFT JOIN student_categories 
                                 ON student_categories.id=finance_fee_particulars.receiver_id and 
                                       finance_fee_particulars.receiver_type='StudentCategory'", 
      :conditions => "finance_fee_particulars.is_instant=false and 
                                finance_fee_particulars.batch_id='#{params[:fees_list][:batch_id]}' and 
                                finance_fee_category_id='#{finance_fee_category.id}' and 
                                finance_fee_particulars.receiver_type='#{params[:fees_list][:type]}'")
    disabled_particulars=particulars.select { |d| d.disabled? }.collect(&:id)
    disabled_and_assigned=collection_particular_ids&disabled_particulars
    disabled_and_unassigned=disabled_particulars-collection_particular_ids

    existing_particulars=CollectionParticular.find(:all, :select => "distinct collection_particulars.*", 
      :joins => [:finance_fee_collection, :finance_fee_particular], 
      :include => :finance_fee_particular, 
      :conditions => "finance_fee_particulars.is_instant=false and 
                               finance_fee_collections.id='#{params[:fees_list][:collection_id]}' and 
                               finance_fee_particulars.batch_id='#{params[:fees_list][:batch_id]}' and 
                               finance_fee_particulars.receiver_type='#{params[:fees_list][:type]}'")
    existing_particulars_ids=existing_particulars.collect(&:finance_fee_particular_id)
    new_particular_ids=[]
    new_particular_ids=params[:fees_list][:particular_ids].map { |d| d.to_i } if params[:fees_list][:particular_ids].present?
    particulars_to_be_deleted = existing_particulars_ids-new_particular_ids
    particulars_to_be_added = new_particular_ids-existing_particulars_ids
    
    
    unless (particulars_to_be_deleted&disabled_and_assigned).present? or 
        (particulars_to_be_added&disabled_and_unassigned).present?

      ActiveRecord::Base.transaction do
        begin

          all_fees = FinanceFee.all(:conditions => {:fee_collection_id => finance_fee_collection.id, 
              :batch_id => params[:fees_list][:batch_id]})
          all_fees_ids = all_fees.map(&:id)
          if particulars_to_be_deleted.present?
            # unlink particulars from selected collection
            CollectionParticular.delete_all({:finance_fee_collection_id => finance_fee_collection.id, 
                :finance_fee_particular_id => particulars_to_be_deleted })            
            if all_fees_ids.present?
              # delete all particular level calculated discounts for selected collection
              FinanceFeeDiscount.delete_all("finance_fee_id in (#{all_fees_ids.join(',')}) AND 
                                                             finance_fee_particular_id in (#{particulars_to_be_deleted.join(',')})")
              
              # delete all particular level calculated tax (collections) for selected collection          
              TaxCollection.delete_all({:taxable_entity_id => particulars_to_be_deleted, :taxable_entity_type =>
                    'FinanceFeeParticular', :taxable_fee_id => all_fees_ids, :taxable_fee_type => "FinanceFee" 
                }) if finance_fee_collection.tax_enabled?              
            end
            unless particulars_to_be_added.present?              
              # update tax and discount wrt (instant) particular for other particulars in selected finance fee
              all_fees.each do |fee|
                FinanceFeeParticular.add_or_remove_particular_update_discounts_and_taxes(nil, fee)
              end
            end
          end
          
          p_inc_assoc = finance_fee_collection.tax_enabled? ? [:tax_slabs] : []
          parts_to_be_added = FinanceFeeParticular.find_all_by_id(particulars_to_be_added, 
            :include => p_inc_assoc)
          collection_discounts = finance_fee_collection.fee_discounts
          #          particulars_to_be_added.each do |particular_id|
          collection_discount_mode = all_fees[0].school_discount_mode if all_fees.present?
          parts_to_be_added.each do |particular|
            CollectionParticular.find_or_create_by_finance_fee_collection_id_and_finance_fee_particular_id(finance_fee_collection.id, 
              particular.id)
            slab_id = particular.tax_slabs.try(:last).try(:id) if finance_fee_collection.tax_enabled? && 
              particular.tax_slabs.present?
            if finance_fee_collection.tax_enabled? && particular.tax_slabs.present?
              particular.collectible_tax_slabs.create({ 
                  :tax_slab_id => slab_id,
                  :collection_id => finance_fee_collection.id, :collection_type => 'FinanceFeeCollection' 
                }) 
            end
          end
          if all_fees.present? 
            all_fees.each_with_index do |fee, fi|              
              particular_fee_discounts = []
              particular_tax_collections = []
              f_particulars = this_fee_particulars(parts_to_be_added, fee)
              f_particulars.each_with_index do |particular, i|                
            
                # create tax records for this fee, 
                slab_id = particular.tax_slabs.try(:last).try(:id) if finance_fee_collection.tax_enabled? && 
                  particular.tax_slabs.present?
                particular_fee_discount_hsh = { :finance_fee_particular_id => particular.id }
                particular_tax_collection_hsh = { :taxable_entity_id => particular.id,
                  :taxable_entity_type => "FinanceFeeParticular", :taxable_fee_type => "FinanceFee",
                  :tax_amount => 0, :slab_id => slab_id
                } if slab_id.present?
              
                if collection_discount_mode == "NEW"
                  p_discounts = this_fee_discounts(collection_discounts,fee)
                  p_discounts.each do |discount|
                    fp_amt = particular.amount.to_f
                    d_amount = discount.discount.to_f
                    if discount.is_amount?
                      if discount.master_receiver_type == "FinanceFeeParticular" and 
                          discount.master_receiver_id == particular.id
                        disc_amount = d_amount
                      else
                        ## TO DO :: add logic to adjust minor diff in last particular
                        disc_amount = 0 
                      end
                    else
                      disc_amount = (d_amount * fp_amt * 0.01)
                    end
                    particular_fee_discounts << particular_fee_discount_hsh.dup.merge({:fee_discount_id => 
                          discount.id, :finance_fee_id => fee.id,:discount_amount => disc_amount})
                  end
                end
              
                particular_fee_discounts << particular_fee_discount_hsh.dup.merge!({
                    :finance_fee_id => fee.id, :discount_amount => 0
                  }) if collection_discount_mode == "OLD"
                                            
                particular_tax_collections << particular_tax_collection_hsh.dup.merge({
                    :taxable_fee_id => fee.id })  if finance_fee_collection.tax_enabled? && 
                  particular.tax_slabs.present?
              
                if finance_fee_collection.tax_enabled? && particular.tax_slabs.present?                  
                  TaxCollection.create(particular_tax_collections) 
                end
                particular_tax_collections = []
              
              end
              
              FinanceFeeDiscount.create(particular_fee_discounts) if particular_fee_discounts.present?              
              FinanceFeeParticular.add_or_remove_particular_update_discounts_and_taxes(nil, fee)
            end
          end
          flash_message="<p class='flash-msg'>
                                        #{t('particulars')} #{t('update').downcase} #{t('succesful')}
                                     </p>"
        rescue Exception => e      
          puts e.inspect
          flash_message="<p class='flash-msg'>#{t('flash_msg3')} </p>"
          a={"particular" => {"collection-"+finance_fee_collection.id.to_s => e.message}}
          File.open("#{RAILS_ROOT}/log/finance.yml", "a+") { |f| f.write a.to_yaml }
          raise ActiveRecord::Rollback
        end
      end
    else
      flash_message="<div class='errorExplanation'>
                                    <ul>
                                         <li>
                                            #{t('somebody_has_paid_for_the_collection_already')}. 
                                            #{t('please_revert_transactions_and_try_again')}
                                         </li>
                                    </ul>
                                 </div>"
    end
    render :update do |page|
      page.replace_html "flash-div", :text => flash_message
    end
  end

  def add_or_remove_discount(discount, finance_fee_collection, batch_id, operation)

    receiver=discount.receiver_type.underscore+"_id"

    if discount.is_amount?
      FinanceFee.update_all(["finance_fees.is_paid=finance_fees.balance#{operation}#{discount.discount}<=0,finance_fees.balance=finance_fees.balance#{operation}#{discount.discount}"], 
        ["finance_fees.#{receiver}=#{discount.receiver_id} and finance_fees.batch_id='#{discount.batch_id}' and finance_fees.fee_collection_id='#{finance_fee_collection.id}'"])

    else
      if discount.master_receiver_type=='FinanceFeeParticular'
        particular=discount.master_receiver
        discount_amount=(particular.amount)*(discount.discount/100)
        sql="UPDATE finance_fees ff 
                       SET ff.balance=ff.balance#{operation}#{discount_amount} 
                  WHERE ff.fee_collection_id=#{finance_fee_collection.id} and 
                              ff.#{receiver}=#{discount.receiver_id} and ff.batch_id=#{discount.batch_id}"
      else
        sql="UPDATE finance_fees ff 
                       SET ff.balance=ff.balance#{operation}
                                                (SELECT SUM(finance_fee_particulars.amount)*(#{discount.discount/100}) 
                                                    FROM finance_fee_particulars 
                                            INNER JOIN collection_particulars 
                                                        ON collection_particulars.finance_fee_particular_id=finance_fee_particulars.id  
                                                  WHERE collection_particulars.finance_fee_collection_id=#{finance_fee_collection.id} AND 
                                                              finance_fee_particulars.finance_fee_category_id='#{finance_fee_collection.fee_category_id}' AND 
                                                              finance_fee_particulars.batch_id='#{batch_id}' and 
                                                              ((finance_fee_particulars.receiver_type='Student' and 
                                                                 finance_fee_particulars.receiver_id=ff.student_id) or 
                                                                (finance_fee_particulars.receiver_type='StudentCategory' and 
                                                                 finance_fee_particulars.receiver_id=ff.student_category_id) or 
                                                                (finance_fee_particulars.receiver_type='Batch' and 
                                                                 finance_fee_particulars.receiver_id=ff.batch_id)
                                                               )
                                                ) 
                  WHERE ff.fee_collection_id=#{finance_fee_collection.id} AND 
                              ff.#{receiver}=#{discount.receiver_id} AND ff.batch_id=#{discount.batch_id}"
      end

      sql1="UPDATE finance_fees ff SET ff.is_paid=(ff.balance<=0) where ff.fee_collection_id=#{finance_fee_collection.id} and ff.#{receiver}=#{discount.receiver_id} and ff.batch_id=#{discount.batch_id}"
      ActiveRecord::Base.connection.execute(sql)
      ActiveRecord::Base.connection.execute(sql1)

    end

  end


  def new_instant_particular
    @financefee=FinanceFee.find(params[:id])
    @target_action=params[:current_action]
    @target_controller=params[:current_controller]
    @finance_fee_category=@financefee.finance_fee_collection.fee_category
    @tax_slabs = TaxSlab.all if @financefee.tax_enabled
    respond_to do |format|
      format.js { render :action => 'create_instant_particular' }
    end
  end

  def create_instant_particular
    @status=false
    @fee_particular=FinanceFeeParticular.new(params[:finance_fee_particular])
    @financefee=FinanceFee.find(params[:id])    
    FinanceFeeParticular.transaction do 
      if @fee_particular.save
        #create link between collection and (instant) particular
        CollectionParticular.create(:finance_fee_particular_id => @fee_particular.id, 
          :finance_fee_collection_id => @financefee.fee_collection_id)

        # create finance fee discount        
        @fee_particular.finance_fee_discounts.create({ :finance_fee_id => @financefee.id, 
            :discount_amount => 0 }) if @financefee.school_discount_mode == "OLD"
        #create link between collection and (instant) particular tax slab
        if @financefee.tax_enabled && @fee_particular.tax_slabs.present?
          slab_id = @fee_particular.tax_slabs.try(:last).try(:id)
          @fee_particular.collectible_tax_slabs.create({ 
              :tax_slab_id => slab_id,
              :collection_id => @financefee.fee_collection_id, :collection_type => 'FinanceFeeCollection' 
            }) 
          particular_tax_collection = @fee_particular.tax_collections.new
          particular_tax_collection.taxable_fee = @financefee
          particular_tax_collection.tax_amount = 0
          particular_tax_collection.slab_id = slab_id
          particular_tax_collection.save  
        end
        #FinanceFeeParticular.add_or_remove_particular_or_discount(@fee_particular, @financefee.finance_fee_collection)
        # update tax and discount wrt (instant) particular for other particulars in selected finance fee
        FinanceFeeParticular.add_or_remove_particular_update_discounts_and_taxes(@fee_particular, @financefee)
        @financefee.reload
        @target_action=params[:target_action]
        @target_controller=params[:target_controller]
        @status=true
      end      
    end
    respond_to do |format|
      format.js { render :action => 'instant_particular.js.erb'
      }
      format.html
    end
  end

  def delete_student_particular
    @particular=FinanceFeeParticular.find(params[:id])
    @financefee=FinanceFee.find(params[:finance_fee_id])
    @status = false
    FinanceFeeParticular.transaction do 
      @particular.destroy
      DiscountParticularLog.create(:amount => @particular.amount, :is_amount => true, 
        :receiver_type => "FinanceFeeParticular", :finance_fee_id => @financefee.id, 
        :user_id => current_user.id, :name => @particular.name)
      #      FinanceFeeParticular.add_or_remove_particular_or_discount(@particular, @financefee.finance_fee_collection)      
      FinanceFeeParticular.add_or_remove_particular_update_discounts_and_taxes(@particular, @financefee)
      @status = true
    end
    @target_action=params[:current_action]
    @target_controller=params[:current_controller]
    @financefee.reload    
    respond_to do |format|
      format.js { render :action => 'instant_particular_delete.js.erb'
      }
      format.html
    end

  end

  def new_instant_discount
    @financefee=FinanceFee.find(params[:id])
    @target_action=params[:current_action]
    @target_controller=params[:current_controller]
    @finance_fee_category=@financefee.finance_fee_collection.fee_category
    respond_to do |format|
      format.js { render :action => 'create_instant_discount' }
    end
  end

  def create_instant_discount
    @status=false
    @fee_discount=FeeDiscount.new(params[:fee_discount])
    @financefee=FinanceFee.find(params[:id])
    fee_particulars = @financefee.finance_fee_collection.finance_fee_particulars.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@financefee.student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
    total_payable=fee_particulars.map { |s| s.amount }.sum.to_f
    discount_amount=@fee_discount.is_amount? ? (total_payable*(@fee_discount.discount.to_f)/total_payable) : (total_payable*(@fee_discount.discount.to_f)/100)
    unless (discount_amount.to_f >= @financefee.balance.to_f)
      FinanceFeeParticular.transaction do
        if @fee_discount.save
          CollectionDiscount.create(:fee_discount_id => @fee_discount.id, :finance_fee_collection_id => @financefee.fee_collection_id)
          d_amount = @fee_discount.discount.to_f
          #          no_parts = fee_particulars.length
          fee_particulars.each do |f_p|
            fp_amt = f_p.amount.to_f
            if @school_discount_mode == "NEW_DISCOUNT"
              if @fee_discount.is_amount?
                ## TO DO :: add logic to adjust minor diff in last particular
                disc_amount = (d_amount / total_payable) * fp_amt.to_f
              else
                disc_amount = (d_amount * fp_amt * 0.01)
              end
            else
              disc_amount = 0
            end
            #            disc_amount = @fee_discount.is_amount? ? d_amount / no_parts : (d_amount * fp_amt * 0.01)
            finance_fee_discount = FinanceFeeDiscount.find(:last, 
              :conditions => "finance_fee_id = #{@financefee.id} AND finance_fee_particular_id = #{f_p.id}")
            if finance_fee_discount.present?
              finance_fee_discount.discount_amount = disc_amount
              finance_fee_discount.save
            else
              @fee_discount.finance_fee_discounts.create({
                  :finance_fee_particular_id => f_p.id,
                  :finance_fee_id => @financefee.id,
                  :discount_amount => disc_amount 
                })
            end            
          end
          #        FinanceFeeParticular.add_or_remove_particular_or_discount(@fee_discount, @financefee.finance_fee_collection)
          FinanceFeeParticular.add_or_remove_particular_update_discounts_and_taxes(@fee_discount, @financefee)
          @financefee.reload
          @target_action=params[:target_action]
          @target_controller=params[:target_controller]
          @status=true
        end        
      end
    else
      @fee_discount.errors.add_to_base(t('discount_cannot_be_greater_than_total_amount'))
    end

    respond_to do |format|
      format.js { render :action => 'instant_discount.js.erb'
      }
      format.html
    end
  end

  def delete_student_discount
    @fee_discount=FeeDiscount.find(params[:id])
    @financefee=FinanceFee.find(params[:finance_fee_id])
    FinanceFeeParticular.transaction do
      @fee_discount.destroy
      DiscountParticularLog.create(:amount => @fee_discount.discount, :is_amount => @fee_discount.is_amount, :receiver_type => "FeeDiscount", :finance_fee_id => @financefee.id, :user_id => current_user.id, :name => @fee_discount.name)
      #FinanceFeeParticular.add_or_remove_particular_or_discount(@fee_discount, @financefee.finance_fee_collection)
      FinanceFeeParticular.add_or_remove_particular_update_discounts_and_taxes(@fee_discount, @financefee)
    end
    @target_action=params[:current_action]
    @target_controller=params[:current_controller]
    @financefee.reload
    respond_to do |format|
      format.js { render :action => 'instant_discount_delete.js.erb'
      }
      format.html
    end

  end

  def get_paid_fees
    #    fees="'FinanceFee', 'HostelFee', 'TransportFee'"
    transport_fee_sql=
      " LEFT JOIN transport_fees 
                 ON transport_fees.id=finance_transactions.finance_id AND
                       finance_transactions.finance_type='TransportFee' AND 
                       transport_fees.groupable_type='Batch' AND 
                       transport_fees.groupable_id=#{@current_batch.id} 
        LEFT JOIN transport_fee_collections 
                 ON transport_fee_collections.id=transport_fees.transport_fee_collection_id "
    
    hostel_fee_sql=
      " LEFT JOIN hostel_fees 
                 ON hostel_fees.id=finance_transactions.finance_id AND 
                       finance_transactions.finance_type='HostelFee' AND 
                       hostel_fees.batch_id=#{@current_batch.id}
        LEFT JOIN hostel_fee_collections 
                 ON hostel_fee_collections.id=hostel_fees.hostel_fee_collection_id "
    
    fees_combining=
      " LEFT JOIN finance_fees 
                 ON finance_fees.id=finance_transactions.finance_id AND
                       finance_transactions.finance_type='FinanceFee' AND 
                       finance_fees.batch_id=#{@current_batch.id} 
        LEFT JOIN finance_fee_collections 
                 ON finance_fee_collections.id=finance_fees.fee_collection_id"
    
    fees_combining_sql = fees_combining + hostel_fee_sql + transport_fee_sql

    where_condition_for_check = "finance_fees.id    IS NOT NULL OR 
                                                 hostel_fees.id      IS NOT NULL OR 
                                                 transport_fees.id IS NOT NULL"
    
    hostel_sql="(hostel_fee_collections.name)"

    transport_sql="(transport_fee_collections.name)"

    finance_transaction_ledgers_sql=
      #      "SELECT DISTINCT multi_fees_transactions.id, " +
    "SELECT DISTINCT finance_transaction_ledgers.id," +
      #                                         multi_fees_transactions.amount, 
    #                                         multi_fees_transactions.created_at creation_time,
    "if(finance_transaction_ledgers.status = 'PARTIAL',
                                            SUM(finance_transactions.amount),
                                            finance_transaction_ledgers.amount
                                          ) amount, 
                                         finance_transaction_ledgers.created_at creation_time," +
      "CONCAT_WS(
                                            '||', group_concat(finance_fee_collections.name SEPARATOR '||' ),
                                            group_concat( #{hostel_sql} SEPARATOR '||' ),
                                            group_concat( #{transport_sql} SEPARATOR '||' ) 
                                          ) AS collection_name,
                                         concat( users.first_name,' ', users.last_name ) AS cashier, 
                                         users.id AS usersid,
                                         'multi_fees_transaction' AS transaction_type," +
      #                                         `finance_transaction_receipts`.receipt_number AS receipt_no," + 
    #                                         group_concat(finance_transactions.receipt_no) AS receipt_no,
    "if(
                                            finance_transaction_ledgers.transaction_mode = 'MULTIPLE',
                                              group_concat(finance_transactions.receipt_no),
                                              if(finance_transaction_ledgers.receipt_no is NULL, 
                                                group_concat(finance_transactions.receipt_no),
                                                finance_transaction_ledgers.receipt_no
                                              )
                                        ) receipt_no," +
      "finance_transactions.reference_no,
                                         finance_transactions.payment_mode,
                                         finance_transactions.payment_note," +
      #                                         multi_fees_transactions.transaction_date,
    "finance_transaction_ledgers.transaction_date,
                                         if(
                                            fee_refunds.id is null,false,true
                                         ) refund_exists " +
      #                             FROM `multi_fees_transactions` 
    "FROM `finance_transaction_ledgers` " +
      #                             INNER JOIN `finance_transaction_receipts` ON
    #                                       `finance_transaction_receipts`.`receipt_transaction_id` = `multi_fees_transactions`.`id` AND
    #`finance_transaction_receipts`.`receipt_transaction_type` = 'MultiFeesTransaction'" +
    #                             INNER JOIN `multi_fees_transactions_finance_transactions` ON 
    #                             "INNER JOIN `multi_fees_transactions_finance_transactions` ON 
    #                                        `multi_fees_transactions_finance_transactions`.multi_fees_transaction_id = `multi_fees_transactions`.id and 
    #                                          multi_fees_transactions.school_id=#{MultiSchool.current_school.id} 
    "INNER JOIN `finance_transactions` ON 
                                        `finance_transactions`.transaction_ledger_id = `finance_transaction_ledgers`.id
                             LEFT JOIN fee_refunds ON 
                                          fee_refunds.finance_fee_id=finance_transactions.finance_id and 
                                          finance_transactions.finance_type='FinanceFee' 
                             LEFT JOIN users ON 
                                           users.id=finance_transactions.user_id 
                             #{fees_combining_sql}  
                             WHERE " +
      #                                          (multi_fees_transactions.student_id='#{@student.id}') and 
    "(finance_transaction_ledgers.payee_id='#{@student.id}' AND 
      finance_transaction_ledgers.payee_type='Student' AND
                                            (finance_transaction_ledgers.status='ACTIVE' or 
                                             finance_transaction_ledgers.status='PARTIAL')
                                          ) and                                       
                                          (#{where_condition_for_check}) group by id "
    finance_transactions_sql=
      "UNION ALL(
                                         SELECT finance_transactions.id,
                                                     finance_transactions.amount,
                                                     finance_transactions.created_at creation_time,
                                                     if(
                                                        finance_transactions.finance_type='FinanceFee',
                                                        finance_fee_collections.name,
                                                        if(
                                                            finance_transactions.finance_type='HostelFee',
                                                            #{hostel_sql},
                                                            #{transport_sql}
                                                          )
                                                       ) as collection_name,
                                                     concat(users.first_name,' ',users.last_name) AS cashier, 
                                                     users.id as usersid,
                                                     'normal_fees_transaction' AS transaction_type,                                                     
                                                     finance_transactions.receipt_no,
                                                     finance_transactions.reference_no,
                                                     finance_transactions.payment_mode,
                                                     finance_transactions.payment_note,
                                                     finance_transactions.transaction_date,
                                                     if(fee_refunds.id is null,false,true) refund_exists 
                                         FROM `finance_transactions` " +
      #                                         INNER JOIN `finance_transaction_receipts` ON
    #                                                    `finance_transaction_receipts`.`receipt_transaction_id` = `finance_transactions`.`id` AND
    #`finance_transaction_receipts`.`receipt_transaction_type` = 'FinanceTransaction'"
    #                                         LEFT OUTER JOIN `multi_fees_transactions_finance_transactions` ON 
    #                                                     `multi_fees_transactions_finance_transactions`.finance_transaction_id = `finance_transactions`.id 
    "LEFT OUTER JOIN `finance_transaction_ledgers` ON 
                                                     `finance_transaction_ledgers`.id = `finance_transactions`.transaction_ledger_id 
                                         #{fees_combining_sql} 
                                         LEFT JOIN users ON 
                                                      users.id=finance_transactions.user_id 
                                         LEFT JOIN fee_refunds ON 
                                                      fee_refunds.finance_fee_id=finance_transactions.finance_id and 
                                                      finance_transactions.finance_type='FinanceFee' 
                                         WHERE (
                                                        finance_transactions.payee_id='#{@student.id}' and (#{where_condition_for_check}) and " +
      #                                                        multi_fees_transactions.id is NULL and 
    "finance_transaction_ledgers.id is NULL and 
                                                        finance_type in ('FinanceFee','HostelFee','TransportFee')
                                                      )
                                        )"
    order_param=" ORDER BY creation_time DESC"
    #@paid_fees=FinanceTransactionLedger.paginate_by_sql("#{finance_transaction_ledgers_sql} #{finance_transactions_sql}#{order_param}", :page => params[:page], :per_page => 10, :order => 'creation_time desc')
    @paid_fees=FinanceTransactionLedger.paginate_by_sql("#{finance_transaction_ledgers_sql} #{order_param}", 
      :page => params[:page], :per_page => 10, :order => 'creation_time desc')
    #    @paid_fees=FinanceTransactionLedger.paginate_by_sql("#{finance_transaction_ledgers_sql} #{order_param}", :page => params[:page], :per_page => 10, :order => 'creation_time desc')
    #    @paid_fees=MultiFeesTransaction.paginate_by_sql("#{multi_fees_transactions_sql} #{finance_transactions_sql}#{order_param}", :page => params[:page], :per_page => 10, :order => 'creation_time desc')
  end

  def pay_all_fees_index
    @batches = Batch.find(:all, :conditions => {:is_deleted => false, :is_active => true}, 
      :joins => :course, :select => "`batches`.*,CONCAT(courses.code,'-',batches.name) as course_full_name", 
      :order => "course_full_name")
  end

  def search_students_for_pay_all_fees
    if params[:query].length>= 3
      @students = Student.find(:all,
        :select => "students.id AS id, students.admission_no AS admission_no, students.roll_number,
                           CONCAT(students.first_name,' ',students.middle_name,' ',students.last_name) AS fullname,
                           CONCAT(courses.code,'-',batches.name) AS batch_full_name,
                           (SELECT SUM(ff.balance) FROM finance_fees ff WHERE  ff.student_id=students.id AND 
                            FIND_IN_SET(id,GROUP_CONCAT(distinct finance_fees.id))) AS fee_due,
                           #{transport_fee_due} AS transport_due, #{hostel_fee_due} AS hostel_due",
        :joins => join_sql_for_student_fees, :group => "students.id",
        :conditions => ["ltrim(first_name) LIKE ? OR ltrim(middle_name) LIKE ? OR ltrim(last_name) LIKE ? OR 
                                   admission_no = ? OR (concat(ltrim(rtrim(first_name)), \" \",ltrim(rtrim(last_name))) LIKE ? ) OR 
                                   (concat(ltrim(rtrim(first_name)), \" \", ltrim(rtrim(middle_name)), \" \",ltrim(rtrim(last_name))) LIKE ? ) ",
          "#{params[:query]}%", "#{params[:query]}%", "#{params[:query]}%", "#{params[:query]}", 
          "#{params[:query]}%", "#{params[:query]}%"],
        :order => "batches.id ASC, students.first_name ASC") unless params[:query] == ''
    else
      @students = Student.find(:all,
        :select => "students.id AS id, students.admission_no AS admission_no, students.roll_number,
                           CONCAT(students.first_name,' ',students.middle_name,' ',students.last_name) AS fullname,
                           CONCAT(courses.code,'-',batches.name) AS batch_full_name,
                           (SELECT SUM(ff.balance) FROM finance_fees ff WHERE  ff.student_id=students.id AND 
                            FIND_IN_SET(id,GROUP_CONCAT(DISTINCT finance_fees.id))) AS fee_due,
                           #{transport_fee_due} AS transport_due, #{hostel_fee_due} AS hostel_due",
        :joins => join_sql_for_student_fees, :group => "students.id", 
        :conditions => ["admission_no = ? ", params[:query]],
        :order => "batches.id ASC, students.first_name ASC") unless params[:query] == ''
    end

    render :layout => false


  end

  def join_sql_for_student_fees(batch_id=nil)
    if batch_id.present?
      join_batch_id = (batch_id == "current_batch") ? "students.batch_id" : "#{batch_id}"
      #      if batch_id == "current_batch"
      #        transport_sql = "AND transport_fees.groupable_id=students.batch_id"
      #        hostel_sql = "AND hostel_fees.batch_id=students.batch_id"
      #        finance_sql = "AND finance_fees.batch_id=students.batch_id"
      #      else
      transport_sql = "AND transport_fees.groupable_id=#{join_batch_id}"
      hostel_sql = "AND hostel_fees.batch_id=#{join_batch_id}"
      finance_sql = "AND finance_fees.batch_id=#{join_batch_id}"
      #      end
    else
      transport_sql = hostel_sql = finance_sql=""
    end


    result  = "INNER JOIN batches ON batches.id=students.batch_id 
               INNER JOIN courses ON courses.id=batches.course_id
               LEFT JOIN finance_fees 
                      ON finance_fees.student_id=students.id #{finance_sql}"
    result +=" LEFT JOIN transport_fees 
                      ON transport_fees.receiver_id=students.id AND 
                         transport_fees.receiver_type='Student' AND 
                         transport_fees.is_active=1 #{transport_sql}" if FedenaPlugin.
      can_access_plugin?("fedena_transport")
    result +=" LEFT JOIN hostel_fees 
                      ON hostel_fees.student_id=students.id AND 
                         hostel_fees.is_active=1 #{hostel_sql}" if FedenaPlugin.
      can_access_plugin?("fedena_hostel")
    result
  end
  
  def list_students_by_batch
    if params[:fees_submission][:batch_id].present?
      @batch_id=params[:fees_submission][:batch_id]
      having="(count(distinct finance_fees.id)>0)"
      having+=" or (count(hostel_fees.id)>0)" if FedenaPlugin.can_access_plugin?("fedena_hostel")
      having+=" or (count(distinct transport_fees.id)>0)" if FedenaPlugin.can_access_plugin?("fedena_transport")
      @students=Student.find(:all,
        :select => "students.id AS id, CONCAT(students.first_name,' ',students.middle_name,' ',
                                                                     students.last_name) AS fullname,
                           CONCAT(courses.code,'-',batches.name) AS batch_full_name,
                           students.admission_no AS admission_no,
                           (SELECT SUM(ff.balance) 
                               FROM finance_fees ff 
                             WHERE  ff.student_id=students.id AND 
                                          FIND_IN_SET(id,GROUP_CONCAT(DISTINCT finance_fees.id))
                            ) AS fee_due,
                            #{transport_fee_due} AS transport_due,
                            #{hostel_fee_due} AS hostel_due,students.roll_number",
        :joins => join_sql_for_student_fees(@batch_id),
        :group => "students.id",
        :having => having,
        :order => "batches.id asc,students.first_name asc"
      )
    else
      @students=[]
    end
    respond_to do |format|
      format.js
    end
  end

  def transport_fee_due batch_id=nil
    if FedenaPlugin.can_access_plugin?("fedena_transport")
      batch_id_join = batch_id.present? ? "tf.groupable_type='Batch' AND 
                                           tf.groupable_id=#{batch_id} AND " : ""
      #      "(SELECT SUM(tf.balance) 
      "(SELECT SUM(ROUND(tf.balance,#{@precision}))
           FROM transport_fees tf 
        WHERE tf.receiver_id=students.id AND tf.receiver_type='Student' AND 
                    #{batch_id_join}
                    FIND_IN_SET(tf.id,GROUP_CONCAT(DISTINCT transport_fees.id))
       )"
    else
      0
    end
  end

  def transport_fee_count batch_id=nil
    if FedenaPlugin.can_access_plugin?("fedena_transport")
      batch_id_join = batch_id.present? ? "tf.groupable_type='Batch' AND 
                                           tf.groupable_id=#{batch_id} AND " : 
        "students.batch_id AND "
      "(SELECT COUNT(DISTINCT tf.id) 
           FROM transport_fees tf 
        WHERE tf.receiver_id=students.id AND tf.receiver_type='Student' AND 
                    #{batch_id_join}
                    FIND_IN_SET(tf.id,GROUP_CONCAT(DISTINCT transport_fees.id)))"
    else
      0
    end
  end
  
  def hostel_fee_due batch_id=nil
    if FedenaPlugin.can_access_plugin?("fedena_hostel")
      batch_id_join = batch_id.present? ? "hf.batch_id = #{batch_id} AND " : ""
      #      "(SELECT SUM(hf.balance) 
      "(SELECT SUM(ROUND(hf.balance,#{@precision}))
             FROM hostel_fees hf 
          WHERE hf.student_id = students.id AND 
                      #{batch_id_join}
                      FIND_IN_SET(id,GROUP_CONCAT(DISTINCT hostel_fees.id)))"
    else
      0
    end
  end
  
  def hostel_fee_count batch_id = nil
    if FedenaPlugin.can_access_plugin?("fedena_hostel")
      batch_id_join = batch_id.present? ? "hf.batch_id = #{batch_id} AND " : 
        "students.batch_id AND "
      "(SELECT COUNT(DISTINCT hf.id) 
           FROM hostel_fees hf 
        WHERE hf.student_id=students.id AND 
                    #{batch_id_join}                   
                    FIND_IN_SET(hf.id,GROUP_CONCAT(DISTINCT hostel_fees.id)))"
    else
      0
    end
  end

  #  def student_search_autocomplete
  #    students= Student.active.find(:all, :select => "students.*,sum(finance_fees.balance) as fee_due,sum(transport_fees.balance) as transport_due,sum(hostel_fees.balance) as hostel_due",
  #                                  :joins => join_sql_for_student_fees,
  #                                  :conditions => ["(admission_no LIKE ? OR first_name LIKE ?) and students.id<>#{params[:student_id]}", "%#{params[:query]}%", "%#{params[:query]}%"],
  #                                  :group => "students.id",
  #                                  :having => "(count(distinct transport_fees.id)>0) or (count(distinct finance_fees.id)>0) or (count(hostel_fees.id)>0)",
  #                                  :order => "batches.id asc,students.first_name asc").uniq
  #    suggestions=students.collect { |s| s.full_name.length+s.admission_no.length > 20 ? s.full_name[0..(18-s.admission_no.length)]+".. "+"(#{s.admission_no})"+" - " : s.full_name+"(#{s.admission_no})" }
  #    receivers=students.map { |st| "{'receiver': 'Student','id': #{st.id}}" }
  #    if receivers.present?
  #      render :json => {'query' => params["query"], 'suggestions' => suggestions, 'data' => receivers}
  #    else
  #      render :json => {'query' => params["query"], 'suggestions' => ["#{t('no_users')}"], 'data' => ["{'receiver': #{false}}"]}
  #    end
  #  end
  def student_search_autocomplete
    having="(count(distinct finance_fees.id)>0)"
    having+=" or (count(hostel_fees.id)>0)" if FedenaPlugin.can_access_plugin?("fedena_hostel")
    having+=" or (count(distinct transport_fees.id)>0)" if FedenaPlugin.can_access_plugin?("fedena_transport")
    students= Student.active.find(:all, :select => "students.*,sum(finance_fees.balance) as fee_due,#{transport_fee_due} as transport_due,#{hostel_fee_due} as hostel_due",
      :joins => join_sql_for_student_fees,
      :conditions => ["(admission_no LIKE ? OR first_name LIKE ?) and students.id<>#{params[:student_id]}", "%#{params[:query]}%", "%#{params[:query]}%"],
      :group => "students.id",
      :having => having,
      :order => "batches.id asc,students.first_name asc").uniq
    suggestions=students.collect { |s| s.full_name.length+s.admission_no.length > 20 ? s.full_name[0..(18-s.admission_no.length)]+".. "+"(#{s.admission_no})"+" - " : s.full_name+"(#{s.admission_no})" }
    receivers=students.map { |st| "{'receiver': 'Student','id': #{st.id}}" }
    if receivers.present?
      render :json => {'query' => params["query"], 'suggestions' => suggestions, 'data' => receivers}
    else
      render :json => {'query' => params["query"], 'suggestions' => ["#{t('no_users')}"], 'data' => ["{'receiver': #{false}}"]}
    end
  end

  def generate_overall_fee_receipt_pdf
    @student=Student.find(params[:student_id])
    status=true
    begin
      @transactions=FinanceTransaction.find(params[:transaction_id], :joins => :transaction_ledger, 
        :select => "finance_transactions.*,
                        if(finance_transaction_ledgers.transaction_mode = 'MULTIPLE',
                        finance_transactions.receipt_no, finance_transaction_ledgers.receipt_no) receipt_no")      
    rescue ActiveRecord::RecordNotFound
      status=false
    end
    unless status
      respond_to do |format|
        format.html { render :file => "#{Rails.root}/public/404.html", :status => :not_found }
      end
    else
      @config = Configuration.get_multiple_configs_as_hash ['PdfReceiptSignature', 'PdfReceiptSignatureName', 
        'PdfReceiptCustomFooter','PdfReceiptAtow','PdfReceiptNsystem', 'PdfReceiptHalignment']
      @default_currency = Configuration.default_currency
      @current_batch=Batch.find(params[:batch_id])
      @overall_receipt=OverallReceipt.new(params[:student_id], params[:transaction_id])
      render :pdf => 'generate_overall_fee_receipt_pdf', 
        :template => 'finance_extensions/generate_overall_fee_receipt_pdf.erb', 
        :margin => {:top => 10, :bottom => 10, :left => 5, :right => 5}, 
        :header => {:html => {:content => ''}}, 
        :footer => {:html => {:content => ''}}, 
        :show_as_html => params.key?(:debug)
    end
  end

  # tax report 
  def tax_report
    
  end
  
  def update_tax_report
    
    if validate_date
      # finance fee tax payments
      all_tax_payments = TaxPayment.finance_fee_tax_payments(@start_date, @end_date)      
      # hostel tax payments if plugin is enabled
      all_tax_payments += TaxPayment.hostel_fee_tax_payments(@start_date, 
        @end_date) if FedenaPlugin.can_access_plugin?("fedena_hostel")
      # transport tax payments if plugin is enabled
      all_tax_payments += TaxPayment.transport_fee_tax_payments(@start_date, 
        @end_date) if FedenaPlugin.can_access_plugin?("fedena_transport")
      # instant fee tax payments if plugin is enabled
      all_tax_payments += TaxPayment.instant_fee_tax_payments(@start_date, 
        @end_date) if FedenaPlugin.can_access_plugin?("fedena_instant_fee")
      
      @tax_payments = all_tax_payments.group_by { |tax|  "#{tax.slab_name} - &rlm;(#{precision_label(tax.slab_rate)}%)&rlm;" }
      
      @total_tax = all_tax_payments.map(&:tax_amount).sum.to_f
            
      @target_action="update_tax_report"
      
      if request.xhr?
        render(:update) do|page|
          page.replace_html "fee_report_div", :partial=>"update_tax_report_partial"
        end
      end
    else
      if request.xhr?
        render_date_error_partial
      else
        flash[:warn_notice] = "error"
        redirect_to :action => :tax_report
      end
    end
  end
  
  def show_date_filter
    month_date
    @target_action=params[:target_action]
    if request.xhr?
      render(:update) do|page|
        page.replace_html "date_filter", :partial=>"filter_dates"
      end
    end
  end
  
  def fees_student_structure
    @batches = Batch.find(:all, :conditions => {:is_deleted => false, :is_active => true}, 
      :joins => :course, :order => "course_full_name",
      :select => "batches.*,
                  CONCAT(courses.code,'-',batches.name) as course_full_name")
  end  
  
  def list_students_by_batch_for_structure
    if params[:fees_submission][:batch_id].present?
      @batch_id=params[:fees_submission][:batch_id]
      #      having="(count(distinct finance_fees.id)>0)"
      #      having+=" or (count(hostel_fees.id)>0)" if FedenaPlugin.can_access_plugin?("fedena_hostel")
      #      having+=" or (count(distinct transport_fees.id)>0)" if FedenaPlugin.can_access_plugin?("fedena_transport")
      @students=Student.find(:all,
        :select => "students.id AS id, CONCAT(students.first_name,' ',students.middle_name,' ',
                                                                     students.last_name) AS fullname,
                           students.batch_id AS batch_id,
                           CONCAT(courses.code,'-',batches.name) AS batch_full_name,
                           students.admission_no AS admission_no,
                           (SELECT SUM(ff.balance) 
                               FROM finance_fees ff 
                             WHERE  ff.student_id=students.id AND ff.batch_id = #{@batch_id} AND 
                                          FIND_IN_SET(id,GROUP_CONCAT(DISTINCT finance_fees.id))
                            ) AS fee_due,
                           (SELECT COUNT(DISTINCT ff.id) 
                               FROM finance_fees ff 
                         INNER JOIN finance_fee_collections ffc 
			                           ON ffc.id = ff.fee_collection_id
	                       INNER JOIN collection_particulars cp
			                           ON cp.finance_fee_collection_id = ffc.id
	                       INNER JOIN finance_fee_particulars ffp
			                           ON ffp.id = cp.finance_fee_particular_id
                             WHERE  ff.student_id=students.id AND 
                                    ff.batch_id = #{@batch_id} AND 
                                    FIND_IN_SET(ff.id,GROUP_CONCAT(DISTINCT finance_fees.id)) AND		       
                                    ffc.is_deleted=false AND 
                                    ((ffp.receiver_type='Batch' AND ffp.receiver_id=ff.batch_id) OR 
                                     (ffp.receiver_type='Student' AND ffp.receiver_id=ff.student_id) OR 
                                     (ffp.receiver_type='StudentCategory' AND ffp.receiver_id=ff.student_category_id))
                            ) AS fee_count, students.roll_number,
                            #{transport_fee_due(@batch_id)} AS transport_due,
                            #{transport_fee_count(@batch_id)} AS transport_count,
                            #{hostel_fee_due(@batch_id)} AS hostel_due,
                            #{hostel_fee_count(@batch_id)} AS hostel_count",
        :joins => join_sql_for_student_fees(@batch_id),
        :group => "students.id", #:having => having, 
        :conditions => {:batch_id => @batch_id},
        :order => "batches.id asc,students.first_name asc"
      )
    else
      @students=[]
    end
    respond_to do |format|
      format.js
    end
  end
  
  def search_student_list_for_structure
    
    if params[:query].length>= 3
      @students = Student.find(:all,
        :select => "students.id AS id, students.admission_no AS admission_no, students.roll_number,
                           students.batch_id AS batch_id,
                           CONCAT(students.first_name,' ',students.middle_name,' ',students.last_name) AS fullname,
                           CONCAT(courses.code,'-',batches.name) AS batch_full_name,
                           (SELECT SUM(ROUND(ff.balance,#{@precision})) FROM finance_fees ff WHERE  ff.student_id=students.id AND 
                            FIND_IN_SET(id,GROUP_CONCAT(distinct finance_fees.id))) AS fee_due,
                           (SELECT COUNT(ff.id) FROM finance_fees ff WHERE  ff.student_id=students.id AND 
                            ff.batch_id = students.batch_id AND 
                            FIND_IN_SET(id,GROUP_CONCAT(distinct finance_fees.id))) AS fee_count,
                           #{transport_fee_count} AS transport_count, #{hostel_fee_count} AS hostel_count,
                           #{transport_fee_due} AS transport_due, #{hostel_fee_due} AS hostel_due",
        :joins => join_sql_for_student_fees("current_batch"), :group => "students.id",
        :conditions => ["ltrim(first_name) LIKE ? OR ltrim(middle_name) LIKE ? OR ltrim(last_name) LIKE ? OR 
                                   admission_no = ? OR (concat(ltrim(rtrim(first_name)), \" \",ltrim(rtrim(last_name))) LIKE ? ) OR 
                                   (concat(ltrim(rtrim(first_name)), \" \", ltrim(rtrim(middle_name)), \" \",ltrim(rtrim(last_name))) LIKE ? ) ",
          "#{params[:query]}%", "#{params[:query]}%", "#{params[:query]}%", "#{params[:query]}", 
          "#{params[:query]}%", "#{params[:query]}%"],
        :order => "batches.id ASC, students.first_name ASC") unless params[:query] == ''
    else
      @students = Student.find(:all,
        :select => "students.id AS id, students.admission_no AS admission_no, students.roll_number,
                           students.batch_id AS batch_id,
                           CONCAT(students.first_name,' ',students.middle_name,' ',students.last_name) AS fullname,
                           CONCAT(courses.code,'-',batches.name) AS batch_full_name,
                           (SELECT SUM(ROUND(ff.balance,#{@precision})) FROM finance_fees ff WHERE  ff.student_id=students.id AND 
                            FIND_IN_SET(id,GROUP_CONCAT(DISTINCT finance_fees.id))) AS fee_due,
                           (SELECT COUNT(ff.id) FROM finance_fees ff WHERE  ff.student_id=students.id AND 
                            ff.batch_id = students.batch_id AND 
                            FIND_IN_SET(id,GROUP_CONCAT(distinct finance_fees.id))) AS fee_count,
                           #{transport_fee_count} AS transport_count, #{hostel_fee_count} AS hostel_count,
                           #{transport_fee_due} AS transport_due, #{hostel_fee_due} AS hostel_due",
        :joins => join_sql_for_student_fees("current_batch"), :group => "students.id", 
        :conditions => ["admission_no = ? ", params[:query]],
        :order => "batches.id ASC, students.first_name ASC") unless params[:query] == ''
    end

    render :layout => false
  end
  
  def view_fees_structure
    # id => student id
    # batch_id => batch id
    @student = Student.find(params[:id])
    @batch = Batch.find(params[:id2])    
    @student_fees = ActiveSupport::OrderedHash.new
    # finance fees
    @student_fees["finance_fees"] = fetch_fees 'FinanceFee'
    # hostel fees    
    @student_fees["hostel_fees"] = fetch_fees "HostelFee" if FedenaPlugin.
      can_access_plugin?("fedena_hostel")    
    # transport fees
    @student_fees["transport_fees"] = fetch_fees "TransportFee" if FedenaPlugin.
      can_access_plugin?("fedena_transport")    
  end
  
  def fetch_fees fee_type
    tbl_name = fee_type.underscore.pluralize
    inc_assoc = "#{fee_type.underscore}_collection"
    conditions = []
    conditions << "#{tbl_name}.is_active = true" unless fee_type == 'FinanceFee'
    if fee_type == 'TransportFee'
      conditions << "receiver_id = #{@student.id}"
      conditions << "receiver_type = 'Student'"
      conditions << "groupable_id = #{@batch.id}"
      conditions << "groupable_type = 'Batch'"
    else
      conditions << "#{tbl_name}.student_id = #{@student.id}"
      conditions << "#{tbl_name}.batch_id = #{@batch.id}"      
    end
    
    fee_joins = "INNER JOIN finance_fee_collections ffc 
                         ON ffc.id = #{tbl_name}.fee_collection_id
                 INNER JOIN collection_particulars cp 
                         ON cp.finance_fee_collection_id = ffc.id
                 INNER JOIN finance_fee_particulars ffp	
                         ON ffp.id = cp.finance_fee_particular_id "
    fees_where = fee_type == 'FinanceFee' ? " AND 
                  ffc.is_deleted = false AND 
                  ((ffp.receiver_type='Batch' and ffp.receiver_id=#{tbl_name}.batch_id) or 
                   (ffp.receiver_type='Student' and ffp.receiver_id=#{tbl_name}.student_id) or 
                   (ffp.receiver_type='StudentCategory' and ffp.receiver_id=#{tbl_name}.student_category_id)
                  )" : ""
    trans_select = "(SELECT SUM(ft.amount) FROM finance_transactions ft 
                            WHERE  ft.finance_id=#{tbl_name}.id AND 
                                         ft.finance_type='#{fee_type}' AND 
                                         FIND_IN_SET(ft.finance_id,GROUP_CONCAT(distinct #{tbl_name}.id))
                            ) AS paid_amount"
    trans_joins = "LEFT JOIN finance_transactions ft
                                    ON ft.finance_id = #{tbl_name}.id AND 
                                          ft.finance_type ='#{fee_type}'"
    fee_name = (fee_type.constantize rescue nil) 
    conditions = conditions.compact.join(" AND ")
    conditions += fees_where if fee_type == 'FinanceFee'
    joins = "#{fee_type == 'FinanceFee' ? fee_joins : ''} #{trans_joins}"
    fee_name.present? ? (fee_name.all(:conditions => conditions, 
        :include => "#{inc_assoc}", :joins => joins, :group => "#{tbl_name}.id",
        :select => "#{tbl_name}.*, #{trans_select}, MAX(ft.transaction_date) AS last_transaction_date")) : []
  end
  
  def fees_structure_for_student
    @student = Student.find(params[:id])    
    tbl_name = params[:fee_type]
    fee_type = tbl_name.singularize
    fee_name = fee_model_name tbl_name
    collection_name = ("#{fee_type.camelize}Collection".constantize rescue nil)
    @date = collection_name.find params[:id2] if collection_name.present?
    inc_assoc = @date.tax_enabled? ? {:tax_collections => :tax_slab, 
      :finance_transactions => :transaction_ledger} : {}
    @fee = @student.fee_by_date(@date, fee_name, inc_assoc)
    #    @fee = @student.finance_fee_by_date(@date)
    #      :conditions => ["is_deleted IS NOT NULL"])
    #    @paid_fees = @fee.finance_transactions.all(:include => :transaction_ledger)
    @paid_fees = @fee.finance_transactions
    if fee_type == "finance_fee"
      @financefee = @fee
      @fee_category = FinanceFeeCategory.find(@date.fee_category_id)
      particular_and_discount_details 
      bal=(@total_payable-@total_discount).to_f
      @transaction_date=params[:transaction_date].present? ? Date.parse(params[:transaction_date]) : Date.today
      days=(Date.today-@date.due_date.to_date).to_i
      auto_fine=@date.fine
      if days > 0 and auto_fine
        @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], :order => 'fine_days ASC')
        @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule
        if @fine_rule and @financefee.balance==0
          @fine_amount=@fine_amount-@financefee.finance_transactions.all(:conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
        end
      end    
    else
      @total_fine = @fee.fine_amount.to_f
      if @fee.tax_enabled?
        @tax_collections = @fee.tax_collections
        @total_tax = @tax_collections.map do |x| 
          FedenaPrecision.set_and_modify_precision(x.tax_amount).to_f
        end.sum.to_f
        @tax_slabs = @tax_collections.group_by {|x| x.tax_slab }
      end
    end
  end
  
  def fee_structure_pdf
    @student = Student.find(params[:id])
    tbl_name = params[:fee_type]
    fee_type = tbl_name.singularize
    fee_name = fee_model_name tbl_name
    collection_name = ("#{fee_type.camelize}Collection".constantize rescue nil)
    @date = collection_name.find params[:id2] if collection_name.present?
    inc_assoc = @date.tax_enabled? ? {:tax_collections => :tax_slab, 
      :finance_transactions => :transaction_ledger} : {}
    #    fee_type = params[:fee_type]
    fee_name = fee_model_name tbl_name    
    if fee_name.present?
      @fee = @student.fee_by_date(@date, fee_name, inc_assoc)
      #      @fee = fee_model_name.last(:conditions => {:student_id => @student.id,
      #          :fee_collection_id => params[:id2]}, :include => [:finance_transactions, 
      #          :finance_fee_collection,
      #          {:tax_collections => :tax_slab}]) 
      @config = Configuration.get_multiple_configs_as_hash ['PdfReceiptSignature', 'PdfReceiptSignatureName', 
        'PdfReceiptCustomFooter','PdfReceiptAtow','PdfReceiptNsystem', 'PdfReceiptHalignment']    
      @default_currency = Configuration.default_currency        
      @tax_config = Configuration.get_multiple_configs_as_hash(['FinanceTaxIdentificationLabel',
          'FinanceTaxIdentificationNumber']) if @fee.tax_enabled?
      get_student_invoice(@fee,tbl_name)      
      render :pdf => 'fee_structure_pdf',
        :template=>'finance_extensions/fee_structure_pdf.erb',
        :margin =>{:top=>2,:bottom=>20,:left=>5,:right=>5},
        :header => {:html => { :content=> ''}}, 
        :footer => {:html => {:content => ''}}, 
        :show_as_html => params.key?(:debug)          
    else
      flash[:notice] = "#{t('flash_msg5')}"
      redirect_to :controller => 'user', :action => 'dashboard' and return
    end    
  end
    
  private
  
  def fee_model_name fee_type
    case fee_type
    when "finance_fees"
      (fee_type.singularize.camelize.constantize rescue nil)
    when "hostel_fees"
      FedenaPlugin.can_access_plugin?("fedena_hostel") ? 
        (fee_type.singularize.camelize.constantize rescue nil) : nil      
    when "transport_fees"
      FedenaPlugin.can_access_plugin?("fedena_transport") ? 
        (fee_type.singularize.camelize.constantize rescue nil) : nil      
    end
  end
  
  def invoice_number_enabled?
    @invoice_enabled = Configuration.get_config_value('EnableInvoiceNumber').to_i == 1
  end
  
  def lock_particular_wise_auto_creation
    FinanceTransaction.particular_wise_pay_lock=true
    yield
    FinanceTransaction.particular_wise_pay_lock=false
  end
end
