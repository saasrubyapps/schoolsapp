class TcTemplateStudentDetailsController < ApplicationController
  before_filter :login_required
  before_filter :template_presence_required
  filter_access_to :all
  check_request_fingerprint :create_new_field, :update_field, :priority_change
  
  
  def index
    @template_id = current_template.id
    get_student_details(current_template)
  end
  
  def edit
    @flag = false
    @student_additional_fields=StudentAdditionalField.all(:conditions=>["status = ?",true])
    @field=TcTemplateFieldStudentDetail.find(params[:id])
    if @field.field_info.field_type == "system"
      k=TcTemplateVersion::SYSTEM_FIELDS.select{|_,value| value[:field][@field.field_info.field_format_value]}
    else
      k=TcTemplateVersion::CUSTOM_FIELDS.select{|_,value| value[:field][@field.field_info.field_format_value]}
    end
    unless k.present?
      @selected_field_value = @field.field_info.field_format_value
    else
      @selected_field_value = k[0][0]
    end
    if @field.parent_field.present?
      @student_details = current_template.tc_template_field_student_details.parent_fields
      @parent = @field.parent_field
      @flag = true
    end
  end
  
  def show
    
  end
  
  def update_field
    if params.present?
      @flash = params[:flash]
      @result = TcTemplateFieldStudentDetail.edit_field(params[:tc_template_field_student_details])
      unless @result.blank?
        @errors=true
      else
        get_student_details(current_template)
      end
    end
  end
  
  def delete_field
    if params.present?
      TcTemplateFieldStudentDetail.delete_field(params)
      get_student_details(current_template)
    else
      @errors = true
    end   
  end
  
  def new_field
    @new_student_detail_field = TcTemplateFieldStudentDetail.new
    @student_additional_fields=StudentAdditionalField.all(:conditions=>["status = ?",true])
    @subfield = params[:sub_field]
    if @subfield 
      @student_details = current_template.tc_template_field_student_details.parent_fields
    end
  end
  
  def create_new_field
    if params.present?
      @flash = params[:flash]
      @result=TcTemplateFieldStudentDetail.create_new_field(params[:tc_template_field_student_details])
      unless @result.blank?
        @errors=true
      else
        get_student_details(current_template)
      end
    end  
  end
  
  def priority_change
    if params.present?
      TcTemplateFieldStudentDetail.change_priority(params[:tc_template_student_details][:tc_template_student_details_attributes])
      get_student_details(current_template)
    else
      @errors=true
    end
  end
  
  def cancel
    current_template = TcTemplateVersion.find(params[:version_id])
    get_student_details(current_template)
    render :update do |page|
      page.replace_html 'other_details',:partial=>'reorder'
    end
  end
  
  private
  
  def template_presence_required
    unless TcTemplateVersion.current
      TcTemplateVersion.initialize_first_template
    end
  end
  
  def current_template
    TcTemplateVersion.current
  end
  
  def get_student_details(current_template)
    @parent_student_details = current_template.tc_template_field_student_details.parent_fields
    @child_student_details = current_template.tc_template_field_student_details.child_fields
  end
  
end
