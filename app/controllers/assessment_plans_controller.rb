class AssessmentPlansController < ApplicationController
  before_filter :login_required
  filter_access_to :all
  require 'lib/override_errors'
  helper OverrideErrors
  
  before_filter :find_academic_year, :only=>[:index,:create, :build_terms]
  
  check_request_fingerprint :add_courses
  
  def index
    @plans = @academic_year.assessment_plans
    @academic_years = AcademicYear.all
  end
  
  def new
    @academic_year = AcademicYear.find params[:academic_year_id]
    @plan = @academic_year.assessment_plans.new
    @plan.build_terms(1)
  end
  
  def create
    @plan = AssessmentPlan.new(params[:assessment_plan])
    if @plan.save
      flash[:notice] = t('exam_plan_created')
      redirect_to :action=> 'show', :id=>@plan.id
    else
      render :new
    end
  end
  
  def destroy
    @plan = AssessmentPlan.find params[:id]
    if !@plan.has_dependencies? and @plan.destroy
      flash[:notice] = t('plan_deleted_successfully')
    else
      flash[:notice] = t('cant_delete_plan')
    end
    render :js=>"window.location='#{assessment_plans_path}'"
  end
  
  def show
    @plan = AssessmentPlan.find params[:id]
    @academic_year = @plan.academic_year
    @has_dependencies = @plan.has_dependencies?
  end
  
  def build_terms
    count = params[:count].to_i
    @plan = params[:object_id].present? ? AssessmentPlan.find(params[:object_id]) : @academic_year.assessment_plans.new
    @plan.build_terms(count)
    render :update do |page|
      page.replace_html 'terms', :partial => 'new_term_strips', :locals=>{:plan=>@plan}
    end
  end
  
  def manage_courses
    @assessment_plan = AssessmentPlan.find params[:id]
    @plan_courses = @assessment_plan.assessment_plans_courses.all(:include => :course)
  end
  
  def add_courses
    @assessment_plan = AssessmentPlan.find params[:id]
    @assessment_plan.build_courses unless params[:assessment_plan].present?
    if request.put? and @assessment_plan.update_attributes(params[:assessment_plan])
      flash[:notice] = t('courses_linked')
      redirect_to :action => :manage_courses, :id => @assessment_plan.id
    end
  end
  
  def unlink_course
    @assessment_plan = AssessmentPlan.find params[:id]
    p_course = AssessmentPlansCourse.find params[:course_id]
    if @assessment_plan.has_dependency_for_course(p_course.course)
      flash[:notice] = t('exam_groups_already_created')
    else
      flash[:notice] = t('courses_unlinked') if p_course.destroy
    end
    redirect_to :action => :manage_courses, :id => @assessment_plan.id
  end
  
  def change_academic_year
    @academic_year = AcademicYear.find params[:id]
    @plans = @academic_year.assessment_plans
    render :update do |page|
      page.replace_html 'assessment_plans_listing', :partial=>'assessment_plans_list'
      page.replace_html 'inner-tab-menu', :partial=>'planner_create_link'
    end
  end
  
  def delete_assessment_group
    @assessment_group = AssessmentGroup.find params[:assessment_group_id]
    @plan = AssessmentPlan.find params[:assessment_plan_id]
    @result = @assessment_group.check_and_destroy
  end
  
  def delete_planner_assessment
    assessment_group = AssessmentGroup.find params[:id]
    assessment_group.destroy
    flash[:notice] = t('exams.flash5')
    redirect_to :action => :show, :id => assessment_group.assessment_plan_id
  end
  
  private
  
  def find_academic_year
    @academic_year = (params[:academic_year_id].present? ? AcademicYear.find(params[:academic_year_id]) : AcademicYear.active.first)
    if @academic_year.nil?
      flash[:notice] = "#{t('set_up_academic_year')}"
      redirect_to :controller=>:academic_years ,:action=>:index and return
    end
  end
end