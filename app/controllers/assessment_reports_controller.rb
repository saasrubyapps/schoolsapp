class AssessmentReportsController < ApplicationController
  before_filter :login_required
  filter_access_to :all, :except => [:students_term_reports, :student_term_report_pdf, :student_exam_reports, :student_exam_report_pdf]
  filter_access_to [:students_term_reports, :student_term_report_pdf, :student_exam_reports, :student_exam_report_pdf], :attribute_check=>true, :load_method => lambda { ((current_user.student? or current_user.parent?) and params[:student_id].present?) ? Student.find(params[:student_id]) : current_user}
  require 'lib/override_errors'
  helper OverrideErrors
  
  def settings
    @plan = AssessmentPlan.find params[:assessment_plan_id]
    if request.post?
      AssessmentReportSetting.set_setting_values(params[:assessment_report_setting], @plan.id)
      flash[:notice] = "#{t('flash_msg8')}"
      redirect_to :action=> 'settings', :assessment_plan_id => @plan.id
    else
      @student_fields = AssessmentReportSetting::SETTINGS_WITH_VALUES
      @setting = AssessmentReportSetting.get_multiple_settings_as_hash AssessmentReportSetting::SETTINGS, @plan.id
      @student_additional_fields = StudentAdditionalField.find(:all, :conditions=> "status = true", :order=>"priority ASC")
    end
  end
  
  def report_header_info
    @setting = AssessmentReportSetting.get_multiple_settings_as_hash ["HeaderSpace","UseCbseLogo"], params[:plan_id]
    render :update do |page|
      page.replace_html 'report_desc',:partial=>'report_with_normal_header' if params[:id]=="0"
      page.replace_html 'report_desc',:partial=>'report_without_normal_header' if params[:id]=="1"
    end
  end
  
  def report_signature_info
    @setting = AssessmentReportSetting.get_multiple_settings_as_hash ["Signature", "SignLeftText", "SignCenterText", "SignRightText"]+
      AssessmentReportSetting::SIGN_KEYS, params[:plan_id]
    render :update do |page|
      page.replace_html 'report_sign',:partial=>'report_with_signature' if params[:id]=="0"
      page.replace_html 'report_sign',:text=>'' if params[:id]=="1"
    end
  end
  
  def preview
    @plan = AssessmentPlan.find params[:assessment_plan_id]
    @general_records = AssessmentReportSetting.result_as_hash @plan.id
    @batch=Batch.active.last(:joins=>:students)
    @grading_levels = (@batch.present? ? @batch.grading_level_list : GradingLevel.default)
    @config = Configuration.get_multiple_configs_as_hash ['InstitutionName', 'InstitutionAddress', 'InstitutionPhoneNo','InstitutionEmail','InstitutionWebsite']
    @student= @batch.students.last if @batch.present?
    render :pdf => "Normal Report Preview",:margin=>{:left=>10,:right=>10,:top=>5,:bottom=>5},:show_as_html=>params.key?(:d),:header => {:html => nil},:footer => {:html => nil}
  end
  
  def students_term_reports
    @term = AssessmentTerm.find params[:term_id]
    @plan = @term.assessment_plan
    @course = Course.find params[:course_id]
    @generated_report = @term.course_report(@course.id)
    @batches = @generated_report.batches.all(:conditions => { :is_deleted => false, :is_active => true }, :include => :students, :order => 'name')
    @batch = params[:batch_id].present? ? @course.batches.find(params[:batch_id]) : @batches.try(:first)
    @is_student_report = params[:student_id].present?
    @from_manage_exam = params[:from_manage_exam].present?
    if @batch.present?
      @report_published = @generated_report.fetch_status(@batch.id)
      @students = @generated_report.fetch_students(@batch.id)
      if @is_student_report
        @student = Student.find(params[:student_id])
        @schol_report = @student.individual_reports.first(:conditions => {:reportable_id => @term.id, :reportable_type => 'AssessmentTerm'})
      end
    end
  end
  
  def student_exam_reports
    @report = AssessmentGroup.find(params[:group_id], :include => 
        {:assessment_group_batches => [:subject_assessments, {:batch => :students}]})
    @academic_year = @report.academic_year
    @assessment_group = AssessmentGroup.find params[:group_id]
    @course = Course.find params[:course_id]
    @term = @report.parent
    @generated_report = @assessment_group.course_report(@course.id)
    @batches = @generated_report.batches.all(:conditions => { :is_deleted => false, :is_active => true }, :include => :students, :order => 'name')
    @batch = params[:batch_id].present? ? @course.batches.find(params[:batch_id]) : @batches.try(:first)
    @is_student_report = params[:student_id].present?
    @from_manage_exam = params[:from_manage_exam].present?
    if @batch.present?
      @report_published = @generated_report.fetch_status(@batch.id)
      @students = @generated_report.fetch_students(@batch.id)
      if @is_student_report
        @student =  Student.find params[:student_id]
        @schol_report = @student.individual_reports.first(:conditions => {:reportable_id => @assessment_group.id, :reportable_type => 'AssessmentGroup'}) if @student.present?
      end
    end
  end
  
  def refresh_students
    @batch = Batch.find params[:batch_id]
    @students = @batch.students.active
    @student = @students.try(:first)
    render :update do |page|
      page.replace_html 'student_report', :text => ''
      page.replace_html 'remarks_section', :text => ''
      page.replace_html 'pdf_link', :text => ''
      page.replace_html 'student_select', :partial => 'student_select', :locals => {:exam_type => params[:exam_type], :reportable_id => params[:reportable_id]}
    end
  end
  
  def refresh_report
    @reportable = if params[:exam_type] == 'term_report'
      AssessmentTerm.find params[:reportable_id]
    elsif params[:exam_type] == 'plan_report'
      AssessmentPlan.find params[:reportable_id]
    else
      AssessmentGroup.find params[:reportable_id]
    end
    @student = Student.find params[:student_id]
    @exam_type = params[:exam_type]
    @schol_report = @student.individual_reports.first(:conditions => {:reportable_id => @reportable.id, :reportable_type => @reportable.class.table_name.classify})
    render :update do |page|
      page.replace_html 'student_report', :partial => "student_#{params[:exam_type]}"
      page.replace_html 'remarks_section', :partial => "#{params[:exam_type]}_remarks_section"
      page.replace_html 'pdf_link', :partial => 'pdf_link'
    end
  end
  
  def student_term_report_pdf
    @student = Student.find params[:student_id]
    @term = AssessmentTerm.find params[:reportable_id]
    @generated_report = @term.course_report(@student.batch.course_id)
    #    @general_records = AssessmentReportSetting.result_as_hash @term.assessment_plan_id
    @general_records = AssessmentReportSettingCopy.result_as_hash(@generated_report.id, @term.assessment_plan_id)
    @schol_report = @student.individual_reports.first(:conditions => {:reportable_id => @term.id, :reportable_type => 'AssessmentTerm'})
    @grade_set = @term.final_assessment.grade_set if @term.final_assessment
    render :pdf => "Student Term Report - #{@student.admission_no}",:margin=>{:left=>10,:right=>10,:top=>5,:bottom=>5},:show_as_html=>params.key?(:d),:header => {:html => nil},:footer => { :html => nil }
  end
    
  def generate_exam_reports
    @report = AssessmentGroup.find(params[:group_id], :include => 
        {:assessment_group_batches => [:subject_assessments, {:batch => :students}]})
    @course = Course.find params[:course_id]
    @academic_year = @report.academic_year
    @term = @report.parent
    @all_batches = @course.batches_in_academic_year(@academic_year.id)
    @generated_report = (@report.course_report(@course.id)||GeneratedReport.new(:report => @report, :course_id => @course.id))
    if params[:generated_report].present?
      @generated_report.attributes = params[:generated_report]
      @generated_report.save
    end
    @batches = @generated_report.get_batches(@report, @course)
  end
  
  def generate_term_reports
    @report = AssessmentTerm.find(params[:term_id], :include => :assessment_groups)
    @course = Course.find params[:course_id]
    @generated_report = (@report.course_report(@course.id)||GeneratedReport.new(:report => @report, :course_id => @course.id))
    if params[:generated_report].present?
      @generated_report.attributes = params[:generated_report]
      @generated_report.save
    end
    @batches = @generated_report.get_term_batches(@course)
    @academic_year = @report.assessment_plan.academic_year
  end
  
  def regenerate_reports
    generated_report = GeneratedReport.find params[:report_id]
    report_batch = generated_report.generated_report_batches.first(:conditions => {:batch_id => params[:batch_id]})
    if report_batch and report_batch.update_attributes(:generation_status => 4)
      generated_report.generate_report
    end
    if generated_report.report_type == 'AssessmentTerm'
      redirect_to :action=> 'generate_term_reports', :term_id => generated_report.report_id, :course_id => params[:course_id]
    elsif generated_report.report_type == 'AssessmentPlan'
      redirect_to :action=> 'generate_planner_reports', :assessment_plan_id => generated_report.report_id, :course_id => params[:course_id]
    else
      redirect_to :action=> 'generate_exam_reports', :group_id => generated_report.report_id, :course_id => params[:course_id]
    end
  end
  
  def student_exam_report_pdf
    @student = Student.find params[:student_id]
    @assessment_group = AssessmentGroup.find params[:reportable_id]
    @generated_report = @assessment_group.course_report(@student.batch.course_id)
    @general_records = AssessmentReportSettingCopy.result_as_hash(@generated_report.id,@assessment_group.assessment_plan_id)
    #    @general_records = AssessmentReportSetting.result_as_hash @assessment_group.assessment_plan_id
    @schol_report = @student.individual_reports.first(:conditions => {:reportable_id => @assessment_group.id, :reportable_type => 'AssessmentGroup'})
    render :pdf => "Student Exam Report - #{@student.admission_no}", :margin=>{:left=>10,:right=>10,:top=>5,:bottom=>5},:show_as_html=>params.key?(:d),:header => {:html => nil},:footer => {:html => nil}
  end
  
  def publish_reports
    generated_report = GeneratedReport.find params[:report_id]
    generated_report.publish_reports(params[:course_id], params[:batch_id])
    flash[:notice] = "#{t('report_published')}"
    if generated_report.report_type == 'AssessmentTerm'
      redirect_to :action=> 'generate_term_reports', :term_id => generated_report.report_id, :course_id => params[:course_id]
    elsif generated_report.report_type == 'AssessmentPlan'
      redirect_to :action=> 'generate_planner_reports', :assessment_plan_id => generated_report.report_id, :course_id => params[:course_id]
    else
      redirect_to :action=> 'generate_exam_reports', :group_id => generated_report.report_id, :course_id => params[:course_id]
    end
  end
  
  def generate_planner_reports
    @report = AssessmentPlan.find params[:assessment_plan_id]
    @course = Course.find params[:course_id]
    @generated_report = (@report.course_report(@course.id)||GeneratedReport.new(:report => @report, :course_id => @course.id))
    if params[:generated_report].present?
      @generated_report.attributes = params[:generated_report]
      @generated_report.save
    end
    @batches_in_year = @course.batches_in_academic_year(@report.academic_year_id)
    @batches = @generated_report.get_plan_batches(@batches_in_year,@report)
    @academic_year = @report.academic_year
  end
  
  def students_planner_reports
    @plan = AssessmentPlan.find params[:plan_id]
    @course = Course.find params[:course_id]
    @generated_report = @plan.course_report(@course.id)
    @batches = @generated_report.batches.all(:conditions => { :is_deleted => false, :is_active => true }, :include => :students, :order => 'name')
    @batch = params[:batch_id].present? ? @course.batches.find(params[:batch_id]) : @batches.try(:first)
    @is_student_report = params[:student_id].present?
    @from_manage_exam = params[:from_manage_exam].present?
    if @batch.present?
      @report_published = @generated_report.fetch_status(@batch.id)
      @students = @generated_report.fetch_students(@batch.id)
      if @is_student_report
        @student = Student.find(params[:student_id])
        @schol_report = @student.individual_reports.first(:conditions => {:reportable_id => @plan.id, :reportable_type => 'AssessmentPlan'})
      end
    end 
  end
  
  def student_plan_report_pdf
    @student = Student.find params[:student_id]
    @assessment_plan = AssessmentPlan.find params[:reportable_id]
    @generated_report = @assessment_plan.course_report(@student.batch.course_id)
    @general_records = AssessmentReportSettingCopy.result_as_hash(@generated_report.id,@assessment_plan.id)
    @schol_report = @student.individual_reports.first(:conditions => {:reportable_id => @assessment_plan.id, :reportable_type => 'AssessmentPlan'})
    @grade_set = @assessment_plan.final_assessment.grade_set
    render :pdf => "Student PLan Report - #{@student.admission_no}", :margin=>{:left=>10,:right=>10,:top=>5,:bottom=>5},:show_as_html=>params.key?(:d),:header => {:html => nil},:footer => {:html => nil}
    
  end
  
end