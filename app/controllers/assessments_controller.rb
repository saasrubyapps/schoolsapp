class AssessmentsController < ApplicationController
  before_filter :login_required
  filter_access_to :all
  require 'lib/override_errors'
  helper OverrideErrors
  
  def show
    @assessment_group = AssessmentGroup.find params[:id]
    @course = Course.find(params[:course_id])
    @academic_year = @assessment_group.academic_year #AcademicYear.find params[:academic_year_id]
    @type = @assessment_group.exam_type
    batch_ids = @course.batches_in_academic_year(@academic_year.id).collect(&:id).uniq
    @assessments = @assessment_group.fetch_batch_assessments(batch_ids)
    @all_activated =  (batch_ids - @assessments.keys).blank?
    @assess_active = @assessments.present?
    @inactive_subjects = @assessment_group.fetch_inactive_assessments(batch_ids)
  end
  
  def link_attributes
    @batch = Batch.find params[:batch_id]
    @course = @batch.course
    @assessment_group = AssessmentGroup.find params[:assessment_group_id]
    @academic_year = AcademicYear.find params[:academic_year_id]
    @subjects = @batch.all_subjects
    @attribute_profiles = AssessmentAttributeProfile.all(:select=>'DISTINCT assessment_attribute_profiles.*',:joins=>:assessment_attributes)
    @assessment_group_batch = @assessment_group.assessment_group_batches.find_or_initialize_by_batch_id(@batch.id, 
      :include => {:subject_attribute_assessments => :assessment_attribute_profile})
    @subjects_with_marks = @assessment_group_batch.subjects_with_marks
    if request.post? or request.put?
      @assessment_group_batch.update_attributes(params[:assessment_group_batch])
      flash[:notice] = t('exams_activated_for_selected_batches')
      redirect_to :action => :show, :id => @assessment_group.id, :course_id => @course.id, :academic_year_id => @academic_year.id
    else
      @assessment_group_batch.build_attribute_assessments(@subjects)
    end
  end
  
  def update_profile_info
    profile = AssessmentAttributeProfile.find params[:id]
    render :json => {'attributes' => profile.assessment_attributes.count, 'formula' => profile.formula.capitalize, 'max_marks' => profile.maximum_marks}
  end
  
   def activate_exam
    @course = Course.find params[:course_id]
    find_assessment_group
    @batches = @course.batches_in_academic_year(@assessment_group.academic_year_id)
    @assessments = @assessment_group.fetch_batch_assessments @batches.collect(&:id).uniq
    @batches.reject! {|batch| @assessments.keys.include?(batch.id)}
    if request.post?
      batch_ids = params[:batch_ids]
      if batch_ids.present?
        @assessment_group.create_assessments(batch_ids,@course)
        flash[:notice] = t('exams_activated_for_selected_batches')
      else
        flash[:notice] = t('assessment_not_activated')
      end
      redirect_to :action => :show, :id => @assessment_group.id, :course_id => @course.id, :academic_year_id => @academic_year.id
    end
  end
  
  def activate_subject
    find_assessment_group
    batch = Batch.find params[:batch_id]
    subject = Subject.find params[:subject_id]
    assessment = @assessment_group.insert_subject_attribute_assessments(batch.id, subject, @type)
    flash[:notice] = (assessment ? t('exams_activated_for_subject', {:sub_name => subject.name}) : t('exams_not_activated_for_subject', {:sub_name => subject.name}))
    redirect_to :action => :show, :id => @assessment_group.id, :course_id => batch.course_id, :academic_year_id => @academic_year.id
  end

  def schedule_dates
    @assessment_group = AssessmentGroup.find params[:id]
    @course = Course.find params[:course_id]
    @batches = AssessmentSchedule.fetch_batches(@assessment_group, @course)
    @academic_year = @assessment_group.academic_year
    @assessment_schedule = AssessmentSchedule.new(:assessment_group_id => @assessment_group.id, :course_id => @course.id)
  end
  
  def edit_dates
    fetch_data
    @batches = AssessmentSchedule.fetch_batches(@assessment_group, @course, @assessment_schedule.id)
    @assessment_schedule.set_timings
    render :schedule_dates
  end
  
  def save_schedule
    @assessment_schedule = (params[:schedule_id].present? ? AssessmentSchedule.find(params[:id]) : AssessmentSchedule.new(params[:assessment_schedule]))
    if (params[:id].present? ? @assessment_schedule.update_attributes(params[:assessment_schedule]) : @assessment_schedule.save)
      flash[:notice] = t('flash1')
      redirect_to :action => :new, :id => params[:group_id], :schedule_id => @assessment_schedule.id
    else
      @assessment_group = AssessmentGroup.find params[:group_id]
      @course = Course.find params[:course_id]
#      @batches = @course.batches_in_academic_year(@assessment_group.academic_year_id) 
      @batches = AssessmentSchedule.fetch_batches(@assessment_group, @course)
      @academic_year = @assessment_group.academic_year
      render :schedule_dates
    end
  end
  
  def new
    fetch_data
    @batches = @assessment_schedule.batches.all(:include => [:course, :subjects])
    @assessment_form = AssessmentForm.build_form(@assessment_schedule, @assessment_group, @batches)
    @subjects = AssessmentForm.fetch_subjects(@batches)
  end
  
  def create
    @assessment_form = AssessmentForm.new(params[:assessment_form])
    fetch_data
    if @assessment_form.valid?
      @assessment_form.save_assessments(@assessment_schedule)
      redirect_to :action => :show, :id => @assessment_group.id, :course_id => @course.id, :academic_year_id => @academic_year.id
    else
      @batches = @assessment_schedule.batches.all(:include => [:course, :subjects])
      @subjects = AssessmentForm.fetch_subjects(@batches)
      render :new
    end
  end
  
  def edit
    @assessment_group = AssessmentGroup.find params[:id]
    @batch = Batch.find(params[:batch_id])
    @course = @batch.course
    @batches = @course.batches_in_academic_year(@assessment_group.academic_year_id)
    @assessment_schedule = @batch.assessment_schedules.first(:conditions => {:assessment_group_id => @assessment_group.id})
    @group_batch = AssessmentGroupBatch.first(:conditions => {:assessment_group_id => @assessment_group.id, :batch_id => @batch.id})
    @academic_year = AcademicYear.find(params[:academic_year_id])
    @subjects = AssessmentForm.fetch_batch_subjects(@batch)
    @assessment_marks = AssessmentGroupBatch.batch_subject_assessments_with_marks(@batch, @assessment_group)
  end
  
  def update
    @group_batch = AssessmentGroupBatch.find(params[:id])
    @assessment_group = @group_batch.assessment_group
    @batch = @group_batch.batch
    @course = @batch.course
    if @group_batch.update_attributes(params[:assessment_group_batch])
      redirect_to :action => :show, :id => @assessment_group.id, :course_id => @course.id, :academic_year_id => params[:academic_year_id]
    else
      @assessment_schedule = AssessmentSchedule.find(params[:schedule_id])
      @academic_year = AcademicYear.find(params[:academic_year_id])
      @subjects = AssessmentForm.fetch_batch_subjects(@batch)
      render :edit
    end
  end
  
  def attribute_scores
    fetch_action_details
    @assessment = SubjectAttributeAssessment.find(params[:assessment_id], 
      :include => [:subject, {:attribute_assessments => [{:assessment_marks => {:assessment => {:subject_attribute_assessment => :assessment_group_batch}}}, :assessment_attribute]}])
    @subject = @assessment.subject
    @students = @subject.fetch_students.all(:order => Student.sort_order,:conditions=>{ :batch_id => @batch.id})
    @profile = @assessment.assessment_attribute_profile
    #@formula = @profile.formula
    @formula = (@profile.formula == 'bestof')? 'Best of' : @profile.formula
    @grades = @assessment_group.grade_set.grades_json if (@assessment_group.scoring_type == 3)
    if request.put? or request.post?
      @assessment.update_attributes(params[:subject_attribute_assessment])
      if params[:save_and_submit_marks].present?
        if @assessment.submit_marks(@students)
          marks_submitted
        else
          flash.now[:notice] = t('please_enter_marks_for_all_students')
        end
      else
        flash.now[:notice] = t('scores_saved')
      end
    end
    @scores = @assessment.fetch_attribute_scores
  end
  
  def activity_scores
    fetch_action_details
    @students = @batch.students.all(:order => Student.sort_order)    
    @activity_assessment = ActivityAssessment.find(params[:assessment_id], :include => {:assessment_marks => {:assessment => :assessment_group_batch}})
    @grades = @assessment_group.grade_set.grades
    if request.put? or request.post?
      @activity_assessment.update_attributes(params[:activity_assessment])
      if params[:save_and_submit_marks].present?
        if @activity_assessment.submit_marks(@students)
          marks_submitted
        else
          flash.now[:notice] = t('please_enter_marks_for_all_students')
          @students_scores = @activity_assessment.build_student_marks(@students)
        end
      else
        @students_scores = @activity_assessment.build_student_marks(@students)
        flash.now[:notice] = t('scores_saved')
      end
    else
      @students_scores = @activity_assessment.build_student_marks(@students)
    end
  end
  
  def subject_scores
    @assessment = SubjectAssessment.find(params[:id], :include => {:assessment_marks => {:assessment => :assessment_group_batch}})
    fetch_header_details
    if request.put? or request.post?
      @assessment.update_attributes(params[:subject_assessment])
      if params[:save_and_submit_marks].present?
        if @assessment.submit_marks(@students)
          marks_submitted
        else
          flash.now[:notice] = t('please_enter_marks_for_all_students')
          @assessment_marks = @assessment.build_student_marks(@students)
        end
      else
        @assessment_marks = @assessment.build_student_marks(@students)
        flash.now[:notice] = t('scores_saved')
      end
    else
      @assessment_marks = @assessment.build_student_marks(@students)
    end
  end
  
  def exam_timings
    @courses = Course.active.all(:joins => {:assessment_group_batches => :subject_assessments}, :group => "id")
    @course = Course.find params[:course_id]
    @assessment_groups = AssessmentGroupBatch.course_assessment_groups(@course.id)
    @assessment_group = AssessmentGroup.find params[:group_id]
    @term=@assessment_group.parent
    @batches = AssessmentGroupBatch.assessment_group_batches(@assessment_group.id, @course.id)
    @assessments = AssessmentGroupBatch.course_assessments(@assessment_group.id, @course.id).group_by(&:batch)
  end
  
  def exam_timings_pdf
    @data_hash = AssessmentGroupBatch.fetch_exam_timings_data(params)
    render :pdf => 'exam_timings_pdf'
  end
  
  def fetch_groups
    @course = Course.find params[:course_id]
    @assessment_groups = AssessmentGroupBatch.course_assessment_groups(@course.id)
    if request.xhr?
      render :update do |page|
        page.replace_html 'groups_list', :partial=> 'groups_list'
        page.replace_html 'batches_list', :partial=> 'batches_list'
      end
    end
  end
  
  def fetch_batches
    @course = Course.find params[:course_id]
    @assessment_group = AssessmentGroup.find params[:group_id]
    @batches = AssessmentGroupBatch.assessment_group_batches(@assessment_group.id, @course.id)
    if request.xhr?
      render :update do |page|
        page.replace_html 'batches_list', :partial=> 'batches_list'
      end
    end
  end
  
  def fetch_timetables
    @course = Course.find params[:course_id]
    @assessment_group = AssessmentGroup.find params[:group_id]
    @term=@assessment_group.parent
    @assessments = AssessmentGroupBatch.course_assessments(@assessment_group.id, @course.id)
    unless params[:batch_id] == "All" 
      @assessments = @assessments.batch_equals(params[:batch_id]) 
      @batch = Batch.find(params[:batch_id]) 
    end
    @assessments = @assessments.group_by(&:batch)
    if request.xhr?
      render :update do |page|
        page.replace_html 'exam_timetables', :partial=> 'exam_timetables'
      end
    end
  end
  
  def reset_assessments
    @assessment_group = AssessmentGroup.find params[:assessment_group_id]
    @batch = Batch.find params[:batch_id]
    @agb = @assessment_group.assessment_group_batches.first(:conditions=>{:batch_id => @batch.id})
    if @agb
      @assessment_group.delete_schedules(@agb.course_id, @batch.id) if @agb.destroy
      flash[:notice] = t('exam_deleted')
    else
      flash[:notice] = t('exam_missing')
    end
  end
  
  def manage_derived_assessment
    @assessment_group = DerivedAssessmentGroup.find params[:id]
    @course = Course.find params[:course_id]
    @academic_year = AcademicYear.find params[:academic_year_id]
    @batches = @course.batches_in_academic_year(@academic_year.id)
    @subject_hash = @assessment_group.get_subject_list(@batches.collect(&:id).uniq)
    @assessments = @assessment_group.fetch_batch_assessments(@batches.collect(&:id).uniq)
    @assess_active = @assessments.present?
    @batches = @assessment_group.get_submitted_batches(@course)
  end
  
  def calculate_derived_marks
    @assessment_group = DerivedAssessmentGroup.find params[:assessment_id]
    batch = Batch.find params[:batch_id]
    @assessment_group.calculate_derived_marks(batch.id)
    redirect_to :action=> 'manage_derived_assessment', :id => @assessment_group.id, :course_id => batch.course_id, :academic_year_id => @assessment_group.academic_year_id
  end
  
  def show_derived_mark
    @course = Course.find params[:course_id]
    @batch = Batch.find params[:batch_id]
    @subject = Subject.find params[:sub_id]
    @academic_year = AcademicYear.find params[:academic_year_id]
    @assessment_group = DerivedAssessmentGroup.find params[:ag_id]
    @assessment_marks = @assessment_group.build_derived_marks(params[:batch_id],params[:sub_id])
  end
  
  private
  
  
  def fetch_data
    @assessment_schedule = AssessmentSchedule.find(params[:schedule_id]||params[:id])
    @assessment_group = @assessment_schedule.assessment_group
    @course = @assessment_schedule.course
    @academic_year = @assessment_group.academic_year
  end
  
  def fetch_action_details
    @batch = Batch.find params[:batch_id]
    @course = @batch.course
    find_assessment_group
    @agb = @assessment_group.assessment_group_batches.first(:conditions=>{:batch_id => @batch.id})
  end
  
  def find_assessment_group
    @assessment_group = AssessmentGroup.find params[:assessment_group_id]
    @academic_year = @assessment_group.academic_year
    @type = @assessment_group.exam_type
  end
  
  def fetch_header_details
    @subject = @assessment.subject
    @agb = @assessment.assessment_group_batch
    @batch = @agb.batch
    @assessment_group = @agb.assessment_group
    @academic_year = @assessment_group.academic_year
    @type = @assessment_group.exam_type
    @students = @subject.fetch_students.all(:order => Student.sort_order,:conditions=>{ :batch_id => @batch.id})
    @course = @batch.course
    @grades = @assessment_group.grade_set.try(:grades)
  end
  
  def marks_submitted
    redirect_to :action => :show, :id => @assessment_group.id, :course_id => @course.id
  end
end
