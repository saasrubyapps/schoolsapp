module FeeReceiptMod
  def invoice_student_data(student)
    @inv_data["full_name"] = student.full_name
    @inv_data["roll_number"] = student.roll_number
    @inv_data["admission_no"] = student.admission_no
    @inv_data["full_course_name"] = student.batch.full_name
    @inv_data["guardian_name"] = student.try(:immediate_contact).try(:full_name)
  end
  def get_student_invoice(fee,finance_type)
    @inv_data = Hash.new
    @inv_data["finance_type"] = fee.class.name
    @inv_data["invoice_no"] = fee.invoice_no    
    @inv_data["currency"] = Configuration.currency
    if fee.tax_enabled? and fee.tax_collections.present?      
      @inv_data['tax_slab_collections'] = fee.tax_collections.group_by {|tc| tc.tax_slab }
      @inv_data['total_tax'] = fee.tax_collections.present? ? fee.tax_collections.map do |x| 
        precision_label(x.tax_amount).to_f 
      end.sum.to_f : 0
    end
    @inv_data["collection"] = collection = fee.send("#{finance_type.singularize}_collection")
    @inv_data["done_transactions"] = done_transactions = @inv_data["paid_fees"] = fee.finance_transactions
    done_amount=0
    done_transactions.each { |t| done_amount += t.amount }
    @inv_data["done_amount"] = done_amount    
    case finance_type
    when 'finance_fees'
      student = fee.student
      invoice_student_data(student)
      @inv_data["due_date"] = collection.due_date
      @inv_data["fee_category"] = collection.fee_category
      # TODO Refractor this method
      # particulars
      @inv_data["fee_particulars"] = fee.finance_fee_particulars
      @inv_data["categorized_particulars"] = @inv_data["fee_particulars"].group_by(&:receiver_type)
      # discounts
      @inv_data["discounts"] = fee.fee_discounts    
      @inv_data["categorized_discounts"] = @inv_data["discounts"].group_by(&:master_receiver_type)
      
      @inv_data["total_discounts"]= 0
      @inv_data["total_payable"] = @inv_data["fee_particulars"].map { |s| s.amount }.sum.to_f
      
      @inv_data["total_discount"] = @inv_data["discounts"].map { |d| 
        d.master_receiver_type=='FinanceFeeParticular' ? 
          (d.master_receiver.amount * d.discount.to_f/(d.is_amount? ? d.master_receiver.amount : 100)) : 
          @inv_data["total_payable"] * d.discount.to_f/(d.is_amount? ? 
            @inv_data["total_payable"] : 100) }.sum.to_f unless @inv_data["discounts"].nil?        
      
      @inv_data["remainder_amount"] = @inv_data["total_payable"] - 
        @inv_data["total_discount"] - @inv_data["done_amount"]
      
      bal = (@inv_data["total_payable"] - @inv_data["total_discount"]).to_f
      
      days = fee.is_paid ? (done_transactions.present? ? 
          (done_transactions.last.created_at.to_date - collection.due_date.to_date) : 0) : 
        (Date.today - collection.due_date.to_date).to_i
      auto_fine = collection.fine
      if days > 0 and auto_fine
        @inv_data["fine_rule"]=auto_fine.fine_rules.find(:last, 
          :conditions => ["fine_days <= '#{days}' and 
                           created_at <= '#{collection.created_at}'"], 
          :order => 'fine_days ASC')
        @inv_data["fine_amount"] = @inv_data["fine_rule"].is_amount ? 
          @inv_data["fine_rule"].fine_amount : (bal*@inv_data["fine_rule"].fine_amount)/100 if 
        @inv_data["fine_rule"]
      end

      # Extract discount hash
      discounts=[]
            
      @inv_data["discounts"].each do |d|
        # discount_text = d.is_amount == true ? "Discount: #{d.name}" : "#{d.name}-#{d.discount}% "
        discount_text = d.is_amount == true ? "#{d.name} " : "#{d.name}-#{d.discount}% "
        if d.master_receiver_type=='FinanceFeeParticular'
          particular=d.master_receiver
          name="#{discount_text}  &#x200E;(#{particular.name}) &#x200E;"
          amount=particular.amount * d.discount.to_f/ (d.is_amount? ? particular.amount : 100)
        else
          name=discount_text
          amount = @inv_data["total_payable"] * d.discount.to_f/ (d.is_amount? ? @inv_data["total_payable"] : 100)
        end        
        discounts << {"name" => name, "amount" => amount}
      end
      
      @inv_data["discounts_list"]=discounts
      
      # Generate fine list
      fine_list=[]
      #      unless (@inv_data["fine"].present? && @inv_data["fine"].to_f > 0.0) #No need to list fines for particular wise payments
      #        fine=OpenStruct.new #TODO check whether this save memmory
      #        unless @fts_hash[ft.id]["fine_amount"].blank? || @fts_hash[ft.id]["fine_amount"].to_f ==0.0
      #          name= "#{t('fine_on')} " + format_date(Date.today)
      #          amount= @fts_hash[ft.id]["fine_amount"].to_f
      #          fine=OpenStruct.new({:name => name, :amount => amount})
      #          # fine_list<<fine #FIXME fine is duplicated
      #        end
      #      end
      # paid fees
      paid_fine=false
      paid_automatic_fine=0.to_f
      @inv_data["paid_fees"].each do |transaction|
        if transaction.fine_included
          paid_fine=true
          paid_automatic_fine = paid_automatic_fine + transaction.fine_amount.to_f if 
          transaction.description=='fine_amount_included'
          name= "#{t('fine_on')} " + format_date(transaction.transaction_date)
          amount= transaction.fine_amount.to_f          
          fine_list << {"name" => name, "amount" => amount}
        end
      end
      #fine rules
      unless @inv_data["financefee"].blank?
        if @inv_data["fine_rule"].present?
          name= t('fine_on') +' '+ format_date(collection.due_date.to_date + @inv_data["fine_rule"].fine_days.days)
          name += @inv_data["fine_rule"].is_amount ? "" : " (#{@inv_data["fine_rule"].fine_amount}&#x200E;%)"
          amount = @inv_data["fine_amount"].to_f - paid_automatic_fine                      
          fine_list<< {"name" => name, "amount" => amount} if amount > 0          
        end
      end
      @inv_data["fine_list"] = fine_list
      @inv_data["total_fine_amount"] = fine_list.sum { |fine| fine["amount"].to_f }
      # Total amount to pay
      @inv_data["total_amount_to_pay"] = @inv_data["total_payable"] - 
        @inv_data["total_discount"] + @inv_data["total_fine_amount"]
      @inv_data["total_amount_to_pay"] += @inv_data["total_tax"] if fee.tax_enabled? and fee.tax_collections.present?
      
      @inv_data["total_amount_paid"] = done_amount
      @inv_data["total_due_amount"] = @inv_data["total_amount_to_pay"] - 
        @inv_data["total_amount_paid"].to_f
    when 'hostel_fees'
      student = fee.student
      invoice_student_data(student)
      struct=OpenStruct.new({"name" => "Rent", "amount" => fee.rent})
      @inv_data["categorized_particulars"]=[[[struct]]]      
      #      @inv_data["particulars"]= {"name" => "Rent", "amount" => fee.rent}      
      @inv_data["due_date"] = collection.due_date
      #      @inv_data["invoice_no"]=""
      @inv_data["fine"] = fee.fine_amount.to_f      
      @inv_data["total_payable"] = @inv_data["rent"] = fee.rent
      @inv_data["total_discount"]=0.0
      @inv_data["fine_amount"]=@inv_data["fine"]
      @inv_data["total_amount_to_pay"] = fee.rent
      @inv_data["total_amount_to_pay"] += @inv_data["fine"]
      @inv_data["total_amount_to_pay"] += @inv_data["total_tax"].to_f if fee.tax_enabled? and fee.tax_collections.present?
      @inv_data["total_amount_paid"] = done_amount #ft.finance.rent.to_f-ft.hostel_fee_finance_transaction.transaction_balance.to_f+@inv_data["fine"]
      #      @inv_data["total_due_amount"] = fee.hostel_fee_finance_transaction.transaction_balance.to_f
      @inv_data["total_due_amount"]= (@inv_data["total_amount_to_pay"] - done_amount).to_f
      @inv_data["total_fine_amount"] = @inv_data["fine"]
    when 'transport_fees'      
      student = fee.receiver
      invoice_student_data(student)
      #        struct=OpenStruct.new({:name => "Fare", :amount => (ft.finance.bus_fare-ft.fine_amount)})      
      struct=OpenStruct.new({"name" => "Rent", "amount" => fee.bus_fare})
      # TODO Refractor the code base so that we don't have to use 3 dimensional array workaround
      @inv_data["categorized_particulars"]=[[[struct]]]      

      @inv_data["due_date"] = collection.due_date

      @inv_data["fine"] = fee.fine_amount.to_f      
      @inv_data["total_payable"] = @inv_data["bus_fare"] = fee.bus_fare
      @inv_data["total_discount"]=0.0
      @inv_data["fine_amount"]=@inv_data["fine"]
      @inv_data["total_amount_to_pay"]= fee.bus_fare
      @inv_data["total_amount_to_pay"] += @inv_data["fine"].to_f
      @inv_data["total_amount_to_pay"] += @inv_data["total_tax"].to_f if fee.tax_enabled? and fee.tax_collections.present?
      #      @inv_data["done_amount"]=done_amount
      @inv_data["total_amount_paid"]=done_amount
      @inv_data["total_due_amount"]= (@inv_data["total_amount_to_pay"] - done_amount).to_f
      @inv_data["total_fine_amount"]=@inv_data["fine"]    
    else
      return
    end
  end
  
  def get_student_fee_receipt_new(transaction_ids, particular_wise=false, particular_id=nil)
    @fts_hash=ActiveSupport::OrderedHash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
    fts=FinanceTransaction.find_all_by_id(transaction_ids, :joins => :transaction_ledger, 
      :include => {:finance => {:tax_collections => :tax_slab}},
      :select => "finance_transactions.*,if(finance_transaction_ledgers.transaction_mode = 'MULTIPLE',
                        finance_transactions.receipt_no, finance_transaction_ledgers.receipt_no) receipt_no")    
    overall_tax_enabled = false
    fts.each do |ft|
      tax_enabled = ft.finance.tax_enabled?
      overall_tax_enabled = true if tax_enabled
      @fts_hash[ft.id]["tax_enabled"]=tax_enabled
      @fts_hash[ft.id]["finance_type"]=ft.finance_type
      @fts_hash[ft.id]["receipt_no"]=ft.receipt_no
      @fts_hash[ft.id]["invoice_no"]= (ft.finance_type == "InstantFee" ? "" : ft.finance.invoice_no)
      @fts_hash[ft.id]["amount"]=ft.amount
      @fts_hash[ft.id]["transaction_date"]=ft.transaction_date
      @fts_hash[ft.id]["fine_amount"]=ft.fine_amount
      @fts_hash[ft.id]["auto_fine"]=ft.auto_fine
      @fts_hash[ft.id]["payment_mode"]=ft.payment_mode
      @fts_hash[ft.id]["payment_note"]=ft.payment_note
      @fts_hash[ft.id]["reference_no"]=ft.reference_no
      @fts_hash[ft.id]["currency"] = Configuration.currency
      if ft.payee_type == "Student"
        unless ft.payee.present?
          ars=ArchivedStudent.find_by_former_id(ft.payee_id)
          ft.payee=ars
          ft.payee_type='Student'
          ft.payee_id=ars.former_id
        end
        @fts_hash[ft.id]["payee"]["type"]=ft.payee_type
        @fts_hash[ft.id]["payee"]["full_name"]=ft.payee.full_name
        @fts_hash[ft.id]["payee"]["roll_number"]=ft.payee.roll_number
        @fts_hash[ft.id]["payee"]["admission_no"]=ft.payee.admission_no
        @fts_hash[ft.id]["payee"]["full_course_name"]=ft.fetch_finance_batch.nil? ? ft.payee.batch.full_name : ft.fetch_finance_batch.full_name
        @fts_hash[ft.id]["payee"]["guardian_name"]=ft.payee.try(:immediate_contact).try(:full_name)
      elsif ft.payee_type == "Employee"
        unless ft.payee.present?
          ae=ArchivedEmployee.find_by_former_id(ft.payee_id)
          ft.payee=ae
          ft.payee_type='Employee'
          ft.payee_id=ae.former_id
        end
        @fts_hash[ft.id]["payee"]["type"]=ft.payee_type
        @fts_hash[ft.id]["payee"]["full_name"]=ft.payee.full_name
        @fts_hash[ft.id]["payee"]["employee_number"]=ft.payee.employee_number
        @fts_hash[ft.id]["payee"]["employee_department_name"]=ft.payee.employee_department.name
      else
        @fts_hash[ft.id]["payee"]["full_name"]=ft.finance.guest_payee
      end
      
      if @fts_hash[ft.id]['tax_enabled'] and ft.trans_type == "collection_wise"# fetch taxes collected
        tax_collections = (ft.finance_type != 'FinanceFee') ? 
          ft.finance.tax_collections.all(:include => :tax_slab) :   tax_collections = ft.finance.tax_collections        
        @fts_hash[ft.id]['tax_slab_collections'] = tax_collections.group_by {|tc| tc.tax_slab }
        @fts_hash[ft.id]['total_tax'] = tax_collections.map {|x| precision_label(x.tax_amount).to_f }.sum.to_f
      end
      
      case ft.finance_type
      when 'FinanceFee'
        @fts_hash[ft.id]["collection"] = ft.finance.finance_fee_collection
        @fts_hash[ft.id]["financefee"] = ft.payee.finance_fee_by_date(ft.finance.finance_fee_collection)
        @fts_hash[ft.id]["due_date"] = ft.finance.finance_fee_collection.due_date
        @fts_hash[ft.id]["fee_category"] = FinanceFeeCategory.find(ft.finance.finance_fee_collection.fee_category_id, :conditions => ["is_deleted = false"])
        # TODO Refractor this method
        particular_and_discount_details1(ft)
        if ft.particular_wise?
          particular_payment_details(ft)
          # @fts_hash[ft.id]["due_date"] =""
          @fts_hash[ft.id][:is_particular_wise]=true
        else
          @fts_hash[ft.id][:is_particular_wise]=false
        end
        ff=ft.finance
        @fts_hash[ft.id]["paid_fees"] = ff.finance_transactions.all(:conditions => ["finance_transactions.id <= ? ", ft.id])
        @fts_hash[ft.id]["done_transactions"] = done_transactions = ff.finance_transactions.all(:conditions => ["finance_transactions.id < ? ", ft.id])
        done_amount=0
        done_transactions.each do |t|
          done_amount+=t.amount
        end
        @fts_hash[ft.id]["done_amount"] = done_amount
        @fts_hash[ft.id]["remainder_amount"] = @fts_hash[ft.id]["total_payable"]-@fts_hash[ft.id]["total_discount"]-@fts_hash[ft.id]["done_amount"]


        unless particular_id.nil?
          @fts_hash[ft.id]["previous_payments"]=ff.finance_transactions.sum('finance_fee_particulars.amount', :joins => 'inner join particular_payments on particular_payments.finance_transaction_id=finance_transactions.id left outer join finance_fee_particulars on finance_fee_particulars.id=particular_payments.finance_fee_particular_id', :conditions => ["finance_transactions.id < ? and finance_fee_particulars.id =? ", ft.id, particular_id])
        end
        bal=(@fts_hash[ft.id]["total_payable"]-@fts_hash[ft.id]["total_discount"]).to_f
        days=(ft.transaction_date.to_date - ft.finance.finance_fee_collection.due_date.to_date).to_i
        auto_fine=ft.finance.finance_fee_collection.fine
        if days > 0 and auto_fine
          @fts_hash[ft.id]["fine_rule"]=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{ft.finance.finance_fee_collection.created_at}'"], :order => 'fine_days ASC')
          #            unless particular_wise==true
          @fts_hash[ft.id]["fine_amount"]=@fts_hash[ft.id]["fine_rule"].is_amount ? @fts_hash[ft.id]["fine_rule"].fine_amount : (bal*@fts_hash[ft.id]["fine_rule"].fine_amount)/100 if @fts_hash[ft.id]["fine_rule"]
          #            end
        end

        # Extract discount hash
        discounts=[]
        unless ft.particular_wise? #No need to list discounts for particular wise payments
          discount=OpenStruct.new #TODO check whether this save memmory
          @fts_hash[ft.id]["discounts"].each do |d|
            # discount_text = d.is_amount == true ? "Discount: #{d.name}" : "#{d.name}-#{d.discount}% "
            discount_text = d.is_amount == true ? "#{d.name} " : "#{d.name}-#{d.discount}% "
            if d.master_receiver_type=='FinanceFeeParticular'
              particular=d.master_receiver
              name="#{discount_text}  &#x200E;(#{particular.name}) &#x200E;"
              amount=particular.amount * d.discount.to_f/ (d.is_amount? ? particular.amount : 100)
            else
              name=discount_text
              amount=@fts_hash[ft.id]["total_payable"] * d.discount.to_f/ (d.is_amount? ? @fts_hash[ft.id]["total_payable"] : 100)
            end
            discount=OpenStruct.new({:name => name, :amount => amount})
            discounts<<discount
          end
        end
        @fts_hash[ft.id][:discounts_list]=discounts
        # get particular wise hash
        if ft.particular_wise?
          particulars=[]
          @fts_hash[ft.id]["particular_payments"].each do |payment|
            amount=payment.particular_amount.to_f
            remaining_balance=payment.particular_amount.to_f - previous_payments(ft.id, payment.particular_id).to_f
            discount=payment.payment_discount.to_f
            payment_amount=payment.payment_amount.to_f
            amount_paid=payment_amount-discount
            balance=remaining_balance- (amount_paid+discount)
            particular=OpenStruct.new({
                :name => payment.particular_name,
                :amount => amount,
                :remaining_balance => remaining_balance,
                :discount => discount,
                :amount_paid => amount_paid,
                :balance => balance
              })
            particulars<<particular
          end
          @fts_hash[ft.id][:particulars_list]=particulars
        end
        # Generate fine list
        fine_list=[]
        unless ft.particular_wise? || (@fts_hash[ft.id]["fine"].present? && @fts_hash[ft.id]["fine"].to_f > 0.0) #No need to list fines for particular wise payments
          fine=OpenStruct.new #TODO check whether this save memmory
          unless @fts_hash[ft.id]["fine_amount"].blank? || @fts_hash[ft.id]["fine_amount"].to_f ==0.0
            name= "#{t('fine_on')} " + format_date(Date.today)
            amount= @fts_hash[ft.id]["fine_amount"].to_f
            fine=OpenStruct.new({:name => name, :amount => amount})
            # fine_list<<fine #FIXME fine is duplicated
          end
        end
        # paid fees
        paid_fine=false
        paid_automatic_fine=0.to_f
        @fts_hash[ft.id]["paid_fees"].each do |transaction|
          if transaction.fine_included
            paid_fine=true
            paid_automatic_fine = paid_automatic_fine+transaction.fine_amount.to_f if transaction.description=='fine_amount_included'
            name= "#{t('fine_on')} " + format_date(transaction.transaction_date)
            amount= transaction.fine_amount.to_f
            fine=OpenStruct.new({:name => name, :amount => amount})
            fine_list<<fine
          end
        end
        #fine rules
        unless @fts_hash[ft.id]["financefee"].blank?
          if @fts_hash[ft.id]["fine_rule"].present?
            name= t('fine_on') +' '+ format_date(@fts_hash[ft.id]["collection"].due_date.to_date+@fts_hash[ft.id]["fine_rule"].fine_days.days)
            name+= @fts_hash[ft.id]["fine_rule"].is_amount ? "" : " (#{@fts_hash[ft.id]["fine_rule"].fine_amount}&#x200E;%)"
            amount=@fts_hash[ft.id]["fine_amount"].to_f-paid_automatic_fine
            if amount > 0
              fine=OpenStruct.new({:name => name, :amount => amount})
              fine_list<<fine
            end
          end
        end
        @fts_hash[ft.id][:fine_list]=fine_list
        @fts_hash[ft.id]["total_fine_amount"]=fine_list.sum { |fine| fine.amount }
        # Total amount to pay
        @fts_hash[ft.id]["total_amount_to_pay"] = @fts_hash[ft.id]["total_payable"]-@fts_hash[ft.id]["total_discount"]+@fts_hash[ft.id]["total_fine_amount"]
        @fts_hash[ft.id]["total_amount_to_pay"] += @fts_hash[ft.id]["total_tax"] if tax_enabled and @fts_hash[ft.id]['tax_slab_collections'].present?
        @fts_hash[ft.id]["previously_paid_amount"]=@fts_hash[ft.id]["done_amount"]
        @fts_hash[ft.id]["total_amount_paid"]=@fts_hash[ft.id]["previously_paid_amount"]+@fts_hash[ft.id]["amount"]
        @fts_hash[ft.id]["total_due_amount"]=@fts_hash[ft.id]["total_amount_to_pay"]-@fts_hash[ft.id]["total_amount_paid"]+0
      when 'HostelFee'
        @fts_hash[ft.id][:is_particular_wise]=false
        @fts_hash[ft.id]["collection"] = ft.finance.hostel_fee_collection
        struct=OpenStruct.new({:name => "Rent", :amount => ft.finance.rent})
        @fts_hash[ft.id]["categorized_particulars"]=[[[struct]]]
        @fts_hash[ft.id]["done_amount"]=0.0
        @fts_hash[ft.id]["due_date"] =ft.finance.hostel_fee_collection.due_date
        @fts_hash[ft.id]["receipt_no"]=ft.receipt_no
        @fts_hash[ft.id]["invoice_no"]=ft.finance.invoice_no
        ff=ft.finance
        @fts_hash[ft.id]["fine"] = ft.fine_included ? ft.fine_amount : 0.0
        if ft.fine_included
          name= "#{t('fine_on')} " + format_date(ft.transaction_date)
          amount= ft.fine_amount.to_f
          fine=OpenStruct.new({:name => name, :amount => amount})
          @fts_hash[ft.id][:fine_list]=[fine]
        end
        # Summary
        #        @fts_hash[ft.id]["total_payable"]=ft.finance.rent-ft.fine_amount
        @fts_hash[ft.id]["total_payable"]=ft.finance.rent
        #        @fts_hash[ft.id]["total_payable"] += @fts_hash[ft.id]["total_tax"] if tax_enabled
        @fts_hash[ft.id]["total_discount"]=0.0
        @fts_hash[ft.id]["fine_amount"]=@fts_hash[ft.id]["fine"]
        @fts_hash[ft.id]["total_amount_to_pay"]=ft.finance.rent
        @fts_hash[ft.id]["total_amount_to_pay"] += @fts_hash[ft.id]["fine"]
        @fts_hash[ft.id]["total_amount_to_pay"] += @fts_hash[ft.id]["total_tax"] if tax_enabled
        @fts_hash[ft.id]["previously_paid_amount"]=ft.hostel_fee_finance_transaction.previous_payments
        @fts_hash[ft.id]["done_amount"]=@fts_hash[ft.id]["previously_paid_amount"]
        @fts_hash[ft.id]["total_amount_paid"]=ft.finance.rent.to_f-ft.hostel_fee_finance_transaction.transaction_balance.to_f+@fts_hash[ft.id]["fine"]
        @fts_hash[ft.id]["total_due_amount"]=ft.hostel_fee_finance_transaction.transaction_balance.to_f
        @fts_hash[ft.id]["total_fine_amount"]=@fts_hash[ft.id]["fine"]
      when 'TransportFee'
        @fts_hash[ft.id][:is_particular_wise]=false
        @fts_hash[ft.id]["done_amount"]=0.0
        #        struct=OpenStruct.new({:name => "Fare", :amount => (ft.finance.bus_fare-ft.fine_amount)})
        struct=OpenStruct.new({:name => "Fare", :amount => (ft.finance.bus_fare)})
        # TODO Refractor the code base so that we don't have to use 3 dimensional array workaround
        @fts_hash[ft.id]["categorized_particulars"]=[[[struct]]]

        @fts_hash[ft.id]["collection"] = ft.finance.transport_fee_collection
        @fts_hash[ft.id]["due_date"] =ft.finance.transport_fee_collection.due_date
        @fts_hash[ft.id]["receipt_no"]=ft.receipt_no
        @fts_hash[ft.id]["invoice_no"]=ft.finance.invoice_no
        ff=ft.finance
        @fts_hash[ft.id]["fine"] = ft.fine_included ? ft.fine_amount : 0.0
        if ft.fine_included
          name= "#{t('fine_on')} " + format_date(ft.transaction_date)
          amount= ft.fine_amount.to_f
          fine=OpenStruct.new({:name => name, :amount => amount})
          @fts_hash[ft.id][:fine_list]=[fine]
        end
        # Summary
        #        @fts_hash[ft.id]["total_payable"]=ft.finance.bus_fare-ft.fine_amount
        @fts_hash[ft.id]["total_payable"]=ft.finance.bus_fare
        #        @fts_hash[ft.id]["total_payable"] += @fts_hash[ft.id]["total_tax"] if tax_enabled
        @fts_hash[ft.id]["total_discount"]=0.0
        @fts_hash[ft.id]["fine_amount"]=@fts_hash[ft.id]["fine"]
        @fts_hash[ft.id]["total_amount_to_pay"]=ft.finance.bus_fare
        @fts_hash[ft.id]["total_amount_to_pay"] += @fts_hash[ft.id]["fine"]
        @fts_hash[ft.id]["total_amount_to_pay"] += @fts_hash[ft.id]["total_tax"] if tax_enabled
        @fts_hash[ft.id]["previously_paid_amount"]=ft.transport_fee_finance_transaction.previous_payments
        @fts_hash[ft.id]["done_amount"]=@fts_hash[ft.id]["previously_paid_amount"]
        @fts_hash[ft.id]["total_amount_paid"]=ft.finance.bus_fare.to_f-ft.transport_fee_finance_transaction.transaction_balance.to_f+@fts_hash[ft.id]["fine"]
        @fts_hash[ft.id]["total_due_amount"]=ft.transport_fee_finance_transaction.transaction_balance.to_f
        @fts_hash[ft.id]["total_fine_amount"]=@fts_hash[ft.id]["fine"]
      when 'InstantFee'
        # TODO attributes
        collection=ft.finance.attributes.merge({:category_name => ft.finance.category_name})
        @fts_hash[ft.id]["due_date"]=@fts_hash[ft.id]["collection"]["pay_date"]
        @fts_hash[ft.id]["collection"] = OpenStruct.new(collection) #using struct to maintain backword compatiability , convert back to hash in future
        ff=ft.finance
        @fts_hash[ft.id]["instant_fee_details"] = ff.instant_fee_details
        # Instant Fee is always particular wise
        @fts_hash[ft.id][:is_particular_wise]=true
        particulars=[]
        tax_amounts=[]
        total_discount = 0
        total_amount = 0
        @fts_hash[ft.id]["instant_fee_details"].each do |payment|
          amount=payment.amount.to_f
          remaining_balance=0.0
          discount=((payment.discount/100)*payment.amount).to_f
          payment_amount=payment.net_amount
          amount_paid=payment_amount
          total_amount += (payment_amount.to_f - payment.tax_amount.to_f)
          balance=0.0
          total_discount += discount
          tax_amount = payment.tax_amount
          tax_amounts << payment.tax_amount
          particular=OpenStruct.new({
              :name => payment.particular_name,
              :amount => amount,
              :remaining_balance => remaining_balance,
              :discount => discount,
              :amount_paid => amount_paid,
              :balance => balance,
              :tax_amount => tax_amount
            })
          particulars<<particular
        end
        @fts_hash[ft.id][:particulars_list]=particulars
        #summary
        @fts_hash[ft.id]["total_payable"]= total_amount #ft.amount
        @fts_hash[ft.id]["total_discount"]=total_discount
        @fts_hash[ft.id]["total_tax"] = tax_amounts.compact.sum
        @fts_hash[ft.id]["fine_amount"]=0.0
        @fts_hash[ft.id]["total_amount_to_pay"]=ft.amount
        @fts_hash[ft.id]["previously_paid_amount"]=0.0
        @fts_hash[ft.id]["total_amount_paid"]=@fts_hash[ft.id]["amount"]
        @fts_hash[ft.id]["total_due_amount"]=@fts_hash[ft.id]["total_amount_to_pay"]-@fts_hash[ft.id]["total_amount_paid"]+0
        @fts_hash[ft.id]["total_fine_amount"]=0.0        
      else
        return
      end
    end    
    @tax_config = Configuration.get_multiple_configs_as_hash(['FinanceTaxIdentificationLabel',
        'FinanceTaxIdentificationNumber']) if overall_tax_enabled
  end

  def get_receipt_dummy_data(particular_wise=false,tax_enabled=false)
    # FIXME use HashWithIndifferentAccess ?
    transaction_hash=HashWithIndifferentAccess.new { |h, k| h[k] = Hash.new(&h.default_proc) }
    transaction_id=0
    transaction_hash[transaction_id][:finance_type]=""
    transaction_hash[transaction_id][:receipt_no]="#{Configuration.find_by_config_key('fee_receipt_no')}1"
    transaction_hash[transaction_id][:invoice_no]="#{Configuration.find_by_config_key('fee_invoice_no')}1"
    transaction_hash[transaction_id][:amount]=50.0
    transaction_hash[transaction_id][:fine_amount]=""
    transaction_hash[transaction_id][:auto_fine]=""
    transaction_hash[transaction_id][:payment_mode]="Cash"
    transaction_hash[transaction_id][:payment_note]="Cash payment"
    transaction_hash[transaction_id][:reference_no]=""
    transaction_hash[transaction_id][:currency] = Configuration.currency
    transaction_hash[transaction_id][:payee]={
      :type => "Student",
      :full_name => "John Doe",
      :roll_number => "2",
      :admission_no => "23",
      :full_course_name => "Standard 1",
      :guardian_name => "Joseph Doe",

    }
    transaction_hash[transaction_id][:tax_enabled]=tax_enabled
    particulars=[]
    4.times do |i|
      struct=OpenStruct.new({:name => "Particular "+(i+1).to_s, :amount => 100})
      particulars<<struct
    end
    transaction_hash[transaction_id][:categorized_particulars]=[[particulars]]
    transaction_hash[transaction_id][:collection]=FinanceFeeCollection.new(:name => "Quarter 1 Fees")
    transaction_hash[transaction_id][:collection].invoice_enabled = (Configuration.get_config_value('EnableInvoiceNumber').to_i == 1)
    transaction_hash[transaction_id][:due_date]=Date.today # ?
    transaction_hash[transaction_id][:transaction_date]=Date.today # ?
    transaction_hash[transaction_id][:fee_category]=""
    transaction_hash[transaction_id][:paid_fees]=""
    transaction_hash[transaction_id][:done_amount]=50.0
    transaction_hash[transaction_id][:total_amount_to_pay]=""
    # discounts
    discount=OpenStruct.new({:name => "discount", :amount => 100})
    tax_slab = TaxSlab.new({:name => "Tax 5%", :rate => 5})
    tax=OpenStruct.new({:tax_amount => 100}) if tax_enabled
    transaction_hash[transaction_id][:tax_slab_collections]={tax_slab => [tax]} if tax_enabled
    discounts=[discount]
    transaction_hash[transaction_id][:discounts_list]=discounts
    
    #fine
    transaction_hash[transaction_id][:fine_amount]=10.0
    fine=OpenStruct.new({:name => "fine", :amount => 10.0})
    fine_list=[fine]
    transaction_hash[transaction_id][:fine_list]=fine_list
    # Particular amount
    particular_payment={
      :particular_name => "test",
      :particular_amount => "200",
      :remaining_balance => "150",
      :discount => "50",
      :paid_amount => "100",
      :balance => "50"
    }
    particular_payments=[particular_payment]
    transaction_hash[transaction_id][:particular_payments]=particular_payments
    transaction_hash[transaction_id][:is_particular_wise]=false
    # Summary
    transaction_hash[transaction_id][:total_payable]=400
    transaction_hash[transaction_id][:total_discount]=100.0
    transaction_hash[transaction_id][:total_tax]=100 if tax_enabled
    # transaction_hash[transaction_id][:fine_amount]=10.0
    transaction_hash[transaction_id][:total_fine_amount]=transaction_hash[transaction_id][:fine_amount]
    transaction_hash[transaction_id][:total_amount_to_pay]=400.0
    transaction_hash[transaction_id][:total_amount_to_pay] = transaction_hash[transaction_id][:total_payable]-transaction_hash[transaction_id][:total_discount]+transaction_hash[transaction_id][:fine_amount]
    transaction_hash[transaction_id][:total_amount_to_pay] += transaction_hash[transaction_id][:total_tax] if tax_enabled
    transaction_hash[transaction_id][:previously_paid_amount]=transaction_hash[transaction_id][:done_amount]
    transaction_hash[transaction_id][:total_amount_paid]=transaction_hash[transaction_id][:previously_paid_amount]+transaction_hash[transaction_id][:amount]
    transaction_hash[transaction_id][:total_due_amount]=transaction_hash[transaction_id][:total_amount_to_pay]-transaction_hash[transaction_id][:total_amount_paid]+0
    return transaction_hash
  end

  def particular_payment_details(ft)
    @fts_hash[ft.id]["particular_payments"]=ft.particular_payments.all(
      :select => 'finance_fee_particulars.id as particular_id,
                      finance_fee_particulars.name as particular_name,
                      finance_fee_particulars.amount as particular_amount,
                      particular_payments.id as payment_id,
                      particular_payments.amount as payment_amount,
                      particular_discounts.id discount_id,
                      sum(particular_discounts.discount) as payment_discount',
      :joins => 'left outer join particular_discounts on particular_discounts.particular_payment_id=particular_payments.id
                     left outer join finance_fee_particulars on finance_fee_particulars.id=particular_payments.finance_fee_particular_id',
      :group => 'finance_fee_particulars.id')
  end

  def previous_payments(ftid, pid)
    ft=FinanceTransaction.find ftid
    ff=ft.finance
    payments=ff.finance_transactions.sum('particular_payments.amount', :joins => 'inner join particular_payments on particular_payments.finance_transaction_id=finance_transactions.id left outer join finance_fee_particulars on finance_fee_particulars.id=particular_payments.finance_fee_particular_id', :conditions => ["finance_transactions.id < ? and finance_fee_particulars.id =? ", ft.id, pid])
    payments
  end

  def particular_and_discount_details1(ft)
    @fts_hash[ft.id]["fee_particulars"] = ft.finance.finance_fee_collection.finance_fee_particulars.
      all(:conditions => "batch_id=#{ft.finance.batch_id}").select do |par| 
      (par.receiver_type=='Student' and 
          par.receiver_id==ft.payee_id) ? par.receiver=ft.payee : par.receiver; (par.receiver.present?) and 
        (par.receiver==ft.payee or 
          par.receiver==ft.finance.student_category or 
          par.receiver==ft.finance.batch) 
    end
    @fts_hash[ft.id]["categorized_particulars"]=@fts_hash[ft.id]["fee_particulars"].group_by(&:receiver_type)
    @fts_hash[ft.id]["discounts"] = ft.finance.finance_fee_collection.fee_discounts.all(:conditions => 
        "batch_id=#{ft.finance.batch_id}").select do |par| (par.receiver.present?) and 
        ((par.receiver==ft.finance.student or 
            par.receiver==ft.finance.student_category or 
            par.receiver==ft.finance.batch) and 
          (par.master_receiver_type!='FinanceFeeParticular' or 
            (par.master_receiver_type=='FinanceFeeParticular' and 
              (par.master_receiver.receiver.present? and 
                @fts_hash[ft.id]["fee_particulars"].collect(&:id).include? par.master_receiver_id) and 
              (par.master_receiver.receiver==ft.finance.student or 
                par.master_receiver.receiver==ft.finance.student_category or 
                par.master_receiver.receiver==ft.finance.batch)))) 
    end
    
    @fts_hash[ft.id]["categorized_discounts"]=@fts_hash[ft.id]["discounts"].
      group_by(&:master_receiver_type)
    @fts_hash[ft.id]["total_discounts"]= 0
    @fts_hash[ft.id]["total_payable"] = @fts_hash[ft.id]["fee_particulars"].map { |s| s.amount }.
      sum.to_f
    @fts_hash[ft.id]["total_discount"] = @fts_hash[ft.id]["discounts"].map { |d| 
      d.master_receiver_type=='FinanceFeeParticular' ? 
        (d.master_receiver.amount * d.discount.to_f/(d.is_amount? ? d.master_receiver.amount : 100)) : 
        @fts_hash[ft.id]["total_payable"] * d.discount.to_f/(d.is_amount? ? 
          @fts_hash[ft.id]["total_payable"] : 100) }.sum.to_f unless @fts_hash[ft.id]["discounts"].nil?    
  end

  def payer_sql
    conditions = []
    conditions << "p.school_id=#{MultiSchool.current_school.id}" if defined?(MultiSchool)
    conditions << "1=1" unless defined?(MultiSchool)
    where_condition = conditions.join(" AND ")
    payer="(select p.id payer_id,concat(p.first_name,' ',p.middle_name,' ',p.last_name) payer_name,null batchid,NULL as payer_batch_dept1,p.admission_no payer_no,'Student' payer_type,'Student' payer_type_info from students p  where (#{where_condition}))"
    payer+=" UNION ALL (select p.former_id payer_id,concat(p.first_name,' ',p.middle_name,' ',p.last_name) payer_name,NULL batchid,null as  payer_batch_dept1,p.admission_no payer_no,'Student' payer_type,'Archived Student' payer_type_info from archived_students p where (#{where_condition}))"
    payer+=" UNION ALL (select p.id payer_id,concat(p.first_name,' ',p.middle_name,' ',p.last_name) payer_name,NULL batchid,employee_departments.name payer_batch_dept1,p.employee_number payer_no,'Employee' payer_type,'Employee' payer_type_info from employees p inner join employee_departments on employee_departments.id=p.employee_department_id where (#{where_condition}))"
    payer+=" UNION ALL (select p.former_id payer_id,concat(p.first_name,' ',p.middle_name,' ',p.last_name) payer_name,NULL batchid,employee_departments.name payer_batch_dept1,p.employee_number payer_no,'Employee' payer_type,'Archived Employee' payer_type_info from archived_employees p inner join employee_departments on employee_departments.id=p.employee_department_id where (#{where_condition}))"
    payer+=" UNION ALL (select p.id payer_id,p.guest_payee payer_name,NULL batchid,NULL payer_batch_dept1,NULL payer_no,'Guest' payer_type,'Guest' payer_type_info from instant_fees p where p.guest_payee IS NOT NULL and (#{where_condition}))" if (FedenaPlugin.can_access_plugin?("fedena_instant_fee"))
    payer
  end

  def fee_sql(query=nil)
    unless query.nil?
      havings = ["having collection_name LIKE '%#{query}%'"]
    else
      havings = []
    end
    conditions=[]
    conditions << "cf.school_id=#{MultiSchool.current_school.id}" if defined?(MultiSchool)
    conditions << "1=1" unless defined?(MultiSchool)
    where_condition = conditions.join(" AND ")
    fee="(select c_batch.bat_id as batchid,c_batch.payer_batch_dept1 payer_batch_dept1,fc.name collection_name,cf.id fin_id,'FinanceFee' fin_type,cf.fee_collection_id collection_id from finance_fees cf inner join finance_fee_collections fc on fc.id=cf.fee_collection_id inner join  #{select_corresponding_batch_sql} c_batch on c_batch.bat_id = cf.batch_id where (#{where_condition}) group by cf.id #{havings})"
    fee+=" UNION ALL (select c_batch.bat_id as batchid,c_batch.payer_batch_dept1 payer_batch_dept1, hc.name collection_name,cf.id fin_id,'HostelFee' fin_type,cf.hostel_fee_collection_id collection_id from hostel_fees cf inner join hostel_fee_collections hc on hc.id=cf.hostel_fee_collection_id  inner join  #{select_corresponding_batch_sql} c_batch on c_batch.bat_id = cf.batch_id  where (#{where_condition}) group by cf.id #{havings})" if (FedenaPlugin.can_access_plugin?("fedena_hostel"))
    fee+=" UNION ALL (select c_batch.bat_id as batchid,c_batch.payer_batch_dept1 payer_batch_dept1, tc.name collection_name,cf.id fin_id,'TransportFee' fin_type,cf.transport_fee_collection_id collection_id from transport_fees cf inner join transport_fee_collections tc on tc.id=cf.transport_fee_collection_id  left outer join  #{select_corresponding_batch_sql} c_batch on c_batch.bat_id = cf.groupable_id  and cf.groupable_type='Batch' where (#{where_condition}) group by cf.id #{havings})" if (FedenaPlugin.can_access_plugin?("fedena_transport"))
    fee+=" UNION ALL (select c_instant_batch.bat_id as batchid,c_instant_batch.payer_batch_dept1 payer_batch_dept1,  ic.name collection_name,cf.id fin_id,'InstantFee' fin_type,cf.instant_fee_category_id collection_id from instant_fees cf inner join instant_fee_categories ic on ic.id=cf.instant_fee_category_id left outer join  #{select_corresponding_batch_sql} c_instant_batch on c_instant_batch.bat_id = cf.groupable_id  and cf.groupable_type='Batch'  where ic.name IS NOT NULL and (#{where_condition}) group by cf.id #{havings})" if (FedenaPlugin.can_access_plugin?("fedena_instant_fee"))
    fee+=" UNION ALL (select c_instant_batch.bat_id as batchid,c_instant_batch.payer_batch_dept1 as payer_batch_dept1,cf.custom_category collection_name,cf.id fin_id,'InstantFee' fin_type,NULL collection_id from instant_fees cf left outer join #{select_corresponding_batch_sql} c_instant_batch on c_instant_batch.bat_id = cf.groupable_id  where cf.custom_category IS NOT NULL and (#{where_condition}) group by cf.id #{havings})" if (FedenaPlugin.can_access_plugin?("fedena_instant_fee"))
    fee
  end

  def select_corresponding_batch_sql
    "(select concat(co.course_name,' - ',bat.name) as payer_batch_dept1,bat.id as bat_id from batches bat inner join courses co on co.id = bat.course_id)"
  end

  def fetched_fee_receipts(*args)
    FinanceTransaction.scoped(
      :select => "finance_transactions.id ftid,finance_transactions.trans_type,
                          finance_transactions.finance_type fin_type,finance_transactions.reference_no,
                          if(finance_transaction_ledgers.transaction_mode = 'MULTIPLE',
                             finance_transactions.receipt_no, finance_transaction_ledgers.receipt_no ) receipt_no,
                          finance_transactions.payment_mode,
                          finance_transactions.transaction_date,finance_transactions.amount,f.batchid,
                          u.payer_type_info,u.payer_name,f.collection_id,u.payer_type,u.payer_no,u.payer_id,
                          (ifnull(f.payer_batch_dept1,u.payer_batch_dept1)) as payer_batch_dept ,
                          concat(c.first_name,' ',c.last_name) full_user_name,c.username uname,c.id user_id, f.collection_name,f.fin_id",
      :joins => "inner join finance_transaction_ledgers ON finance_transaction_ledgers.id = finance_transactions.transaction_ledger_id
                        inner join (#{fee_sql}) f on finance_transactions.finance_id=f.fin_id and 
                                                                 finance_transactions.finance_type=f.fin_type 
                        left outer join (#{payer_sql}) u on (ifnull(finance_transactions.payee_id,finance_transactions.finance_id)=u.payer_id) and 
                                                                 ifnull(finance_transactions.payee_type,'Guest')=u.payer_type 
                        left outer join users c on finance_transactions.user_id = c.id",
      :conditions => ["((finance_type IN ('TransportFee','HostelFee','FinanceFee','InstantFee')))"],
      :order => "finance_transactions.id desc"
    )
  end

  def get_student_fee_receipt(transaction_ids, particular_wise=false, particular_id=nil)
    @fts_hash=Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
    fts=FinanceTransaction.find_all_by_id(transaction_ids, :joins => :transaction_ledger, 
      :include => [{:finance => {:tax_collections => :tax_slab}}],
      :select => "finance_transactions.*,if(finance_transaction_ledgers.transaction_mode = 'MULTIPLE',
                        finance_transactions.receipt_no, finance_transaction_ledgers.receipt_no) receipt_no")
    overall_tax_enabled = false    
    fts.each do |ft|
      tax_enabled = ft.finance.tax_enabled?
      overall_tax_enabled = true if tax_enabled
      @fts_hash[ft.id]["finance_type"]=ft.finance_type
      @fts_hash[ft.id]["receipt_no"]=ft.receipt_no
      @fts_hash[ft.id]["invoice_no"]=ft.finance.invoice_no
      @fts_hash[ft.id]["amount"]=ft.amount
      @fts_hash[ft.id]["transaction_date"]=ft.transaction_date
      @fts_hash[ft.id]["fine_amount"]=ft.fine_amount
      @fts_hash[ft.id]["auto_fine"]=ft.auto_fine
      @fts_hash[ft.id]["payment_mode"]=ft.payment_mode
      @fts_hash[ft.id]["payment_note"]=ft.payment_note
      @fts_hash[ft.id]["reference_no"]=ft.reference_no
      @fts_hash[ft.id]["currency"] = Configuration.currency
      if ft.payee_type == "Student"
        unless ft.payee.present?
          ars=ArchivedStudent.find_by_former_id(ft.payee_id)
          ft.payee=ars
          ft.payee_type='Student'
          ft.payee_id=ars.former_id
        end
        @fts_hash[ft.id]["payee"]["type"]=ft.payee_type
        @fts_hash[ft.id]["payee"]["full_name"]=ft.payee.full_name
        @fts_hash[ft.id]["payee"]["roll_number"]=ft.payee.roll_number
        @fts_hash[ft.id]["payee"]["admission_no"]=ft.payee.admission_no
        @fts_hash[ft.id]["payee"]["full_course_name"]= ft.fetch_finance_batch.nil? ? ft.payee.batch.full_name : ft.fetch_finance_batch.full_name
        @fts_hash[ft.id]["payee"]["guardian_name"]=ft.payee.try(:immediate_contact).try(:full_name)
      elsif ft.payee_type == "Employee"
        unless ft.payee.present?
          ae=ArchivedEmployee.find_by_former_id(ft.payee_id)
          ft.payee=ae
          ft.payee_type='Employee'
          ft.payee_id=ae.former_id
        end
        @fts_hash[ft.id]["payee"]["type"]=ft.payee_type
        @fts_hash[ft.id]["payee"]["full_name"]=ft.payee.full_name
        @fts_hash[ft.id]["payee"]["employee_number"]=ft.payee.employee_number
        @fts_hash[ft.id]["payee"]["employee_department_name"]=ft.payee.employee_department.name
      else
        @fts_hash[ft.id]["payee"]["full_name"]=ft.finance.guest_payee
      end
      @fts_hash[ft.id]["finance_transaction"]=ft
      
      @fts_hash[ft.id]["tax_enabled"] = tax_enabled      
      if @fts_hash[ft.id]['tax_enabled'] # fetch taxes collected        
        tax_collections = (ft.finance_type != 'FinanceFee') ? 
          ft.finance.tax_collections.all(:include => :tax_slab) :   tax_collections = ft.finance.tax_collections        
        @fts_hash[ft.id]['tax_slab_collections'] = tax_collections.group_by {|tc| tc.tax_slab }
        @fts_hash[ft.id]['total_tax'] = tax_collections.map { |x| precision_label(x.tax_amount).to_f }.sum.to_f
      end
        
      case ft.finance_type
      when 'FinanceFee'
        @fts_hash[ft.id]["collection"] = ft.finance.finance_fee_collection
        @fts_hash[ft.id]["financefee"] = ft.payee.finance_fee_by_date(ft.finance.finance_fee_collection)
        @fts_hash[ft.id]["due_date"] = ft.finance.finance_fee_collection.due_date
        @fts_hash[ft.id]["fee_category"] = FinanceFeeCategory.find(ft.finance.finance_fee_collection.fee_category_id, :conditions => ["is_deleted = false"])

        particular_and_discount_details1(ft)
        if particular_wise==true
          particular_payment_details(ft)
        end
        ff=ft.finance
        @fts_hash[ft.id]["paid_fees"] = ff.finance_transactions.all(:conditions => ["finance_transactions.id <= ? ", ft.id])
        @fts_hash[ft.id]["done_transactions"] = done_transactions = ff.finance_transactions.all(:conditions => ["finance_transactions.id < ? ", ft.id])
        done_amount=0
        done_transactions.each do |t|
          done_amount+=t.amount
        end
        @fts_hash[ft.id]["done_amount"] = done_amount
        @fts_hash[ft.id]["remainder_amount"] = @fts_hash[ft.id]["total_payable"]-@fts_hash[ft.id]["total_discount"]-@fts_hash[ft.id]["done_amount"]
        unless particular_id.nil?
          @fts_hash[ft.id]["previous_payments"]=ff.finance_transactions.sum('finance_fee_particulars.amount', :joins => 'inner join particular_payments on particular_payments.finance_transaction_id=finance_transactions.id left outer join finance_fee_particulars on finance_fee_particulars.id=particular_payments.finance_fee_particular_id', :conditions => ["finance_transactions.id < ? and finance_fee_particulars.id =? ", ft.id, particular_id])
        end
        bal=(@fts_hash[ft.id]["total_payable"]-@fts_hash[ft.id]["total_discount"]).to_f
        days=(ft.transaction_date.to_date - ft.finance.finance_fee_collection.due_date.to_date).to_i
        auto_fine=ft.finance.finance_fee_collection.fine
        if auto_fine.present? and (days > 0 and (!ft.finance.is_paid) or ft.successor_transactions.present?)
          @fts_hash[ft.id]["fine_rule"]=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{ft.finance.finance_fee_collection.created_at}'"], :order => 'fine_days ASC')
          unless particular_wise==true
            @fts_hash[ft.id]["fine_amount"]=@fts_hash[ft.id]["fine_rule"].is_amount ? @fts_hash[ft.id]["fine_rule"].fine_amount : (bal*@fts_hash[ft.id]["fine_rule"].fine_amount)/100 if @fts_hash[ft.id]["fine_rule"]
          end
        end

        
      when 'HostelFee'
        @fts_hash[ft.id]["collection"] = ft.finance.hostel_fee_collection        
        # @finance_transaction=ft
        @fts_hash[ft.id]["finance"]=ft.finance
        @fts_hash[ft.id]["fine"] = ft.fine_included ? ft.fine_amount : 0.0
      when 'TransportFee'
        # @finance_transaction=ft
        @fts_hash[ft.id]["finance"]=ft.finance
        @fts_hash[ft.id]["collection"] = ft.finance.transport_fee_collection
        ff=ft.finance
        @fts_hash[ft.id]["fine"] = ft.fine_included ? ft.fine_amount : 0.0
        @fts_hash[ft.id]["due_date"] = ft.finance.transport_fee_collection.due_date
      when 'InstantFee'
        @fts_hash[ft.id]["collection"] = ft.finance
        ff=ft.finance
        @fts_hash[ft.id]["instant_fee_details"] = ff.instant_fee_details
      else
        return
      end
    end
    @tax_config = Configuration.get_multiple_configs_as_hash(['FinanceTaxIdentificationLabel',
        'FinanceTaxIdentificationNumber']) if overall_tax_enabled
  end

  def overall_receipt_details(student_id, transaction_ids)
    @overall_receipt=OverallReceipt.new(student_id, transaction_ids)

  end


  class OverallReceipt < Struct.new(:student_id, :transaction_ids, :query_sql)

    def fetch_collection_details
      process_fine FinanceTransaction.find_by_sql(query_sql)
    end

    def fetch_fees_summary
      collection_details = fetch_collection_details
      total_amount_to_pay = collection_details.map do |fcd| 
        fcd.balance.to_f + fcd.balance_addition_actual_amount.to_f + fcd.fine_amount.to_f 
      end.sum
      
      total_due_amount = collection_details.map do |fcd| 
        fcd.balance.to_f + fcd.balance_addition_due_amount.to_f 
      end.sum
      
      total_transaction_amount = collection_details.map do |fcd| 
        fcd.transaction_amount.to_f 
      end.sum
      
      FinanceTransaction.find(transaction_ids.last,
        :include => :transaction_ledger,
        :joins => "INNER JOIN users ON users.id=finance_transactions.user_id",
        :select => "finance_transactions.reference_no AS reference_no,               
                          finance_transactions.transaction_ledger_id,            
                          finance_transactions.transaction_date AS transaction_date,
                          finance_transactions.payment_mode, CONCAT(users.first_name,' ',users.last_name) AS cname,
                          users.id AS usersid,finance_transactions.payment_note,
                          #{total_amount_to_pay} AS total_amount_to_pay, 
                          #{total_due_amount} AS total_due_amount,
                          #{total_transaction_amount} AS total_transaction_amount")
    end


    def process_fine arr
      arr.each { |f| f.fine_amount=(f.is_amount==0 ? f.fine_amount : (f.balance.to_f+f.balance_addition_actual_amount.to_f)*f.fine_amount.to_f/100) }
    end

    def query_sql
      "#{finance_fee_sql} #{transport_fee_sql} #{hostel_fee_sql}".squish
    end

    def finance_fee_sql    
      "SELECT finance_transactions.transaction_date, finance_transactions.finance_id,
                   finance_transactions.id AS transaction_id, 
                   IF(ftl.transaction_mode = 'MULTIPLE', finance_transactions.receipt_no, ftl.receipt_no) receipt_no, 
                   `finance_transactions`.amount AS transaction_amount,
                   ff.balance AS balance, ffc.name AS fee_collection_name, ffc.due_date,
                   fine_rules.fine_amount AS fine_amount,
                   (SELECT IFNULL(SUM(finance_transactions.fine_amount),0)
                       FROM finance_transactions
                     WHERE finance_transactions.finance_id=ff.id AND 
                                 finance_transactions.finance_type='FinanceFee' AND 
                                 description= 'fine_amount_included'
                   ) AS paid_fine,
                   fine_rules.is_amount,
                   (SELECT IFNULL(SUM(ft.amount-ft.fine_amount),0)
                       FROM finance_transactions ft
                     WHERE ft.finance_id=ff.id AND ft.finance_type='FinanceFee' and 
                                 ft.id > finance_transactions.id
                   ) AS balance_addition_due_amount,
                   (SELECT IFNULL(SUM(ft.amount-ft.fine_amount),0)
                       FROM finance_transactions ft
                     WHERE ft.finance_id=ff.id AND 
                                 ft.finance_type='FinanceFee'
                   ) AS balance_addition_actual_amount,
                   ffc.tax_enabled AS tax_enabled,
                   ffc.invoice_enabled AS invoice_enabled,
                   IF(ffc.invoice_enabled,fi.invoice_number,NULL) AS invoice_number,
                   IF(ffc.tax_enabled,ff.tax_amount,0) AS tax_amount
          FROM `finance_transactions`
  INNER JOIN finance_transaction_ledgers ftl
              ON ftl.id = finance_transactions.transaction_ledger_id
  INNER JOIN finance_fees ff 
              ON ff.id=finance_transactions.finance_id AND 
                    finance_transactions.finance_type='FinanceFee'
    LEFT JOIN fee_invoices fi
              ON fi.fee_id = finance_transactions.finance_id AND 
                        fi.fee_type = 'FinanceFee'
  INNER JOIN finance_fee_collections ffc 
              ON ffc.id=ff.fee_collection_id #{fine_join_sql}
       WHERE (finance_transactions.id in (#{transaction_ids.join(',')})) AND 
                   (`finance_transactions`.`school_id` = #{MultiSchool.current_school.id})"
    
    end

    def transport_fee_sql
      if defined? TransportFeeCollection
        "UNION ALL 
              SELECT finance_transactions.transaction_date, finance_transactions.finance_id,
                          finance_transactions.id AS transaction_id,
                          IF( ftl.transaction_mode = 'MULTIPLE', finance_transactions.receipt_no, ftl.receipt_no
                          ) receipt_no,
                          finance_transactions.amount AS transaction_amount,
                          tfft.transaction_balance AS balance, tfc.name AS fee_collection_name, tfc.due_date,
                          '' AS fine_amount,'' AS is_amount, 0 AS paid_fine,0 AS balance_addition_due_amount,
                          (tf.bus_fare-tfft.transaction_balance) AS balance_addition_actual_amount,
                          tfc.tax_enabled AS tax_enabled,
                          tfc.invoice_enabled AS invoice_enabled,
                          IF(tfc.invoice_enabled,fi.invoice_number,NULL) AS invoice_number,
                          IF(tfc.tax_enabled,tf.tax_amount,0) AS tax_amount
                FROM `finance_transactions`
       INNER JOIN finance_transaction_ledgers  ftl
                    ON ftl.id = finance_transactions.transaction_ledger_id
       INNER JOIN transport_fee_finance_transactions tfft 
                    ON tfft.finance_transaction_id=finance_transactions.id
       INNER JOIN transport_fees tf 
                    ON tf.id=finance_transactions.finance_id AND 
                          finance_transactions.finance_type='TransportFee'
          LEFT JOIN fee_invoices fi
                    ON fi.fee_id = finance_transactions.finance_id AND 
                        fi.fee_type = 'TransportFee'
       INNER JOIN transport_fee_collections tfc
                    ON tfc.id=tf.transport_fee_collection_id
             WHERE (finance_transactions.id in (#{transaction_ids.join(',')})) AND 
                          (`finance_transactions`.`school_id` = #{MultiSchool.current_school.id})"
      else
        # plugin_disabled_fetch_query
      end
    end

    def hostel_fee_sql
      if defined? HostelFeeCollection
        "UNION ALL 
              SELECT finance_transactions.transaction_date, finance_transactions.finance_id,
                           finance_transactions.id AS transaction_id, 
                          IF( ftl.transaction_mode = 'MULTIPLE', finance_transactions.receipt_no, ftl.receipt_no ) receipt_no,
                          `finance_transactions`.amount AS transaction_amount,
                          hfft.transaction_balance AS balance, hfc.name AS fee_collection_name, hfc.due_date,
                          '' AS fine_amount, '' AS is_amount, 0 AS paid_fine, 0 as balance_addition_due_amount,
                          (hf.rent-hfft.transaction_balance) AS balance_addition_actual_amount,
                          hfc.tax_enabled AS tax_enabled,
                          hfc.invoice_enabled AS invoice_enabled,
                          IF(hfc.invoice_enabled,fi.invoice_number,NULL) AS invoice_number,
                          IF(hfc.tax_enabled,hf.tax_amount,0) AS tax_amount
                 FROM `finance_transactions`
         INNER JOIN finance_transaction_ledgers ftl
                     ON ftl.id = finance_transactions.transaction_ledger_id
         INNER JOIN hostel_fee_finance_transactions hfft 
                     ON hfft.finance_transaction_id=finance_transactions.id
         INNER JOIN hostel_fees hf 
                     ON hf.id=finance_transactions.finance_id AND 
                           finance_transactions.finance_type='HostelFee'
         LEFT JOIN fee_invoices fi
                   ON fi.fee_id = finance_transactions.finance_id AND 
                         fi.fee_type = 'HostelFee'
         INNER JOIN hostel_fee_collections hfc 
                     ON hfc.id=hf.hostel_fee_collection_id
               WHERE (finance_transactions.id in (#{transaction_ids.join(',')})) AND 
                           (`finance_transactions`.`school_id` = #{MultiSchool.current_school.id})"
      else
        # plugin_disabled_fetch_query
      end
    end

    def plugin_disabled_fetch_query
      "UNION ALL SELECT finance_transactions.transaction_date,finance_transactions.finance_id,finance_transactions.id as transaction_id,finance_transactions.receipt_no,`finance_transactions`.amount as transaction_amount,'' as balance,'' as fee_collection_name,'' as due_date,'' as fine_amount,'' as is_amount,0 as paid_fine,0 as balance_addition_due_amount,0 as balance_addition_actual_amount   FROM `finance_transactions`
       WHERE (finance_transactions.id in (#{transaction_ids.join(',')})) and (`finance_transactions`.`school_id` = #{MultiSchool.current_school.id})"
    end

    def fine_join_sql
      "LEFT JOIN `fines` ON `fines`.id = `ffc`.fine_id AND fines.is_deleted is false
          LEFT JOIN `fine_rules` ON fine_rules.fine_id = fines.id  AND fine_rules.id= (
            SELECT max(id) FROM fine_rules ffr WHERE ffr.fine_id=fines.id AND ffr.created_at <= ffc.created_at AND ffr.fine_days <= DATEDIFF(
              COALESCE(Date(finance_transactions.transaction_date),CURDATE()),
              ffc.due_date
            )
        )"
    end

    def automatic_finance_fee_fine(batch_id, finance_fees_id, transaction_id, transaction_date)
      
      joins="INNER JOIN `finance_fee_collections` 
                             ON `finance_fee_collections`.id = `finance_fees`.fee_collection_id
                 INNER JOIN `fines` 
                             ON `fines`.id = `finance_fee_collections`.fine_id AND fines.is_deleted is false
                    LEFT JOIN `fine_rules` 
                             ON fine_rules.fine_id = fines.id  AND 
                                   fine_rules.id = (SELECT id 
                                                               FROM fine_rules ffr 
                                                             WHERE ffr.fine_id=fines.id AND 
                                                                         ffr.created_at <= finance_fee_collections.created_at AND 
                                                                         ffr.fine_days <= DATEDIFF(COALESCE(Date('#{transaction_date}'),
                                                                                                                                    CURDATE()),
                                                                                                     finance_fee_collections.due_date)
                                                             ORDER BY fine_days DESC LIMIT 1)"
      # old code for fetching fine rule 
      # (SELECT max(id) FROM fine_rules ffr WHERE ffr.fine_id=fines.id AND ffr.created_at <= finance_fee_collections.created_at AND ffr.fine_days <= DATEDIFF(COALESCE(Date('#{transaction_date}'),CURDATE()),finance_fee_collections.due_date))
      conditions= "finance_fees.batch_id=#{batch_id} and finance_fees.id=#{finance_fees_id}"
      FinanceFee.find(:all, :joins => joins,
        :select => "SUM(IF(fine_rules.is_amount,
                                       fine_rules.fine_amount,
                                       ((finance_fees.balance + 
                                         (SELECT IFNULL(SUM(finance_transactions.amount - 
                                                                          finance_transactions.fine_amount +
                                                                          IF(finance_fees.tax_enabled,
                                                                              finance_transactions.tax_amount,
                                                                              0)                                                                         
                                                                         ),0) 
                                             FROM finance_transactions
                                           WHERE finance_transactions.finance_id=finance_fees.id AND 
                                                       finance_transactions.finance_type='FinanceFee'
                                          ) - IF(finance_fees.tax_enabled,
                                                  finance_fees.tax_amount,0)
                                         ) * fine_rules.fine_amount/100))) AS fine_amount,
                           (SELECT IFNULL(SUM(finance_transactions.fine_amount),0) 
                               FROM finance_transactions  
                             WHERE finance_transactions.finance_id=#{finance_fees_id} AND 
                                         finance_transactions.finance_type='FinanceFee' and 
                                         finance_transactions.id<=#{transaction_id}
                           ) AS all_paid_fine_amount,
                           (SELECT IF(finance_fees.is_paid,
                                            SUM(finance_transactions.auto_fine),
                                            IFNULL(SUM(finance_transactions.fine_amount),0)
                                         ) 
                               FROM finance_transactions  
                             WHERE finance_transactions.finance_id=#{finance_fees_id} AND 
                                         finance_transactions.finance_type='FinanceFee' AND 
                                         description= 'fine_amount_included' and 
                                          finance_transactions.id<=#{transaction_id}
                            ) as auto_paid_fine,
                            finance_fees.is_paid as is_paid",
        :conditions => conditions,
        :group => "finance_fees.student_id").first
    end

  end

end