require 'i18n'
class DelayedSendSmsToFeeDefaulters
  attr_accessor :student_ids
  include ApplicationHelper
  def initialize(student_ids)
    @students = Student.fee_defaulters_info(student_ids)
  end
    
  def perform
    sms_setting = SmsSetting.new()
      if sms_setting.student_sms_active
        @students.each do |student|
            if student.is_sms_enabled
              student_message = I18n.t('student_fee_defaulter_message',
                :student_first_name=>student.first_name,
                :currency=>Configuration.currency,
                :balance=>precision_label(student.balance),
                :date_today=>format_date(FedenaTimeSet.current_time_to_local_time(Time.now).to_date))
              recipients = []
              recipients.push student.phone2.split(',') if student.phone2.present?
              if recipients.present?
                recipients.flatten!
                recipients.uniq!
                SmsManager.new(student_message,recipients).perform
              end
            end
        end
      end
      if sms_setting.parent_sms_active
        @students.each do |student|
            if student.immediate_contact.present? and student.is_sms_enabled
              guardian_message = I18n.t('parent_fee_defaulter_message',
                :student_full_name=>student.full_name,
                :currency=>Configuration.currency,
                :balance=>precision_label(student.balance),
                :date_today=>format_date(FedenaTimeSet.current_time_to_local_time(Time.now).to_date)) 
              guardian = student.immediate_contact
              recipients = []
              recipients.push guardian.mobile_phone.split(',') if guardian.mobile_phone.present?
              if recipients.present?
                recipients.flatten!
                recipients.uniq!
                SmsManager.new(guardian_message,recipients).perform
              end
            end
        end
      end
  end
  
  def initialize_with_school_id(student_ids)
    @school_id = MultiSchool.current_school.id
    initialize_without_school_id(student_ids)
  end
  
  alias_method_chain :initialize,:school_id
  
  
  def perform_with_school_id
    MultiSchool.current_school = School.find(@school_id)
    perform_without_school_id
  end
  
  alias_method_chain :perform,:school_id
  
end
