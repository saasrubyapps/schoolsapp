class DelayedNotification
  def initialize(*args)
    opts = args.extract_options!
    @recipient_ids  = Array(opts[:recipient_ids]).flatten.uniq
    @notification_id = opts[:notification_id]
  end
  
  def perform
    notification = Notification.find @notification_id
    if notification
      @recipient_ids.reject!(&:blank?)
      @recipient_ids.each do |rec_id|
        notification.notification_recipients.create(:recipient_id=>rec_id)
      end
    end
  end
  
  def initialize_with_school_id(*args)
    @school_id = MultiSchool.current_school.id
    initialize_without_school_id(*args)
  end
  
  alias_method_chain :initialize,:school_id
  
  
  def perform_with_school_id
    MultiSchool.current_school = School.find(@school_id)
    perform_without_school_id
  end
  
  alias_method_chain :perform,:school_id
  
end