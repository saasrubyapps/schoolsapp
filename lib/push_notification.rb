class PushNotification
  attr_accessor :data, :user_ids, :school_id

  def initialize (data, user_ids, school_id = nil)
    @data = data
    @user_ids = user_ids
    @school_id = school_id || MultiSchool.current_school.id
  end

  def self.push_notify (opts)
    return unless Setting.available?
    payload = new(opts[:data], opts[:user_ids], opts[:school_id])
    Delayed::Job.enqueue(payload, :queue=> 'push_notification')
  end

  def perform
  end

  class Setting < ActiveRecord::Base
    self.table_name = 'mobile_push_settings'

    def self.available?
      table_exists? && exists?(:school_id => MultiSchool.current_school.try(:id) || 0)
    end
  end

end