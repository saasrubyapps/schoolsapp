class DelayedAssessmentMarksSubmission < Struct.new(:assessment_id, :assessment_type)
  def perform
    assessments = assessment_type.constantize.find(assessment_id)
    
    @rollback = false
    ActiveRecord::Base.transaction do
      send("#{assessment_type.underscore}_conversion", assessments)
      raise ActiveRecord::Rollback if @rollback
      end
  ensure
    Array(assessments).each{|assessment| assessment.update_attributes(:submission_status => 3)} if @rollback
  end
  
  def assessment_group_batch_conversion(agb)
    group = agb.assessment_group
    grade_set = group.grade_set
    grades = grade_set.grades.sorted_marks if grade_set
    all_groups = group.all_assessment_groups
    batch = agb.batch
    batch_groups = batch.assessment_group_batches
#    subjects = Subject.find all_groups.first.assessment_group_batches.detect {|agbs| agbs.batch_id == batch.id}.try(:subjects_list).uniq
    sub_ids = []
    all_groups.each do |gp|
      gp.assessment_group_batches.all(:conditions => {:batch_id => batch.id}).each do |agbs|
        sub_ids = sub_ids + agbs.subject_ids
      end
    end
    subjects = Subject.find sub_ids.uniq
    ConvertedAssessmentMark.delete_all(["assessment_group_batch_id = ?", agb.id])
    batch.students.each do |student|
      student_subjects = student.subjects.collect(&:id)
      subjects.each do |subject|
        next if subject.elective_group_id? and !(student_subjects.include? subject.id)
        all_marks = {}
        derived_maximum = group.maximum_marks_for(subject,batch.course)
        all_groups.each do |ag|
          b_group = batch_groups.detect{|g| g.assessment_group_id == ag.id }
          group_maximum = ag.maximum_marks_for(subject,batch.course)
          converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == subject.id and 
                  cam.markable_type == 'Subject' and cam.assessment_group_batch_id == b_group.id}
          stu_mark = ((converted_mark.present? and !converted_mark.is_absent) ? converted_mark.mark.to_f : 0)
          all_marks[ag.id] = {:mark => stu_mark, :max_mark => group_maximum,
            :converted_mark => ((stu_mark/group_maximum)*derived_maximum), :grade => converted_mark.try(:grade), :credit_points => converted_mark.try(:credit_points)
          } if converted_mark.present?
        end
        final_score = all_marks.present? ? group.calculate_final_score(all_marks,derived_maximum) : nil
        if final_score.present?
          converted_mark = ConvertedAssessmentMark.new(:markable => subject, :assessment_group_batch_id => agb.id, 
          :assessment_group_id => group.id, :student_id => student.id, :mark => final_score, :actual_mark => all_marks)
          case group.scoring_type
          when 1
            converted_mark.passed = (converted_mark.mark >= group.minimum_marks.to_f)
          when 3
            percentage = ((final_score/derived_maximum)*100)
            if grade_set
              mark_grade = grade_set.select_grade_for(grades, percentage)
              converted_mark.grade = mark_grade.try(:name)
              converted_mark.passed = mark_grade.try(:pass_criteria)
              converted_mark.credit_points = mark_grade.try(:credit_points)
            end
          end
          @rollback = true unless converted_mark.save
        end
      end
    end
    @rollback = true unless agb.update_attributes(:marks_added => true, :submission_status => 2)
  end
  
  def subject_attribute_assessment_conversion(subject_assessment)
    attribute_assessments = subject_assessment.attribute_assessments
    group_batch = subject_assessment.assessment_group_batch
    change_marks_added_status(group_batch, false)
    group = group_batch.assessment_group
    grade_set = group.grade_set
    grades = grade_set.grades.sorted_marks if grade_set
    attribute_profile = subject_assessment.assessment_attribute_profile
    subject = subject_assessment.subject
    group_maximum = group.maximum_marks_for(subject,group_batch.batch.course)
    student_marks = {}
    ConvertedAssessmentMark.delete_all(["assessment_group_batch_id = ? AND markable_id = ? AND markable_type = ?", 
        group_batch.id, subject.id, subject.class.to_s])
    attribute_assessments.each do |assessment|
      attribute = assessment.assessment_attribute
      assessment.assessment_marks.each do |assessment_mark|
        marks = student_marks[assessment_mark.student_id]||{}
        unless assessment_mark.is_absent
          grade = grades.detect{|g| g.id == assessment_mark.grade_id} if assessment_mark.grade_id
          marks[assessment.assessment_attribute_id] = {:mark => assessment_mark.marks, :grade => assessment_mark.grade, 
            :credit_points => grade.try(:credit_points), :max_mark => attribute.maximum_marks.to_f, 
            :converted_mark => ((assessment_mark.marks.to_f/attribute.maximum_marks.to_f)*attribute_profile.maximum_marks.to_f)}
        end
        student_marks[assessment_mark.student_id] = marks
      end
    end
    student_marks.each do |student_id, marks|
      is_absent = marks.blank?
      converted_mark = ConvertedAssessmentMark.new(:markable => subject, :assessment_group_batch_id => group_batch.id, 
        :assessment_group_id => group.id, :student_id => student_id, :is_absent => is_absent)
      unless is_absent
        subject_mark = attribute_profile.calculate_final_score(marks) 
        converted_mark.mark = ((subject_mark/attribute_profile.maximum_marks.to_f)*group_maximum)
        converted_mark.actual_mark = marks
        case group.scoring_type
        when 1
          converted_mark.passed = (converted_mark.mark >= group.minimum_marks.to_f)
        when 3
          percentage = ((converted_mark.mark/group_maximum)*100)
          mark_grade = grade_set.select_grade_for(grades, percentage) if grade_set
          converted_mark.attributes = {:grade => mark_grade.try(:name), :credit_points => 
              mark_grade.try(:credit_points), :passed => mark_grade.try(:pass_criteria)} if mark_grade
        end
      end
      @rollback = true unless converted_mark.save
    end
    attribute_assessments.each do |assessment|
      @rollback = true unless assessment.update_attributes(:marks_added => true, :submission_status => 2)
    end
    @rollback = true unless subject_assessment.update_attributes(:marks_added => true, :submission_status => 2 ) unless @rollback
    
    other_attributes = subject_assessment.attribute_assessments.all(:conditions => {:marks_added => false})
    if other_attributes.blank?
      subject_assessment.reload
      subject_assessment.marks_added = true
      subject_assessment.send(:update_without_callbacks)
    end
    other_assessments = group_batch.subject_attribute_assessments.all(:conditions => {:marks_added => false})
    if other_assessments.blank?
      change_marks_added_status(group_batch, true)
    end
  end
  
  def activity_assessment_conversion(assessments)
    group_batch = assessments.assessment_group_batch
    change_marks_added_status(group_batch, false)
    group = group_batch.assessment_group
    activity = assessments.assessment_activity
    grade_set = group.grade_set
    grades = grade_set.grades if grade_set
    ConvertedAssessmentMark.delete_all(["assessment_group_batch_id = ? AND markable_id = ? AND markable_type = ?", 
        group_batch.id, activity.id, activity.class.to_s])
    assessments.assessment_marks.each do |assessment_mark|
      converted_mark = ConvertedAssessmentMark.new(:markable => activity, :assessment_group_batch_id => group_batch.id, 
        :assessment_group_id => group.id, :student_id => assessment_mark.student_id, :is_absent => assessment_mark.is_absent)
      unless assessment_mark.is_absent
        grade = grades.detect{|g| g.id == assessment_mark.grade_id}
        converted_mark.grade = assessment_mark.grade
        converted_mark.credit_points = grade.credit_points
        converted_mark.passed = grade.pass_criteria
        converted_mark.actual_mark = {:grade => assessment_mark.grade, :credit_points => grade.credit_points, :passed => grade.pass_criteria}
      end
      @rollback = true unless converted_mark.save
    end
    @rollback = true unless assessments.update_attributes(:marks_added => true, :submission_status => 2)
    other_activities = group_batch.activity_assessments.all(:conditions => {:marks_added => false})
    if other_activities.blank?
      change_marks_added_status(group_batch, true)
    end
  end
  
  def subject_assessment_conversion(assessments)
    group_batch = assessments.assessment_group_batch
    change_marks_added_status(group_batch, false)
    group = group_batch.assessment_group
    subject = assessments.subject
    group_maximum = group.maximum_marks_for(subject,group_batch.batch.course)
    grade_set = group.grade_set
    grades = grade_set.grades if grade_set
    ConvertedAssessmentMark.delete_all(["assessment_group_batch_id = ? AND markable_id = ? AND markable_type = ?", 
        group_batch.id, subject.id, subject.class.to_s])
    assessments.assessment_marks.each do |assessment_mark|
      converted_mark = ConvertedAssessmentMark.new(:markable => subject, :assessment_group_batch_id => group_batch.id, 
        :assessment_group_id => group.id, :student_id => assessment_mark.student_id, :is_absent => assessment_mark.is_absent)
      unless assessment_mark.is_absent
        case group.scoring_type
        when 1
          converted_mark.mark = (assessment_mark.marks.to_f/assessments.maximum_marks.to_f)*group_maximum
          converted_mark.passed = (converted_mark.mark >= group.minimum_marks.to_f)
          converted_mark.actual_mark = {:mark => assessment_mark.marks.to_f, :passed => (assessment_mark.marks.to_f >= assessments.minimum_marks.to_f)}
        when 2
          grade = grades.detect{|g| g.id == assessment_mark.grade_id}
          converted_mark.grade = assessment_mark.grade
          converted_mark.credit_points = grade.credit_points
          converted_mark.passed = grade.pass_criteria
          converted_mark.actual_mark = {:grade => assessment_mark.grade, :credit_points => grade.credit_points, :passed => grade.pass_criteria}
        when 3
          converted_mark.mark = (assessment_mark.marks.to_f/assessments.maximum_marks.to_f)*group_maximum
          grade = grades.detect{|g| g.id == assessment_mark.grade_id}
          converted_mark.grade = assessment_mark.grade
          converted_mark.credit_points = grade.credit_points
          converted_mark.passed = grade.pass_criteria
          converted_mark.actual_mark = {:mark => assessment_mark.marks.to_f, :grade => assessment_mark.grade, 
            :credit_points => grade.credit_points, :passed => grade.pass_criteria}
        end
      end
      @rollback = true unless converted_mark.save
    end
    @rollback = true unless assessments.update_attributes(:marks_added => true, :submission_status => 2)
    other_subjects = group_batch.subject_assessments.all(:conditions => {:marks_added => false})
    if other_subjects.blank?
      change_marks_added_status(group_batch, true)
    end
  end
  
  def change_marks_added_status(group_batch, status)
    if group_batch
      group_batch.reload
      group_batch.marks_added = status
      group_batch.send(:update_without_callbacks)
    end
  end
end
