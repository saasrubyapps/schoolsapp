class DelayedEmployeeLeave <  Struct.new(:parameters, :leave_reset_id, :retry_flag)
  def perform
    emp_ids = JSON.parse(parameters)
    @employees = Employee.find(:all , :conditions => ["id IN (?)",emp_ids], :include => {:leave_group => :leave_group_leave_types})
    update_employee_leaves(leave_reset_id, retry_flag)
  end



  def update_employee_leaves(leave_reset_id, retry_flag)
    log = Logger.new("log/reset.log")
    ignore_lop = Configuration.ignore_lop
    ignore_lop = ignore_lop.config_value
    reset_logs = LeaveReset.find(leave_reset_id)
    @employees.each do |employee|
      errors = []
      reasons = []
      employee_reset_logs = LeaveResetLog.find_all_by_employee_id_and_retry_status(employee.id,true)
      ActiveRecord::Base.transaction do
        emp_log = LeaveResetLog.new({:employee_id => employee.id, :status => 1, :leave_reset_id => leave_reset_id})
        emp_leaves = EmployeeLeave.find_all_by_employee_id(employee.id)
        inactive_leaves = EmployeeLeaveType.all(:conditions => ["is_active = false"])
        all_leaves = EmployeeLeave.find_all_by_employee_id(employee.id)
        leave_group = employee.leave_group
        previous_reset_date = employee.last_reset_date.to_date
        emp_additional_leaves = employee.employee_additional_leaves.select{|e| e.is_deductable == true }
        unless retry_flag
          non_deducted_lops = emp_additional_leaves.collect{|e| e.is_deducted if e.attendance_date >= previous_reset_date}.include? false
          if ignore_lop == "true" && non_deducted_lops
            reasons << "non_deducted_lop_present"
          end
        end
        
        if reset_logs.reset_date < previous_reset_date
          reasons << "reset_date_overlap"
        end

        pending_leave_applications = employee.apply_leaves.all(:conditions => ["approved  IS NULL"])
        if pending_leave_applications.present?
          reasons << "pending_leave_applications"
        end
        if employee.joining_date <= reset_logs.reset_date
          if reasons.empty?
            if leave_group.present?
              leave_types = leave_group.leave_group_leave_types
              unless emp_leaves.present?
                leave_types.each do |t|
                  e = EmployeeLeave.new(:employee_id => employee.id, :employee_leave_type_id => t.employee_leave_type_id, :leave_group_id => t.leave_group_id, :leave_count => t.leave_count, :leave_taken => 0, :reset_date => (reset_logs.reset_date < t.employee_leave_type.reset_date ? t.employee_leave_type.reset_date : reset_logs.reset_date),:reseted_at=> Time.now, :additional_leaves => 0, :is_additional => false)
                  errors << e.errors.full_messages unless e.save
                end
              else
                g_type_ids = leave_types.collect(&:employee_leave_type_id)
                e_type_ids = emp_leaves.collect(&:employee_leave_type_id)
                leave_types.each do |type|
                  if e_type_ids.include? type.employee_leave_type_id
                    emp_lev = emp_leaves.detect{|e| e.employee_leave_type_id == type.employee_leave_type_id }
                    leave_type = type.employee_leave_type
                    default_leave_count = type.leave_count
                    if leave_type.carry_forward
                      carry_forward_leave = leave_type.carry_forward_type
                      leave_taken = emp_lev.leave_taken
                      available_leave = emp_lev.leave_count
                      if leave_taken <= available_leave
                        balance_leave = available_leave - leave_taken
                  
                        if carry_forward_leave == 2
                          if balance_leave >= leave_type.max_carry_forward_leaves.to_f
                            new_count = leave_type.max_carry_forward_leaves
                          else
                            new_count = balance_leave
                          end
                        elsif carry_forward_leave == 1
                          new_count = balance_leave
                        end
                        available_leave = new_count.to_f
                        available_leave += default_leave_count.to_f
                        leave_taken = 0
                        add_leaves = 0
                        errors << emp_lev.errors.full_messages unless emp_lev.update_attributes(:additional_leaves => add_leaves,:reseted_at=> Time.now ,:leave_taken => leave_taken,:leave_count => available_leave, :reset_date => (reset_logs.reset_date < type.employee_leave_type.reset_date ? type.employee_leave_type.reset_date : reset_logs.reset_date), :is_active => true, :leave_group_id => type.leave_group_id, :is_additional => false)
                      else
                        available_leave = default_leave_count.to_f
                        leave_taken = 0
                        add_leaves = 0
                        errors << emp_lev.errors.full_messages unless emp_lev.update_attributes(:additional_leaves => add_leaves,:reseted_at => Time.now,:leave_taken => leave_taken,:leave_count => available_leave, :reset_date => (reset_logs.reset_date < type.employee_leave_type.reset_date ? type.employee_leave_type.reset_date : reset_logs.reset_date), :is_active => true, :leave_group_id => type.leave_group_id, :is_additional => false)
                      end
                    else
                      available_leave = default_leave_count.to_f
                      leave_taken = 0
                      add_leaves = 0
                      errors << emp_lev.errors.full_messages unless emp_lev.update_attributes(:additional_leaves => add_leaves,:reseted_at => Time.now, :leave_taken => leave_taken,:leave_count => available_leave, :reset_date => (reset_logs.reset_date < type.employee_leave_type.reset_date ? type.employee_leave_type.reset_date : reset_logs.reset_date), :is_active => true, :leave_group_id => type.leave_group_id, :is_additional => false)
                    end
                  else
                    emp_lev = EmployeeLeave.new(:employee_id => employee.id, :employee_leave_type_id => type.employee_leave_type_id, :leave_group_id => type.leave_group_id, :leave_count => type.leave_count, :leave_taken => 0, :reset_date => (reset_logs.reset_date < type.employee_leave_type.reset_date ? type.employee_leave_type.reset_date : reset_logs.reset_date),:reseted_at=> Time.now, :additional_leaves => 0, :is_additional => false)
                    errors << emp_lev.errors.full_messages unless emp_lev.save
                  end
                end
                (e_type_ids - g_type_ids).each do |r_type|
                  emp_lev = emp_leaves.detect{|e| e.employee_leave_type_id == r_type }
                  errors << emp_lev.errors.full_messages unless emp_lev.update_attributes(:additional_leaves => 0,:reseted_at=> Time.now ,:leave_taken => 0,:leave_count => 0, :reset_date => reset_logs.reset_date, :is_active => false, :leave_group_id => nil)
                end
              end
            else
              if emp_leaves.present?
                emp_leaves.each do |e|
                  errors << e.errors.full_messages unless e.update_attributes(:additional_leaves => 0,:reseted_at=> Time.now ,:leave_taken => 0,:leave_count => 0, :reset_date => reset_logs.reset_date, :is_active => false, :leave_group_id => nil)
                end
              end
            end
            if inactive_leaves.present?
              inactive_leaves.each do |lt|
                emp_lev = all_leaves.detect{|e| e.employee_leave_type_id == lt.id }
                errors << emp_lev.errors.full_messages unless emp_lev.update_attributes(:additional_leaves => 0,:reseted_at=> Time.now ,:leave_taken => 0,:leave_count => lt.max_leave_count, :reset_date => (reset_logs.reset_date < lt.reset_date ? lt.reset_date : reset_logs.reset_date), :is_active => (g_type_ids.present? ? (g_type_ids.include? lt.id) : false), :leave_group_id => nil, :is_additional => false) if emp_lev.present?
              end
            end
            if errors.empty?
              emp_log.status = 2
            else
              emp_log.status = 3
              reasons << "technical_error"
            end            
            employee.update_attribute("last_reset_date",reset_logs.reset_date) if reasons.empty?
          else
            emp_log.status = 3
          end
        else
          emp_log.status = 3
          reasons << "reset_date_before_joining_date"
        end
        emp_log.retry_status = true if reasons.count == 1 && reasons.include?("non_deducted_lop_present")
        emp_log.reason = reasons
        emp_log.save
        employee_reset_logs.each do |emp|
          emp.update_attribute("retry_status", false)
        end
        
        raise ActiveRecord::Rollback unless errors.empty?
      end
    end
    emp_reset_logs = reset_logs.leave_reset_logs
    if emp_reset_logs.collect{|l| l.status}.uniq.length == 1 && (emp_reset_logs.collect{|l| l.status}.include? 2)
      reset_logs.update_attribute("status", 2)
    elsif emp_reset_logs.collect{|l| l.status}.include? 3
      reset_logs.update_attribute("status", 3)
    end
  end
end