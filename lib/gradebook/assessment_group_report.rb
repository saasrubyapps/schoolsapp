# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

module Gradebook
  class AssessmentGroupReport
    
    attr_accessor :build_coscholastic_report, :student, :students, :headers, :main_header
    
    def initialize(group,batch_group, batch_id)
      @group = group
      @batch_group = batch_group
      @batch =  Batch.find(batch_id, :include => [:subjects,:course, {:students => :converted_assessment_marks}])
      @headers = []
    end
    
    def prepare_data
      @students = batch.students.all(:include => :subjects)
      @subjects = batch.subjects.all(:conditions=>{:no_exams => false,:is_deleted => false})
      @grade_set = group.grade_set
      @exam_type = group.exam_type
      if exam_type.activity
        @activities = batch_group.activity_assessments.collect(&:assessment_activity)
      elsif exam_type.subject_attribute||exam_type.subject_wise_attribute
        @attribute_assessments = batch_group.subject_attribute_assessments.all(:joins => :attribute_assessments,
            :include => {:attribute_assessments => :assessment_attribute}, :group => 'subject_attribute_assessments.id')
      end
      
      self
    end
    
    def build_header
      if exam_type.subject
        @headers = [I18n.t('subject')]
      elsif exam_type.activity
        @headers = [I18n.t('activity'), I18n.t('grade')]
        @headers << I18n.t('credit_points') if @grade_set.enable_credit_points
      elsif exam_type.subject_attribute||exam_type.subject_wise_attribute
        @headers = [I18n.t('subject'), I18n.t('attributes'), I18n.t('marks')]
      end
      unless exam_type.activity
        case group.scoring_type
        when 1
          @headers << group.marks_text_with_max_marks if exam_type.subject
          @headers << group.total_marks_with_max_marks if exam_type.subject_attribute||exam_type.subject_wise_attribute
        when 2
          @headers << I18n.t('grade')
        when 3
          @headers << group.marks_text_with_max_marks if exam_type.subject
          @headers << group.total_marks_with_max_marks if exam_type.subject_attribute||exam_type.subject_wise_attribute
          @headers << I18n.t('grade')
          @headers << I18n.t('credit_points') if @grade_set.enable_credit_points
        end
      end
    end
    
    def build_scholastic_report
      if exam_type.subject
        subject_assessment_report
      elsif exam_type.activity
        activity_assessment_report
      elsif exam_type.subject_attribute||exam_type.subject_wise_attribute
        attribute_assessment_report
      end
    end
    
    
    private
    
    attr_accessor :batch, :group, :exam_type, :batch_group, :grade_set
    
    def subject_assessment_report
      marks = []
      student_subjects = student.subjects.collect(&:id)
      @subjects.each do |subject|
        next if subject.elective_group_id? and !(student_subjects.include? subject.id)
        ind_marks = [subject.name]
        converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == subject.id and 
            cam.markable_type == 'Subject' and cam.assessment_group_batch_id == batch_group.id}
        mark_present = (converted_mark.present? and !converted_mark.is_absent)
        case group.scoring_type
        when 1
          ind_marks << (mark_present ? converted_mark.mark_with_omm : '-')
        when 2
          ind_marks << (mark_present ? converted_mark.grade : '-')
        when 3
          ind_marks << (mark_present ? converted_mark.mark_with_omm : '-')
          ind_marks << (mark_present ? converted_mark.grade : '-')
          ind_marks << (mark_present ? converted_mark.credit_points.to_f : '-') if grade_set.enable_credit_points
        end
        marks << ind_marks
      end
      
      marks
    end
    
    def activity_assessment_report
      marks = []
      @activities.each do |activity|
        ind_marks = [activity.name]
        converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == activity.id and 
            cam.markable_type == 'AssessmentActivity' and cam.assessment_group_batch_id == batch_group.id}
        mark_present = (converted_mark.present? and !converted_mark.is_absent)
        ind_marks << (mark_present ? converted_mark.grade : '-')
        ind_marks << (mark_present ? converted_mark.credit_points.to_f : '-') if grade_set.enable_credit_points
        marks << ind_marks
      end
      
      marks
    end
    
    def attribute_assessment_report
      marks = []
      student_subjects = student.subjects.collect(&:id)
      @subjects.each do |subject|
        next if subject.elective_group_id? and !(student_subjects.include? subject.id)
        ind_marks = [subject.name]
        converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == subject.id and 
            cam.markable_type == 'Subject' and cam.assessment_group_batch_id == batch_group.id}
        mark_present = (converted_mark.present? and !converted_mark.is_absent)
        attributes = []
        attribute_marks = []
        subject_attrs = @attribute_assessments.select{|a| a.subject_id == subject.id}
        subject_attrs.each do |s_attr|
          s_attr.attribute_assessments.each do |attr|
            attributes << attr.assessment_attribute.name_with_max_mark
            attribute_marks << (mark_present ? (((converted_mark.actual_mark||{})[attr.assessment_attribute_id].present? and 
                    (converted_mark.actual_mark||{})[attr.assessment_attribute_id][:mark].present?) ? 
                  (converted_mark.actual_mark||{})[attr.assessment_attribute_id][:mark] : '-') : '-')
          end
        end
        ind_marks << (attributes.present? ? attributes : '-')
        ind_marks << (attribute_marks.present? ? attribute_marks : '-')
        case group.scoring_type
        when 1
          ind_marks << (mark_present ? converted_mark.mark_with_omm : '-')
        when 3
          ind_marks << (mark_present ? converted_mark.mark_with_omm : '-')
          ind_marks << (mark_present ? converted_mark.grade : '-')
          ind_marks << (mark_present ? converted_mark.credit_points.to_f : '-') if grade_set.enable_credit_points
        end
        marks << ind_marks
      end
      
      marks
    end
  end
end
