# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

module Gradebook
  class TermReport
    
    attr_accessor :student, :students, :all_groups, :batch_groups, :batch, :headers, :main_header
    
    def initialize(term, final_assessment, batch_id)
      @term = term
      @final_assessment = final_assessment
      @batch = Batch.find(batch_id, :include => [:subjects,:course, {:students => :converted_assessment_marks}])
      @headers = []
    end
    
    def prepare_data
      @subjects = @batch.subjects.all(:conditions=>{:no_exams => false,:is_deleted => false})
      @show_percentage = @final_assessment.show_percentage? if @final_assessment
      @term_groups = term_groups
      @display_groups = display_groups
      @display_group_ids = @display_groups.collect(&:id)
      @all_groups = (@final_assessment.present? ? @final_assessment.all_assessment_groups : @term.assessment_groups.without_derived)
      @batch_groups = @batch.assessment_group_batches
      @activity_group = @term.assessment_groups.all(:conditions => {:type => 'ActivityAssessmentGroup'}, 
        :include => {:assessment_group_batches => {:activity_assessments => :assessment_activity}})
      @students = @batch.students.all(:include => :subjects)
      
      self
    end
    
    def term_groups
      if @final_assessment.present?
        assessment_groups
      else
        @term.subject_and_derived_assessments
      end
    end
    
    def display_groups
      if @final_assessment.present?
        @term_groups
      else
        @term.get_assessment_groups_for_term_report
      end
    end
    
    def derived_assessments
      @term_groups.select{|g| g.type == "DerivedAssessmentGroup"}
    end
    
    def build_header
      @display_groups.each{|group| @headers << group.display_name_with_max_marks}
      if @final_assessment.present?
        @headers << @final_assessment.display_name_with_max_marks
        @headers << @final_assessment.display_name_with_percentage if @show_percentage
      end
    end
    
    def build_scholastic_report
      marks = []
      student_subjects = student.subjects.collect(&:id)
      @subjects.each do |subject|
        next if subject.elective_group_id? and !(student_subjects.include? subject.id)
        ind_marks = [subject.name]
        final_marks = {}
        final_maximum = @final_assessment.maximum_marks_for(subject,@batch.course) if @final_assessment
        @term_groups.each do |group|
          grade_set = group.grade_set
          grades = grade_set.grades.sorted_marks if grade_set
          group_maximum = group.maximum_marks_for(subject,@batch.course)
          
          b_group = @batch_groups.detect{|g| g.assessment_group_id == group.id}
          converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == subject.id and 
              cam.markable_type == 'Subject' and cam.assessment_group_batch_id == b_group.id}
          ind_marks << ((converted_mark.present? and !converted_mark.is_absent) ? converted_mark.mark_with_grade : '-') if (@display_group_ids.include? group.id)
          final_score = ((converted_mark.present? and !converted_mark.is_absent) ? converted_mark.mark.to_f : 0)
          if @final_assessment.present? and (@final_assessment.assessment_groups.include? group) and converted_mark.present?
            final_marks[group.id] = {:mark => final_score, :max_mark => group_maximum,
              :converted_mark => ((final_score/group_maximum)*final_maximum)
            }
          end
        end
        if @final_assessment.present?
          marks_obtained = final_marks.present? ? @final_assessment.calculate_final_score(final_marks,final_maximum) : nil
          grade_set = @final_assessment.grade_set
          grades = grade_set.grades.sorted_marks if grade_set
          if marks_obtained.present?
            percentage = ((marks_obtained/final_maximum)*100)
            mark_grade = grade_set.select_grade_for(grades, percentage) if grade_set and @final_assessment.scoring_type == 3
            ind_marks << "#{marks_obtained.round(2)}#{@final_assessment.overrided_mark(subject, @batch.course)}#{mark_grade.present? ? " &#x200E;(#{mark_grade.try(:name)})&#x200E;" : ""}"
            ind_marks << percentage.round(2) if @show_percentage
          else
            ind_marks << "-"
            ind_marks << '-' if @show_percentage
          end
        end
        marks << ind_marks
      end
      
      marks
    end
    
    def build_coscholastic_report
      activities = []
      
      assessment_profiles = AssessmentActivityProfile.find(@activity_group.collect(&:assessment_activity_profile_id),:include=> :assessment_activities)
      @activity_group.group_by(&:assessment_activity_profile_id).each_pair do |key, groups|
        agbs = []
        activity_profile = []
        groups.each{|group| agbs << group.assessment_group_batches.detect{|g_b| g_b.batch_id == @batch.id}}
        agbs = agbs.compact
        header = []
        agbs.each do |agb|
          next unless agb.marks_added?
          header << agb.assessment_group.display_name
          header << I18n.t('grade')
        end
        activity_profile << header
        profile = assessment_profiles.detect{|profile| profile.id == key}
        profile.assessment_activities.each do |activity|
          marks = []
          agbs.each do |agb|
            next unless agb.marks_added?
            converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == activity.id and 
                  cam.markable_type == 'AssessmentActivity' and cam.assessment_group_batch_id == agb.id}
            marks << activity.name
            marks << converted_mark.try(:grade)|| '-'
          end
          activity_profile << marks if marks.present?
        end
        activities << activity_profile
      end
      
      
      activities
    end
    
    
    private
    
    def assessment_groups
      childrens = []
      @final_assessment.assessment_groups.each do |group|
        childrens += group.assessment_groups if group.derived_assessment? and group.show_child_in_term_report?
        childrens << group
      end
      return childrens.uniq
    end
    
    
  end
end
