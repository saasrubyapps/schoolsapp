# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

module Gradebook
  class PlannerReport
    
    attr_accessor :students, :headers, :main_header, :all_groups, :derived_assessments, :batch_groups, :student
    
    def initialize(plan, final_assessment, batch_id)
      @plan = plan
      @final_assessment = final_assessment
      @batch = Batch.find(batch_id, :include => [:subjects,:course, {:students => :converted_assessment_marks}])
      @headers = []
      @main_header = []
    end

    def prepare_data
      @subjects = @batch.subjects.all(:conditions=>{:no_exams => false,:is_deleted => false})
      @all_assessment_groups = @plan.plan_assessment_groups
      @show_percentage = @final_assessment.show_percentage? unless @final_assessment.no_exam
      @display_groups = display_groups
      @display_group_ids = @display_groups.collect(&:id)
      @plan_groups = (@final_assessment.no_exam? ?  subject_and_derived_assessments : @display_groups)
      @derived_assessments = @plan_groups.select{|group| group.type == 'DerivedAssessmentGroup'}
      @all_groups = (@final_assessment.no_exam? ? non_derived_assessments : @final_assessment.all_assessment_groups)
      @batch_groups = @batch.assessment_group_batches
      @students = @batch.students.all(:include => [:subjects, :converted_assessment_marks])
      
      self
    end

    def display_groups
      childrens = []
      @final_assessment.assessment_groups.each do |group|
        childrens += group.assessment_groups if group.derived_assessment? and group.show_child_in_planner_report?
        childrens << group
      end
      return childrens.uniq
    end
    

    def subject_and_derived_assessments
      @all_assessment_groups.select{|group| group.type != 'ActivityAssessmentGroup'}
    end
    
    def non_derived_assessments
      @all_assessment_groups.select{|group| group.type != 'DerivedAssessmentGroup'}
    end
    
    def build_header
      @headers = []
      @main_header = []
      @display_groups.group_by(&:parent_id).each_pair do |term_id, groups|
        term = AssessmentTerm.find term_id
        percentage_present = groups.detect{|g| g.is_final_term? and g.show_percentage? }
        groups_count = percentage_present ? (groups.count + 1) : groups.count
        @main_header << [term_id, groups_count, (term.try(:name) || 'Term 1 (100)') ]
        groups.each do |group|
          @headers << group.display_name_with_max_marks
          @headers <<  group.display_name_with_percentage if group.is_final_term? and group.show_percentage?
        end
      end
      unless @final_assessment.no_exam?
        @headers << @final_assessment.display_name_with_max_marks 
        @main_header << ['',1,''] #Empty Col Head for final marks
      end
      if @show_percentage
        @headers << @final_assessment.display_name_with_percentage
        @main_header << ['',1,''] #Col Head for final Percentage
      end
    end
    
    def build_scholastic_report
      marks = []
      student_subjects = student.subjects.collect(&:id)
      @subjects.each do |subject|
        next if subject.elective_group_id? and !(student_subjects.include? subject.id)
        ind_marks = [subject.name]
        final_marks = {}
        final_maximum = @final_assessment.maximum_marks_for(subject,@batch.course) unless @final_assessment.no_exam?
        @display_groups.group_by(&:parent_id).each_pair do |_, groups|
          groups.each do |group|
            grade_set = group.grade_set
            grades = grade_set.grades.sorted_marks if grade_set
            group_maximum = group.maximum_marks_for(subject,@batch.course)
            
            b_group = @batch_groups.detect{|g| g.assessment_group_id == group.id}
            converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == subject.id and 
                cam.markable_type == 'Subject' and cam.assessment_group_batch_id == b_group.id}
            ind_marks << ((converted_mark.present? and !converted_mark.is_absent) ? converted_mark.mark_with_grade : '-') if (@display_group_ids.include? group.id)
            
            
            final_score = ((converted_mark.present? and !converted_mark.is_absent) ? converted_mark.mark.to_f : 0)
            if group.is_final_term? and group.show_percentage?
              percentage = ((final_score/group_maximum)*100).round(2)
              ind_marks << percentage if @display_group_ids.include? group.id
            end
            
            if !@final_assessment.no_exam and (@all_assessment_groups.include? group) and converted_mark.present?
              final_marks[group.id] = {:mark => final_score, :max_mark => group_maximum,
                :converted_mark => ((final_score/group_maximum)*final_maximum)
              }
            end
          end
        end
        (@plan_groups - @display_groups).each do |group|
          grade_set = group.grade_set
          grades = grade_set.grades.sorted_marks if grade_set
          group_maximum = group.maximum_marks_for(subject,@batch.course)
          b_group = @batch_groups.detect{|g| g.assessment_group_id == group.id}
          converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == subject.id and 
                cam.markable_type == 'Subject' and cam.assessment_group_batch_id == b_group.id}
          final_score = ((converted_mark.present? and !converted_mark.is_absent) ? converted_mark.mark.to_f : 0)
          if !@final_assessment.no_exam and converted_mark.present?
            final_marks[group.id] = {:mark => final_score, :max_mark => group_maximum,
              :converted_mark => ((final_score/group_maximum)*final_maximum)
            }
          end
        end
        unless @final_assessment.no_exam
          marks_obtained = final_marks.present? ? @final_assessment.calculate_final_score(final_marks,final_maximum) : nil
          grade_set = @final_assessment.grade_set
          grades = grade_set.grades.sorted_marks if grade_set
          
          if marks_obtained.present?
            percentage = ((marks_obtained.round(2)/final_maximum)*100)
            mark_grade = grade_set.select_grade_for(grades, percentage) if grade_set and @final_assessment.scoring_type == 3
            ind_marks << "#{marks_obtained.round(2)}#{@final_assessment.overrided_mark(subject, @batch.course)}#{mark_grade.present? ? " &#x200E;(#{mark_grade.try(:name)})&#x200E;" : ""}"
            ind_marks << percentage.round(2) if @show_percentage
          else
            ind_marks << "-"
            ind_marks << '-' if @show_percentage
          end
        end
        marks << ind_marks
      end
      
      marks
    end
    
    def build_coscholastic_report
      activities = []
      
      activity_assessments = @plan.activity_assessments(@plan.assessment_term_ids)
      assessment_profiles = AssessmentActivityProfile.find(activity_assessments.collect(&:assessment_activity_profile_id),:include=> :assessment_activities)
      
      
      activity_assessments.group_by(&:assessment_activity_profile_id).each_pair do |key, groups|
        agbs = []
        activity_profile = []
        groups.each{|group| agbs << group.assessment_group_batches.detect{|g_b| g_b.batch_id == batch.id}}
        agbs = agbs.compact
        header = []
        agbs.each do |agb|
          next unless agb.marks_added?
          header << agb.assessment_group.display_name
          header << I18n.t('grade')
        end
        activity_profile << header
        profile = assessment_profiles.detect{|profile| profile.id == key}
        profile.assessment_activities.each do |activity|
          marks = []
          agbs.each do |agb|
            next unless agb.marks_added?
            converted_mark = student.converted_assessment_marks.detect{|cam| cam.markable_id == activity.id and 
                  cam.markable_type == 'AssessmentActivity' and cam.assessment_group_batch_id == agb.id}
            marks << activity.name
            marks << converted_mark.try(:grade)|| '-'
          end
          activity_profile << marks if marks.present?
        end
        activities << activity_profile
        
      end
      
      activities
    end

    private

    attr_accessor  :plan, :batch
  end
end
