module Notifier
  def inform(recipient_ids,content,initiator,links={})
    unless recipient_ids.blank?
      notification = Notification.new(:content=>content,:initiator=>initiator,:payload=>links)
      notification.recipient_ids = Array(recipient_ids).flatten
      notification.recipient_ids.reject!(&:blank?)
      if notification.save
        if notification.recipient_ids.count > 1
          Delayed::Job.enqueue(DelayedNotification.new(:recipient_ids => notification.recipient_ids,:notification_id=>notification.id),{:queue=>"notification"});
        else
          notification.recipient_ids.each do |rec_id|
            notification.notification_recipients.create(:recipient_id=>rec_id)
          end
        end
      end
    end
  end
end