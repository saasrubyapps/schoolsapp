Dir[File.dirname(__FILE__) + '/gradebook/*.rb'].each {|file| require file }

class DelayedGenerateAssessmentReport < Struct.new(:generated_report_id)
  
  def perform
    generated_report = GeneratedReport.find generated_report_id
    report_batches = generated_report.generated_report_batches.pending_batches
    report_batches.each do |rep_batch|
      batch_report_generation(generated_report, rep_batch)
    end
    make_report_settings_copy(generated_report.report_type, generated_report.report_id, generated_report_id)
  end
  
  def batch_report_generation(generated_report, rep_batch)
    @rollback = false
    @errors = []
    ActiveRecord::Base.transaction do
      send("#{generated_report.report_type.underscore}_report", generated_report.report_id, rep_batch.batch_id, rep_batch.id)
      rep_batch.update_attributes({:generation_status => 2, :last_error => nil, :report_published => false}) unless @rollback
      raise ActiveRecord::Rollback if @rollback
    end
  ensure
    rep_batch.update_attributes({:generation_status => (rep_batch.generation_status == 4 ? 5 : 3), :last_error => @errors}) if @rollback
  end
  
  def make_report_settings_copy(model, object_id, report_id)
    assessment_plan_id = model.camelize.constantize.find(object_id).try(:assessment_plan_id)
    settings_hash = AssessmentReportSetting.clone_settings(assessment_plan_id)
    copy = AssessmentReportSettingCopy.find_or_initialize_by_generated_report_id(report_id)
    copy.update_attributes(:settings => settings_hash)
  end
  
  def assessment_plan_report(plan_id, batch_id, report_batch_id)
    plan = AssessmentPlan.find plan_id
    final_assessment = plan.final_assessment
    if final_assessment.new_record?
      log_error! I18n.t('final_plan_not_configured')
      return
    else
      report = Gradebook::PlannerReport.new(plan, final_assessment, batch_id).prepare_data
      mark_status_check!(report.all_groups,report.derived_assessments,report.batch_groups)
      return if @rollback
      reset_individual_reports!(plan,plan.class.to_s, report.students.collect(&:id))
      insert_report(report,report_batch_id, plan)
    end
  end
  
  def assessment_term_report(term_id,batch_id, report_batch_id)
    term = AssessmentTerm.find(term_id)
    final_assessment = term.final_assessment
    report = Gradebook::TermReport.new(term, final_assessment, batch_id).prepare_data
    mark_status_check!(report.all_groups,report.derived_assessments,report.batch_groups)
    return if @rollback
    reset_individual_reports!(term,term.class.to_s, report.students.collect(&:id))
    reset_final_marks(final_assessment, report.batch) if final_assessment
    insert_report(report,report_batch_id, term)
    insert_final_marks(final_assessment,report.batch) unless @rollback
  end
  
  def assessment_group_report(group_id, batch_id, report_batch_id)
    group = AssessmentGroup.find(group_id, :include => :assessment_group_batches)
    batch_group = group.assessment_group_batches.detect{|g| g.batch_id == batch_id}
    if batch_group.present? and  batch_group.childrens_present?
      log_error! "#{I18n.t('no_marks_entered')}" unless batch_group.try(:marks_added)
    else
      log_error! "#{I18n.t('not_scheduled')}"
    end
    return if @rollback
    report = Gradebook::AssessmentGroupReport.new(group, batch_group, batch_id).prepare_data
    reset_individual_reports!(group,'AssessmentGroup',report.students.collect(&:id))
    insert_report(report,report_batch_id, group)
  end
  
  def insert_report(report,report_batch_id, reportable)
    report.build_header
    report.students.each do |student|
      report.student = student
      marks = report.build_scholastic_report
      activities = report.build_coscholastic_report
      student_report = IndividualReport::Report.new(nil, report.main_header , report.headers, marks, activities)
      ind_report = IndividualReport.new(:reportable => reportable, :student_id => student.id, :report => student_report, :generated_report_batch_id => report_batch_id)
      log_error! unless ind_report.save
    end
  end
  
  def reset_final_marks(final_assessment, batch)
    agb = final_assessment.assessment_group_batches.first(:conditions => {:batch_id => batch.id})
    ConvertedAssessmentMark.delete_all(["assessment_group_batch_id = ?", agb.id]) if agb
  end
  
  def insert_final_marks(final_assessment,batch)
    if final_assessment
      agb = final_assessment.assessment_group_batches.find_or_initialize_by_batch_id(:batch_id => batch.id)
      agb.send(:create_without_callbacks) if agb.new_record?
      agb.reload
      DelayedAssessmentMarksSubmission.new(agb.id, 'AssessmentGroupBatch').perform
    end
  end
  
  def mark_status_check!(all_groups, derived_groups, batch_groups)
    all_groups.each do |group|
      b_group = batch_groups.detect{|g| g.assessment_group_id == group.id}
      log_error! unless b_group.present? and  b_group.childrens_present? and b_group.try(:marks_added) 
      if b_group.present? and  b_group.childrens_present?
        log_error! "#{group.name} - #{I18n.t('no_marks_entered')}" unless b_group.try(:marks_added) 
      else
        log_error! "#{group.name} - #{I18n.t('not_scheduled')}"
      end
    end
    
    derived_groups.each do |d_group|
      ab_group = batch_groups.detect{|agb| (agb.assessment_group_id == d_group.id)}
      log_error! "#{d_group.name} - #{I18n.t('marks_not_calculated')}" unless ab_group.present? and ab_group.try(:marks_added)
    end
  end
  
  def log_error!(errors = nil)
    @rollback = true
    @errors << errors if errors
  end
  
  def reset_individual_reports!(obj,obj_type, student_ids)
    IndividualReport.delete_all(["reportable_id = ? AND reportable_type = ? AND student_id IN (?)", obj.id, obj_type , student_ids])
  end
  
end
