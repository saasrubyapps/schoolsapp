function drawRegister(dpt_id,date){
    params = 'dept_id='+dpt_id
    if (date!=undefined){
        params = params + '&next='+date;
    }
    j("#loader").show();
    if (dpt_id == ""){
        j("#register").children().remove();
        j("#loader").hide();
    }else{
        new Ajax.Request('/employee_attendances/show.json',{
            parameters: params,
            asynchronous:true,
            evalScripts:true,
            method:'get',
            onComplete:function(resp){
                registerBuilder(resp.responseJSON);
                j("#loader").hide();
            }
        });
    }
}

function registerBuilder(data){
    json_data = data;
    today = data.today;
    dept = data.dept.employee_department;

    var register = j("#register");

    var header = j('<div/>', {
        'class': 'header'
    });

    j(register).html(header);

    var prev = j('<div/>', {
        'class': 'prev'
    }).appendTo(header)

    var prev_link = j("<a>", {
        text: "◄",
        href: "",
        'onclick': "loadMonth('prev'); return false;"
    }).appendTo(prev);

    var month = j('<div/>', {
        'class': 'month',
        text: today
    }).appendTo(header)

    var next = j('<div/>', {
        'class': 'next'
    }).appendTo(header)

    var next_link = j("<a>", {
        text: "►",
        href: "",
        'onclick': "loadMonth('next'); return false;"
    }).appendTo(next);

    var extender = j("<div/>",{
        'class': 'extender'
    }).appendTo(header);

    drawBox();
    
}


function loadMonth(month){
    if(month == 'prev'){
        m = (new Date(json_data.month_year )).getMonth() ;
    }else if(month == 'next'){
        m = parseInt((new Date(json_data.month_year )).getMonth()) + 2;
    }
    if(m == 0){
        m = 12;
        y = (new Date(json_data.month_year)).getFullYear() - 1;
    }
    if(m == 13){
        m = 1;
        y = (new Date(json_data.month_year)).getFullYear() + 1;
    }
    d = (new Date(json_data.month_year)).getDate();
    date = y.toString() + "-"+m.toString() +"-"+ d.toString();
    j("#loader").show();
    drawRegister(dept.id,date);
}

function drawBox(){
    
    date_headers = json_data.date_headers;
    employees = json_data.employees;
    absence = json_data.absence;
    current_day = json_data.current_day;
    current_date = json_data.current_date;
    selected_date = json_data.selected_date;
    var register = j("#register");

    var header = j('<div/>', {
        'class': 'box-1'
    }).appendTo(register);

    var table = j('<table/>', {
        id: 'register-table'
    }).appendTo(header);

    var tr_head = j('<tr/>', {
        'class': 'tr-head'
    }).appendTo(table);

    var td_header_name = j('<td/>', {
        'class': 'head-td-name themed_text',
        text: 'Name'
    }).appendTo(tr_head);

    j.each(date_headers,function(i,v){
        var td = j('<td/>', {
            'class': 'head-td-date'
        }).appendTo(tr_head);

        var day = j('<div/>', {
            'class': 'day themed_text',
            text: v.day
        }).appendTo(td);

        var date = j('<div/>', {
            'class': 'date',
            text: v.date
        }).appendTo(td);
        
    })

    j.each(employees,function(i,v){
        var emp_tr = j('<tr/>', {
            'class': 'tr-odd'
        }).appendTo(table);

        var emp_td = j('<td/>', {
            'class': 'td-name'
        }).appendTo(emp_tr);

        j(emp_td).html(v.employee.first_name+'<span>(' +v.employee.employee_number +')</span>&lrm;');
        var emp_name = j('<td/>', {
            'class': 'date'
        }).appendTo(emp_td);

        var span = j('<span/>', {}).appendTo(emp_name);

        var emp_name_text = j('<td/>', {
            'class': 'themed_text',
            text: v.employee.first_name + (v.employee.middle_name || "") + (v.employee.last_name || "")
        }).appendTo(span);

        j.each(date_headers,function(i,val){
            m = ("0" + String((new Date(json_data.month_year)).getMonth() + 1) ).slice(-2);
            y = (new Date(json_data.month_year)).getFullYear();
            
            var emp_day_td = j('<td/>', {
                id: "attendance-employee-" + String(v.employee.id) + "-day-" + y.toString()+"-"+m.toString()+"-" + String(val.date),
                'class': 'td-mark'
            }).appendTo(emp_tr);
            
            if (current_date == selected_date && val.date == current_day){
                j(emp_day_td).addClass("active");
            }
            
            attendance = absence[v.employee.id];
            emp_id = v.employee.id;
           
            date = y.toString() + "-"+m.toString() +"-"+ val.date.toString();

            if(attendance!= undefined){
                employee_dates = attendance.map(function(e){
                    return e.date
                });
                j.each(attendance,function(i,att){
                    if(att.date ==  date){
                        att_id = att.att_id;
                        return false;
                    }else{
                        att_id = undefined;
                    }
                })
                if (att_id != undefined && employee_dates.indexOf(date) > -1 ){
                    var mark_att = j("<a>", {
                        text: "X",
                        href: "",
                        'data' : {
                            id: emp_id,
                            date: date,
                            att_id: att_id
                        },
                        'class': "absent themed_text",
                        'onclick': "edit_attendance(j(this)); return false;"
                    }).appendTo(emp_day_td);
                }
                else{

                    var no_att = j("<a>", {
                        href: "",
                        'class': 'present',
                        'data' : {
                            id: emp_id,
                            date: date
                        },
                        'onclick': "new_attendance(j(this)); return false;"
                    }).appendTo(emp_day_td);
                }
               
            }
            else{
                var no_att = j("<a>", {
                    href: "",
                    'class': 'present',
                    'data' : {
                        id: emp_id,
                        date: date
                    },
                    'onclick': "new_attendance(j(this)); return false;"
                }).appendTo(emp_day_td);
            }

            var emp_date_cell = j("<div/>", {
                'class':"date"
            }).appendTo(emp_day_td);

            var emp_span = j("<span/>", {
                'class':"themed_text",
                text: val.day + " "+val.date
            }).appendTo(emp_date_cell);

            j(emp_span).append("<div>" + v.employee.first_name + v.employee.middle_name + v.employee.last_name + "</div>")

        })

        
   
    })
    
    j(register).append(header);
}




function new_attendance(ele){
    emp_id = j(ele).data("id");
    date = j(ele).data("date");
    params = 'id='+date+'&id2='+emp_id+'&date='+date
    new Ajax.Request('/employee_attendances/new',{
        parameters: params,
        asynchronous:true,
        evalScripts:true,
        method:'get',
        onComplete:function(resp){
        //            registerBuilder(resp.responseJSON);
        //            j("#loader").hide();
        }
    });
}

function edit_attendance(ele){
    id = j(ele).data("att_id");
    date = j(ele).data("date");
    new Ajax.Request('/employee_attendances/edit/'+id,{
        parameters: 'date='+date,
        asynchronous:true,
        evalScripts:true,
        method:'get',
        onComplete:function(resp){
        //            registerBuilder(resp.responseJSON);
        //            j("#loader").hide();
        }
    });
//{:url => edit_employee_attendance_path(@absent[0]), :method => 'get'}, :class=> 'absent themed_text') %>
}