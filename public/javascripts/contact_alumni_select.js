function select_batch_and_year_function() {
    course_name=j('#select_course_id').val();
    var formURL = "/alumni/contact_select_batches";
    j.ajax(
    {
        url: formURL,
        type: "GET",
       data: {
                query: {
                            course_id:course_name
                       }
            },
        success: function (data, textStatus, jqXHR) {
            j("#select_batches_partial").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
    var formURL = "/alumni/contact_select_passout_year";
    j.ajax(
    {
        url: formURL,
        type: "GET",
       data: {
                query: {
                            course_id:course_name
                }
            },
        success: function (data, textStatus, jqXHR) {
            j("#select_passout_year_partial").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function select_batch_function() {
    course_name=j('#select_course_id').val();
    passout_year=j('#select_passd_out_year_id').val();
    var formURL = "/alumni/batch_list_in_contact";
    j.ajax(
    {
        url: formURL,
        type: "GET",
       data: {
                query: {
                            course_id:course_name,
                            pass_out_year:passout_year
                       }
            },
        success: function (data, textStatus, jqXHR) {
            j("#select_batches_partial").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown){
        }
    });

}

