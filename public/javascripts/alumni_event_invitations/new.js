var course_id
var year_div_total_count;
var batc_div_total_count;
var courses_array=new Array

function modify_field_value(th_is){
    var box = j(th_is);
    var h_field = box.find(".sub-hidden-box");
    var type=h_field.attr('name')// to find from where the request is coming
    if((h_field.val()==1)||(h_field.val()==2))//check agian logic is not gud
        {
          h_field.val(0);
          delete_data_from_json(type,box);
          setting_child_check_box(type,box,h_field.val());
        }
    else{
            h_field.val(1);
            create_json_data(type,box);
            setting_child_check_box(type,box,h_field.val());
    }
  }
 function change_color_div(box,type){
      var checks_data=j("#"+type+"_list_div").find(".check-text-pair");
      if(checks_data.size()>=1){
      j.each(checks_data, function(idx,elem){
          j(elem).removeClass('highlight')
      });
      box.parent().addClass('highlight');
   }
  }
  function modify_box_icon(box){
    var h_field = box.find(".sub-hidden-box");
    var currnt_icon_type=box.attr('class').split(' ')[1];
    if(h_field.val()==0){
      box.removeClass(currnt_icon_type)
      box.addClass("img-0");
    }
    else {
      box.removeClass("img-0");
      box.addClass("img-1");
    }
  }
function delete_icon_set(box){
   var currnt_icon_type=box.attr('class').split(' ')[1];////////////////
   box.removeClass(currnt_icon_type)
   box.addClass("img-0");
}
function  delete_data_from_json(type,box){
   var delete_flag=0;
   if(type=="course"){
       course_id=box.find(".course-hidden-box").val();
       var data=check_data_avilable(courses_array,course_id);
       if(data[0]){
           courses_array.splice(data[1],1);
           delete_icon_set(box)
       }
    }
   else if(type=="year"){
       var parent_details=get_parent_details(box,type)
       var parent_data=check_data_avilable(courses_array,parent_details[1])
       var parent_array_obj=courses_array[parent_data[1]]
       if(parent_data[0]){
           var array_obj=parent_array_obj.years;
           if(array_obj.size()==1){
             delete_flag=1
           }
           data=check_data_avilable(array_obj,parent_details[0]);
           if(data[0]){
               array_obj.splice(data[1],1);
               delete_icon_set(box)
               if(delete_flag==1){
                   courses_array.splice(parent_data[1],1)
               }
               modify_box_icon(box);
               setting_parent_check_box(parent_array_obj,parent_details[1],"year")
           }
       }
   }
    else{
        var parent_details=get_parent_details(box,type)
        var course_data=check_data_avilable(courses_array,parent_details[3])
        var course_array_obj=courses_array[course_data[1]].years;
        var year_data=check_data_avilable(course_array_obj,parent_details[2]);
        var parent_array_obj=courses_array[course_data[1]].years[year_data[1]]
        var array_obj=parent_array_obj.batches;
        if(array_obj.size()==1){
            delete_flag=1
        }
        var data=check_data_avilable(array_obj,parent_details[0]);
        if(data[0]){
           c=array_obj.splice(data[1],1)
           delete_icon_set(box)
           if(delete_flag==1){
               course_array_obj.splice(year_data[1],1)
           }
           modify_box_icon(box)
           setting_parent_check_box(parent_array_obj,parent_details[2],type)
           setting_parent_check_box(parent_array_obj,parent_details[3],"year")
        }

    }
 }
function preview_event(event_id)
{
  if (event_id.length!==0) {
    j.ajax({
        url: '/alumni_event_invitations/event_preview/'+event_id,
        type: 'GET',
        success: function(result) {
                j('#preview').html(result);
                j('#submit_div a').css({"background":"#27292B"});

        }
    });
  }

}
function create_json_data(type,current_div){
    if(type=="course"){
            create_course_into_json(current_div)
        }
    else if(type=="year"){
        create_year_into_json(current_div)
    }
    else{
        create_batch_into_json(current_div)
    }

}
function setting_child_check_box(type,box,data_val){
 var child_div;
 var checks=new Array;
    if(type=="course"){
    checks=j("#year_list_div").find(".sub-hidden-box-for-course");
    if(checks.size()>0)
    {
    j.each(checks, function(idx,elem){
        if (j(elem).val()==box.find('.course-hidden-box').val()){
            child_div=j(elem).parent();
            icon_type=child_div.attr('class').split(' ')[1];
            child_div.removeClass(icon_type);
            if(data_val==1){
                var h_field = child_div.find(".sub-hidden-box").val(1);
                child_div.addClass("img-1");
                create_year_into_json(child_div);
                setting_child_check_box("year",child_div,data_val);
            }
            else{
                child_div.addClass('img-0');
                setting_child_check_box("year",child_div,data_val);
            }
        }
    });
    }
 }else if(type=="year"){
    checks=j("#batch_list_div").find(".sub-hidden-box-for-year");
    if(checks.size()>0)
    {
     j.each(checks, function(idx,elem){
            if (j(elem).val()==box.find('.sub-hidden-box-for-course').attr('name')){
                child_div=j(elem).parent();
                icon_type=child_div.attr('class').split(' ')[1];
                child_div.removeClass(icon_type);
                if(data_val==1){
                    child_div.addClass("img-1");
                    var h_field = child_div.find(".sub-hidden-box").val(1);
                    create_batch_into_json(child_div)
                }
                else{
                    child_div.addClass('img-0')
                    var h_field = child_div.find(".sub-hidden-box").val(0);

                }
            }
        });
     }
   }
}
function create_course_into_json(current_div){
 var parent_details=get_parent_details(current_div,"course")
 var years_array=new Array
    course_hash={
                 id:parent_details[0],
                 name:parent_details[1],
                 is_seleted:1,
                 years:years_array
             }
        courses_array.push(course_hash)
        modify_box_icon(current_div)
}
function create_year_into_json(current_div){
    var parent_details=get_parent_details(current_div,"year")
    var batches_array=new Array
    years_hash={
                 id:parent_details[0],
                 name:parent_details[0],
                 is_seleted:1,
                 batches:batches_array
               }
    array_obj=courses_array;
    var data=check_data_avilable(array_obj,parent_details[1])
    if(data[0]){// course already present
        var parent_array_obj=courses_array[data[1]]
        parent_array_obj.years.push(years_hash) //pushing the  year  into curresponding hash
        modify_box_icon(current_div)
        setting_parent_check_box(parent_array_obj,parent_details[1],"year")

    }
    else{
        parent_course_div=j("#course_"+parent_details[1]);//getting parent course div
        create_course_into_json(parent_course_div);
        create_year_into_json(current_div)
    }
}
function create_batch_into_json(current_div){
    var parent_details=get_parent_details(current_div,"batch")
    array_obj=courses_array;
    var data=check_data_avilable(array_obj,parent_details[3]);
    if(data[0]){
            year_test_flag=false
            course_array_obj=courses_array[data[1]]
            array_obj=courses_array[data[1]].years
            var data_year=check_data_avilable(array_obj,parent_details[2]);
            if(data_year[0]){
                batches_hash={id:parent_details[0],is_seleted:1,batch_name:parent_details[1]}
                var parent_array_obj=courses_array[data[1]].years[data_year[1]]
                parent_array_obj.batches.push(batches_hash)
                modify_box_icon(current_div)
                setting_parent_check_box(parent_array_obj,parent_details[2],"batch")
                setting_parent_check_box(course_array_obj,parent_details[3],"year")
            }else{
                year_div=j("#year_"+parent_details[2])
                create_year_into_json(year_div)
                create_batch_into_json(current_div)
             }
    }else{
        year_div=j("#year_"+parent_details[2])
        create_year_into_json(year_div)
        create_batch_into_json(current_div)
    }
}
function check_data_avilable(array_obj,id){
 var flag=false
 var index
     j.each(array_obj, function(idx, elem){
            if(elem.id==id)
                {
                    flag=true
                    index=idx
                }
        });
    return [flag,index]
}

//function_to_set_parent_check_box
 function setting_parent_check_box(array_obj,parent_id,div_name){
        var selected_div_count_full=j("#"+div_name+"_list_div").find(".img-1").length
        var selected_div_count_patial=j("#"+div_name+"_list_div").find(".img-2").length
        var total_count
        var total_count_selectecd=selected_div_count_full+selected_div_count_patial
        var parent_div_type
        if(div_name=="year"){
            total_count=year_div_total_count
            parent_div_type="course"
        }else if(div_name=="batch"){
            total_count=batch_div_total_count
            parent_div_type="year"
        }
           if(total_count_selectecd>=total_count && selected_div_count_patial==0){
                set_checkbox(array_obj,parent_div_type,parent_id,"img-1",1)
           }
           else if(total_count_selectecd==0){
                set_checkbox(array_obj,parent_div_type,parent_id,"img-0",0)
           }
           else{
                set_checkbox(array_obj,parent_div_type,parent_id,"img-2",2)
           }
 }
function set_checkbox(array_obj,type,id,cat_ry,val){
       var target_parent_div=j("#"+type+"_"+id)
       var taget_parent_class=target_parent_div.attr('class').split(' ')[1]
       array_obj.is_seleted=val
       target_parent_div.removeClass(taget_parent_class)
       j("#"+type+"_"+id).addClass(cat_ry);
       var taget_parent_class=target_parent_div.attr('class').split(' ')[1]
       h_field = target_parent_div.find(".sub-hidden-box");
       h_field.val(val);
}

function populate_list(type,id){
    var return_data
    if(type==1){
        course_id=id
        box_div=j("#course_"+id);
        change_color_div(box_div,"course")
        j("#year_list_div").find(".check-text-pair").hide();
        j("#batch_list_div").find(".check-text-pair").hide();
        j("#loader_in_year").show();
        return_data=find_parent_div("course",id)
    }
    if(type==2){
        box_div=j("#year_"+id);
        change_color_div(box_div,"year")
        j("#batch_list_div").find(".check-text-pair").hide();
        j("#loader_in_batch").show();
        return_data=find_parent_div("year",id)
    }
    j.ajax({
        url: '/alumni_event_invitations/populate_list',
        type: 'GET',
         data: {
             query:{
                    id:id,
                    type:type,
                    course_id:course_id
                 }
        },
        beforeSend:function(){
            if(type==1){
                j("#loader_in_year").show();
                }
            if(type==2){
                j("#loader_in_batch").show();
            }
        },
        success: function(result) {
                if(type==1){  //to render the year div
                     j("#loader_in_year").hide();
                     j('#year_list_div').html(result);
                     year_div_total_count=j('#year_list_div .check-text-pair').length;
                     setting_child_check_box("course",return_data[0],return_data[1]);
                     if(return_data[1]==2){
                            retrive_data_from_json("course",id)
                     }
                }
                else if(type==2){ //to render the batch div
                     j("#loader_in_batch").hide();
                     j('#batch_list_div').html(result);
                     batch_div_total_count=j('#batch_list_div .check-text-pair').length;
                     setting_child_check_box("year",return_data[0],return_data[1]);
                     if(return_data[1]==2){
                               retrive_data_from_json("year",id)
                     }
                }
        }
    });

}
function get_parent_details(current_div,type){
    //type is to find how manty levels of data is needed
   if(type=="course")
    {
        var course_id=current_div.find(".course-hidden-box").val();
        var course_name=current_div.find(".course-hidden-box").attr('name');
        return [course_id,course_name]
    }
    if(type=="year"){
        var year_id=current_div.find(".sub-hidden-box-for-course").attr('name');
        var parent_course_id=current_div.find(".sub-hidden-box-for-course").val();
        return [year_id,parent_course_id]
    }
    if(type=="batch"){
        var batch_id=current_div.find(".sub-hidden-box-for-year").attr('name');
        var batch_name=current_div.parent().text().trim();
        var parent_year_id=current_div.find(".sub-hidden-box-for-year").val();
        var parent_course_id=j("#year_"+parent_year_id).find(".sub-hidden-box-for-course").val();
        return [batch_id,batch_name,parent_year_id,parent_course_id]
    }
}

function  find_parent_div(parent_div_type,id){
    var flag=0;
    var parent_div_box=j("#"+parent_div_type+"_"+id)
    var icon_type=parent_div_box.attr('class').split(' ')[1];
    if(icon_type=="img-1"){
        flag=1
    }
    if(icon_type=="img-2"){
        flag=2
     }
    return [parent_div_box,flag]
}
 function retrive_data_from_json(parent_div_type,id){
     var data
     if(parent_div_type=="course")
         {
            data=check_data_avilable(courses_array,id)
            if(data[0]){
                child_array_obj=courses_array[data[1]].years
                set_check_box_in_loading("year",child_array_obj)

             }
         }
      else if(parent_div_type=="year"){
            var parent_course_id=j("#year_"+id).find(".sub-hidden-box-for-course").val();
            parent_data=check_data_avilable(courses_array,parent_course_id)
            data=check_data_avilable(courses_array[parent_data[1]].years,id)
            if(data[0]){
                child_array_obj=courses_array[parent_data[1]].years[data[1]].batches
                set_check_box_in_loading("batch",child_array_obj)
            }
        }

 }
 function set_check_box_in_loading(type,child_array_obj){
    j.each(child_array_obj, function(idx, elem){
           var id=elem.id
           if(type!="batches"){
                   if((elem.is_seleted==2)){
                       set_checkbox_from_json(id,type,"img-2",2)

                   }
           }
           if(elem.is_seleted==1){
               set_checkbox_from_json(id,type,"img-1",1)
           }
           if(elem.is_seleted==0){
               set_checkbox_from_json(id,type,"img-0",0)
           }
    });
 }
 function set_checkbox_from_json(id,type,cat_gry,val){
       var target_parent_div=j("#"+type+"_"+id)
       target_parent_div.removeClass("img-0")
       target_parent_div.addClass(cat_gry);
       h_field = target_parent_div.find(".sub-hidden-box");
       h_field.val(val);
 }
