var organisers_array=[];
var orginal_data
var organiser;
var dep_id;
var department_name;
var department_id;
function check_selection() {
    if((j('#'+dep_id).length)){
        j('.select-user').each(function(){
            var org_id=j(this).val();
            var org_name=j("#name_div_"+org_id).text();
            if(j('#'+org_id).prop('checked') == true){
                var flag=true;
                j(organisers_array).each(function (a,b){
                    if(b.id==org_id){
                        b.is_checked=true;
                        flag=false;
                    }
                });
                if(flag==true){
                    organiser={
                        id:org_id,
                        name:org_name,
                        is_checked:true,
                        dep_id:dep_id,
                        dep_name:department_name
                    }
                    organisers_array.push(organiser);

                }
            }
            else if(j('#'+org_id).prop('checked') == false){
                j(organisers_array).each(function (a,b){
                    if(b.id==org_id){
                        b.is_checked=false
                        organisers_array=removeByIndex(organisers_array,a)
                    }
                });
            }

        });
        display_count(dep_id);
    }
    else{
     if ((typeof department_name != 'undefined')&& (dep_id)){
         test_flag=false;
        j('.select-user:checked').each(function(){
            var org_id=j(this).val();
            var org_name=j("#name_div_"+org_id).text();
            organiser={
                id:org_id,
                name:org_name,
                is_checked:true,
                dep_id:dep_id,
                dep_name:department_name
            }
            test_flag=true;
            organisers_array.push(organiser);
            check_checkAll_button();
        });
        if(test_flag){
            j('#selected_organisers').append('<div class="selected_dep"><div onclick="select_department_function('+dep_id+')" id="'+dep_id+'">'+department_name+'<span>&#x200E;&#x200E;</span></div><span class="remove-icon" onclick="delete_department_function(\''+dep_id+'\')"></span></div>');
        }
     }
   }
    display_count(dep_id);
}

function removeByIndex(arr, index) {
    arr.splice(index, 1);
    return arr
}
function send_selected(){
  Modalbox.close();
  dispaly_employee_organisers();
  j('#added_org').show();
  j('#flash_box').attr('tabindex',-1).focus();
}

function dispaly_employee_organisers(){
//To dispaly selected organiser-employee  in to new page
var org_type=j('#tag_employee').val();
j('#organiser_div_employee').empty();
console.log(organisers_array)
j(organisers_array).each(function (a,elem){
    console.log(elem.name)
    j('#organiser_div_employee').append('<div class="organiser"><label class="organiser_name">'+elem.name+'</label><label class="organiser_position">'+org_type+'</label></div>')
});
}
function sort_selected_orgainsers(dep_id){
    j(organisers_array).each(function (a,elem){
        if (elem.dep_id==dep_id)
        {
            j('#user_list table tbody:last').find('tr:first').after('<tr><td class="chk"><div class="name_div"><input type="checkbox" id='+elem.id+' onclick="check_checkAll_button()" class="select-user" value='+elem.id+'></div></td><td><div class="name_div" style="padding-left: 10px" id="name_div_'+elem.id+'">'+elem.name+'</div></td></tr>');
            if(elem.is_checked==true) {
                j( "#"+elem.id ).prop("checked",true);
            }
        }

    });
}
function select_department_function(flag) {
    if (flag==0){
        dep_id=j("#select_department_id option:selected").val();
        department_name=j("#select_department_id option:selected").text();
    }
    else{
        dep_id=flag.id;
        department_name=flag.textContent.slice(0,-3);
    }
    department_id=dep_id.replace("dep_","");
    j(".chk").parent('tr').text("");
    j('#loader_in_org').show();
    var formURL = "/alumni_event_invitations/list_organiser_employees";
    var organisers_id=get_organisers_id(dep_id);
    j.ajax(
    {
        url: formURL,
        type: "GET",
        data: {
            query: {
                department_id:department_id,
                organisers_array:organisers_id
            }
        },
        success: function (data, textStatus, jqXHR) {
           j("#user_list").html(data);
             sort_selected_orgainsers(dep_id);
             check_checkAll_button();
        },
        error: function (jqXHR, textStatus, errorThrown){
        }
    })
}
function get_organisers_id(dep_id){
   var organisers_id=[];
    j(organisers_array).each(function (a,elem){
        if (elem.dep_id==dep_id)
        {
            organisers_id.push(elem.id);
        }
    });
    return organisers_id
}
function display_count(dep_id)
{
    var dep_count=0;
    var total_count=0;
    j(organisers_array).each(function (a,elem){
        if (elem.dep_id==dep_id)
        {
         dep_count++;
        }
    });
    j('#'+dep_id+' span').html("&#x200E;("+dep_count+")&#x200E;");
    get_total_count();
}
function get_total_count()
{
    var total_count=0;
     j(organisers_array).each(function (a,elem){
        if (elem.is_checked==true)
        {
         total_count++;
        }
    });
    j("#dispaly_total_count span").html(total_count);
}
function set_checked_all()
{
   if(j('.select-user_full').prop('checked') == true)
      {
        j('.select-user').each(function(){
            j(this).prop('checked', true);
        });
   }
   else{
        j('.select-user').each(function(){
            j(this).prop('checked', false);
        });
   }

}
function close_employee_model(){
       organisers_array=orginal_data
       Modalbox.close();
}
function delete_department_function(cat_id){
    var i=organisers_array.length
    while(i--){
        j(organisers_array).each(function (a,elem){
            if (elem.dep_id==cat_id)
            {
                organisers_array.splice(a,1);
                a=a-1;
            }
        });
    }
    j("#"+cat_id).parent().remove();
    console.log(cat_id)
    if (j('#parent_data').val()==cat_id)
    {
            j(".chk").parent('tr').text("");
    }
    display_students_count(cat_id)
}
function set_initial_employee_data(data){
  orginal_data=data
  if(organisers_array.length==0){
     organisers_array=data
  }
   j(organisers_array).each(function (a,b){
    if((j('#'+b.dep_id).length)){
    }
    else{
        j('#selected_organisers').append('<div class="selected_dep"><div onclick="select_department_function('+b.dep_id+')" id="'+b.dep_id+'">'+b.dep_name+'&#x200E;<span>()</span>&#x200E;</div><span class="remove-icon" onclick="delete_department_function(\''+b.dep_id+'\')"></span></div>');
    }
    display_count(b.dep_id);
  });
}
function check_checkAll_button(){
  var all_check_box= j("#user_list").find(".select-user").length;
  var all_clicked_check_box= j("#user_list").find(".select-user:checked").length;
  var test=(all_check_box==all_clicked_check_box)&& all_check_box!=0 ? j('.select-user_full').prop('checked',true):j('.select-user_full').prop('checked',false);
}