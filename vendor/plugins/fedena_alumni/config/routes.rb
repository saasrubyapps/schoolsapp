ActionController::Routing::Routes.draw do |map|
    map.resources :alumni_event_invitations,:collection=>{:organiser_employee=>[:get,:post],:populate_list=>[:get],:select_batches=>[:get],:list_organiser_students=>[:get],:organiser_student=>[:get],:list_organiser_employees=>[:get],:batch_list_using_cource_and_passout_year=>[:get],:authenticate_alumni=>[:get],:accept_invitation=>[:get],:reject_invitation=>[:get],:alumni_event_organisers_json=>[:get]}
  	map.resources :alumni,:only => [:new, :create,:destroy,:show],:collection=>{:view_alumni_students=>[:get],:invite_alumni_students=>[:get,:post],:contact=>[:get,:post],:alumni_student_list=>[:get],:select_batches=>[:get],
    :select_alumni=>[:get],:delete_comment=>[:delete],:select_passout_year=>[:get],:contact_select_passout_year=>[:get],:contact_select_batches=>[:get],:contact_student_and_batch=>[:get],:batch_list_in_contact=>[:get],:student_and_batch_list=>[:get]}
    map.resources :alumni_events,:collection=>{:alumni_show=>[:get]},:has_many => :comments
    map.alumni_event_preview 'alumni/:id/:title', :controller=>'alumni',:action=>'preview_event'
    map.alumni_show_alumni_events 'alumni_events/:id/:title', :controller=>'alumni_events',:action=>'alumni_show',:has_many => :comments
end
