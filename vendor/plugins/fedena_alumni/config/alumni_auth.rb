authorization do

	role :alumni_control_organiser do
		has_permission_on [:alumni_event_invitations],
			:to => [
				:new,
				:create
				] ,:join_by => :and  do
					if_attribute :id =>  nil
					if_attribute :is_current_user_an_organiser =>true
				end
		has_permission_on [:alumni_event_invitations],
			:to => [
				:new,
				:create
				] ,:join_by => :and  do
					if_attribute :get_organiser_users => contains { user }
					if_attribute :is_expired? => false
				end
	 	has_permission_on [:alumni_event_invitations],
			:to => [
				:event_preview,
				:populate_list,
				:select_batches,
				:list_organiser_students,
				:list_organiser_employees,
				:batch_list_using_cource_and_passout_year,
				:alumni_event_organisers_json
			]
		has_permission_on [:alumni_event_invitations],
			:to => [
				:index
			] do
				if_attribute :alumni_event_organiser? =>  true
			end
		has_permission_on [:alumni],
		  :to => [
		  	:index,
				:invite_alumni_students,
				:alumni_student_list,
				:select_batches,
				:select_passout_year,
				:contact_select_passout_year,
				:contact_select_batches,
				:contact_student_and_batch,
				:batch_list_in_contact,
				:student_and_batch_list,
				:preview_event
			] do
		      if_attribute :alumni_event_organiser? => true
		    end
		has_permission_on [:alumni_events],
		  :to => [
				:show
			] do
				if_attribute :get_organiser_users => contains { user }
			end

		# has_permission_on [:comments],
		#   :to => [
		# 		:new,
		# 		:create,
		# 		:destroy,
		# 		:show,
		# 		:index
		# 	]
	end
	role :manage_alumni  do
		has_permission_on [:alumni_event_invitations],
			:to => [
				:new,
				:create
				] ,:join_by => :or  do
					if_attribute :id =>  nil
					if_attribute :is_expired? => false
				end
		has_permission_on [:alumni_event_invitations],
			:to => [
				:index,
				:event_preview,
				:organiser_employee,
				:populate_list,
				:select_batches,
				:list_organiser_students,
				:organiser_student,
				:list_organiser_employees,
				:batch_list_using_cource_and_passout_year,
				:alumni_event_organisers_json
			]
		has_permission_on [:alumni],
		  	:to => [
        :index,
        :new,
        :create,
				:view_alumni_students,
				:invite_alumni_students,
				:contact,
				:alumni_student_list,
				:select_batches,
				:select_passout_year,
				:contact_select_passout_year,
				:contact_select_batches,
				:contact_student_and_batch,
				:batch_list_in_contact,
				:student_and_batch_list,
				:preview_event
			]
		has_permission_on [:alumni_events],
		  :to => [
				:show,
				:edit,
				:update,
				:destroy
			]
		has_permission_on [:comments],
		  :to => [
				:new,
				:create,
				:destroy,
				:show,
				:index
			]
	  end

	# admin privileges
	role :admin do
		includes :manage_alumni
	end

	# employee -privileges
	role :alumni_event_organiser do
		includes :alumni_control_organiser
	end
	role :student do
		includes :alumni_control_organiser
	end
	role :employee do
		includes :alumni_control_organiser
	end
end
