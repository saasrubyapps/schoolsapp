Gretel::Crumbs.layout do
  crumb :alumni_index do
    link I18n.t('alumni_text'), {:controller=>"alumni",:action=>"index"}
  end
  crumb :alumni_view_alumni_students do
    link I18n.t('view_alumni_students'), {:controller=>"alumni",:action=>"view_alumni_students"}
     parent :alumni_index
  end
  crumb :alumni_event_invitations_index do
    link I18n.t('view_event_invites'), {:controller=>"alumni_event_invitations",:action=>"index"}
     parent :alumni_index
  end

  crumb :alumni_event_invitations_show do |alumni_event|
     link  alumni_event.title,{:controller=>"alumni_event_invitations",:action=>"show",:id=> alumni_event.id}
     parent :alumni_event_invitations_index
  end
  crumb :alumni_event_invitations_new_event do |alumni_event|
    link  I18n.t('invite_alumni'),{:controller=>"alumni_event_invitations",:action=>"new",:id=> alumni_event.id}
    parent :alumni_event_invitations_show,alumni_event
  end
  crumb :alumni_contact do
    link I18n.t('contact_alumni'), {:controller=>"alumni",:action=>"contact"}
     parent :alumni_index
  end
  crumb :alumni_event_invitations_new do
    link I18n.t('invite_alumni'), {:controller=>"alumni_event_invitations",:action=>"new"}
    parent :alumni_index
  end
end
