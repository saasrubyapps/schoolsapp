require 'translator'
require File.join(File.dirname(__FILE__), "lib", "fedena_alumni")
require File.join(File.dirname(__FILE__), "config", "breadcrumbs")
FedenaPlugin.register = {
  :name=>"fedena_alumni",
  :description=>"Fedena Alumni Plugin",
  :more_menu=>{:title=>"Alumni Plugin",:controller=>"student",:action=>"index"},
  :auth_file=>"config/alumni_auth.rb",
  :dashboard_menu=>{:title=>"alumni_label",:controller=>"alumni",:action=>"index",
  :options=>{:class=>"option_buttons",:id => "alumni_button", :title => "alumni_description"}
  },
  :css_overrides=>[{:controller=>"user",:action=>"dashboard"}],
  :icon_class_link=>{:plugin_name=>"fedena_alumni",:stylesheet_path=>"alumni_link_icon.css"},
  :autosuggest_menuitems=>[
    {:menu_type => 'link' ,:label => "alumni",:value =>{:controller => :alumni,:action => :index}},
    {:menu_type => 'link' ,:label => "contact_alumni",:value =>{:controller => :alumni,:action => :contact}},
    {:menu_type => 'link' ,:label => "view_alumni_students",:value =>{:controller => :alumni,:action => :view_alumni_students}},
    {:menu_type => 'link' ,:label => "view_alumni_event_invites",:value =>{:controller => :alumni_event_invitations,:action => :index}},
  ],
  :multischool_models=>%w{AlumniEvent AlumniEventComment AlumniEventOrganiser AlumniEventInvitation},
  :multischool_classes=>%w{FedenaAlumniEventInviter FedenaAlumniEventContact FedenaAlumniEventCancelInvitation }
}
FedenaAlumni.attach_overrides

Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
  I18n.load_path.unshift(locale)
end

if RAILS_ENV == 'development'
  ActiveSupport::Dependencies.load_once_paths.\
    reject!{|x| x =~ /^#{Regexp.escape(File.dirname(__FILE__))}/}
end
