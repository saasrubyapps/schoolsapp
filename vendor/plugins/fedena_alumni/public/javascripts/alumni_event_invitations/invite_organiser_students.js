var student_organisers_array=[];
var organiser;
var student_batch_name;
var batch_id;
var student_organisers_orginal_data
function check_student_selection() {
    if((j('#'+batch_id).length)){
        j('.select-user').each(function(){
            var student_id=j(this).val();
             var student_name=j("#name_div_"+student_id).text();
            if(j('#'+student_id).prop('checked') == true){
                var flag=true;
                j(student_organisers_array).each(function (a,b){
                    if(b.id==student_id){
                        b.is_checked=true;
                        flag=false;
                    }
                });
                if(flag==true){
                    organiser={
                        id:student_id,
                        name:student_name,
                        is_checked:true,
                        batch_id:batch_id,
                        dep_name:batch_name
                    }
                    student_organisers_array.push(organiser);

                }
            }
            else if(j('#'+student_id).prop('checked') == false){
                j(student_organisers_array).each(function (a,b){
                    if(b.id==student_id){
                        b.is_checked=false
                        student_organisers_array=removeByIndex(student_organisers_array,a)
                    }
                });
            }

        });
        display_students_count(batch_id);
    }
    else{
        test_flag=false;
        j('.select-user:checked').each(function(){
            var student_id=j(this).val();
            var student_name=j("#name_div_"+student_id).text();
            organiser={
                id:student_id,
                name:student_name,
                is_checked:true,
                batch_id:batch_id,
                batch_name:batch_name
            }
            test_flag=true;
            student_organisers_array.push(organiser);
        });
        if(test_flag){
            j('#selected_organisers').append('<div class="selected_dep"><div  onclick="select_organisers_student_function('+batch_id+')" id="'+batch_id+'">'+batch_name+'<span></span></div><span class="remove-icon" onclick="delete_batch_function(\''+batch_id+'\')"></span></div>');
        }
  }
       display_students_count(batch_id);
}

function removeByIndex(arr, index) {
    arr.splice(index, 1);
    return arr
}
function send_student_selected(){
j('#added_org').show();
Modalbox.close();
dispaly_student_organisers();
j('#flash_box').attr('tabindex',-1).focus();
}
function dispaly_student_organisers(){
//To dispaly selected organiser-students  in to new page
j('#organiser_div_student').empty();
var org_type=j('#tag_student').val();
j(student_organisers_array).each(function (a,elem){
    j('#organiser_div_student').append('<div class="organiser"><label class="organiser_name">'+elem.name+'</label><label class="organiser_position">'+org_type+'</label><div>')
});
}

function sort_selected_student_orgainsers(batch_id){

    j(student_organisers_array).each(function (a,elem){
        if (elem.batch_id==batch_id)
        {
            j('#user_list table tbody:last').find('tr:first').after('<tr><td class="chk"><div class="name_div"><input type="checkbox" id='+elem.id+' class="select-user" value='+elem.id+'></div></td><td><div class="name_div" style="padding-left: 10px" id="name_div_'+elem.id+'">'+elem.name+'</div></td></tr>');
            if(elem.is_checked==true) {
                j( "#"+elem.id ).prop("checked",true);
            }
        }

    });
}
function select_organisers_student_function(flag) {

    if (flag==0){
        batch_name=j("#select_batch_id option:selected").text();
        batch_id=j("#select_batch_id option:selected").val();
    }
    else{
        batch_id=flag.id;
        batch_name=flag.textContent
    }
    student_batch_id=batch_id.replace("batch_","");
    j(".chk").parent('tr').text("");
    j('#loader_in_org').show();
    var formURL = "/alumni_event_invitations/list_organiser_students";
    var student_organisers_id=get_students_organisers_id(batch_id);
    j.ajax(
    {
        url: formURL,
        type: "GET",
        data: {
            query: {
                batch_id:student_batch_id,
                organisers_student_array:student_organisers_id
            }
        },
        success: function (data, textStatus, jqXHR) {
           j("#user_list").html(data);
             sort_selected_student_orgainsers(batch_id);
             check_checkAll_button();
        },
        error: function (jqXHR, textStatus, errorThrown){
        }
    })
}
function get_students_organisers_id(batch_id){
   var organisers_id=[];
    j(student_organisers_array).each(function (a,elem){
        if (elem.batch_id==batch_id)
        {
            organisers_id.push(elem.id);
        }
    });
    return organisers_id
}
function display_students_count(batch_id)
{
    var dep_count=0;
    var total_count=0;
    j(student_organisers_array).each(function (a,elem){
        if (elem.batch_id==batch_id)
        {
         dep_count++;
        }
    });
    j(student_organisers_array).each(function (a,elem){
        if (elem.is_checked==true)
        {
         total_count++;
        }
    });
    j('#'+batch_id+' span').html("&#x200E;("+dep_count+")&#x200E;");
    j("#dispaly_total_count span").html(total_count);
}


function select_batch_function()
{
    course_name=j("#select_course_id option:selected").val();
    var formURL = "/alumni_event_invitations/select_batches";
    j.ajax(
    {
        url: formURL,
        type: "GET",
       data: {
                query: {
                            course_id:course_name
                       }
            },
        success: function (data, textStatus, jqXHR) {
            j("#select_batches_partial").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
function delete_batch_function(batch_id){
   var i=student_organisers_array.length
        while(i--){
         j(student_organisers_array).each(function (a,elem){
            if (elem.batch_id==batch_id)
            {
                student_organisers_array.splice(a,1);
            }
        });
    }
    j("#"+batch_id).parent().remove();
    if(j('#parent_data').val()==batch_id)
    {
            j(".chk").parent('tr').text("");
    }
    display_students_count(batch_id)

}
function set_initial_student_data(data){
   student_organisers_orginal_data=data
  if(student_organisers_array.length==0){
    
     student_organisers_array=data
  }
   j(student_organisers_array).each(function (a,b){
    if((j('#'+b.batch_id).length)){
    }
    else{
        j('#selected_organisers').append('<div class="selected_dep"><div  onclick="select_organisers_student_function('+b.batch_id+')" id="'+b.batch_id+'">'+b.batch_name+'<span></span></div><span class="remove-icon" onclick="delete_batch_function(\''+b.batch_id+'\')"></span></div>');
    }
    display_students_count(b.batch_id);
  });
}
function close_student_model(){
       student_organisers_array=student_organisers_orginal_data
       Modalbox.close();
}
function check_checkAll_button(){
  var all_check_box= j("#user_list").find(".select-user").length;
  var all_clicked_check_box= j("#user_list").find(".select-user:checked").length;
  var test=(all_check_box==all_clicked_check_box) ? j('.select-user_full').prop('checked',true):j('.select-user_full').prop('checked',false);
}