class AlumniMailer < ActionMailer::Base

	def contact_email(student_email,student_name,message,subject)
		#TODO implement host stripping with better logic
		AlumniMailer.default_url_options[:host] = Fedena.hostname.gsub("http://","").gsub("https://","")
	    recipients    student_email
	    from          ""
	    subject       Configuration.get_config_value('InstitutionName').empty? ? "Fedena" : Configuration.get_config_value('InstitutionName') + " "+t('alumni') +" : "+subject
	    headers       "return-path" => 'noreply@fedena.com'
	    content_type  "text/html; charset=utf-8"
	    sent_on       Time.now
	    body          :student =>student_name ,:message=>message
  	end
  	def invitation_email(recipient,alumni_event,auth_token,message)
		AlumniMailer.default_url_options[:host] = Fedena.hostname.gsub("http://","").gsub("https://","")
  		recipients    recipient.email
	    from          ""
	    subject       Configuration.get_config_value('InstitutionName').empty? ? "Fedena" : Configuration.get_config_value('InstitutionName') +" "+t('alumni_alumni_event_invitation')
	    headers       "return-path" => 'noreply@fedena.com'
	    content_type   "text/html; charset=utf-8"
	    sent_on       Time.now
      body          :student => recipient,:alumni_event=>alumni_event,:message=>message, :url => authenticate_alumni_alumni_event_invitations_url+"/"+auth_token #authenticate_alumni_alumni_event_invitations_url(auth_token) is not working
  	end
	def cancel_invitation_email(recipient,alumni_event)
		AlumniMailer.default_url_options[:host] = Fedena.hostname.gsub("http://","").gsub("https://","")
		recipients    recipient.email
	    from          ""
	    subject       Configuration.get_config_value('InstitutionName').empty? ? "Fedena" : Configuration.get_config_value('InstitutionName') +" "+t('alumni_event_invitation_cancelled')
	    headers       "return-path" => 'noreply@fedena.com'
	    content_type   "text/html; charset=utf-8"
	    sent_on       Time.now
     	body          :student => recipient,:alumni_event=>alumni_event
  	end

end
