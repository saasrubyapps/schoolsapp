class AlumniEventInvitation < ActiveRecord::Base
  validates_uniqueness_of :alumni_event_id, :scope => :archived_student_id
  belongs_to :alumni_event
  belongs_to :archived_student
  before_create :generate_token
  def self.get_all_invitations_count()
   @count_data = AlumniEvent.all(:select=>"DISTINCT(event_id),
  													status,COUNT(DISTINCT(alumni_event_invitations.id)) as count,
  													alumni_events.*,COUNT(case when status=1 then alumni_event_invitations.status end ) as accepted,
  													COUNT(case when status=0 then alumni_event_invitations.status end ) as not_responded,
  													COUNT(case when status=2 then alumni_event_invitations.status end ) as rejected",
                            :joins=>:alumni_event_invitations,:group=>'alumni_events.id'
												)
     return @count_data
  end
  def self.get_invitation_count(event_id)
   @count_data = AlumniEvent.find(:all,:conditions=>{:event_id=>event_id},:select=>"DISTINCT(event_id),
  													status,COUNT(DISTINCT(alumni_event_invitations.id)) as count,
  													alumni_events.*,COUNT(case when status=1 then alumni_event_invitations.status end ) as accepted,
  													COUNT(case when status=0 then alumni_event_invitations.status end ) as not_responded,
  													COUNT(case when status=2 then alumni_event_invitations.status end ) as rejected",
                            :joins=>:alumni_event_invitations,:group=>'alumni_events.id'
												)
     return @count_data
  end
  def send_invitation(message)
    if is_responded? || !self.is_valid?
      return
    end
    recipient=self.archived_student
    alumni_event=self.alumni_event
    auth_token=self.auth_token
    unless recipient.email.empty?
      AlumniMailer.deliver_invitation_email(recipient,alumni_event,auth_token,message)
    end
  end
  def is_responded?()
    self.status!=0
  end
  def is_accepted?()
    self.status==1
  end
  def is_rejected?()
    self.status==2
  end
  def accept_invitation()
    self.status=1;
    self.save!
  end
  def reject_invitation()
    self.status=2;
    self.save!
  end
  def reset_invitation()
    self.status=0;
    self.save!
  end
  def cancel_invitation
    recipient=self.archived_student
    alumni_event=self.alumni_event
    unless recipient.email.empty?
      AlumniMailer.deliver_cancel_invitation_email(recipient,alumni_event)
    end
  end
  def is_valid?
    if self.archived_student && self.alumni_event
      return true
    else
      return false
    end
  end
  def get_status()
    case self.status
      when 0
        return "not_responded"
      when 1
        return "accepted"
      when 2
        return "rejected"
      else
        return "invalid_state"
      end
  end

  private
    def generate_token
      self.auth_token=Digest::SHA1.hexdigest([Time.now, rand].join)
    end

end
