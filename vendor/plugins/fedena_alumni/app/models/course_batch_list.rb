#no longer in use since plan to persist the invitation list is dropped
# exists for histrorical reasons
class CourseBatchList < ActiveRecord::Base
	belongs_to :group,:polymorphic=>true
	def self.get_student_list
		archived_student_list=Set.new
		CourseBatchList.all(:conditions=>"group_type='Course'").each do | course|
			archived_student_list+=get_archievd_students_by_course(course.group.id)
		end
		CourseBatchList.all(:conditions=>"group_type='YearOfCompletion'").each do | yoc|
			archived_student_list+=get_student_list_by_year_of_completion_and_course(yoc.group.year,yoc.group.course.id)
		end
		CourseBatchList.all(:conditions=>"group_type='Batch'").each do | batch|
			puts batch.group.archived_students
		end
		return archived_student_list
	end
	private
		def self.get_archievd_students_by_course(course_id)
			archived_student_list=[]
			course=Course.find_by_id(course_id)
			course.batches.each do |batch|
				archived_student_list<<batch.archived_students
 			end
 			return archived_student_list
		end
		def self.get_student_list_by_year_of_completion_and_course(year_of_completion,course_id)
			archived_student_list=[]
			course=Course.find_by_id(course_id)
			batches=course.batches.all(:conditions=>"EXTRACT(YEAR FROM end_date)>#{year_of_completion}")
			batches.each do |batch|
				archived_student_list<<batch.archived_students
 			end
 			return archived_student_list
		end
end
