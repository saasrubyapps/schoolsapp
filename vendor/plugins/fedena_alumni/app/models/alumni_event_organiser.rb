class AlumniEventOrganiser < ActiveRecord::Base
  belongs_to :alumni_event
  belongs_to :organiser,:polymorphic=>true
  validates_uniqueness_of :alumni_event_id, :scope => :organiser_id
  named_scope :student, :conditions => {:organiser_type => 'Student'}
  named_scope :employee, :conditions => {:organiser_type => 'Employee'}
end
