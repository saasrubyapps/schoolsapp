class AlumniEvent < ActiveRecord::Base
	belongs_to :event, :dependent => :destroy #TODO to be verified
	has_many :comments,:class_name=>'AlumniEventComment', :dependent => :destroy
	has_many :alumni_event_organisers, :dependent => :destroy
	has_many :alumni_event_invitations, :dependent => :destroy
  validates_presence_of :event_id
  validates_uniqueness_of :event_id
	# before_destroy :inform_deletion_to_invited_students
  def get_invitation_status
    AlumniEvent.find(:first,:select=>"event_id,COUNT(alumni_event_invitations.id) as count,COUNT(case when status=1 then alumni_event_invitations.status end ) as accepted,COUNT(case when status=0 then alumni_event_invitations.status end ) as not_responded,COUNT(case when status=2 then alumni_event_invitations.status end ) as rejected",:joins=>"LEFT JOIN `alumni_event_invitations` ON alumni_event_invitations.alumni_event_id = alumni_events.id",:group=>'alumni_events.id',:conditions=>{:id=>self.id})
  end
  def self.get_invitation_status
    AlumniEvent.find(:all,:select=>"DISTINCT(event_id),status,
        COUNT(DISTINCT(alumni_event_invitations.id)) as count,alumni_events.*,COUNT(case when status=1 then alumni_event_invitations.status end ) as accepted,
        COUNT(case when status=0 then alumni_event_invitations.status end ) as not_responded,
        COUNT(case when status=2 then alumni_event_invitations.status end ) as rejected",
        :joins=>"LEFT JOIN `alumni_event_invitations` ON alumni_event_invitations.alumni_event_id = alumni_events.id",
        :group=>'alumni_events.id',
        :order=>'alumni_events.created_at DESC'
        )
  end
  def self.get_organising_events_status(user)
    entry=user.student_entry || user.employee_entry
    AlumniEvent.find(:all,
        :select=>"DISTINCT(event_id),status,
          COUNT(DISTINCT(alumni_event_invitations.id)) as count,alumni_events.*,COUNT(case when status=1 then alumni_event_invitations.status end ) as accepted,
          COUNT(case when status=0 then alumni_event_invitations.status end ) as not_responded,
          COUNT(case when status=2 then alumni_event_invitations.status end ) as rejected",
        :joins=>"LEFT JOIN `alumni_event_invitations` ON alumni_event_invitations.alumni_event_id = alumni_events.id JOIN alumni_event_organisers on alumni_event_organisers.alumni_event_id=alumni_events.id",
        :group=>'alumni_events.id',
        :order=>'alumni_events.created_at DESC',
        :conditions=>"alumni_event_organisers.organiser_id=#{entry.id}"
        )
  end
	def is_expired?
		if(self.event.end_date <= Time.current)
			return true
		else
			return false
		end
	end
  def employee_alumni_event_organisers_json
    organisers=self.alumni_event_organisers
    organisers_list=[]
    organisers.each { |organiser|
      employee_hash={}
      if organiser.organiser_type=="Employee"
        employee_hash[:id]=organiser.organiser.id
        employee_hash[:name]=organiser.organiser.full_name
        employee_hash[:is_checked]=true
        employee_hash[:dep_id]="dep_#{organiser.organiser.employee_department.id}"
        employee_hash[:dep_name]=organiser.organiser.employee_department.name
        organisers_list.push(employee_hash)
      end
    }
    organisers_list
  end
  def student_alumni_event_organisers_json
    organisers=self.alumni_event_organisers
    organisers_list=[]
    organisers.each { |organiser|
      student_hash={}
      if organiser.organiser_type=="Student"
        student_hash[:id]=organiser.organiser.id
        student_hash[:name]=organiser.organiser.full_name
        student_hash[:is_checked]=true
        student_hash[:batch_id]="batch_#{organiser.organiser.batch.id}"
        student_hash[:batch_name]=organiser.organiser.batch.name
        organisers_list.push(student_hash)
      end
    }
    organisers_list
  end
	def self.retrive_batches(course_id,year)
    if year.present?
      @batches=Batch.find(:all,:conditions=>["course_id=? and is_active=false and EXTRACT(YEAR FROM end_date)=?",course_id,year]).to_set
    else
      @batches=Batch.find(:all,:conditions=>["course_id=? and is_active=false",course_id]).to_set
    end
      return @batches
  end
  def self.create_json_file(input_batch_id_list)
      batches=Array.new
      batches_hash = Hash.new
      input_batch_id_list.each do |b|
      batch_hash = Array.new
      batch=Batch.find(b)
      batch_hash = Hash.new
      batch_hash[:id]=batch.id
      batch_hash[:name]=batch.name
      students = Array.new
      batch.archived_students.each do|archived_student|
        student_hash = {:id=>archived_student.id,:admission_no=>archived_student.admission_no,:pass_out_year=>"#{archived_student.date_of_leaving.strftime("%Y")}",:name=>"#{archived_student.full_name}",:email=>"#{archived_student.email}",:is_selected=>false}
        students.push(student_hash)
      end
      batch_hash[:students]=students
      batches.push(batch_hash)
    end
    batches_hash[:batches]=batches
    return batches_hash
  end
  def send_invitations(message)
    invitations=self.alumni_event_invitations
    Delayed::Job.enqueue FedenaAlumniEventInviter.new(invitations,message)
  end
  def self.send_contact_email(students,message,subject)
    Delayed::Job.enqueue FedenaAlumniEventContact.new(students,message,subject)
  end
  def get_organiser_users
    self.alumni_event_organisers.collect(&:organiser).collect(&:user)
  end
  def self.get_students_list(selected_students_json)
  	student_list=[]
  	list_object=JSON.parse(selected_students_json)
  	list_object.each do |course_hash|
  		course_id= course_hash["id"].to_i
  		course=Course.find(course_id)
  		if course_hash['years'].empty?
			  course.batches.each do |batch|
          student_list+=batch.archived_students
        end
		  else # specific year
        course_hash['years'].each do |year|
				  bach_end_year= year["name"].to_i
				  batches=Batch.find(:all,:conditions=>["course_id=? and is_active=false and EXTRACT(YEAR FROM end_date)=?",course_id,bach_end_year]).to_set
				  if year['batches'].empty?
					  batches.each do |batch|
						  student_list+=batch.archived_students
            end
				  else # specific batch
            year['batches'].each do |batch_hash|
						  batch=Batch.find_by_id(batch_hash["id"].to_i)
						  student_list+=batch.archived_students
            end
				  end
        end
      end
    end
    return student_list
  end
  def is_current_user_an_organiser
    Authorization.current_user.alumni_event_organiser?
  end
	def inform_deletion_to_invited_students
		invitations=self.alumni_event_invitations
		Delayed::Job.enqueue FedenaAlumniEventCancelInvitation.new(self.id,invitations)
	end
end
