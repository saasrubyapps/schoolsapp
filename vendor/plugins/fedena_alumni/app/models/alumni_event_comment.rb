class AlumniEventComment < ActiveRecord::Base
  belongs_to :alumni_event
  belongs_to :commentor,:polymorphic=>true
  validates_presence_of :body
end
