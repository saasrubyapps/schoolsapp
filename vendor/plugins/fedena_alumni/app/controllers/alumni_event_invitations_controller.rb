class AlumniEventInvitationsController < ApplicationController
	before_filter :login_required,:except => ["authenticate_alumni","accept_invitation","reject_invitation"]
	# filter_access_to :all
	before_filter :set_archived_student_and_event, :only => [:accept_invitation,:reject_invitation]
	filter_access_to :new,:batch_list_using_cource_and_passout_year,:show,:create,:alumni_event_organisers_json,:populate_list,:select_batches,:attribute_check => true,:load_method => lambda { params[:event_id] && Event.find(params[:event_id])? AlumniEvent.find_or_create_by_event_id(params[:event_id]) : AlumniEvent.new }
	filter_access_to :index,:attribute_check => true,:load_method => lambda {current_user}

	def index
		#TODO move to model
		if current_user.alumni_event_organiser? && !current_user.role_symbols.include?(:manage_alumni)
			@alumni_event_invitations= AlumniEvent.get_organising_events_status(current_user).paginate(:page => params[:page],:per_page=>15)
		else
			@alumni_event_invitations= AlumniEvent.get_invitation_status.paginate(:page => params[:page],:per_page=>15)
		end
	end

	def batch_list_using_cource_and_passout_year
		passout_year=params[:passout_year].to_i
		course=Course.find_by_course_name(params[:course])
		date=Date.new(passout_year)
		cource=course.batches.all(:select=>"name",:conditions=>'end_date>"'+date.to_s+'"')
		render :text=>cource.to_json
	end
	def show
		@event_oraganiser=AlumniEventOrganiser.find_all_by_alumni_event_id(params[:id])
		@alumni_event = AlumniEvent.find(params[:id])
		@event=@alumni_event.event
		@event_oraganisers=@alumni_event.alumni_event_organisers
    	@invitation_status=@alumni_event.get_invitation_status
		@comments=AlumniEventComment.find_all_by_alumni_event_id(params[:id],:order=>"id DESC")
		@alumni_event_invitation_count=AlumniEventInvitation.get_invitation_count(params[:id])
	end
	#TODO this fuction is to splitted
	def create
		alumni_event_id=params[:alumni_event_id]
		message=params[:message]
		event_id=params[:event_id].to_i
		org_employees_list=params[:org_employees_list]
		org_students_list=params[:org_students_list]
		is_comentable=params[:is_commentable]
		selected_students_list=params[:selected_students_list]
		is_student_organiser_list_modified=params[:is_student_organiser_list_modified]
		is_employee_organiser_list_modified=params[:is_employee_organiser_list_modified]
		selected_students_json=JSON.parse(selected_students_list)
		alumni_event=AlumniEvent.find_or_create_by_event_id(event_id)
		selected_students=AlumniEvent.get_students_list(selected_students_json["selected_students"])
		#=================ORGAINISER===========================#
		organisers=[]
		student_organisers=[]
		employee_organisers=[]
		old_organisers=[]
		existing_alumni_event_organisers=alumni_event.alumni_event_organisers.dup
		existing_student_alumni_event_organisers=alumni_event.alumni_event_organisers.student.dup
		existing_employee_alumni_event_organisers=alumni_event.alumni_event_organisers.employee.dup
		employee_json=JSON.parse(org_employees_list)
		student_json=JSON.parse(org_students_list)
		stud_organizer_list=JSON.parse(student_json["student_organisers"])
		emp_organizer_list=JSON.parse(employee_json["employee_organisers"])
		#===Student====#
		if is_student_organiser_list_modified=="true"
			stud_organizer_list.each do |stud_organizer|
				organiser= alumni_event.alumni_event_organisers.find_or_initialize_by_organiser_id_and_organiser_type(stud_organizer["id"],"Student")
				begin
					if organiser.save
						organisers.push organiser
						student_organisers.push organiser
					else
						old_organisers.push organiser
					end
				rescue
					logger.debug "====================some thing might went wrong===================="
				end
			end
			#==Remove removed organisors====#
			(existing_student_alumni_event_organisers-student_organisers).each do |organiser|
				organiser.destroy
			end
		end
		#==Employee==#
		if is_employee_organiser_list_modified=="true"
			emp_organizer_list.each do |emp_organizer|
				organiser= alumni_event.alumni_event_organisers.find_or_initialize_by_organiser_id_and_organiser_type(emp_organizer["id"],"Employee")
				begin
					if organiser.save
						organisers.push organiser
						employee_organisers.push organiser
					else
						old_organisers.push organiser
					end
				rescue
				 	logger.debug "========not saved !!============="
				end
			end
			#==Remove removed organisors====#
			(existing_employee_alumni_event_organisers-employee_organisers).each do |organiser|
				organiser.destroy
			end
		end
		logger.debug "======alumni.orgs #{alumni_event.alumni_event_organisers}=============="
		#=== SEND REMINDER=====#
		recipient_ids=[]
		(organisers-existing_alumni_event_organisers).each do |organiser|
			recipient_ids.push(organiser.organiser.user.id)
		end
#		subject=t('alumni_event_organiser_reminder_subject')
#		body=setup_reminder_body(alumni_event)
    body = "#{t('alumni_event_organiser_reminder_content-1')} <b> #{alumni_event.event.title} </b>"
    links = {:target=>'view_event',:target_param=>'alumni_event_id',:target_value=>alumni_event.id}
    inform(recipient_ids,body,'Event',links)
#		Delayed::Job.enqueue(
#			DelayedReminderJob.new(
#			:sender_id  => current_user.id,
#			:recipient_ids => recipient_ids,
#			:subject=>subject,
#			:body=>body
#			)
#		)
		# remove removed orgs
		logger.debug "===========new=============#{organisers.inspect}============"
		logger.debug "==old====#{existing_alumni_event_organisers.inspect}============"
		#===SEND INVITATION===#
		invitations=[]
		archived_students=selected_students
		archived_students.each do |archived_student|
			invitation=AlumniEventInvitation.new
			invitation.archived_student_id=archived_student.id
			invitation.alumni_event_id=alumni_event.id
			unless invitation.save
				logger.debug "invitation already exist"
			else
				invitations.push invitation
			end
		end
		if(selected_students.length>0)
			alumni_event.send_invitations(message)
			# Delayed::Job.enqueue FedenaAlumniEventInviter.new(invitations,message)
		end
		flash[:notice]=""
		#==set is_commentable==#
		if alumni_event.is_commentable!=to_boolean(is_comentable)
			alumni_event.is_commentable=to_boolean(is_comentable)
			alumni_event.save
			# flash[:notice]+="#{t('changed_commentable_status_to')} #{is_comentable}"
			# flash[:notice]+="<br>"
		end
		if(selected_students.length>0 ||stud_organizer_list.length >0 || emp_organizer_list.length>0 )
			if(selected_students.length>0)
				flash[:notice]+="#{t('number_of_students_invited')}: #{selected_students.length}"
				flash[:notice]+="<br>"
			end
			if stud_organizer_list.length >0
				flash[:notice]+="#{t('number_of_student_organisers_invited')}: #{stud_organizer_list.length}"
				flash[:notice]+="<br>"
			end
			if emp_organizer_list.length >0
				flash[:notice]+="#{t('number_of_employee_organisers_invited')}: #{emp_organizer_list.length}"
				flash[:notice]+="<br>"
			end
		else
			flash[:notice]="No changes in invitees list"
		end
		redirect_to alumni_event_invitations_path
	end
	def alumni_event_organisers_json
		event_id=params[:query][:event_id]
		organiser_type=params[:query][:organisers_type]
		alumni_event=AlumniEvent.find_by_event_id(event_id)
		if organiser_type=="Student"
			render :json=>alumni_event.student_alumni_event_organisers_json
		elsif organiser_type=="Employee"
			render :json=>alumni_event.employee_alumni_event_organisers_json
		else
			render :status=>500
		end

	end
	# TODO move to comments controller
	def delete_comment
		@comment=AlumniEventComment.find(params[:id])
		event_id=@comment.alumni_event_id
		if @comment.delete
			@comments=AlumniEventComment.find_all_by_alumni_event_id(event_id,:order=>"id DESC")
			render :update do |page|
				page.replace_html 'comments-list', :partial=>"event_comments"
			end
		end
	end
	#TODO change to PUT
	def accept_invitation
		archived_student_id=@current_archived_student.id;
		alumni_event_id=@current_alumni_event.id;
		p archived_student_id
		p alumni_event_id
		invitation=AlumniEventInvitation.find_by_archived_student_id_and_alumni_event_id(archived_student_id,alumni_event_id)
		invitation.accept_invitation
		title=invitation.alumni_event.event.title
		snake_case=title.downcase.tr!(" ", "-")
		event_title=(snake_case.nil?)?title:snake_case
		redirect_to  alumni_show_alumni_events_path(:id=>invitation.alumni_event.id,:title=>event_title)

	end
	def reject_invitation
		archived_student_id=@current_archived_student.id;
		alumni_event_id=@current_alumni_event.id;
		invitation=AlumniEventInvitation.find_by_archived_student_id_and_alumni_event_id(archived_student_id,alumni_event_id)
		invitation.reject_invitation
		# redirect_to preview_event_alumni_path(alumni_event_id)
		# redirect_to :controller=>"alumni",:action=>"preview_event",:id => invitation.alumni_event.id
		redirect_to :back

		# render :text=>"thanks for your responce"
	end
	def new
		event_id=params[:event_id]
		@is_just_created=to_boolean(params[:ijc])
		@courses=Course.cousres_containing_inactive_batches
		# @courses=Course.all
		if(event_id) #Alumni event is specified
			# @alumni_event=AlumniEvent.find_by_id(alumni_event)
			@event=Event.find_by_id(event_id)
			if @event 
				@event_specified=true
				@alumni_event=AlumniEvent.find_by_event_id(event_id)
	      		@organisers=@alumni_event.alumni_event_organisers(:group=>:organiser_type)
	      	else
	      		render :status=> 404
	      	end
		else
			@event_specified=false
			if current_user.alumni_event_organiser?
				#TODO active events
				@events=[]
				current_user.organising_alumni_events.each do |alumni_event|
					event=alumni_event.event
					unless event.is_expired?
						@events.push event
					end
				end
			elsif current_user.role_symbols.include? (:manage_alumni) || current_user.admin?
				@events=Event.active_normal_events
			else
				@events=[]
			end
		end
	end
	def event_preview
		event_id=params[:id]
		@event=Event.find_by_id(event_id)
		render :partial=>"event_preview"
	end
	def list_organiser_employees
		@dep_id=params[:query][:department_id]
		@already_selcted__ids=params[:query][:organisers_array]
		unless  @already_selcted__ids.present?
			@organisers=Employee.find_all_by_employee_department_id(@dep_id)
		else
			@organisers=Employee.find_all_by_employee_department_id(@dep_id,:conditions=>["id not in(?)",@already_selcted__ids])
		end
		render :partial => "select_organiser"
	end
	def authenticate_alumni
		auth_key=params[:id]
		invitation=AlumniEventInvitation.find_by_auth_token(auth_key)
		if(invitation)
			session[:user_id] = nil if session[:user_id]
			session[:archived_student_id] = invitation.archived_student.id
			session[:alumni_event_id] = invitation.alumni_event.id
			@current_archived_student=ArchivedStudent.find_by_id(invitation.archived_student.id)
			title=invitation.alumni_event.event.title
			snake_case=title.downcase.tr!(" ", "-")
			event_id=invitation.alumni_event.id
			event_title= snake_case.nil? ?title : snake_case
			if invitation.is_accepted?
				redirect_to alumni_show_alumni_events_path(event_id,event_title)
			else
				redirect_to :controller=>"alumni",:action => "preview_event",:id=>event_id,:title =>event_title
			end
		else
			render :text =>"Event has been cancelled"
		end
	end
	def list_organiser_students
		@batch_id=params[:query][:batch_id]
		@ref="student"
		@already_selcted__ids=params[:query][:organisers_student_array]
		unless  @already_selcted__ids.present?
			@organisers=Student.find_all_by_batch_id(@batch_id)
		else
			@organisers=Student.find_all_by_batch_id(@batch_id,:conditions=>["id not in(?)",@already_selcted__ids])
		end
		render :partial => "select_organiser"
	end
	def organiser_employee
		event_id=params[:event_id]
		@event=Event.find(event_id)
		# @departments=EmployeeDepartment.all
		@departments=EmployeeDepartment.containing_employees
	end
	def organiser_student
		event_id=params[:event_id]
		@ref="student"
		@event=Event.find(event_id)
		# @courses=Course.find(:all,:conditions=>{:is_deleted=>false})
		@courses=Course.cousres_containing_active_batches
	end
	def select_batches
		@course_id=params[:query][:course_id]
		@batches=Batch.find(:all,:conditions=>["course_id=? and is_active=true",@course_id]).to_set
		render :partial => "select_batches"
	end
	def populate_list
		id=params[:query][:id].to_i
		type=params[:query][:type].to_i
		if type==1
			@course_id=id
			@passout_years=Batch.all(:select => "DISTINCT(EXTRACT(YEAR FROM end_date)) as year",:conditions=>{:course_id=>id,:is_active=>false})
			render :partial => "year_list"
		elsif type==2
			@pass_out_year=id;
			course_id=params[:query][:course_id].to_i
			@batches=Batch.find(:all,:conditions=>["course_id=? and is_active=false and EXTRACT(YEAR FROM end_date)=?",course_id,id]).to_set
			render :partial => "batch_list"
		end
	end
	private
		def set_archived_student_and_event
			@current_archived_student=ArchivedStudent.find_by_id(session[:archived_student_id])
			@current_alumni_event=AlumniEvent.find_by_id(session[:alumni_event_id])
			if @current_archived_student.nil? || @current_alumni_event.nil?
	        	render :status=>403
	      	end
		end
		def to_boolean(str)
			str == 'true'
		end
		def setup_reminder_body(alumni_event)
			body=t('hi')
			body+="<br/>"
			body+="\t"
			body+=t('alumni_event_organiser_reminder_content-1')
			body+="<strong> #{alumni_event.event.title} </strong>"
			body+="<br/>"
			body+=t('alumni_event_organiser_reminder_content-2')
			body+="<br/>"
			body+="<a href='"
			body+=alumni_event_url(alumni_event.id)
			body+="'>#{alumni_event_url(alumni_event.id)}</a>"
			return body
		end
end
