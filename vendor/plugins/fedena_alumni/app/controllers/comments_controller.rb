class CommentsController < ApplicationController
  # filter_access_to :all
  before_filter :set_archived_student_and_event, :only => [:create,:index,:destroy]
  def new
  	@alumni_event = AlumniEvent.find(params[:alumni_event_id])
    @comment = @alumni_event.alumni_event_comments.build
  end
  def create
  	@alumni_event = AlumniEvent.find(params[:alumni_event_id])
  	@comment = @alumni_event.comments.build(params[:alumni_event_comments])
    if @current_archived_student
      commentor=@current_archived_student
    elsif current_user.student?
      commentor=current_user.student_entry
    elsif current_user.admin?
      commentor=current_user
    elsif current_user.employee? #privilaged employee
      commentor=current_user.employee_entry
    else
      render :status=> "some thing went wrong"
    end
    @comment.commentor=commentor
    if @comment.save!
      redirect_to alumni_event_comments_path(@alumni_event.id)
    else
      render :text => @comment
    end
  end
  def destroy
    comment_id=params[:id]
  	@alumni_event = AlumniEvent.find(params[:alumni_event_id])
    @comment = @alumni_event.comments.find(comment_id)
    #intilise as false
    is_authenticated=false
    if @current_archived_student.nil? || current_user.nil?
      #for alumni event
      #for existing users
      if current_user
        if current_user.admin? || current_user.role_symbols.include?(:manage_alumni)
          is_authenticated=true
        elsif current_user.id==@comment.commentor.user.id
          is_authenticated=true
        else
          is_authenticated=false
        end
      else @current_archived_student
        if @current_archived_student.id==@comment.commentor.id
          is_authenticated=true
        else
          is_authenticated=false
        end
      end
    else
      # Multipple sessions
    end
    if is_authenticated==true
      @comment.destroy
      redirect_to :back
    else
      redirect_to :back,:status=>403
    end
  end
  def show
    # @comment=AlumniEventComment.find(params[:id])
  end
  def index
    @alumni_event = AlumniEvent.find(params[:alumni_event_id])
    @comments=@alumni_event.comments.reverse.paginate(:page => params[:page],:per_page => 5).reverse
    render :partial => "index"
  end
  private
      def set_archived_student_and_event
        @current_archived_student=ArchivedStudent.find_by_id(session[:archived_student_id])
        @current_alumni_event=AlumniEvent.find_by_id(session[:alumni_event_id])
        if @current_archived_student
          @current_user=nil
        end
      end
end
