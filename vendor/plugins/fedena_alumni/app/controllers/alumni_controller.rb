class AlumniController < ApplicationController
  before_filter :login_required,:except => ["preview_event"]
  before_filter :set_archived_student_and_event, :only => [:preview_event]
  filter_access_to :index,:contact,:create,:view_alumni_students ,:attribute_check => true,:load_method => lambda { current_user }

  unloadable

  def new
  	@alumni=AlumniEvent.new
  	@event=Event.new
    @default_start_date=Time.now
  	@default_end_date=@default_start_date+1.hour
  end
  def show
    redirect_to alumni_event_path(params[:id])
  end
  #TODO move to alumni_event_controller
  def create
    @alumni_event= AlumniEvent.new
    params_to_save = {:event => {:title => params[:events][:title], :description => params[:events][:description], :start_date => params[:events][:start_date], :is_common => true, :end_date => params[:events][:end_date], :batch_events_attributes => {}}}
    @event = Event.new(params_to_save[:event])
#    @event=@alumni_event.create_event(params[:events])
  	if @event.save 
      @event.update_attribute(:is_common,false)
      @alumni_event = AlumniEvent.create(:event_id => @event.id, :is_commentable => false)
      flash[:notice]="#{t('alumni_event_created_successfully')}"
      respond_to do |format|
        format.js { render :action => 'create' }
      end
    else
      @error=true
      respond_to do |format|
        format.js { render :action => 'create' }
      end
    end
  end
  #for archved_student
  #TODO move?
  def preview_event
    alumni_event_id=params[:id]
    @alumni_event=AlumniEvent.find_by_id(alumni_event_id)
    @event_oraganisers=@alumni_event.alumni_event_organisers
    @invitation_status=@alumni_event.get_invitation_status
    @invitation=AlumniEventInvitation.find_by_alumni_event_id_and_archived_student_id(alumni_event_id,@current_archived_student.id)
    render :layout => 'alumni_view'
  end
  def view_alumni_students
    @course=Course.cousres_containing_inactive_batches
    @passout_years=Batch.all(:select => "DISTINCT(EXTRACT(YEAR FROM end_date)) as year",:conditions=>"is_active=false")
  end

  def select_batches
    @year=params[:query][:pass_out_year]
    @course_id=params[:query][:course_id]
    if @course_id.present?
      @select_batches=AlumniEvent.retrive_batches(@course_id,@year)
    else
      @select_batches=Batch.find(:all,:conditions=>["is_active=false and EXTRACT(YEAR FROM end_date)=?",@year]).to_set
    end
    render :partial => "select_batches"
  end
  def select_passout_year
    @course_id=params[:query][:course_id]
    @passout_years=Batch.all(:select => "DISTINCT(EXTRACT(YEAR FROM end_date)) as year",:conditions=>{:course_id=>@course_id,:is_active=>false})
    render :partial => "select_passout_year"
  end
 def select_alumni
    @batch_id=params[:select_batch][:batch_id]
    @archived_students=ArchivedStudent.paginate(:page => params[:page],:per_page=>10,:conditions=>{:batch_id=>@batch_id})
    render(:update) do |page|
        page.replace_html 'alumni_studets_partial_div', :partial=> 'alumni_students'
    end
  end
  def contact
    if request.post?
      message=params[:message]
      subject=params[:subject]
      #convert plain link to html links
      # message=auto_link(message, :html => { :target => '_blank' })
      selected_students_list=params[:selected_students_list]
      selected_students_json=JSON.parse(selected_students_list)
      selected_students=AlumniEvent.get_students_list(selected_students_json["selected_students"])
      alumni_students_array = Array.new
      selected_students.each do |student|
         alumni_data={:name=>student.full_name,:email=>student.email}
         alumni_students_array.push(alumni_data)
      end
      if message.empty? || selected_students.empty?
        flash[:notice]=""
        if message.empty?
          flash[:notice]+="#{t('enter_valid_message')}"
        end
        if selected_students.empty?
          flash[:notice]+="<br>" unless flash[:notice].empty?
          flash[:notice]+="#{t('no_archived_students_selected')}"
        end
        redirect_to contact_alumni_path
        return
      end
      AlumniEvent.send_contact_email(alumni_students_array,message,subject)
      flash[:notice]="#{t('message_sent')}"
      redirect_to alumni_index_path
    else
      @courses=Course.cousres_containing_inactive_batches
    end
  end

  def student_and_batch_list
    @input_batch_ids=params[:select_batch][:batch_id]
    @student_list=AlumniEvent.create_json_file(@input_batch_ids)
    @results=@student_list.to_json
    respond_to do |format|
      format.html {
        render(:update) do |page|
          page.replace_html 'student_and_batch_list_div', :partial=> 'student_and_batch_list'
        end
        # render :partial=>"student_and_batch_list"
      }
      format.xml {  }
      format.json {render :json=> @results}
    end
  end
  private
    def set_archived_student_and_event
      @current_archived_student=ArchivedStudent.find(session[:archived_student_id])
      @current_alumni_event=AlumniEvent.find(session[:alumni_event_id])
      if @current_archived_student.nil? || @current_alumni_event.nil?
        render :status=>403
      end
    end
end
