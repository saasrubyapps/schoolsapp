class AlumniEventsController < ApplicationController
  before_filter :set_archived_student_and_event, :only => [:alumni_show]
  unloadable
  before_filter :login_required,:except => ["alumni_show"]
  # filter_access_to :all
  filter_access_to :show ,:attribute_check => true,:load_method => lambda { AlumniEvent.find(params[:id]) }
  filter_access_to :edit,:update,:destroy,:attribute_check => true,:load_method => lambda {AlumniEvent.find(params[:id])}
  def show
    @alumni_event = AlumniEvent.find(params[:id])
    if @alumni_event.event.nil?
      render :status=>404
    end
    @event_oraganisers=@alumni_event.alumni_event_organisers
    @event=@alumni_event.event
    @invitation_status=@alumni_event.get_invitation_status
    @comments=@alumni_event.comments.reverse.paginate(:page =>1,:per_page => 5)
 end
  def alumni_show
    alumni_event_id=params[:id]
  	@alumni_event=AlumniEvent.find_by_id(alumni_event_id)
  	unless @alumni_event && @alumni_event.event
  		raise ActionController::RoutingError.new('Not Found')
  	end
    if(@alumni_event!=@current_alumni_event)
      render :status => 403,:text=>"You are not authorised"
    end
  	@event_oraganisers=@alumni_event.alumni_event_organisers
  	@invitation_status=@alumni_event.get_invitation_status
    @comments=@alumni_event.comments.reverse.paginate(:page =>1,:per_page => 5)
  	render :layout => 'alumni_view'
  end
  def edit
    @alumni_event=AlumniEvent.find(params[:id])
    @event= @alumni_event.event
    @date=Time.now
  end
  def update
    @alumni_event=AlumniEvent.find(params[:id])
    @event=@alumni_event.event
    @event.update_attributes(params[:events])
    if @event.save && @alumni_event.save
      flash[:notice]="#{t('alumni_event_updated_successfully')}"
      respond_to do |format|
        format.js { render :action => 'update' }
      end
    else
      @error=true
      respond_to do |format|
        format.js { render :action => 'update' }
      end
    end
  end
  def destroy
    alumni_event_id=params[:id]
    alumni_event=AlumniEvent.find(alumni_event_id)
    alumni_event.destroy
    flash[:notice]="#{t('alumni_event_deleted_successfully')}"
    redirect_to alumni_event_invitations_path
  end
  def index
    redirect_to alumni_event_invitations_path
  end
  private
    def set_archived_student_and_event
      @current_archived_student=ArchivedStudent.find_by_id(session[:archived_student_id])
      @current_alumni_event=AlumniEvent.find_by_id(session[:alumni_event_id])
      if @current_archived_student.nil? || @current_alumni_event.nil?
        render :status=>404,:text => "You are not authorised"
      end
    end
  end
