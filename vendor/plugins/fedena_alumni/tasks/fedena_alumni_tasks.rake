namespace :fedena_alumni do
  desc "Install Fedena Alumni Plugin Module"
  task :install do
    system "rsync --exclude=.svn -ruv vendor/plugins/fedena_alumni/public ."
  end
end
