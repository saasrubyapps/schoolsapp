class AddIndexToAlumniEventOrganisers< ActiveRecord::Migration
  def self.up
    add_index :alumni_event_organisers, [:alumni_event_id, :organiser_id,:organiser_type],:unique => true,:name=>'alumni_organiser_uniq'
  end

  def self.down
    remove_index :alumni_event_organisers,:name=> 'alumni_organiser_uniq'
  end
end
