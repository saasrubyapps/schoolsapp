class AddSchoolIdToTables < ActiveRecord::Migration
  def self.up
    [:alumni_events,:alumni_event_organisers,:alumni_event_comments,:alumni_event_invitations].each do |c|
      add_column c,:school_id,:integer
      add_index c,:school_id
    end
  end

  def self.down
    [:alumni_events,:alumni_event_organisers,:alumni_event_comments,:alumni_event_invitations].each do |c|
      remove_index c,:school_id
      remove_column c,:school_id
    end
  end
end
