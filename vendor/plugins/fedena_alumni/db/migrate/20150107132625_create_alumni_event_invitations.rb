class CreateAlumniEventInvitations < ActiveRecord::Migration
  def self.up
    create_table :alumni_event_invitations do |t|
      t.references :alumni_event
      t.references :archived_student
      t.integer :status,:default=>0
      t.string :auth_token

      t.timestamps
    end
  end

  def self.down
    drop_table :alumni_event_invitations
  end
end
