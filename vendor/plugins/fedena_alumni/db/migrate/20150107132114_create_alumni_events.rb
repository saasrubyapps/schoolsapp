class CreateAlumniEvents < ActiveRecord::Migration
  def self.up
    create_table :alumni_events do |t|
      t.boolean :is_commentable,:default=>0
      t.references :event
      t.timestamps
    end
  end

  def self.down
    drop_table :alumni_events
  end
end
