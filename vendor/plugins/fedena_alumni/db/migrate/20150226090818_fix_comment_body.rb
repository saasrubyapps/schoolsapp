class FixCommentBody < ActiveRecord::Migration
  def self.up
	   change_column :alumni_event_comments, :body, :text
  end

  def self.down
  end
end
