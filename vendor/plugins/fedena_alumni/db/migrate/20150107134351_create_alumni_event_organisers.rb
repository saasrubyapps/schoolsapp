class CreateAlumniEventOrganisers < ActiveRecord::Migration
  def self.up
    create_table :alumni_event_organisers do |t|
      t.references :alumni_event
      t.references :organiser,:polymorphic => {:default => 'Employee'}
      t.timestamps
    end
  end

  def self.down
    drop_table :alumni_event_organisers
  end
end
