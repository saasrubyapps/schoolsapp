class CreateAlumniEventComments < ActiveRecord::Migration
  def self.up
    create_table :alumni_event_comments do |t|
      t.references :alumni_event
      t.references :commentor,:polymorphic => {:default => 'Employee'}
      t.string :body

      t.timestamps
    end
  end

  def self.down
    drop_table :alumni_event_comments
  end
end
