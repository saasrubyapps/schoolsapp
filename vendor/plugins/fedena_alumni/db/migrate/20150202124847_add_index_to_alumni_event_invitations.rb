class AddIndexToAlumniEventInvitations < ActiveRecord::Migration
  def self.up
    add_index :alumni_event_invitations, [:alumni_event_id, :archived_student_id],:unique => true,:name=>'alumni_invitation_uniq'
  end

  def self.down
    remove_index :alumni_event_invitations, :name=> 'alumni_invitation_uniq'
  end
end
