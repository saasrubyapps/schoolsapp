menu_link_present = MenuLink rescue false
unless menu_link_present == false
  collaboration_category = MenuLinkCategory.find_by_name("collaboration")

  MenuLink.create(:name=>'alumni_text',:target_controller=>'alumni',:target_action=>'index',:higher_link_id=>nil,:icon_class=>'alumni-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>collaboration_category.id) unless MenuLink.exists?(:name=>'alumni_text')
  higher_link = MenuLink.find_by_name('alumni_text')
  MenuLink.create(:name=>'view_alumni_students',:target_controller=>'alumni',:target_action=>'view_alumni_students',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>collaboration_category.id) unless MenuLink.exists?(:name=>'view_alumni_students')
  # MenuLink.create(:name=>'invite_alumni_to_an_event',:target_controller=>'alumni_event_invitations',:target_action=>'new',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>collaboration_category.id) unless MenuLink.exists?(:name=>'invite_alumni_to_an_event')
  MenuLink.create(:name=>'view_event_invites',:target_controller=>'alumni_event_invitations',:target_action=>'index',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>collaboration_category.id) unless MenuLink.exists?(:name=>'view_event_invites')
  MenuLink.create(:name=>'contact_alumni',:target_controller=>'alumni',:target_action=>'contact',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>collaboration_category.id) unless MenuLink.exists?(:name=>'contact_alumni')

end
Privilege.reset_column_information
Privilege.find_or_create_by_name :name => "ManageAlumni",:description => 'manage_alumni_privilege'
if Privilege.column_names.include?("privilege_tag_id")
  Privilege.find_by_name('ManageAlumni').update_attributes(:privilege_tag_id=>PrivilegeTag.find_by_name_tag('administration_operations').id, :priority=>500 )
end
