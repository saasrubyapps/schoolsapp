require 'net/http'
class FedenaAlumniEventInviter
  attr_accessor :invitations,:message
  def initialize(invitations,message)
    @invitations=invitations
    @message=message
  end
  def perform
    @invitations.each do |i|
      invitation = AlumniEventInvitation.find(i.id)
      invitation.send_invitation(message)
    end
  end
  def on_permanent_failure
    #do nothing for now
    p "failed"
  end
end
