require 'net/http'
class FedenaAlumniEventContact
  attr_accessor :students,:message,:subject
  def initialize(students,message,subject)
    @students=students
    @message=message
    @subject=subject
  end
  def perform
   	@students.each do |student|
      unless student[:email].empty?
        AlumniMailer.deliver_contact_email(student[:email],student[:name],@message,@subject)
      end
    end
  end
  def on_permanent_failure

  end
end
