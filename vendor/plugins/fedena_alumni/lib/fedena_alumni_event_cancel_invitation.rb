require 'net/http'
class FedenaAlumniEventCancelInvitation
  attr_accessor :alumni_event_id,:invitations
  def initialize(alumni_event_id,invitations)
    @alumni_event=AlumniEvent.find(alumni_event_id)
    # @invitations=@alumni_event.alumni_event_invitations
    @invitations=invitations
  end
  def perform
    @invitations.each do |invitation|
      invitation.cancel_invitation
    end
  end
  def on_permanent_failure
    #do nothing for now
    p "failed"
  end
end
