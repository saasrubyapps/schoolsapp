# FedenaAlumni
# ForadianTestPlugin
require 'dispatcher'
module FedenaAlumni
  def self.attach_overrides
    Dispatcher.to_prepare :fedena_alumni do
      ::ArchivedStudent.instance_eval { has_many :alumni_event_comments, :as=>:commentor, :dependent => :destroy}
      ::Employee.instance_eval { has_many :alumni_event_comments, :as=>:commentor, :dependent => :destroy}
      ::Employee.instance_eval { has_many :alumni_event_organisers, :as=>:organiser , :dependent => :destroy}
      ::Student.instance_eval { has_many :alumni_event_comments, :as=>:commentor, :dependent => :destroy}
      ::Student.instance_eval { has_many :alumni_event_organisers, :as=>:organiser , :dependent => :destroy}
      ::Course.instance_eval {include CourseExtension}
      ::Student.instance_eval {include StudentExtension}
      ::Employee.instance_eval {include EmployeeExtension}
      ::User.instance_eval {include UserExtension}
      ::Event.instance_eval {include EventExtension;after_destroy :remove_alumni_event}
      ::EmployeeDepartment.instance_eval {include EmployeeDepartmentExtension}
    end
  end
end
module EventExtension
  def remove_alumni_event
    alumni_event=AlumniEvent.find_by_event_id(self.id)
    unless alumni_event.nil?
      alumni_event.destroy
    end
  end
end
module CourseExtension
  def inactive_batches
    self.batches.all(:conditions=>{:is_active=>false,:is_deleted=>false})
  end
  def self.included(base)
      base.instance_eval do
        def self.cousres_containing_inactive_batches
          course_list=[]
          Course.all.each do |course|
            if course.inactive_batches.length >0
              course_list<< course
            end
          end
          course_list
        end
        def self.cousres_containing_active_batches
          course_list=[]
          Course.all.each do |course|
            if course.active_batches.length >0
              course_list<< course
            end
          end
          course_list
        end
      end
  end
end
module StudentExtension
  def alumni_event_organiser?
    !self.alumni_event_organisers.empty?
  end
end
module EmployeeExtension
  def alumni_event_organiser?
    !self.alumni_event_organisers.empty?
  end
end
module UserExtension
  def alumni_event_organiser?
    if self.student?
      student=self.student_entry
      unless student.alumni_event_organisers.empty?
        return true
      else
        return false
      end
    elsif self.employee?
      employee=self.employee_entry
      unless employee.alumni_event_organisers.empty?
        return true
      else
        return false
      end
    else
      return false
    end
  end
  def organising_alumni_events
    if self.student?
      student=self.student_entry
      return student.alumni_event_organisers.collect(&:alumni_event)
    elsif self.employee?
      employee=self.employee_entry
      return employee.alumni_event_organisers.collect(&:alumni_event)
    else
      return []
    end
  end
end
module EventExtension
  def is_expired?
    if(self.end_date <= Time.current)
      return true
    else
      return false
    end
  end
  def self.included(base)
    base.instance_eval do
      def self.active_normal_events
        event_list=[]
        Event.all.each do |event|
          unless (event.is_exam  || event.is_due || (event.end_date <= Time.current))
            event_list<<event
          end
        end
        return event_list
      end
    end
  end
end
module EmployeeDepartmentExtension
  def self.included(base)
    base.instance_eval do
      def self.containing_employees
        department_list=[]
        EmployeeDepartment.all.each do |department|
          if department.employees.count >0
            department_list<<department
          end
        end
        return department_list
      end
    end
  end
end
