class GalleryCategory < ActiveRecord::Base
  xss_terminate
  has_many :gallery_photos, :dependent => :destroy
  has_many :gallery_category_privileges, :dependent => :destroy

  validates_presence_of :name
  validates_uniqueness_of :name, :case_sensitive => false
  named_scope :old_data,{ :conditions => { :old_data => true , :is_delete=>false }}
  named_scope :new_data,{ :conditions => { :old_data => false, :is_delete=>false }}


  def delay_destroy
    if self.update_attribute(:is_delete, true)
      self.instance_variable_set(:@destroy_later, true)
      Delayed::Job.enqueue(self, :queue => 'gallery')
    else
      return false
    end
  end

  def perform
    if self.instance_variable_get(:@destroy_later)
      self.destroy
    end
  end

end
