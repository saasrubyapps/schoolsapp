
class HostelFee < ActiveRecord::Base

  attr_accessor_with_default :has_fine,false

  belongs_to :student
  belongs_to :hostel_fee_collection
  belongs_to :batch
  has_one :finance_transaction, :as => :finance
  has_many :hostel_fee_finance_transactions
  has_many :finance_transactions ,:through=>:hostel_fee_finance_transactions, :dependent => :destroy, :order => "finance_transactions.id DESC"
  has_many :finance_transactions_with_fine,:through=>:hostel_fee_finance_transactions,:source=>:finance_transaction,:conditions=>"finance_transactions.fine_included =1"
  named_scope :unpaid ,:conditions=>"balance > 0.0"
  #tax associations
  has_many :tax_collections, :as => :taxable_fee, :dependent => :destroy
  has_many :tax_particulars, :through => :tax_collections, :source => :taxable_entity, :source_type => "HostelFeeCollection"
  has_many :tax_payments, :as => :taxed_fee
  has_many :taxed_particulars, :through => :tax_payments, :source => :taxed_entity, :source_type => "HostelFee"
  #invoice associations
  has_many :fee_invoices, :as => :fee
  cattr_accessor :invoice_number_enabled  
  after_create :add_invoice_number, :if => Proc.new { |fee| fee.invoice_number_enabled.present? }
  before_destroy :mark_invoice_number_deleted
  accepts_nested_attributes_for :tax_collections, :allow_destroy => true
  
  validates_numericality_of :balance,:greater_than_or_equal_to=>0

  delegate :name,:to=>:hostel_fee_collection,:allow_nil=>true

  before_save :verify_precision
  before_validation :set_balance,:if=> Proc.new{|hf| hf.balance.nil?}


  def verify_precision
    self.rent = FedenaPrecision.set_and_modify_precision self.rent
  end

  named_scope :active , :joins=>[:hostel_fee_collection] ,:conditions=>{:hostel_fee_collections=>{:is_deleted=>false}}

  def invoice_no
    fee_invoices.present? ? fee_invoices.try(:first).try(:invoice_number) : ""
  end
  
  def add_invoice_number
    fee_invoices.create
  end
  
  def mark_invoice_number_deleted
    fee_invoice = fee_invoices.try(:last)
    fee_invoice.mark_deleted if fee_invoice.present?
  end
  
  def  is_paid?
    balance==0
  end

  def fine_amount
    finance_transactions_with_fine.sum(:fine_amount)
  end

  def payee_name
    if student.nil?
      archived_student= ArchivedStudent.find_by_former_id(student_id)
      if archived_student
        "#{archived_student.full_name}(#{archived_student.admission_no})"
      else
        "#{t('user_deleted')}"
      end
    else
      student.full_name
    end
  end

  private

  def set_balance
    self.balance=self.rent
    self.balance+= self.tax_amount.to_f if self.tax_enabled?
  end

end


