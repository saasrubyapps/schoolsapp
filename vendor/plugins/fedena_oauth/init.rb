require 'oauth2'
require File.join(File.dirname(__FILE__), "lib", "fedena_oauth")

FedenaPlugin.register = {
  :name=>"fedena_google_sso",
  :description=>"Fedena Google Sso",
  :auth_file=>"config/oauth_auth.rb",
  :multi_school_settings_hook=>{:title=>"Google OAuth",:destination=>{:controller=>:oauth_settings,:action=>:settings,:provider=>"google"},:plugin_name=>"fedena_google_sso"},
  :login_hook=>{:title=>"Google",:destination=>{:action=>'new', :controller=>'oauth', :provider=>"google"},:config_key=>"EnableOauth"},
  :multischool_models=>%w{GoogleToken}
}

Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
  I18n.load_path.unshift(locale)
end

FedenaGoogleSso.attach_overrides

if RAILS_ENV == 'development'
  ActiveSupport::Dependencies.load_once_paths.reject!{|x| x =~ /^#{Regexp.escape(File.dirname(__FILE__))}/}
end
