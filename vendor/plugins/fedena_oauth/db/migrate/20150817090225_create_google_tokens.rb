class CreateGoogleTokens < ActiveRecord::Migration
  def self.up
    create_table :google_tokens do |t|
      t.integer :user_id
      t.text :google_access_token
      t.text :google_refresh_token
      t.text :google_expired_at
      t.integer :school_id

      t.timestamps
    end
  end

  def self.down
    drop_table :google_tokens
  end
end
