available_plugin = AvailablePlugin rescue false
if available_plugin
  available_plugin_rows = AvailablePlugin.all
  available_plugin_rows.each do|a|
    p = a.plugins
    if p.include?("fedena_oauth")
      p.delete("fedena_oauth")
      p.push("fedena_google_sso")
      p.sort!
      a.update_attributes(:plugins=>p)
      puts "Completed modifying plugins for #{a.associated_type} #{a.associated_id}"
    end
  end
end