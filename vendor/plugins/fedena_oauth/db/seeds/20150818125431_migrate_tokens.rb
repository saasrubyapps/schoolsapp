token_present = GoogleToken rescue false
if token_present
  School.all.each do|s|
    MultiSchool.current_school = s
    auth_users = User.find(:all,:conditions=>["google_access_token is NOT NULL or google_refresh_token is NOT NULL or google_expired_at is NOT NULL"])
    unless auth_users.empty?
      puts "migrating google tokens for school #{s.id}"
      auth_users.each do|u|
        GoogleToken.create(:user_id=>u.id,:google_access_token=>u.google_access_token,:google_refresh_token=>u.google_refresh_token,:google_expired_at=>u.google_expired_at)
      end
    end
  end
end