class TransportController < ApplicationController
  before_filter :login_required
  filter_access_to :employee_transport_details,:attribute_check => true,:load_method => lambda {Employee.find(params[:id]).user}
  filter_access_to :all,:except => [:student_transport_details]
  filter_access_to [:student_transport_details], :attribute_check => true, :load_method => lambda { Student.find(params[:id]) }
  before_filter  :set_precision
  before_filter :protect_other_student_data, :only =>[:student_transport_details]

  def dash_board

  end
  def search_ajax
    if params[:option] == "student"
      if params[:query].length>= 3
        @students = Student.find(:all,
          :conditions => ["first_name LIKE ? OR middle_name LIKE ? OR last_name LIKE ?
                            OR admission_no = ? OR (concat(first_name, \" \", last_name) LIKE ? ) ",
            "#{params[:query]}%","#{params[:query]}%","#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}" ],
          :order => "batch_id asc,first_name asc",:include=>[{:batch=>:course},:transport]) unless params[:query] == ''
      else
        @students = Student.find(:all,
          :conditions => ["admission_no = ? " , params[:query]],
          :order => "batch_id asc,first_name asc",:include=>[{:batch=>:course},:transport]) unless params[:query] == ''
      end
      render :partial => "student_search_ajax"
    else
      if params[:query].length>= 3
        @employees = Employee.find(:all,
          :conditions => ["(first_name LIKE ? OR middle_name LIKE ? OR last_name LIKE ?
                       OR employee_number = ? OR (concat(first_name, \" \", last_name) LIKE ? ))",
            "#{params[:query]}%","#{params[:query]}%","#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}" ],
          :order => "employee_department_id asc,first_name asc",:include=>[:employee_department,:transport]) unless params[:query] == ''
      else
        @employees = Employee.find(:all,
          :conditions => ["(employee_number = ? )", "#{params[:query]}"],
          :order => "employee_department_id asc,first_name asc",:include=>[:employee_department,:transport]) unless params[:query] == ''
      end
      render :partial => "employee_search_ajax"
    end
  end

  def transport_details
    @vehicles = Vehicle.all
  end

  def pdf_report
    @transport = Transport.find_all_by_vehicle_id(params[:id])
    @vehicle = Vehicle.find_by_id(params[:id])
    render :pdf=>'pdf_report'
  end

  def ajax_transport_details
    unless params[:id].blank?
      @transport = Transport.find_all_by_vehicle_id(params[:id])
      @vehicle = Vehicle.find_by_id(params[:id])
      #@route = Route.find_by_main_route(params[@vehicle])
      render(:update) do |page|
        page.replace_html 'transport_details', :partial=>'ajax_transport_details'
      end
    else
      render(:update) do |page|
        page.replace_html 'transport_details', :text=>''
      end
    end
  end

  def add_transport
    if params[:user] == 'student'
      @user = Student.find params[:id]
    elsif params[:user] == 'employee'
      @user = Employee.find params[:id]
    end
    @transport = Transport.new
    @routes = Route.all( :conditions=>["main_route_id is NULL"])
    @vehicles = []
    @destination = []
    if request.post?
      @transport = Transport.new(params[:transport])
      @transport.receiver = @user
      if @transport.save
        flash[:notice] = "#{t('flash1')}"
        redirect_to :controller => :transport
      end
    end
  end

  def update_vehicle
    render :update do |page|
      unless params[:route].blank?
        @vehicles = Vehicle.find(:all,:conditions=>"main_route_id=#{params[:route]} and status='Active'")
        @destination = Route.find(:all,:conditions=>"id = #{params[:route]} OR main_route_id = #{params[:route]}") if params[:route].present?
        page.replace_html 'update_vehicle', :partial=>'update_vehicle'
        page.replace_html 'update_destination', :partial=>'update_destination'
      else
        @vehicles = []
        @destination = []
        @cost = ""
        page.replace_html 'update_vehicle', :partial=>'update_vehicle'
        page.replace_html 'update_destination', :partial=>'update_destination'
        page.replace_html 'seat_description', :text=>''
        page.replace_html 'fare', :partial=>'load_fare'
      end
    end
  end

  def load_fare
    @route = Route.find_by_id(params[:route])
    @cost = @route.cost
    render(:update) do |page|
      page.replace_html 'fare', :partial=>'load_fare'
    end
  end

  def seat_description
    @vehicle = Vehicle.find_by_id(params[:id])
    @no_of_seat = @vehicle.no_of_seats
    @allocated_seats = Transport.find_all_by_vehicle_id(@vehicle.id)
    @available_seats = @no_of_seat - @allocated_seats.count

    render(:update) do |page|
      page.replace_html 'seat_description', :partial=>'seat_description'
    end
  end

  def delete_transport
    @transport = Transport.find(params[:id])
    @transport.destroy
    flash[:notice] = "#{t('flash2')}"
     redirect_to :controller => :vehicles,:action=>'show',:id=>@transport.vehicle_id,:selected_value=>@transport.receiver_type
   end

  def edit_transport
    @transport = Transport.find(params[:id])
    redirect_vehicle_id=@transport.vehicle_id
    @routes = Route.all( :conditions=>["main_route_id IS NULL"])
    @vehicles = @transport.get_vehicles
    unless @transport.route.main_route_id.nil?
      @destination = Route.find(:all, :conditions=>"main_route_id = #{@transport.route.main_route_id} OR id = #{@transport.route.main_route_id} ")
    else
      @destination = Route.find(:all, :conditions=>"main_route_id = #{@transport.route.id} OR id = #{@transport.route.id} ")
    end
    if request.post?
      if params[:transport][:vehicle_id].nil?
        params[:transport][:vehicle_id]=""
      end
      if @transport.update_attributes(params[:transport])
        flash[:notice] = "#{t('flash3')}"
        redirect_to :controller => :vehicles,:action=>'show',:id=>redirect_vehicle_id,:selected_value=>@transport.receiver_type
      end
    else
      #      flash[:warn_notice] = "<p>#{params[:user].humanize} already assigned with a vehicle.</p>"
    end
  end

  def student_transport_details
    @current_user = current_user
    @available_modules = Configuration.available_modules
    @student = Student.find(params[:id])
    @transport = @student.transport
    unless @transport.nil?
      @vehicle = @transport.vehicle
      @route = @transport.route
    end
  end

  def employee_transport_details
    @current_user = current_user
    #    @new_reminder_count = Reminder.find_all_by_recipient(@current_user.id, :conditions=>"is_read = false")
    @available_modules = Configuration.available_modules
    @employee = Employee.find(params[:id])
    @transport = @employee.transport
    unless @transport.nil?
      @vehicle = @transport.vehicle
      @route = @transport.route
    end
  end
  def vehicle_report
    @sort_order=params[:sort_order]
    if @sort_order.nil?
      @vehicles=Vehicle.paginate(:select=>"vehicles.*,no_of_seats-count(transports.id) as available_seats",:conditions=>["status LIKE ?","active"],:joins=>[:transports],:group=>'vehicles.id',:per_page=>10,:page=>params[:page],:order=>'vehicle_no')
    else
      @vehicles=Vehicle.paginate(:select=>"vehicles.*,no_of_seats-count(transports.id) as available_seats",:conditions=>["status LIKE ?","active"],:joins=>[:transports],:group=>'vehicles.id',:per_page=>10,:page=>params[:page],:order=>@sort_order)
    end
    if request.xhr?
      render :update do |page|
        page.replace_html "information", :partial => "vehicle_details"
      end
    end
  end
  def vehicle_report_csv
    sort_order=params[:sort_order]
    if sort_order.nil?
      vehicles=Vehicle.all(:select=>"vehicles.*,no_of_seats-count(transports.id) as available_seats",:conditions=>["status LIKE ?","active"],:joins=>[:transports],:group=>'vehicles.id',:order=>'vehicle_no')
    else
      vehicles=Vehicle.all(:select=>"vehicles.*,no_of_seats-count(transports.id) as available_seats",:conditions=>["status LIKE ?","active"],:joins=>[:transports],:group=>'vehicles.id',:order=>sort_order)
    end
    csv_string=FasterCSV.generate do |csv|
      cols=["#{t('no_text')}","#{t('vehicle_no')}","#{t('route') }","#{t('no_of_seats') }","#{t('available_seats')}"]
      csv << cols
      vehicles.each_with_index do |s,i|
        col=[]
        col<< "#{i+1}"
        col<< "#{s.vehicle_no}"
        col<< "#{s.main_route.nil? ? t('deleted_route') : s.main_route.destination}"
        col<< "#{s.no_of_seats}"
        col<< "#{s.available_seats}"
        col=col.flatten
        csv<< col
      end
    end
    filename = "#{t('vehicle')}#{t('details')}- #{Time.now.to_date.to_s}.csv"
    send_data(csv_string, :type => 'text/csv; charset=utf-8; header=present', :filename => filename)
  end
  def single_vehicle_details
    @sort_order=params[:sort_order] || nil
    @vehicle= Vehicle.find(params[:id])
    @receivers=Transport.paginate(:select=>"transports.*,students.first_name,students.middle_name,students.last_name,students.admission_no,employees.first_name as emp_first_name ,employees.middle_name as emp_middle_name,employees.last_name as emp_last_name,employees.employee_number,routes.destination ,IF(transports.receiver_type='Student',students.first_name,employees.first_name) as receiver_name, r.destination as route_name",:joins=>"LEFT OUTER JOIN `routes` ON `routes`.id = `transports`.route_id LEFT OUTER JOIN routes r on r.id=routes.main_route_id LEFT OUTER JOIN students on students.id=transports.receiver_id LEFT OUTER JOIN employees on employees.id=transports.receiver_id",:conditions=>{:vehicle_id=>params[:id]},:per_page=>15,:page=>params[:page],:order=>@sort_order)
    if request.xhr?
      render :update do |page|
        page.replace_html "information", :partial => "single_vehicle_report"
      end
    end
  end
  def single_vehicle_details_csv
    parameters={:sort_order=>params[:sort_order],:vehicle_id=>params[:id]}
    model='transport'
    method='single_vehicle_details'
    csv_report=AdditionalReportCsv.find_by_model_name_and_method_name(model,method)
    if csv_report.nil?
      csv_report=AdditionalReportCsv.new(:model_name=>model,:method_name=>method,:parameters=>parameters)
      if csv_report.save
        Delayed::Job.enqueue(DelayedAdditionalReportCsv.new(csv_report.id),{:queue => "additional_reports"})
      end
    else
      if csv_report.update_attributes(:parameters=>parameters,:csv_report=>nil)
        Delayed::Job.enqueue(DelayedAdditionalReportCsv.new(csv_report.id),{:queue => "additional_reports"})
      end
    end
    flash[:notice]="#{t('csv_report_is_in_queue')}"
    redirect_to :controller=>:report,:action=>:csv_reports,:model=>model,:method=>method
  end
  def reports
  end
  def student_transport_report
    @courses=Course.all(:select => "course_name,section_name,id", :conditions => {:is_deleted => false}, :order => 'course_name ASC')
    @sort_order=params[:sort_order] || nil
    if params[:transport_search].present? and params[:transport_search][:batch_ids].present? and params[:transport_search][:report_type].present?
      if params[:transport_search][:report_type]=="allotted"
        @students = Student.student_transport_details.batch_wise_student_transport(params[:transport_search][:batch_ids]).transport_sort_order(@sort_order).paginate(:per_page => 20, :page => params[:page])
      else
        @students = Student.student_transport_details.batch_wise_all_students(params[:transport_search][:batch_ids]).transport_sort_order(@sort_order).paginate(:per_page => 20, :page => params[:page])
      end
    else
      error_message = "#{t('please')} #{t('select')} #{t('the')} #{t('required_fields')}"
    end
    if request.xhr?
      render :update do |page|
        if error_message.present?
          page.replace_html "information", :text => "<p class='flash-msg'>#{error_message}</p>"
        else
          page.replace_html "information", :partial => "student_transport_report"
        end
      end
    end
  end
  def list_batches
    unless params[:course_id].blank?
      @batches=Batch.all(:select => "name,id,course_id", :conditions => {:course_id => params[:course_id], :is_deleted => false, :is_active => true}, :order => 'name ASC')
      render :update do |page|
        page.replace_html "batch_list", :partial => "list_batches"
      end
    else
      render :update do |page|
        page.replace_html "batch_list", :text => ""
      end
    end
  end
  def students_transport_report_csv
    parameters={:course=>params[:course],:transport_search=>params[:transport_search],:sort_order => params[:sort_order]}
    csv_export('transport', 'students_transport_report', parameters)
  end
  def employee_transport_report
    @departments=EmployeeDepartment.all(:select => "name,code,id", :conditions => {:status => true}, :order => 'name ASC')
    @sort_order=params[:sort_order] || nil
    if params[:transport_search].present? and params[:department][:department_id].present? and params[:transport_search][:report_type].present?
      if params[:transport_search][:report_type]=="allotted"
        @employees = Employee.employee_transport_details.department_wise_employee_transport(params[:department][:department_id]).transport_sort_order(@sort_order).paginate(:per_page => 20, :page => params[:page])
      else
        @employees = Employee.employee_transport_details.department_wise_all_employees(params[:department][:department_id]).transport_sort_order(@sort_order).paginate(:per_page => 20, :page => params[:page])
      end
    else
      error_message = "#{t('please')} #{t('select')} #{t('the')} #{t('required_fields')}"  
    end
    if request.xhr?
      render :update do |page|
        if error_message.present?
          page.replace_html "information", :text => "<p class='flash-msg'>#{error_message}</p>"
        else
          page.replace_html "information", :partial => "employee_transport_report"
        end
      end
    end
  end
  def employee_transport_report_csv
    parameters={:department=>params[:department],:transport_search=>params[:transport_search],:sort_order => params[:sort_order]}
    csv_export('transport', 'employees_transport_report', parameters)
  end
  def route_report
    @main_routes=Route.all(:select => "destination,id", :conditions => ['main_route_id IS NULL'], :order => 'destination ASC') 
    @sort_order=params[:sort_order] || nil
    if params[:transport_search].present? and params[:transport_search][:route_ids].present? and params[:main][:route_id].present? and params[:transport_search][:type].present?
      unless params[:transport_search][:type]=="all"
        @transports = Transport.all_transports.route_and_receiver_type_wise(params[:transport_search][:route_ids],params[:transport_search][:type]).sort_order(@sort_order).paginate(:per_page => 20, :page => params[:page])
      else
        @transports = Transport.all_transports.route_wise(params[:transport_search][:route_ids]).sort_order(@sort_order).paginate(:per_page => 20, :page => params[:page]) 
      end
    else
      error_message = "#{t('please')} #{t('select')} #{t('the')} #{t('required_fields')}"  
    end
    if request.xhr?
      render :update do |page|
        if error_message.present?
          page.replace_html "information", :text => "<p class='flash-msg'>#{error_message}</p>"
        else
          page.replace_html "information", :partial => "route_report"
        end
      end
    end
  end
  def list_routes
    unless params[:route_id].blank?
      @routes=Route.all(:select => "destination,id",:conditions => ['main_route_id = ? or id=?',params[:route_id],params[:route_id]], :order => 'destination ASC')
      render :update do |page|
        page.replace_html "batch_list", :partial => "list_routes"
      end
    else
      render :update do |page|
        page.replace_html "batch_list", :text => ""
      end
    end
  end
  def route_report_csv
    parameters={:main=>params[:main],:transport_search=>params[:transport_search],:sort_order => params[:sort_order]}
    csv_export('transport', 'route_transport_report', parameters)
  end
  private
  def csv_export(model, method, parameters)
    csv_report=AdditionalReportCsv.find_by_model_name_and_method_name(model, method)
    if csv_report.nil?
      csv_report=AdditionalReportCsv.new(:model_name => model, :method_name => method, :parameters => parameters)
      if csv_report.save
        Delayed::Job.enqueue(DelayedAdditionalReportCsv.new(csv_report.id),{:queue => "additional_reports"})
      end
    else
      if csv_report.update_attributes(:parameters => parameters, :csv_report => nil)
        Delayed::Job.enqueue(DelayedAdditionalReportCsv.new(csv_report.id),{:queue => "additional_reports"})
      end
    end
    flash[:notice]="#{t('csv_report_is_in_queue')}"
    redirect_to :controller=> :reports, :action => :csv_reports, :model => model, :method => method
  end

end
