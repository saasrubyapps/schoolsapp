class TransportFeeController < ApplicationController
  require 'authorize_net'
  helper :authorize_net
  before_filter :login_required
  filter_access_to :all
  filter_access_to :student_profile_fee_details,:attribute_check=>true,:load_method=>lambda {Student.find(params[:id])}
  filter_access_to :transport_fee_receipt_pdf,:attribute_check=>true,:load_method=>lambda {FinanceTransaction.find params[:id]}
  before_filter :set_precision
  protect_from_forgery :except => [:student_profile_fee_details]
  before_filter :load_tax_setting, :only => [:transport_fee_collection_new, :transport_fee_collection_create,
    :collection_assign_students, :receiver_wise_collection_new, :receiver_wise_fee_collection_creation ]
  #fingerprint filter
  check_request_fingerprint :transport_fee_collection_create,:transport_fee_collection_pay

  def index

  end

  def transport_fee_collection_new
    @fee_categories = FinanceFeeCategory.find(:all, :conditions => ["is_master = '#{1}' and is_deleted = '#{false}'"])
    @transport_fee_collection = TransportFeeCollection.new
    @batches = Batch.active
    @batches.reject! { |x| x.transports.blank? }
    @tax_slabs = TaxSlab.all if @tax_enabled
  end

  def send_reminder(transport_fee_collection, recipients)
    #
    #    subject = "#{t('fees_submission_date')}"
    #    Delayed::Job.enqueue(DelayedReminderJob.new(:sender_id => current_user.id,
    #        :recipient_ids => recipients,
    #        :subject => subject,
    #        :body => body))
    body = "#{t('transport_text')} #{t('fee_collection_date_for')} <b> #{transport_fee_collection.name} </b> #{t('has_been_published')} #{t('by')} <b>#{current_user.full_name}</b>, #{t('start_date')} : #{format_date(transport_fee_collection.start_date)}  #{t('due_date')} :  #{format_date(transport_fee_collection.due_date)} "
    links = {:target=>'view_fees',:target_param=>'student_id'}  
    inform(recipients,body,'Finance',links)
  end

  def transport_fee_collection_create
    @transport_fee_collection = TransportFeeCollection.new
    @batches = Batch.active
    @batches.reject! { |x| x.transports.blank? }
    @tax_slabs = TaxSlab.all if @tax_enabled
    if request.post?
      unless params[:transport_fee_collection].nil?
        @include_employee = params[:transport_fee_collection][:employee]
        @batchs = params[:transport_fee_collection][:batch_ids]
        parameters= params[:transport_fee_collection]
        parameters.delete("batch_ids")
        parameters.delete("employee")
        @transport_fee_collection=TransportFeeCollection.new(parameters)
        @transport_fee_collection.valid?
        @transport_fee_collection.errors.add_to_base("#{t('please_select_a_batch_or_emp')}")  if(@batchs.blank? and @include_employee.blank?)
        if @transport_fee_collection.errors.empty?
          Delayed::Job.enqueue(DelayedTransportFeeCollectionJob.new(current_user,@batchs,@include_employee,params[:transport_fee_collection]))
          flash[:notice]="Collection is in queue. <a href='/scheduled_jobs/TransportFeeCollection/1'>Click Here</a> to view the scheduled job."
          redirect_to :action => 'transport_fee_collection_view'
        else
          render :action => 'transport_fee_collection_new'
        end
      end
    else
      render :action => 'transport_fee_collection_new'
    end
  end
  def transport_fee_collection_view
    #@transport_fee_collection = ''
    #@batches = Batch.active
    @transport_fee_collection = TransportFeeCollection.paginate(:select => "distinct transport_fee_collections.*", :joins => [:transport_fees], :conditions => "transport_fees.receiver_type='Employee' and is_deleted=false", :per_page => 20, :page => params[:page])
  end


  #def transport_fee_collection_details
  # @transport_fee_details = TransportFee.find_by_transport_fee_collection_id(params[:id])
  #render :update do |page|
  # page.replace_html 'transport_fee_collection_details', :partial => 'transport_fee_collection_details'
  #end
  #end

  def transport_fee_collection_date_edit
    @transport_fee_collection = TransportFeeCollection.find params[:id]
  end

  def transport_fee_collection_date_update
    @transport_fee_collection = TransportFeeCollection.find params[:id]
    render :update do |page|
      if @transport_fee_collection.update_attributes(params[:fee_collection])
        @user_type=params[:user_type]
        @transport_fee_collection.event.update_attributes(:start_date => @transport_fee_collection.due_date.to_datetime, :end_date => @transport_fee_collection.due_date.to_datetime)
        if @user_type=='employee'
          #         @transport_fee_collection = TransportFeeCollection.find(:all, :conditions=>'batch_id IS NULL')
          @transport_fee_collection = TransportFeeCollection.paginate(:select => "distinct transport_fee_collections.*", :joins => [:transport_fees], :conditions => "transport_fees.receiver_type='Employee' and is_deleted=false and transport_fees.is_active=true", :per_page => 20, :page => params[:page])
          page << "Modalbox.hide()" unless params[:page]

          @user_type = 'employee'
          page.replace_html 'fee_collection_list', :partial => 'fee_collection_list'
          page.replace_html 'flash-box', :text => "<p class='flash-msg'>#{t('transport_fee.flash1')}</p>"
          page.replace_html 'batch_list', :text => ''
        elsif @user_type=='student'
          #         @transport_fee_collection = TransportFeeCollection.find_all_by_batch_id(params[:batch_id])
          @transport_fee_collection = TransportFeeCollection.paginate(:all, :select => "distinct transport_fee_collections.*", :joins => "INNER JOIN `transport_fees` ON transport_fees.transport_fee_collection_id = transport_fee_collections.id INNER JOIN students on students.id=transport_fees.receiver_id and transport_fees.receiver_type='Student'", :conditions => "students.batch_id= #{params[:batch_id]} and transport_fees.is_active=true", :per_page => 20, :page => params[:page])

          @user_type = 'student'
          @batches = Batch.active
          page << "Modalbox.hide()" unless params[:page]
          #          page.replace_html 'batch_list', :partial=>'students_batch_list'
          page.replace_html 'fee_collection_list', :partial => 'fee_collection_list'
          page.replace_html 'flash-box', :text => "<p class='flash-msg'>#{t('transport_fee.flash1')}</p>"
        else
          page.replace_html 'batch_list', :text => ''
          page.replace_html 'fee_collection_list', :text => ''
        end
        #        page << "Modalbox.hide()"
      else
        @errors = true
        page.replace_html 'form-errors', :partial => 'transport_fee/errors', :object => @transport_fee_collection
        page.visual_effect(:highlight, 'form-errors')
      end

    end

  end

  def transport_fee_collection_edit
    @transport_fee_collection = TransportFee.find params[:id]
    @batches = Batch.active
    @selected_batches = [1]
  end

  def transport_fee_collection_update
    @transport_fee_collection = TransportFee.find params[:id]
    flash[:notice]="#{t('flash2')}" if @transport_fee_collection.update_attributes(params[:fee_collection]) if request.post?
    @transport_fee_collection_details = TransportFee.find_all_by_name(@transport_fee_collection.name)
  end

  def transport_fee_collection_delete
    @transport_fee_collection = TransportFee.find params[:id]
    @transport_fee_collection.destroy
    flash[:notice] = "#{t('flash3')}"
    redirect_to :controller => 'transport_fee', :action => 'transport_fee_collection_view'
  end

  def transport_fee_pay

    @transport_fee_collection_details = TransportFee.find params[:id]
    category_id = FinanceTransactionCategory.find_by_name("Transport").id
    transaction = FinanceTransaction.new
    transaction.title = @transport_fee_collection_details.transport_fee_collection.name
    transaction.category_id = category_id
    transaction.amount = @transport_fee_collection_details.bus_fare
    transaction.amount += params[:fine].to_f unless params[:fine].nil?
    transaction.fine_included = true unless params[:fine].nil?
    transaction.transaction_date = FedenaTimeSet.current_time_to_local_time(Time.now).to_date
    transaction.payee = @transport_fee_collection_details.receiver
    transaction.finance = @transport_fee_collection_details
    unless transaction.save
      #      @transport_fee_collection_details.update_attribute(:transaction_id, transaction.id)
      render :text => transaction.errors.full_messages and return
    end
    @collection_id = params[:collection_id]
    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(params[:collection_id])
    #    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(@transport_fee_collection_details.transport_fee_collection_id)
    @user = TransportFee.find_by_transport_fee_collection_id_and_id(params[:collection_id], params[:id]) unless params[:id].nil?
    @user ||= @transport_fee.first
    @next_user = @user.next_user
    @prev_user = @user.previous_user
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id)
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id)
    render :update do |page|
      page.replace_html 'transport_fee_collection_details', :partial => 'transport_fee_collection_details'
    end
  end

  def transport_fee_defaulters_view
    @transport_fee_collection = ''
    @batches = Batch.all(
      :joins=>{
        :students=>{
          :transport_fees=>:transport_fee_collection
        }
      },
      :conditions =>["batches.is_deleted=? and batches.is_active=? and transport_fee_collections.is_deleted=? and transport_fee_collections.due_date < ? and transport_fees.balance > ? and transport_fees.receiver_type='Student' and transport_fees.is_active = ? ",false,true,false,Date.today,0,true],:group=>"batches.id")
  end

  def transport_fee_defaulters_details
    @transport_fee_details = TransportFeeCollection.find_all_by_name(params[:name])
    @transport_defaulters = @transport_fee_details.reject { |u| !u.transaction_id.nil? }
    render :update do |page|
      page.replace_html 'transport_fee_defaulters_details', :partial => 'transport_fee_defaulters_details'
    end
  end

  def transport_defaulters_fee_pay
    @transport_fee_defaulters_details = TransportFee.find params[:id]
    category_id = FinanceTransactionCategory.find_by_name("Transport").id
    transaction = FinanceTransaction.new
    transaction.title = @transport_fee_defaulters_details.transport_fee_collection.name
    transaction.category_id = category_id
    transaction.transaction_date = FedenaTimeSet.current_time_to_local_time(Time.now).to_date
    transaction.amount = @transport_fee_defaulters_details.bus_fare
    transaction.amount += params[:fine].to_f unless params[:fine].nil?
    transaction.fine_included = true unless params[:fine].nil?
    transaction.payee = @transport_fee_defaulters_details.receiver
    transaction.finance = @transport_fee_defaulters_details
    #    if transaction.save
    #      @transport_fee_defaulters_details.update_attribute(:transaction_id, transaction.id)
    #    end
    @transport_defaulters = TransportFee.find_all_by_transport_fee_collection_id(@transport_fee_defaulters_details.transport_fee_collection_id)
    @transport_defaulters = @transport_defaulters.reject { |u| !u.transaction_id.nil? }
    @collection_id = params[:collection_id]
    @transport_fee_collection= TransportFeeCollection.find_by_id(params[:collection_id])
    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(params[:collection_id])
    #@transport_fee = @transport_fee.reject{|u| !u.transaction_id.nil? }
    @user = TransportFee.find_by_transport_fee_collection_id_and_id(params[:collection_id], params[:id]) unless params[:id].nil?
    @user ||= @transport_fee_collection.transport_fees.first(:conditions => ["transaction_id is null"])
    @next_user = @user.next_default_user unless @user.nil?
    @prev_user = @user.previous_default_user unless @user.nil?
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id) unless @user.nil?
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id) unless @user.nil?
    render :update do |page|
      page.replace_html 'defaulters_transport_fee_collection_details', :partial => 'defaulters_transport_fee_collection_details'
    end
  end

  def tsearch_logic # transport search fees structure
    @option = params[:option]
    if params[:option] == "student"
      if params[:query].length>= 3
        @students_result = Student.find(:all,
          :conditions => ["first_name LIKE ? OR middle_name LIKE ? OR last_name LIKE ?
                            OR admission_no = ? OR (concat(first_name, \" \", last_name) LIKE ? ) ",
            "#{params[:query]}%", "#{params[:query]}%", "#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}"],
          :order => "batch_id asc,first_name asc") unless params[:query] == ''
        @students_result.reject! { |s| s.transport_fees.empty? }
      else
        @students_result = Student.find(:all,
          :conditions => ["admission_no = ? ", params[:query]],
          :order => "batch_id asc,first_name asc") unless params[:query] == ''
        @students_result.reject! { |s| s.transport_fees.empty? }
      end if params[:query].present?
    else

      if params[:query].length>= 3
        @employee_result = Employee.find(:all,
          :conditions => ["(first_name LIKE ? OR middle_name LIKE ? OR last_name LIKE ?
                       OR employee_number = ? OR (concat(first_name, \" \", last_name) LIKE ? ))",
            "#{params[:query]}%", "#{params[:query]}%", "#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}"],
          :order => "employee_department_id asc,first_name asc") unless params[:query] == ''
        @employee_result.reject! { |s| s.transport_fees.empty? }
      else
        @employee_result = Employee.find(:all,
          :conditions => ["(employee_number = ? )", "#{params[:query]}"],
          :order => "employee_department_id asc,first_name asc") unless params[:query] == ''
        @employee_result.reject! { |s| s.transport_fees.empty? }
      end if params[:query].present?
    end
    render :layout => false
  end

  def fees_student_dates
    if params[:payer_type].present?
      @payer_type=params[:payer_type]
      if params[:payer_type]=='Archived Student'
        @student = ArchivedStudent.find_by_former_id(params[:id])
        unless @student.present?
          flash[:notice] = "#{t('finance.no_payer')}"
          redirect_to :controller => 'user', :action => 'dashboard' and return
        end
        @student.id=@student.former_id
      else
        @student = Student.find_by_id(params[:id])
        unless @student.present?
          flash[:notice] = "#{t('finance.no_payer')}"
          redirect_to :controller => 'user', :action => 'dashboard' and return
        end
      end
    else
      @student = Student.find_by_id(params[:id])
      unless @student.present?
        flash[:notice] = "#{t('finance.no_payer')}"
        redirect_to :controller => 'user', :action => 'dashboard' and return
      end
    end
    @transport_fee_collection=TransportFeeCollection.find_by_id(params[:collection_id])
    @payment_date = params[:payment_date] ? Date.parse(params[:payment_date]) : Date.today_with_timezone
    @transport_fees = TransportFee.active.all(:conditions => ["receiver_type='Student' and receiver_id = #{@student.id} and bus_fare IS NOT NULL"])
    @dates = @transport_fees.map { |t| t.transport_fee_collection }
    @dates.compact!
  end

  def fees_employee_dates
    if params[:payer_type].present?
      @payer_type=params[:payer_type]
      if params[:payer_type]=='Archived Employee'
        @employee = ArchivedEmployee.find_by_former_id(params[:id])
        unless @employee.present?
          flash[:notice] = "#{t('finance.no_payer')}"
          redirect_to :controller => 'user', :action => 'dashboard' and return
        end
        @employee.id=@employee.former_id
      else
        @employee = Employee.find_by_id(params[:id])
        unless @employee.present?
          flash[:notice] = "#{t('finance.no_payer')}"
          redirect_to :controller => 'user', :action => 'dashboard' and return
        end
      end
    else
      @employee = Employee.find_by_id(params[:id])
      unless @employee.present?
        flash[:notice] = "#{t('finance.no_payer')}"
        redirect_to :controller => 'user', :action => 'dashboard' and return
      end
    end
    @transport_fee_collection=TransportFeeCollection.find_by_id(params[:collection_id])
    @transport_fees = TransportFee.active.all(:conditions => ["receiver_type='Employee' and receiver_id = #{@employee.id} and bus_fare IS NOT NULL"])
    @dates = @transport_fees.map { |t| t.transport_fee_collection }
    @dates.compact!
  end

  def fees_submission_student
    @user = params[:id]
    if params[:payer_type].present?
      @payer_type=params[:payer_type]
      if params[:payer_type]=='Archived Student'
        @student = ArchivedStudent.find_by_former_id(params[:student])
        unless @student.present?
          flash[:notice] = "#{t('no_payer')}"
          redirect_to :controller => 'user', :action => 'dashboard' and return
        end
        @student.id=@student.former_id
      else
        @student = Student.find_by_id(params[:student])
        unless @student.present?
          flash[:notice] = "#{t('no_payer')}"
          redirect_to :controller => 'user', :action => 'dashboard' and return
        end
      end
    else
      @student = Student.find_by_id(params[:student])
      unless @student.present?
        flash[:notice] = "#{t('finance.no_payer')}"
        redirect_to :controller => 'user', :action => 'dashboard' and return
      end
    end
    @fine = params[:fees][:fine] if params[:fees].present?
    unless params[:date].blank?
      @transport_fee = TransportFee.find_by_receiver_id_and_transport_fee_collection_id(
        params[:student], params[:date], :include => {:finance_transactions => :transaction_ledger},
        :conditions => "receiver_type = 'Student'")
      @date = @transport_fee.transport_fee_collection
      @tax_slab = @date.collection_tax_slabs.try(:last) if @transport_fee.tax_enabled?
      @transaction = FinanceTransaction.find(@transport_fee.transaction_id) unless @transport_fee.transaction_id.nil?
    end
    @payment_date = params[:payment_date] ? Date.parse(params[:payment_date]) : Date.today
    render :update do |page|
      #page.replace_html "fee_submission", :partial => "fees_details"
      page.replace_html "fees_details", :partial => "fees_details"
    end
  end

  def fees_submission_employee
    #    flash.clear
    #    if params[:date]==""
    #      render :update do |page|
    #        page.replace_html "fee_submission", :text => ""
    #      end
    #    else
    @fine=params[:fees][:fine] if params[:fees].present?
    @user = params[:id]
    if params[:payer_type].present?
      @payer_type=params[:payer_type]
      if params[:payer_type]=='Archived Employee'
        @employee = ArchivedEmployee.find_by_former_id(params[:employee])
        unless @employee.present?
          flash[:notice] = "#{t('no_payer')}"
          redirect_to :controller => 'user', :action => 'dashboard' and return
        end
        @employee.id=@employee.former_id
      else
        @employee = Employee.find_by_id(params[:employee])
        unless @employee.present?
          flash[:notice] = "#{t('no_payer')}"
          redirect_to :controller => 'user', :action => 'dashboard' and return
        end
      end
    else
      @employee = Employee.find_by_id(params[:employee])
      unless @employee.present?
        flash[:notice] = "#{t('finance.no_payer')}"
        redirect_to :controller => 'user', :action => 'dashboard' and return
      end
    end
    unless params[:date].blank?
      @transport_fee = TransportFee.find_by_receiver_id_and_transport_fee_collection_id(params[:employee], params[:date], 
        :include => {:finance_transactions => :transaction_ledger},
        :conditions => "receiver_type = 'Employee'")
      @transport_fee.reload
      @date = @transport_fee.transport_fee_collection
      @tax_slab = @date.collection_tax_slabs.try(:last) if @transport_fee.tax_enabled?
    end
    @payment_date = params[:payment_date] ? Date.parse(params[:payment_date]) : Date.today_with_timezone
    render :update do |page|
      page.replace_html "fee_submission", :partial => "fees_details"
    end
    #    end
  end

  def update_fee_collection_dates
    @transport_fee_collections=TransportFeeCollection.all(
      :joins=>"INNER JOIN transport_fees on transport_fees.transport_fee_collection_id =transport_fee_collections.id and transport_fees.groupable_type='Batch'",
      :conditions =>["transport_fees.groupable_id=? and transport_fee_collections.is_deleted=? and transport_fees.bus_fare > ? and transport_fees.is_active = ?",params[:batch_id],false,0.0,true],
      :group =>"transport_fee_collections.id")
    render :update do |page|
      page.replace_html 'fees_collection_dates', :partial => 'transport_fee_collection_dates'
    end
  end

  def select_payment_mode
    if  params[:payment_mode]=="Others"
      render :update do |page|
        page.replace_html "payment_mode", :partial => "select_payment_mode"
      end
    else
      render :update do |page|
        page.replace_html "payment_mode", :text => ""
      end
    end
  end

  def transport_fee_collection_pay
    @transport_fee = TransportFee.find(params[:fees][:finance_id])        
    @date = @transport_fee.transport_fee_collection
    @tax_slab = @date.collection_tax_slabs.try(:last) 
    if params[:receiver_type]=='Student'
      @student = Student.find(params[:receiver_id])
      @batch=@student.batch
      @students=Student.find(:all,
        :joins=>"inner join transport_fees tf on tf.receiver_id=students.id and tf.receiver_type='Student'", 
        :conditions => "tf.transport_fee_collection_id='#{@date.id}' and tf.is_active=1 and 
                                 students.batch_id='#{@batch.id}'",:order=>"id ASC")
      @prev_student=@students.select{|student| student.id<@student.id}.last||@students.last
      @next_student=@students.select{|student| student.id>@student.id}.first||@students.first
    end
    category_id = FinanceTransactionCategory.find_by_name("Transport").id
    error_flash_proc = ""
    unless params[:fees][:payment_mode].blank?
      FinanceTransaction.transaction do
        @transaction= FinanceTransaction.new(params[:fees])
        @transaction.title = @transport_fee.transport_fee_collection.name
        @transaction.category_id = category_id
        @transaction.transaction_date = params[:transaction_date]
        @transaction.payee = @transport_fee.receiver
        @transaction.save
      end
      if @transaction.errors.empty?
        user_event = UserEvent.first(:conditions => ["user_id = ? AND event_id = ?",@transport_fee.receiver.user_id,@date.event.id])
        user_event.destroy if user_event.present?
        #        @transport_fee.update_attributes(:transaction_id => @transaction.id)
        error_flash_proc = Proc.new{{:text=>"<p class='flash-msg'>#{t('finance.flash14')}.  <a href ='#' onclick='show_print_dialog(#{@transaction.id})'>#{t('print_receipt')}</a></p>"}}
        # flash[:warning]="#{t('finance.flash14')}. <a href ='http://#{request.host_with_port}/finance/generate_fee_receipt_pdf?transaction_id=#{@transaction.id}' target='_blank'>#{t('print_receipt')}</a>"
        flash[:warn_notice]=nil
      else
        flash[:warning] =nil
        error_flash_proc=Proc.new{{:partial => 'render_errors',:locals=>{:object=>'transaction'}}}
      end        
      
    else
      flash[:notice]=nil
      flash[:warn_notice]="#{t('select_one_payment_mode')}"
    end

    render :update do |page|
      @transport_fee.reload
      page.replace_html 'fees_details', :partial => 'fees_details'
      page.replace_html 'flash-msg',  error_flash_proc.present? ? error_flash_proc.call : ""
      # page.replace_html 'flash-msg', :text=>"<p class='flash-msg'>#{flash[:warning]}</p>"
    end
  end

  def transport_fee_collection_details
    @date=TransportFeeCollection.find(params[:date])
    @batch=Batch.find(params[:batch_id])
    @fine = params[:fees][:fine] if params[:fees].present?
    @students=Student.find(:all,:joins=>"inner join transport_fees tf on tf.receiver_id=students.id and tf.receiver_type='Student'", 
      :conditions => "tf.transport_fee_collection_id='#{@date.id}' and tf.is_active=1 and tf.groupable_type='Batch' and tf.groupable_id='#{@batch.id}'",
      :order=>"id ASC")
    if params[:student].present?
      @student = Student.find(params[:student])
    else
      @student=@students.first
    end
    @prev_student=@students.select{|student| student.id<@student.id}.last||@students.last
    @next_student=@students.select{|student| student.id>@student.id}.first||@students.first
    @transport_fee = TransportFee.active.find_by_receiver_id_and_transport_fee_collection_id(@student.id, @date.id, 
      :include => {:finance_transactions => :transaction_ledger},
      :conditions => "receiver_type = 'Student'")
    @transport_fee_collection = @transport_fee.transport_fee_collection
    @tax_slab = @transport_fee_collection.collection_tax_slabs.try(:last) if @transport_fee.tax_enabled
    @transaction = FinanceTransaction.find_by_id(@transport_fee.transaction_id) unless @transport_fee.transaction_id.nil?
    flash.clear
    render :update do |page|
      page.replace_html "fees_detail", :partial => "fees_submission_form"
    end

  end

  #  def transport_fee_collection_details
  #    @collection_id = params[:collection_id]
  #    @transport_fee_collection= TransportFeeCollection.find_by_id(params[:collection_id])
  #    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(params[:collection_id])
  #    @user = TransportFee.find_by_transport_fee_collection_id_and_id(params[:collection_id], params[:id]) unless params[:id].nil?
  #    @user ||= @transport_fee_collection.transport_fees.first
  #    @next_user = @user.next_user unless @user.nil?
  #    @prev_user = @user.previous_user unless @user.nil?
  #    @transaction = FinanceTransaction.find_by_id(@user.transaction_id) unless @user.nil?
  #    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id) unless @user.nil?
  #    render :update do |page|
  #      page.replace_html 'transport_fee_collection_details', :partial => 'transport_fee_collection_details'
  #    end
  #  end

  def update_fine_ajax
    @collection_id = params[:fine][:transport_fee_collection]
    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(params[:fine][:transport_fee_collection])
    @user = TransportFee.find_by_transport_fee_collection_id_and_id(params[:fine][:transport_fee_collection], params[:fine][:_id]) unless params[:fine][:_id].nil?
    @user ||= @transport_fee.first
    @next_user = @user.next_user
    @prev_user = @user.previous_user
    @fine = (params[:fine][:fee])
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id)
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id)
    render :update do |page|
      page.replace_html 'transport_fee_collection_details', :partial => 'transport_fee_collection_details'
    end
  end

  def update_student_fine_ajax
    @collection_id = params[:fine][:transport_fee_collection]
    @transport_fee = TransportFee.find_by_transport_fee_collection_id_and_receiver_id_and_receiver_type(params[:fine][:transport_fee_collection], params[:fine][:_id], 'Student') unless params[:fine][:_id].nil?
    @transport_fee_collection= TransportFeeCollection.find_by_id(@transport_fee.transport_fee_collection_id)
    @transaction = FinanceTransaction.find_by_id(@transport_fee.transaction_id)
    render :update do |page|
      unless params[:fine][:fee].to_f < 0
        @fine = params[:fine][:fee]
        @student = Student.find(params[:fine][:_id])
        page.replace_html 'fee_submission', :partial => 'fees_submission_form', :with => @student
        page.replace_html 'flash-msg', :text => ""
      else
        @student = Student.find(params[:fine][:_id])
        page.replace_html 'fee_submission', :partial => 'fees_submission_form'
        page.replace_html 'flash-msg', :text => "<p class='flash-msg'>Fine amount cannot be negative</p>"
      end
    end
  end


  #  def employee_transport_fee_collection
  #    @transport_fee_collection =TransportFeeCollection.employee
  #  end
  #
  #  def employee_transport_fee_collection_details
  #    @collection_id = params[:collection_id]
  #    @transport_fee_collection= TransportFeeCollection.find_by_id(params[:collection_id])
  #    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(params[:collection_id])
  #    @user = TransportFee.find_by_transport_fee_collection_id_and_id(params[:collection_id], params[:id]) unless params[:id].nil?
  #    @user ||= @transport_fee_collection.transport_fees.first
  #    unless @user.nil?
  #      @next_user = @user.next_user
  #      @prev_user = @user.previous_user
  #      @transaction = FinanceTransaction.find_by_id(@user.transaction_id)
  #      @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id)
  #    end
  #    render :update do |page|
  #      page.replace_html 'transport_fee_collection_details', :partial => 'employee_transport_fee_collection_details'
  #    end
  #  end

  def update_employee_fine_ajax
    @collection_id = params[:fine][:transport_fee_collection]
    @transport_fee = TransportFee.active.find_all_by_transport_fee_collection_id(params[:fine][:transport_fee_collection])
    @user = TransportFee.active.find_by_transport_fee_collection_id_and_id(params[:fine][:transport_fee_collection], params[:fine][:_id]) unless params[:fine][:_id].nil?
    @user ||= @transport_fee.first
    @next_user = @user.next_user
    @prev_user = @user.previous_user
    @fine = (params[:fine][:fee])
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id)
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id)
    render :update do |page|
      page.replace_html 'transport_fee_collection_details', :partial => 'employee_transport_fee_collection_details'
    end
  end

  def update_employee_fine_ajax2
    @collection_id = params[:date]
    @transport_fee = TransportFee.active.find_by_transport_fee_collection_id_and_receiver_id_and_receiver_type(params[:date], params[:emp_id], 'Employee') unless params[:emp_id].nil?
    @transport_fee_collection= TransportFeeCollection.find_by_id(@transport_fee.transport_fee_collection_id)
    @transaction = FinanceTransaction.find_by_id(@transport_fee.transaction_id)
    render :update do |page|
      unless params[:fees][:fine].to_f < 0
        @fine = params[:fees][:fine].to_f
        @employee = Employee.find(params[:emp_id])
        page.replace_html 'fees_details', :partial => 'fees_details'
        page.replace_html 'flash-msg', :text => ""
      else
        @employee = Employee.find(params[:emp_id])
        page.replace_html 'fees_details', :partial => 'fees_details'
        page.replace_html 'flash-msg', :text => "<p class='flash-msg'>Fine amount cannot be negative</p>"
      end
    end
  end

  def defaulters_update_fee_collection_dates
    @transport_fee_collection=TransportFeeCollection.all(
      :joins=>"INNER JOIN transport_fees on transport_fees.transport_fee_collection_id = transport_fee_collections.id",
      :conditions =>["transport_fees.groupable_id=? and transport_fee_collections.is_deleted=? and transport_fee_collections.due_date < '#{Date.today}' and transport_fees.balance > ? and transport_fees.is_active = ? and transport_fees.groupable_type = ? ",params[:batch_id],false,0.0,true,'Batch'],
      :group =>"transport_fee_collections.id")
    # @transport_fee_collection = TransportFeeCollection.find_all_by_batch_id(params[:batch_id])
    render :update do |page|
      page.replace_html 'fees_collection_dates', :partial => 'defaulters_transport_fee_collection_dates'
    end
  end

  def defaulters_transport_fee_collection_details
    @collection_id = params[:collection_id]
    @transport_fee =TransportFee.active.find(:all, :select => "distinct transport_fees.*", :joins => "INNER JOIN students on students.id=transport_fees.receiver_id and transport_fees.receiver_type='Student'", :conditions => "transport_fees.transport_fee_collection_id='#{params[:collection_id]}' and  students.batch_id='#{params[:batch_id]}' and transport_fees.balance > 0 and transport_fees.groupable_type = 'Batch'").sort_by { |s| s.receiver.full_name.downcase unless s.receiver.nil? }
    # @transport_fee = TransportFee.active.find_all_by_transport_fee_collection_id(params[:collection_id], :conditions => 'transaction_id IS NULL')
    # @transport_fee.reject! { |x| x.receiver.nil? }
    #   @transaction = FinanceTransaction.find_by_id(@user.transaction_id) unless @user.nil?
    #  @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id) unless @user.nil?
    render :update do |page|
      page.replace_html 'fee_submission', :partial => 'students_list'
    end
  end

  def fees_submission_defaulter_student
    @fine=params[:fees][:fine] if params[:fees].present?
    @user = params[:id]
    @student = Student.find(params[:student])
    @batch=@student.batch
    @date=TransportFeeCollection.find(params[:date])
    @transport_fee = TransportFee.active.find_by_receiver_id_and_transport_fee_collection_id(@student.id,@date.id, :conditions => "receiver_type = 'Student'")
    @tax_slab = @date.collection_tax_slabs.try(:last) if @transport_fee.tax_enabled?
    @transport_fee_collection = @transport_fee.transport_fee_collection
    flash.clear
    render :update do |page|
      page.replace_html "fee_submission", :partial => "fees_details"
    end
  end

  def update_defaulters_fine_ajax
    @collection_id = params[:fine][:transport_fee_cofind_all_by_transport_fee_collection_idllection]
    @transport_fee = TransportFee.active.find_all_by_transport_fee_collection_id(params[:fine][:transport_fee_collection])
    @user = TransportFee.active.find_by_transport_fee_collection_id_and_id(params[:fine][:transport_fee_collection], params[:fine][:_id]) unless params[:fine][:_id].nil?
    @user ||= @transport_fee.first
    @next_user = @user.next_user unless @user.nil?
    @prev_user = @user.previous_user unless @user.nil?
    @fine = (params[:fine][:fee])
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id)
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id)
    render :update do |page|
      page.replace_html 'defaulters_transport_fee_collection_details', :partial => 'defaulters_transport_fee_collection_details'
    end
  end

  def employee_defaulters_transport_fee_collection
    @transport_fee_collection =TransportFeeCollection.employee
  end

  def employee_defaulters_transport_fee_collection_details
    @collection_id = params[:collection_id]
    @transport_fee_collection= TransportFeeCollection.find_by_id(params[:collection_id])
    @transport_fee = TransportFee.active.find_all_by_transport_fee_collection_id(params[:collection_id], :conditions => "balance >0  AND receiver_type = 'Employee'")
    @transport_fee.reject! { |x| x.receiver.nil? }
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id) unless @user.nil?
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id) unless @user.nil?
    render :update do |page|
      page.replace_html 'fee_submission', :partial => 'students_list'
    end
  end

  def update_employee_defaulters_fine_ajax
    @collection_id = params[:fine][:transport_fee_collection]
    @transport_fee = TransportFee.active.find_all_by_transport_fee_collection_id(params[:fine][:transport_fee_collection])
    @user = TransportFee.active.find_by_transport_fee_collection_id_and_id(params[:fine][:transport_fee_collection], params[:fine][:_id]) unless params[:fine][:_id].nil?
    @user ||= @transport_fee_collection.transport_fees.first(:conditions => ["transaction_id is null"])
    @next_user = @user.next_default_user unless @user.nil?
    @prev_user = @user.previous_default_user unless @user.nil?
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id) unless @user.nil?
    @fine = (params[:fine][:fee])
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id)
    render :update do |page|
      page.replace_html 'defaulters_transport_fee_collection_details', :partial => 'employee_defaulters_transport_fee_collection_details'
    end
  end

  def show_date_filter
    month_date
    @target_action=params[:target_action]
    if request.xhr?
      render(:update) do|page|
        page.replace_html "date_filter", :partial=>"filter_dates"
      end
    end
  end

  def transport_fee_receipt_pdf
    @transaction = FinanceTransaction.find params[:id]
    @transport_fee = @transaction.finance
    @fee_collection = @transport_fee.transport_fee_collection
    @user = @transport_fee.receiver
    @bus_fare = @transaction.fine_included ? ((@transaction.amount.to_f) - (@transaction.fine_amount.to_f)) : @transaction.amount.to_f
    @currency = currency
    if FedenaPlugin.can_access_plugin?("fedena_pay")
      response = @transaction.try(:payment).try(:gateway_response)
      @online_transaction_id = response.nil? ? nil : response[:transaction_id]
      @online_transaction_id ||= response.nil? ? nil : response[:x_trans_id]
      @online_transaction_id ||= response.nil? ? nil : response[:transaction_reference]
    end
    render :pdf => 'transport_fee_receipt', :layout => 'pdf'
  end


  def delete_fee_collection_date

    @user_type=params[:user_type]
    fee_collection = TransportFeeCollection.find(params[:id],:include=>:transport_fees)
    @transport_fees = fee_collection.transport_fees.active
    transport_fees_count = fee_collection.transport_fees.active.all(:group=>"groupable_type,groupable_id").size
    is_paid=@user_type == 'student' ? fee_collection.has_paid_fees_in_batch?(params[:batch_id]) : fee_collection.has_paid_fees_by_employee?
    if @transport_fees.present? && !is_paid
      if transport_fees_count == 1
        event=fee_collection.event
        event.destroy
        fee_collection.destroy
      else
        if @user_type == 'student'
          user_ids = @transport_fees.find_all_by_groupable_id_and_groupable_type(params[:batch_id],'Batch').collect{|x| x.receiver.user_id unless x.receiver.nil?}
          TransportFee.destroy_all("groupable_type='Batch' and groupable_id='#{params[:batch_id]}' and transport_fee_collection_id=#{params[:id]}")
        else
          user_ids = @transport_fees.find_all_by_groupable_type('EmployeeDepartment').collect{|x| x.receiver.user_id unless x.receiver.nil?}
          TransportFee.destroy_all("groupable_type='EmployeeDepartment' and transport_fee_collection_id=#{params[:id]}")
        end
        user_ids.delete(nil) #remove nil value - when student get archived
        #Remove user events        
        UserEvent.destroy_all("user_id  in ( #{user_ids.join(',')} ) and event_id=#{fee_collection.event.id}")

      end
      flash[:notice]="#{t('flash4')}"
    else
      @error_text=true
      render :update do |page|
        flash[:error]=t('transport_fee.flash5')
        page.redirect_to :action => 'transport_fee_collection_view'
      end
    end

  end


  def update_user_ajax
    if params[:user_type] == 'employee'
      #@transport_fee_collection = TransportFeeCollection.find(:all, :conditions=>'batch_id IS NULL').paginate(:page => params[:page],:per_page => 30)
      @transport_fee_collection = TransportFeeCollection.paginate(:select => "distinct transport_fee_collections.*", :joins => [:transport_fees], :conditions => "transport_fees.receiver_type='Employee' and is_deleted=false and transport_fees.is_active=true", :per_page => 20, :page => params[:page])
      @user_type = 'employee'
      render :update do |page|
        page.replace_html 'fee_collection_list', :partial => 'fee_collection_list'
        page.replace_html 'batch_list', :text => ''
      end
    elsif params[:user_type] == 'student'
      @user_type = 'student'
      @batches = Batch.active
      render :update do |page|
        page.replace_html 'batch_list', :partial => 'students_batch_list'
        page.replace_html 'fee_collection_list', :text => ''
      end
    else
      render :update do |page|
        page.replace_html 'batch_list', :text => ''
        page.replace_html 'fee_collection_list', :text => ''
      end
    end
  end

  def update_batch_list_ajax
    @transport_fee_collection = TransportFeeCollection.paginate(:all, :select => "distinct transport_fee_collections.*",
      :joins => "INNER JOIN `transport_fees` ON transport_fees.transport_fee_collection_id = transport_fee_collections.id and transport_fees.groupable_type='Batch'",
      :conditions => "transport_fees.groupable_id= #{params[:batch_id]} and transport_fees.is_active=true",
      :per_page => 20, :page => params[:page])
    @user_type = 'student'
    render :update do |page|
      page.replace_html 'fee_collection_list', :partial => 'fee_collection_list'
    end
  end

  def transport_fees_report
    if validate_date
      @target_action="transport_fees_report"
      @start_date=@start_date.to_s
      @end_date=@end_date.to_s
      transport_id = FinanceTransactionCategory.find_by_name('Transport').id
      @grand_total=TransportFeeCollection.sum(:amount,
        :joins=>{:transport_fees=>:finance_transactions},
        :conditions=>"finance_transactions.transaction_date >= '#{@start_date}' and 
                               finance_transactions.transaction_date <= '#{@end_date}'")
      @transport_fee_collections=TransportFeeCollection.paginate(:per_page => 10,
        :page => params[:page],
        :joins=>{:transport_fees=>:finance_transactions},
        :group=>"transport_fee_collections.id",
        :conditions=>"finance_transactions.transaction_date >= '#{@start_date}' and 
                               finance_transactions.transaction_date <= '#{@end_date}'and 
                               finance_transactions.category_id ='#{transport_id}'",
        :select=>"SUM(finance_transactions.amount) AS amount,
                         IF(transport_fee_collections.tax_enabled,
                               IFNULL(SUM(finance_transactions.tax_amount),0),
                               '-') AS tax_amount,
                         transport_fee_collections.tax_enabled,
                         transport_fee_collections.id AS collection_id,
                         transport_fee_collections.name AS collection_name")
      #      @employee_collection = TransportFeeCollection.find(:all, :joins => "INNER JOIN transport_fees ON transport_fees.transport_fee_collection_id = transport_fee_collections.id INNER JOIN finance_transactions ON finance_transactions.finance_id = transport_fees.id and finance_transactions.finance_type = 'TransportFee' and finance_transactions.transaction_date >= '#{@start_date}' AND finance_transactions.transaction_date <= '#{@end_date}'and finance_transactions.category_id ='#{transport_id}' and transport_fees.receiver_type='Employee'", :group => "transport_fee_collections.id")
      #      @employees = Employee.find(:all
      #      @fees = FinanceTransaction.find(:all, :order => 'created_at desc', :conditions => ["transaction_date >= '#{@start_date}' and transaction_date <= '#{@last_date}'and category_id ='#{transport_id}'"])
      # @collection = TransportFeeCollection.find(:all, :joins => "INNER JOIN transport_fees ON transport_fees.transport_fee_collection_id = transport_fee_collections.id INNER JOIN finance_transactions ON finance_transactions.finance_id = transport_fees.id and finance_transactions.finance_type = 'TransportFee' and finance_transactions.transaction_date >= '#{@start_date}' AND finance_transactions.transaction_date <= '#{@end_date}'and finance_transactions.category_id ='#{transport_id}'", :group => "transport_fee_collections.id")
      #      @student_collection = TransportFeeCollection.find(:all, :joins => "INNER JOIN transport_fees ON transport_fees.transport_fee_collection_id = transport_fee_collections.id INNER JOIN finance_transactions ON finance_transactions.finance_id = transport_fees.id and finance_transactions.finance_type = 'TransportFee' and finance_transactions.transaction_date >= '#{@start_date}' AND finance_transactions.transaction_date <= '#{@end_date}'and finance_transactions.category_id ='#{transport_id}' and transport_fees.receiver_type='Student'", :group => "transport_fee_collections.id")
      #      @employee_collection = TransportFeeCollection.find(:all, :joins => "INNER JOIN transport_fees ON transport_fees.transport_fee_collection_id = transport_fee_collections.id INNER JOIN finance_transactions ON finance_transactions.finance_id = transport_fees.id and finance_transactions.finance_type = 'TransportFee' and finance_transactions.transaction_date >= '#{@start_date}' AND finance_transactions.transaction_date <= '#{@end_date}'and finance_transactions.category_id ='#{transport_id}' and transport_fees.receiver_type='Employee'", :group => "transport_fee_collections.id")
      #      @employees = Employee.find(:all)
      if request.xhr?
        render(:update) do|page|
          page.replace_html "fee_report_div", :partial=>"transport_fees_report"
        end
      end
    else
      render_date_error_partial
    end
  end

  def transport_fees_report_csv
    if date_format_check
      transport_id = FinanceTransactionCategory.find_by_name('Transport').id
      collections=TransportFeeCollection.all(
        :joins=>"INNER JOIN transport_fees tf 
                                  ON tf.transport_fee_collection_id=transport_fee_collections.id 
                       INNER JOIN `transport_fee_finance_transactions` 
                                   ON (`tf`.`id` = `transport_fee_finance_transactions`.`transport_fee_id`) 
                       INNER JOIN `finance_transactions` ft 
                                   ON (`ft`.`id` = `transport_fee_finance_transactions`.`finance_transaction_id`)",
        :group=>"transport_fee_collections.id",
        :conditions=>"ft.transaction_date >= '#{@start_date}' and
                                ft.transaction_date <= '#{@end_date}'and 
                                ft.category_id ='#{transport_id}'",
        :select=>"SUM(ft.amount) AS amount,
                         IF(transport_fee_collections.tax_enabled,
                             IFNULL(SUM(ft.tax_amount),0),
                             '-') AS tax_amount,
                         transport_fee_collections.tax_enabled,
                         transport_fee_collections.id AS collection_id,
                         transport_fee_collections.name AS collection_name")
      tax_enabled_present = collections.map(&:tax_enabled).uniq.include?(true)
      csv_string=FasterCSV.generate do |csv|
        csv << t('transport_fee_collections')
        csv << [t('start_date'),format_date(@start_date)]
        csv << [t('end_date'),format_date(@end_date)]
        csv << ""
        csv << (tax_enabled_present ? [t('collection'), t('tax_text'), t('amount')] : 
            [t('collection'), t('amount')])
        total=0
        collections.each do |collection|
          row =[]
          row << collection.collection_name
          if tax_enabled_present
            row << (collection.tax_amount != '-' ? precision_label(collection.tax_amount) : '-')
          end
          row << precision_label(collection.amount)
          total+=collection.amount.to_f
          csv << row
        end
        csv << ""
        csv << [t('net_income'),precision_label(total)]
      end
      filename = "#{t('transport_fee_collections')}-#{format_date(@start_date)} #{t('to')} #{format_date(@end_date)}.csv"
      send_data(csv_string, :type => 'text/csv; charset=utf-8; header=present', :filename => filename)
    end
  end



  #
  #  def transport_student_course_wise_collection_report
  #    if date_format_check
  #      @fee_collection = FinanceFeeCollection.find(params[:id])
  #      @target_action = "course_wise_collection_report"
  #      @course_ids=@fee_collection.batches.all(:include=>:course).group_by(&:course_id)
  #      if request.xhr?
  #        render(:update) do|page|
  #          page.replace_html "fee_report_div", :partial=>"fees_report"
  #        end
  #      end
  #    end
  #  end
  #
  #
  def category_wise_collection_report
    if validate_date
      @target_action = "category_wise_collection_report"
      @collection=TransportFeeCollection.find(params[:id])
      @grand_total=@collection.finance_transaction.sum(:amount,:conditions=>{:transaction_date=>@start_date..@end_date}).to_f
      @courses=TransportFee.all(:joins=>"INNER JOIN `transport_fee_finance_transactions` ON (`transport_fees`.`id` = `transport_fee_finance_transactions`.`transport_fee_id`) INNER JOIN `finance_transactions` ft ON (`ft`.`id` = `transport_fee_finance_transactions`.`finance_transaction_id`) INNER JOIN batches on batches.id=transport_fees.groupable_id",:group=>"transport_fees.groupable_id",:conditions=>"ft.transaction_date >= '#{@start_date}' AND ft.transaction_date <= '#{@end_date}' and transport_fees.transport_fee_collection_id=#{params[:id]} and transport_fees.groupable_type='Batch'",:select=>"sum(ft.amount) as amount,batches.name as batch_name,batches.course_id as course_id,transport_fees.groupable_id as batch_id").group_by(&:course_id)
      @departments=TransportFee.find(:all,:joins=>"INNER JOIN `transport_fee_finance_transactions` ON (`transport_fees`.`id` = `transport_fee_finance_transactions`.`transport_fee_id`) INNER JOIN `finance_transactions` ft ON (`ft`.`id` = `transport_fee_finance_transactions`.`finance_transaction_id`) inner join employee_departments on employee_departments.id=transport_fees.groupable_id",:conditions=>"ft.transaction_date >= '#{@start_date}' AND ft.transaction_date <= '#{@end_date}' and transport_fees.transport_fee_collection_id=#{params[:id]} and transport_fees.groupable_type='EmployeeDepartment'",:group=>"transport_fees.groupable_id",:select=>"employee_departments.name as dep_name,sum(ft.amount) as amount,transport_fees.groupable_id as dep_id")
      #      @s=TransportFee.all(:joins=>:finance_transaction,:conditions=>{:})
      if request.xhr?
        render(:update) do|page|
          page.replace_html "fee_report_div", :partial=>"department_wise_transport_collection_report_partial"
        end
      end
    else
      render_date_error_partial
    end
  end
  def transport_employee_department_wise_collection_report_csv
    if date_format_check
      @courses=TransportFee.all(:joins=>"INNER JOIN `transport_fee_finance_transactions` ON (`transport_fees`.`id` = `transport_fee_finance_transactions`.`transport_fee_id`) INNER JOIN `finance_transactions` ft ON (`ft`.`id` = `transport_fee_finance_transactions`.`finance_transaction_id`) INNER JOIN batches on batches.id=transport_fees.groupable_id",:group=>"transport_fees.groupable_id",:conditions=>"ft.transaction_date >= '#{@start_date}' AND ft.transaction_date <= '#{@end_date}' and transport_fees.transport_fee_collection_id=#{params[:id]} and transport_fees.groupable_type='Batch'",:select=>"sum(ft.amount) as amount,batches.name as batch_name,batches.course_id as course_id,transport_fees.groupable_id as batch_id").group_by(&:course_id)
      @departments=TransportFee.find(:all,:joins=>"INNER JOIN `transport_fee_finance_transactions` ON (`transport_fees`.`id` = `transport_fee_finance_transactions`.`transport_fee_id`) INNER JOIN `finance_transactions` ft ON (`ft`.`id` = `transport_fee_finance_transactions`.`finance_transaction_id`) inner join employee_departments on employee_departments.id=transport_fees.groupable_id",:conditions=>"ft.transaction_date >= '#{@start_date}' AND ft.transaction_date <= '#{@end_date}' and transport_fees.transport_fee_collection_id=#{params[:id]} and transport_fees.groupable_type='EmployeeDepartment'",:group=>"transport_fees.groupable_id",:select=>"employee_departments.name as dep_name,sum(ft.amount) as amount,transport_fees.groupable_id as dep_id")
      csv_string=FasterCSV.generate do |csv|
        csv << t('transport_fee_collections')
        csv << [t('start_date'),format_date(@start_date)]
        csv << [t('end_date'),format_date(@end_date)]
        net_total=0
        unless @departments.empty?
          total=0
          csv << ""
          csv << t('employees')
          csv << ["",t('department'),t('amount')]
          @departments.each do |dep|
            row=[]
            row << ["",""]
            row << dep.dep_name
            row << precision_label(dep.amount)
            total+=dep.amount.to_f
            csv << row
          end
          net_total+=total
          csv << [t('total'),"",precision_label(total)]
          csv << ""
        end
        unless @courses.empty?
          csv << t('students')
          csv << ["",t('batch'),t('amount')]
          total=0
          @courses.each do |course,batches|
            csv << Course.find(course).course_name
            batches.each do |b|
              row = []
              row << ["",""]
              row << b.batch_name
              row << precision_label(b.amount)
              total=b.amount.to_f
              csv << row
            end
            net_total+=total
            csv << [t('total'),"",precision_label(total)]
          end
        end
        csv << ""
        csv << [t('net_income'),"",precision_label(net_total)]
      end
      filename = "#{t('transport_fee_collection')}-#{format_date(@start_date)} #{t('to')} #{format_date(@end_date)}.csv"
      send_data(csv_string, :type => 'text/csv; charset=utf-8; header=present', :filename => filename)
    end
  end

  def batch_transport_fees_report
    if date_format_check
      @start_date=@start_date.to_s
      @end_date=@end_date.to_s
      @fee_collection = TransportFeeCollection.find(params[:id])
      @batch = @fee_collection.batch
      transport_id = FinanceTransactionCategory.find_by_name('Transport').id
      @transaction =[]
      @fee_collection.finance_transaction.all(:include => :transaction_ledger).each { |f| @transaction<<f if (f.transaction_date.to_s >= @start_date and f.transaction_date.to_s <= @end_date) }
    end
  end

  def employee_transport_fees_report
    if validate_date
      @start_date=@start_date.to_s
      @end_date=@end_date.to_s
      @fee_collection = TransportFeeCollection.find(params[:id])
      transport_id = FinanceTransactionCategory.find_by_name('Transport').id
      @target_action  ='employee_transport_fees_report'
      if params[:type]=='employee'
        @category=EmployeeDepartment.find(params[:dep_id])
        @grand_total=TransportFee.sum(:amount,:joins=>:finance_transactions,
          :conditions=>"transport_fees.groupable_id=#{params[:dep_id]} and 
                                transport_fees.groupable_type='EmployeeDepartment' and 
                                transport_fees.transport_fee_collection_id=#{params[:id]} and 
                                finance_transactions.transaction_date >='#{@start_date}' and 
                                finance_transactions.transaction_date <= '#{@end_date}'and 
                                finance_transactions.category_id=#{transport_id} and 
                                finance_transactions.finance_type='TransportFee'")
        @transactions = FinanceTransaction.paginate(:per_page => 10,:page => params[:page],
          :include => :transaction_ledger,
          :joins=>"INNER JOIN transport_fee_finance_transactions tfft on tfft.finance_transaction_id=finance_transactions.id 
                        INNER JOIN transport_fees tf on tf.id=tfft.transport_fee_id ",
          :conditions=>["tf.groupable_id=#{params[:dep_id]} and tf.groupable_type='EmployeeDepartment' and 
                                 tf.transport_fee_collection_id=#{params[:id]} and finance_transactions.transaction_date >='#{@start_date}' and 
                                 finance_transactions.transaction_date <= '#{@end_date}'and finance_transactions.category_id=#{transport_id} and 
                                 finance_transactions.finance_type='TransportFee'"])
        #        @fee_collection.finance_transaction.all(:joins=>"INNER JOIN employees on employees.id=finance_transactions.payee_id and finance_transactions.payee_type='Employee'",:conditions => "employees.employee_department_id=#{params[:dep_id]} and transaction_date >='#{@start_date}' and transaction_date <= '#{@end_date}'and category_id ='#{transport_id}'")
      else
        @category=Batch.find(params[:batch_id])
        @grand_total=TransportFee.sum(:amount,:joins=>:finance_transactions,:conditions=>"transport_fees.groupable_id=#{params[:batch_id]} and transport_fees.groupable_type='Batch' and transport_fees.transport_fee_collection_id=#{params[:id]} and finance_transactions.transaction_date >='#{@start_date}' and finance_transactions.transaction_date <= '#{@end_date}'and finance_transactions.category_id=#{transport_id} and finance_transactions.finance_type='TransportFee'")
        @transactions = FinanceTransaction.paginate(:per_page => 10,:page => params[:page],
          :include => :transaction_ledger,
          :joins=>"INNER JOIN transport_fee_finance_transactions tfft on tfft.finance_transaction_id=finance_transactions.id 
                       INNER JOIN transport_fees tf on tf.id=tfft.transport_fee_id ",
          :conditions=>["tf.groupable_id=#{params[:batch_id]} and tf.groupable_type='Batch' and 
                                 tf.transport_fee_collection_id=#{params[:id]} and 
                                 finance_transactions.transaction_date >='#{@start_date}' and 
                                 finance_transactions.transaction_date <= '#{@end_date}'and 
                                 finance_transactions.category_id=#{transport_id} and 
                                 finance_transactions.finance_type='TransportFee'"])
      end
      if request.xhr?
        render(:update) do|page|
          page.replace_html "fee_report_div", :partial=>"transport_fees_transactions"
        end
      end
    else
      render_date_error_partial
    end
  end
  def employee_transport_fees_report_csv
    if date_format_check
      @fee_collection = TransportFeeCollection.find(params[:id])
      transport_id = FinanceTransactionCategory.find_by_name('Transport').id
      if params[:type]=='employee'
        @category=EmployeeDepartment.find(params[:dep_id])
        @transactions = FinanceTransaction.all(:include => :transaction_ledger,
          :joins=>"INNER JOIN transport_fee_finance_transactions tfft on tfft.finance_transaction_id=finance_transactions.id 
                        INNER JOIN transport_fees tf on tf.id=tfft.transport_fee_id ",
          :conditions=>["tf.groupable_id=#{params[:dep_id]} and tf.groupable_type='EmployeeDepartment' and 
                                 tf.transport_fee_collection_id=#{params[:id]} and 
                                 finance_transactions.transaction_date >='#{@start_date}' and 
                                 finance_transactions.transaction_date <= '#{@end_date}'and 
                                 finance_transactions.category_id=#{transport_id} and 
                                 finance_transactions.finance_type='TransportFee'"])
        #        @fee_collection.finance_transaction.all(:joins=>"INNER JOIN employees on employees.id=finance_transactions.payee_id and finance_transactions.payee_type='Employee'",:conditions => "employees.employee_department_id=#{params[:dep_id]} and transaction_date >='#{@start_date}' and transaction_date <= '#{@end_date}'and category_id ='#{transport_id}'")
      else
        @category=Batch.find(params[:batch_id])
        @transactions = FinanceTransaction.all(:include => :transaction_ledger,
          :joins=>"INNER JOIN transport_fee_finance_transactions tfft on tfft.finance_transaction_id=finance_transactions.id 
                        INNER JOIN transport_fees tf on tf.id=tfft.transport_fee_id ",
          :conditions=>["tf.groupable_id=#{params[:batch_id]} and tf.groupable_type='Batch' and 
                                 tf.transport_fee_collection_id=#{params[:id]} and 
                                 finance_transactions.transaction_date >='#{@start_date}' and 
                                 finance_transactions.transaction_date <= '#{@end_date}'and 
                                 finance_transactions.category_id=#{transport_id} and 
                                 finance_transactions.finance_type='TransportFee'"])
      end
      csv_string=FasterCSV.generate do |csv|
        csv << t('transport_fee_collections')
        csv << [t('start_date'),format_date(@start_date)]
        csv << [t('end_date'),format_date(@end_date)]
        if params[:type]=="employee"
          csv << [t('department'),@category.name]
        else
          csv << [t('batch'),@category.name]
        end
        csv  << ""
        row =[]
        if params[:type]=='employee'
          row << [t('employee_name')]
        else
          row << [t('student_name')]
        end
        row << t('amount')
        row << t('receipt_no')
        row << t('date_text')
        row << t('payment_mode')
        row << t('payment_notes')
        csv << row
        total=0
        @transactions.each do |t|
          row=[]
          if params[:type]=='student'
            row << "#{t.transport_student_with_out_batch_name.full_name} (#{t.transport_student_with_out_batch_name.admission_no})"
          else
            row << "#{t.transport_employee.full_name}(#{t.transport_employee.employee_number})"
          end
          row << precision_label(t.amount)
          row << t.receipt_number
          row << format_date(t.created_at,:format=>:short_date)
          row << t.payment_mode
          row << t.payment_note
          total+=t.amount.to_f
          csv << row
        end
        csv << ""
        csv << [t('net_income'),precision_label(total)]
      end
      filename = "#{t('transport_fee_collection')}-#{@fee_collection.name}#{format_date(@start_date)} #{t('to')} #{format_date(@end_date)}.csv"
      send_data(csv_string, :type => 'text/csv; charset=utf-8; header=present', :filename => filename)
    end
  end
  def student_profile_fee_details
    if FedenaPlugin.can_access_plugin?("fedena_pay")
      if ((PaymentConfiguration.config_value("enabled_fees").present? and PaymentConfiguration.is_transport_fee_enabled?))
        @active_gateway = PaymentConfiguration.config_value("fedena_gateway")
        if @active_gateway.present?
          @custom_gateway = CustomGateway.find(@active_gateway)
        end
        @partial_payment_enabled = PaymentConfiguration.is_partial_payment_enabled?
      end
    end

    hostname = "#{request.protocol}#{request.host_with_port}"

    @student=Student.find(params[:id])
    @transport_fee= TransportFee.find_by_transport_fee_collection_id_and_receiver_id(params[:id2], params[:id])
    @fee_collection = TransportFeeCollection.find(params[:id2])
    @amount = @transport_fee.bus_fare
    @paid_fees = @transport_fee.finance_transactions(:include => :transaction_ledger)

    if @transport_fee.tax_enabled?
      @tax_collections = @transport_fee.tax_collections.all(:include => :tax_slab)                  
      @total_tax = @tax_collections.map(&:tax_amount).sum.to_f
      #      @tax_slabs = @tax_collections.map {|tax_col| tax_col.tax_slab }.uniq
      @tax_collections = @tax_collections.group_by {|x| x.tax_slab }
    end
    if params[:create_transaction].present?
      gateway_response = Hash.new
      if params[:return_hash].present?
        return_value = params[:return_hash]
        @decrypted_hash = PaymentConfiguration.payment_decryption(return_value)
      end
      if @custom_gateway.present?
        @custom_gateway.gateway_parameters[:response_parameters].each_pair do|k,v|
          unless k.to_s == "success_code"
            gateway_response[k.to_sym] = params[:return_hash].present? ? @decrypted_hash[v.to_sym] : params[v.to_sym]
          end
        end
      end
      @gateway_status = false
      if @custom_gateway.present?
        success_code = @custom_gateway.gateway_parameters[:response_parameters][:success_code]
        @gateway_status = true if gateway_response[:transaction_status] == success_code
      end
      amount_to_pay = precision_label(@transport_fee.balance.to_f).to_f
      amount_from_gateway = 0
      amount_from_gateway = gateway_response[:amount] if @custom_gateway.present?
      wrong_amount = false
      if amount_from_gateway.to_f != amount_to_pay
        wrong_amount = true unless PaymentConfiguration.is_partial_payment_enabled?
      end
      payment = SingleFeePayment.new(:payee => @student,:gateway_response => gateway_response, :status => @gateway_status, :amount => gateway_response[:amount].to_f, :gateway => @active_gateway)
      if payment.save
        finance_payment = FinancePayment.create(:payment_id=>payment.id,:fee_payment => @transport_fee,:fee_collection => @transport_fee.transport_fee_collection)
        unless wrong_amount
          tr_status = ""
          tr_ref = ""
          reason = ""
          if !@transport_fee.is_paid?
            amount_from_gateway = gateway_response[:amount]
            if amount_from_gateway.to_f > 0.0 and payment.status
              logger = Logger.new("#{RAILS_ROOT}/log/payment_processor_error.log")
              pay_status = false
              begin
                retries ||= 0
                pay_status = true
                transaction = FinanceTransaction.new
                transaction.title = @transport_fee.transport_fee_collection.name
                transaction.category_id = FinanceTransactionCategory.find_by_name('Transport').id
                transaction.finance = @transport_fee
                transaction.amount = amount_from_gateway.to_f
                transaction.transaction_date = FedenaTimeSet.current_time_to_local_time(Time.now).to_date
                transaction.payment_mode = "Online Payment"
                transaction.reference_no = gateway_response[:transaction_reference]
                transaction.payee = @transport_fee.receiver
                transaction.save
              rescue ActiveRecord::StatementInvalid => er
                # run code again  to  avoid duplications
                pay_status = false
                retry if (retries += 1) < 2
                logger.info "Error------#{er.message}----for --#{gateway_response}" unless (retries += 1) < 2
              rescue Exception => e
                pay_status = false
                logger.info "Errror-----#{e.message}------for---#{gateway_response}"
              end
            
            

              if pay_status
                #            @transport_fee.update_attributes(:transaction_id => transaction.id)
                #finance_payment = FinancePayment.create(:payment_id=>payment.id,:fee_payment => transaction.finance,:fee_collection => transaction.finance.transport_fee_collection)
                finance_payment.update_attributes(:finance_transaction_id => transaction.id)
                #            online_transaction_id = payment.gateway_response[:transaction_id]
                #            online_transaction_id ||= payment.gateway_response[:x_trans_id]
                #            online_transaction_id ||= payment.gateway_response[:payment_reference]
                online_transaction_id = payment.gateway_response[:transaction_reference]
              end
              if @gateway_status and pay_status
                status = SingleFeePayment.payment_status_mapping[:success]
                payment.update_attributes(:status_description => status)
                flash[:notice] = "#{t('payment_success')} <br>  #{t('payment_reference')} : #{online_transaction_id}"
                tr_status = "success"
                tr_ref = online_transaction_id
                reason = payment.gateway_response[:reason_code]
                if current_user.parent?
                  user = current_user
                else
                  user = @student.user
                end
                if @student.is_email_enabled && user.email.present?
                  begin
                    Delayed::Job.enqueue(OnlinePayment::PaymentMail.new(finance_payment.fee_collection.name, user.email, user.full_name, @custom_gateway.name, FedenaPrecision.set_and_modify_precision(payment.gateway_response[:amount]), online_transaction_id, payment.gateway_response, user.school_details, hostname))
                  rescue Exception => e
                    puts "Error------#{e.message}------#{e.backtrace.inspect}"
                    return
                  end
                end

              else
                status = SingleFeePayment.payment_status_mapping[:failed]
                payment.update_attributes(:status_description => status)
                flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{payment.gateway_response[:reason_code] || 'N/A'} <br> #{t('transaction_id')} : #{payment.gateway_response[:transaction_reference] || 'N/A'}"
                tr_status = "failure"
                tr_ref = payment.gateway_response[:transaction_reference]
                reason = payment.gateway_response[:reason_code]
              end

            else
              status = SingleFeePayment.payment_status_mapping[:failed]
              payment.update_attributes(:status_description => status)
              flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{payment.gateway_response[:reason_code] || 'N/A'} <br> #{t('transaction_id')} : #{payment.gateway_response[:transaction_reference] || 'N/A'}"
              tr_status = "failure"
              tr_ref = payment.gateway_response[:transaction_reference]
              reason = payment.gateway_response[:reason_code]
            end

          else
            flash[:notice] = "#{t('flash_payed')}"
            tr_status = "failure"
            tr_ref = payment.gateway_response[:transaction_reference]
            reason = "#{t('flash_payed')}"
          end
        else
          reason = payment.status == false ? payment.gateway_response[:reason_code] : "#{t('partial_payment_disabled')}"
          flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{reason}"
          tr_status = "failure"
          tr_ref = payment.gateway_response[:transaction_reference]
        end
      else
        flash[:notice] = "#{t('flash_payed')}"
        tr_status = "failure"
        tr_ref = payment.gateway_response[:transaction_reference]
        reason = "#{t('flash_payed')}"
      end
      if session[:mobile] == true
        redirect_to :controller=>"payment_settings", :action=>"complete_payment", :student_id=>@student.id, :fee_collection_id=>@fee_collection.id, :collection_type=>"transport", :transaction_status=>tr_status, :reason=>reason, :transaction_id=>tr_ref
      else
        redirect_to :controller => 'transport_fee', :action => 'student_profile_fee_details', :id => params[:id], :id2 => params[:id2]
      end
    else
      check_if_mobile_user
      if @ret==true
        @page_title=t('fees_text')
        render 'transport_fee/mobile_fee_details', :layout=>"mobile"
      else
        render 'transport_fee/student_profile_fee_details'
      end
    end
  end

  def delete_transport_transaction

    @financetransaction=FinanceTransaction.find(params[:transaction_id])
    @transport_fee= @financetransaction.finance
    if @transport_fee.tax_enabled
      @transport_fee_collection = @transport_fee.transport_fee_collection
      @tax_slab = @transport_fee_collection.collection_tax_slabs.try(:last)       
    end
    @date=@transport_fee.transport_fee_collection
    if FedenaPlugin.can_access_plugin?("fedena_pay")
      finance_payment = @financetransaction.finance_payment
      unless  finance_payment.nil?
        status = Payment.payment_status_mapping[:reverted]
        finance_payment.payment.update_attributes(:status_description => status)
      end
    end
    
    ActiveRecord::Base.transaction do      
      if @financetransaction        
        @financetransaction.cancel_reason = params[:reason]
        transaction_ledger = @financetransaction.transaction_ledger
        if transaction_ledger.transaction_mode == 'SINGLE'          
          transaction_ledger.mark_cancelled(params[:reason])
          @transport_fee.reload
          UserEvent.create(:event_id=>@transport_fee.transport_fee_collection.event.id,:user_id=>@transport_fee.receiver.user_id)  
        else
          if @financetransaction.destroy
            @transport_fee.reload
            UserEvent.create(:event_id=>@transport_fee.transport_fee_collection.event.id,:user_id=>@transport_fee.receiver.user_id)  
          else
            raise ActiveRecord::Rollback
          end
        end        
      end
    end
    
    if @transport_fee.receiver_type=="Employee"
      redirect_to :action => 'fees_submission_employee', :employee => params[:id], :date => params[:date]
    else
      @student=@transport_fee.receiver
      @transport_fee_collection=@transport_fee.transport_fee_collection
      render :update do |page|
        page.replace_html "flash-msg", :text => ""
        page.replace_html "fees_details", :partial=>"fees_details"
      end
      #      render :js=> "new Ajax.Request('/transport_fee/transport_fee_collection_details', {method: 'get',parameters: {student: #{@transport_fee.receiver_id},batch_id:#{@transport_fee.receiver.batch_id},date:#{@transport_fee.transport_fee_collection_id}}});"
    end
    #    render :update do |page|
    #          page.replace_html 'payments_details',:text => ''
    #        end
  end


  def receiver_wise_collection_new
    @transport_fee_collection = TransportFeeCollection.new    
    @tax_slabs = TaxSlab.all if @tax_enabled
    render :update do |page|
      page.replace_html "collection-details", :partial => 'receiver_wise_collection_new'
    end
    # @batches =Batch.find(:all,:select=>"distinct batches.*",:joins=>"INNER JOIN students on students.batch_id=batches.id INNER JOIN transports on students.id=transports.receiver_id and transports.receiver_type='Student'",:conditions=>"batches.is_active=1 and batches.is_deleted=0")
  end


  def collection_creation_and_assign
    @batches =Batch.find(:all, :select => "distinct batches.*", :joins => "INNER JOIN students on students.batch_id=batches.id INNER JOIN transports on students.id=transports.receiver_id and transports.receiver_type='Student'", :conditions => "batches.is_active=1 and batches.is_deleted=0")
    @dates=[]
  end

  def search_student
    students= Student.active.find(:all, :joins => [{:transport => :route}], :conditions => ["(admission_no LIKE ? OR first_name LIKE ?) and transports.bus_fare != 0", "%#{params[:query]}%", "%#{params[:query]}%"]).uniq
    employees= Employee.find(:all, :joins => [{:transport => :route}], :conditions => ["(employee_number LIKE ? OR first_name LIKE ?) and transports.bus_fare != 0", "%#{params[:query]}%", "%#{params[:query]}%"]).uniq
    students_suggestions=students.collect { |s| s.full_name.length+s.admission_no.length > 20 ? s.full_name[0..(18-s.admission_no.length)]+".. "+"(#{s.admission_no})"+" - "+s.transport.bus_fare.to_s+"(#{s.transport.route.destination})" : s.full_name+"(#{s.admission_no})"+" - "+s.transport.bus_fare.to_s+"(#{s.transport.route.destination})" }
    employees_suggestions=employees.collect { |e| e.full_name.length+e.employee_number.length > 20 ? e.full_name[0..(18-e.employee_number.length)]+".. "+"(#{e.employee_number})"+" - "+e.transport.bus_fare.to_s+"(#{e.transport.route.destination})" : e.full_name+"(#{e.employee_number})"+" - "+e.transport.bus_fare.to_s+"(#{e.transport.route.destination})" }
    suggestions=students_suggestions+employees_suggestions
    receivers=students.map { |st| "{'receiver': 'Student','id': #{st.id}, 'bus_fare' : #{st.transport.bus_fare},'user_id':#{st.user_id},'groupable_id':#{st.batch_id},'groupable_type':'Batch'}" }+employees.map { |emp| "{'receiver': 'Employee','id': #{emp.id}, 'bus_fare' : #{emp.transport.bus_fare},'user_id':#{emp.user_id},'groupable_id':#{emp.employee_department_id},'groupable_type':'EmployeeDepartment'}" }
    if receivers.present?
      render :json => {'query' => params["query"], 'suggestions' => suggestions, 'data' => receivers}
    else
      render :json => {'query' => params["query"], 'suggestions' => ["#{t('no_users')}"], 'data' => ["{'receiver': #{false}}"]}
    end
  end

  def receiver_wise_fee_collection_creation
    error=false    
    TransportFeeCollection.transaction do
      if params[:receiver].present?
        invoice_enabled = (Configuration.get_config_value('EnableInvoiceNumber').to_i == 1)
        recipients=[]
        @transport_fee_collection=TransportFeeCollection.new(params[:transport_fee_collection])
        @transport_fee_collection.invoice_enabled = invoice_enabled
        @tax_slab = TaxSlab.find_by_id(params[:transport_fee_collection][:tax_slab_id]) if @tax_enabled
        if @transport_fee_collection.save
          if @tax_slab.present?
            @transport_fee_collection.collectible_tax_slabs.build({:tax_slab_id => @tax_slab.id,
                :collectible_entity_id => @transport_fee_collection,
                :collectible_entity_type => 'TransportFeeCollection' }) 
            
            tax_multiplier = @tax_slab.rate.to_f * 0.01 
          end
          
          params[:receiver].each do |key, values|
            values.each do |k,v|
              v[:invoice_number_enabled] = @transport_fee_collection.invoice_enabled
              v[:tax_enabled] = @transport_fee_collection.tax_enabled
              v[:tax_amount] = v[:bus_fare].to_f * tax_multiplier if @tax_slab.present?
              @transport_fee=@transport_fee_collection.transport_fees.build(v)
              @transport_fee.tax_collections.build({:tax_amount => v[:tax_amount],
                  :slab_id => @tax_slab.id,
                  :taxable_entity_type => "TransportFeeCollection", 
                  :taxable_entity_id => @transport_fee_collection }) if @tax_slab.present?
            end
          end
          @transport_fee_collection.save
        
          event=Event.new(:title => "#{t('transport_fee_text')}", :description => "#{t('fee_name')}: #{@transport_fee_collection.name}", :start_date => @transport_fee_collection.due_date.to_s, :end_date => @transport_fee_collection.due_date.to_s, :is_due => true, :origin => @transport_fee_collection)
          params[:event].each{|i,j|  j.each { |k, v| event.user_events.build(v) }}
          error=true unless event.save
          params[:event].each{|i,j|  j.each { |k, v| recipients<<v[:user_id] }}
          send_reminder(@transport_fee_collection, recipients)
        else
          error=true
        end
      else
        error=true
        @transport_fee_collection=TransportFeeCollection.new(params[:transport_fee_collection])
      end

      if error

        render :update do |page|
          page.replace_html "collection-details", :partial => 'receiver_wise_collection_new'
        end
        raise ActiveRecord::Rollback

      else
        flash[:notice]="#{t('collection_date_has_been_created')}"
        render :update do |page|
          page.redirect_to :action => 'collection_creation_and_assign'
        end
      end

    end
  end

  def allocate_or_deallocate_fee_collection
    @recepient=params[:recepient]
    if @recepient=='employees'
      receiver_type='Employee'
      @employee_departments=EmployeeDepartment.active

    else
      receiver_type='Student'
      @batches = Batch.active
    end
    if request.post?
      error=false
      transport_entry = Transport.find_by_receiver_id_and_receiver_type(params[:fees_list][:receiver_id], receiver_type)
      TransportFee.transaction do
        params[:fees_list][:collection_ids].present? ? colln_ids= params[:fees_list][:collection_ids] : colln_ids = [0]
        receiver=receiver_type.constantize.find(params[:fees_list][:receiver_id])
        #find inactive bus fees
        if transport_entry.present? and transport_entry.auto_update_fare
          active_collections = TransportFee.find(:all, :select=> "transport_fee_collection_id", 
            :conditions=>["receiver_id='#{receiver.id}' and receiver_type='#{receiver_type}' and is_active=false"]).map{|tf| tf.transport_fee_collection_id.to_s}
          amount_ids = (params[:fees_list][:collection_ids]) & active_collections
          TransportFee.update_all({:bus_fare=>transport_entry.bus_fare.to_f, :balance=> transport_entry.bus_fare.to_f}, ["receiver_id='#{receiver.id}' and receiver_type='#{receiver_type}' and transport_fee_collection_id in (?)", amount_ids])
        end
        # bus_fare=receiver.transport.bus_fare
        ids_with_transactions = TransportFee.find(:all,:conditions=>['transaction_id is not null and receiver_id=? and receiver_type=? and is_active=?', receiver.id, receiver_type, true]).map{|tf| tf.transport_fee_collection_id.to_s}
        colln_ids = (colln_ids + ids_with_transactions).uniq
        colln_ids -= [0] if (colln_ids.present? and ids_with_transactions.present?)
        TransportFee.update_all("is_active=false", ["receiver_id='#{receiver.id}' and receiver_type='#{receiver_type}' and transport_fee_collection_id not in (?)", colln_ids])
        TransportFee.update_all(["is_active=true"], ["receiver_id='#{receiver.id}' and receiver_type='#{receiver_type}' and transport_fee_collection_id in (?)", colln_ids])
        receiver.send(:attributes=, params[:new_collection_ids])
        receiver.save(false)
        user_events=UserEvent.create(params[:user_events].values) if params[:user_events].present?

        if (error)
          render :update do |page|
            page.replace_html 'flash-div', :text => "<div id='error-box'><ul><li>#{t('fees_text')} #{t('transport_fee.allocation')} #{t('failed')}</li></ul></div>"
          end
          raise ActiveRecord::Rollback
        else
          render :update do |page|
            if receiver_type=='Student'
              page.replace_html 'flash-div', :text => "<p class='flash-msg'>#{t('fee_collections_are_updated_to_the_student_successfully')} </p>"
            else
              page.replace_html 'flash-div', :text => "<p class='flash-msg'>#{t('transport_fee.fee_collections_are_updated_to_the_employee_successfully')} </p>"
            end
          end
        end
      end
    end
  end

  def list_students_by_batch
    @receivers = Student.find(:all, :select => 'distinct students.*', :joins => [:transport_fees => :transport_fee_collection], :conditions => "students.batch_id='#{params[:batch_id]}' and transport_fees.transaction_id is null and transport_fee_collections.is_deleted=false", :order => 'first_name ASC')
    unless @receivers.blank?
      @receiver = @receivers.first
      # @bus_fare=@receiver.transport.bus_fare.to_f
      @fee_collection_dates=TransportFeeCollection.find(:all, :select => "distinct transport_fee_collections.*,transport_fees.is_active as assigned", :joins => "INNER JOIN `transport_fees` ON transport_fees.transport_fee_collection_id = transport_fee_collections.id", :conditions => "transport_fees.receiver_id='#{@receiver.id}' and transport_fees.receiver_type='Student' and transport_fee_collections.is_deleted=false and transport_fees.transaction_id is NULL")
      # TransportFeeCollection.find(:all,:select=>"transport_fee_collections.*,IF(transport_fees.receiver_id='#{@student.id}',true,false) as assigned",:joins=>"LEFT OUTER JOIN `transport_fees` ON transport_fees.transport_fee_collection_id = transport_fee_collections.id LEFT OUTER JOIN students on students.id=transport_fees.receiver_id and transport_fees.receiver_type='Student'",:conditions=>"students.batch_id='#{params[:batch_id]}'")
    end
    render :partial => 'receivers_list'
  end

  def list_employees_by_department
    @receivers = Employee.find(:all, :select => 'distinct employees.*', :joins => [:transport_fees => :transport_fee_collection], :conditions => "employees.employee_department_id='#{params[:department_id]}' and transport_fees.transaction_id is null and transport_fee_collections.is_deleted=false", :order => 'first_name ASC')
    unless @receivers.blank?
      @receiver = @receivers.first
      # @bus_fare=@receiver.transport.bus_fare.to_f
      @fee_collection_dates=TransportFeeCollection.find(:all, :select => "distinct transport_fee_collections.*,transport_fees.is_active as assigned", :joins => "INNER JOIN `transport_fees` ON transport_fees.transport_fee_collection_id = transport_fee_collections.id", :conditions => "transport_fees.receiver_id='#{@receiver.id}' and transport_fees.receiver_type='Employee' and transport_fee_collections.is_deleted=false and transport_fees.transaction_id is NULL")
      # TransportFeeCollection.find(:all,:select=>"transport_fee_collections.*,IF(transport_fees.receiver_id='#{@student.id}',true,false) as assigned",:joins=>"LEFT OUTER JOIN `transport_fees` ON transport_fees.transport_fee_collection_id = transport_fee_collections.id LEFT OUTER JOIN students on students.id=transport_fees.receiver_id and transport_fees.receiver_type='Student'",:conditions=>"students.batch_id='#{params[:batch_id]}'")
    end
    render :partial => 'receivers_list'
  end


  def list_fees_for_student
    @receiver = Student.find_by_id(params[:receiver])
    @fee_collection_dates=TransportFeeCollection.find(:all, :select => "distinct transport_fee_collections.*,transport_fees.is_active as assigned", :joins => "INNER JOIN `transport_fees` ON transport_fees.transport_fee_collection_id = transport_fee_collections.id", :conditions => "transport_fees.receiver_id='#{@receiver.id}' and transport_fees.receiver_type='Student' and transport_fee_collections.is_deleted=false and transport_fees.transaction_id is NULL")
    render :update do |page|
      page.replace_html 'fees_list', :partial => 'fees_list'
    end
  end

  def list_fees_for_employee
    @receiver = Employee.find_by_id(params[:receiver])
    @fee_collection_dates=TransportFeeCollection.find(:all, :select => "distinct transport_fee_collections.*,transport_fees.is_active as assigned", :joins => "INNER JOIN `transport_fees` ON transport_fees.transport_fee_collection_id = transport_fee_collections.id", :conditions => "transport_fees.receiver_id='#{@receiver.id}' and transport_fees.receiver_type='Employee' and transport_fee_collections.is_deleted=false and transport_fees.transaction_id is NULL")
    render :update do |page|
      page.replace_html 'fees_list', :partial => 'fees_list'
    end
  end

  def list_students_for_collection
    @collection=TransportFeeCollection.find(params[:date_id], :include => :transport_fees)
    student_ids=@collection.transport_fees.all(:conditions => {:receiver_type => 'Student'}).collect(&:receiver_id)
    student_ids=student_ids.join(',')

    students = Student.active.find(:all, :joins => [{:transport => :route}], :conditions => ["(admission_no LIKE ? OR first_name LIKE ?) and students.id not in (#{student_ids}) and batch_id='#{params[:batch_id]}' and transports.bus_fare != 0", "%#{params[:query]}%", "%#{params[:query]}%"]).uniq
    suggestions=students.collect { |s| s.full_name.length+s.admission_no.length > 20 ? s.full_name[0..(18-s.admission_no.length)]+".. "+"(#{s.admission_no})"+" - "+s.transport.bus_fare.to_s+"(#{s.transport.route.destination})" : s.full_name+"(#{s.admission_no})"+" - "+s.transport.bus_fare.to_s+"(#{s.transport.route.destination})" }
    receivers=students.map { |st| "{'receiver': 'Student','id': #{st.id}, 'bus_fare' : #{st.transport.bus_fare},'user_id':#{st.user_id},'groupable_id':#{st.batch_id},'groupable_type':'Batch'}" }
    if receivers.present?
      render :json => {'query' => params["query"], 'suggestions' => suggestions, 'data' => receivers}
    else
      render :json => {'query' => params["query"], 'suggestions' => ["#{t('no_users')}"], 'data' => ["{'receiver': #{false}}"]}
    end
  end

  def render_collection_assign_form
    @transport_fee_collection=TransportFeeCollection.find(params[:id])
    render :update do |page|
      page.replace_html 'students_selection', :partial => 'students_selection'
    end
  end


  def list_fee_collections_for_employees
    @receiver=Employee.find(params[:receiver_id])
    params[:collection_ids].present? ? colln_ids=params[:collection_ids] : colln_ids=[0]
    fee_collections= TransportFeeCollection.find(:all, :include => :event, :select => "distinct transport_fee_collections.*", :joins => :transport_fees, :conditions => ["(name LIKE ?) and transport_fee_collections.id not in (?) and  (transport_fee_collections.batch_id is null)", "%#{params[:query]}%", colln_ids])
    data_values=fee_collections.map { |f| "{'id':#{f.id}, 'event_id' : #{f.event.id}}" }
    render :json => {'query' => params["query"], 'suggestions' => fee_collections.collect { |fc| fc.name.length+fc.start_date.to_s.length > 20 ? fc.name[0..(18-fc.start_date.to_s.length)]+".. "+" - "+fc.start_date.to_s : fc.name+" - "+fc.start_date.to_s }, 'data' => data_values}
  end

  def choose_collection_and_assign
    @batches =Batch.find(:all, :select => "distinct batches.*", :joins => "INNER JOIN students on students.batch_id=batches.id INNER JOIN transports on students.id=transports.receiver_id and transports.receiver_type='Student'", :conditions => "batches.is_active=1 and batches.is_deleted=0")
    @dates=[]
    render :update do |page|
      page.replace_html "collection-details", :partial => 'choose_collection_and_assign'
    end
  end


  def update_fees_collections
    @dates=TransportFeeCollection.all(:select=>"DISTINCT transport_fee_collections.*",:joins=>"inner join transport_fees tf on tf.transport_fee_collection_id=transport_fee_collections.id INNER JOIN students on students.id=tf.receiver_id and tf.receiver_type='Student'",:conditions=>["students.batch_id=?",params[:batch_id]])
    render :update do |page|
      page.replace_html 'fees_collection_dates', :partial => 'fees_collection_dates'
    end
  end

  def collection_assign_students
    @transport_fee_collection=TransportFeeCollection.find(params[:transport_fee_collection][:id])
    event=@transport_fee_collection.event
    student_fees = params[:receiver][:Student].values
    if @transport_fee_collection.tax_enabled
      tax_slab = @transport_fee_collection.collection_tax_slabs.try(:last)
      tax_multiplier = tax_slab.rate.to_f * 0.01 if tax_slab.present?
      tax_collection_hsh = {
        :taxable_entity_id => @transport_fee_collection.id,
        :taxable_entity_type => 'TransportFeeCollection',
        :taxable_fee_type => 'TransportFee'
      } if tax_slab.present?
      student_fees.each_with_index do |student_fee, i|
        if tax_slab.present?
          student_fee["invoice_number_enabled"] = @transport_fee_collection.invoice_enabled
          tax = student_fee["bus_fare"].to_f * tax_multiplier 
          student_fee_tax = tax_collection_hsh.dup.merge({ :tax_amount => tax, :slab_id => tax_slab.id }) 
          student_fee["tax_amount"] = tax
          student_fee["tax_enabled"] = @transport_fee_collection.tax_enabled
          student_fee['tax_collections_attributes'] = [student_fee_tax]
          student_fees[i] = student_fee          
        end
      end
    end
    @transport_fee_collection.update_attributes(:transport_fees_attributes => student_fees)
    
    recipients=[]
    if (params[:event].present? and params[:event][:Student].present?)
      params[:event][:Student].each { |k, v| recipients<<v["user_id"] }
      send_reminder(@transport_fee_collection, recipients)
      user_events=event.user_events.create(params[:event][:Student].values) if event
    end
    flash[:notice]="#{t('collection_date_has_been_created')}"
    redirect_to :action => 'collection_creation_and_assign'
  end

  def show_employee_departments

    @employee_departments=EmployeeDepartment.active.sort_by { |e| e.name.downcase }

    render :update do |page|
      page.replace_html 'batch_or_department', :partial => 'departments'
    end

  end

  def show_student_batches
    @batches = Batch.active

    render :update do |page|
      page.replace_html 'batch_or_department', :partial => 'batches'
    end
  end

  def pay_batch_wise
    @batches=Batch.active.all(:include => :course)
    @transport_fee_collections = []
  end
  def transport_fee_search
  end

  private
  
  def load_tax_setting
    @tax_enabled = Configuration.get_config_value('EnableFinanceTax').to_i == 1
  end  

  def check_if_mobile_user
    user_agents=["android","ipod","opera mini","opera mobi","blackberry","palm","hiptop","avantgo","plucker", "xiino","blazer","elaine", "windows ce; ppc;", "windows ce; smartphone;","windows ce; iemobile", "up.browser","up.link","mmp","symbian","smartphone", "midp","wap","vodafone","o2","pocket","kindle", "mobile","pda","psp","treo"]
    @ret=false
    if FedenaPlugin.can_access_plugin?("fedena_mobile")
      user_agents.each do |ua|
        if request.env["HTTP_USER_AGENT"].downcase=~ /#{ua}/i
          @ret=true
          return
        end
      end
    end
  end
end
