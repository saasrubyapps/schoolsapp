class TransportFeeCollection < ActiveRecord::Base
  belongs_to :batch
  has_many :transport_fees, :dependent => :destroy
  has_many :transport_fee_collection_batches
  has_many :finance_transaction,:through=>:transport_fees
  validates_presence_of :name,:start_date,:due_date
  named_scope :employee, :select => "distinct transport_fee_collections.*", :joins=>[:transport_fees], :conditions => "transport_fee_collections.is_deleted=false and transport_fees.receiver_type='Employee' and transport_fee_collections.due_date < '#{Date.today}'"
  has_one :event, :as=>:origin,:dependent=>:destroy
#  has_many :taxable_slabs, :as => :taxable
#  has_many :tax_slabs, :through => :taxable_slabs
  
  #tax associations  
  has_many :collectible_tax_slabs, :as => :collection, :dependent => :destroy
  has_many :collection_tax_slabs, :through => :collectible_tax_slabs, :class_name => "TaxSlab"
  
  has_many :tax_collections, :as => :taxable_entity, :dependent => :destroy
  has_many :tax_fees, :through => :tax_collections, :source => :taxable_fee, :source_type => "TransportFee"  
  
  cattr_accessor :tax_slab_id
  accepts_nested_attributes_for :transport_fees, :allow_destroy => true

  def validate
#   errors.add :end_date_before_start_date if self.start_date.present? and self.end_date.present? and (self.start_date > self.end_date)
    errors.add :due_date_before_start_date if self.start_date.present? and self.due_date.present? and (self.start_date > self.due_date)
#   errors.add :due_date_before_end_date if self.due_date.present? and self.end_date.present? and (self.end_date > self.due_date)
  end
  def self.shorten_string(string, count)
    if string.length >= count
      shortened = string[0, count]
      splitted = shortened.split(/\s/)
      words = splitted.length
      splitted[0, words-1].join(" ") + ' ...'
    else
      string
    end
  end
  def check_status
    self.has_paid_fees?
  end
  
  def check_status_with_user_type(user_type,batch_id=nil)
    
    if user_type=='student'
      self.has_paid_fees_in_batch?(batch_id)
    else
      self.has_paid_fees_by_employee?
    end
    
  end
  
  def has_paid_fees_by_employee?
     self.transport_fees.all(:conditions => "transaction_id IS NOT NULL and groupable_type='EmployeeDepartment'").present?
  end
  
  def has_paid_fees?
    self.transport_fees.all(:conditions => 'transaction_id IS NOT NULL' ).present?
  end
  
  def has_paid_fees_in_batch?(batch_id)
     self.transport_fees.all(:conditions => "transaction_id IS NOT NULL and groupable_type='Batch' and groupable_id='#{batch_id}'").present?
  end
  
  def transaction_amount(start_date,end_date)
    trans =[]
      self.finance_transaction.each{|f| trans<<f if (f.transaction_date.to_s >= start_date and f.transaction_date.to_s <= end_date)}
    trans.map{|t|t.amount}.sum
  end

  def fee_table
    self.transport_fees.all(:conditions=>"transaction_id IS NULL")
  end
end
