
class Transport < ActiveRecord::Base
  belongs_to :user
  belongs_to :route
  belongs_to :vehicle
  belongs_to :receiver, :polymorphic=>true
 before_validation :delete_existing_records
  validates_presence_of :route_id,:vehicle_id,:bus_fare,:receiver_id
  validates_uniqueness_of :receiver_id,:scope=>:receiver_type,:message => t("this_student/employee_has_already_been_assigned_transport") 
  named_scope :all_transports, :select=>"transports.*,students.first_name,students.middle_name,students.last_name,students.admission_no,employees.first_name as emp_first_name ,employees.middle_name as emp_middle_name,employees.last_name as emp_last_name,employees.employee_number,routes.destination ,IF(transports.receiver_type='Student',students.first_name,employees.first_name) as receiver_name, r.destination as route_name, vehicles.vehicle_no as vehicle_number",:joins=>"LEFT OUTER JOIN `routes` ON `routes`.id = `transports`.route_id LEFT OUTER JOIN routes r on r.id=routes.main_route_id LEFT OUTER JOIN students on students.id=transports.receiver_id LEFT OUTER JOIN employees on employees.id=transports.receiver_id LEFT OUTER JOIN vehicles on vehicles.id=transports.vehicle_id"
  named_scope :sort_order, lambda{|s_order|
    { :order => s_order
    }
  }
  named_scope :route_and_receiver_type_wise, lambda{|route_ids, r_type|
    { :conditions => ["transports.route_id IN (?) and transports.receiver_type=?", route_ids, r_type]
    }
  }
  named_scope :receiver_type_wise, lambda{|r_type|
    { :conditions => ["transports.receiver_type=?", r_type]
    }
  }
  named_scope :route_wise, lambda{|route_ids|
    { :conditions => ["transports.route_id IN (?)", route_ids]
    }
  }
  HUMANIZED_ATTRIBUTES = {
    :route_id => "Destination",
    :main_route => "Route",
    :receiver_id=>""
  }

  before_save :verify_precision 
  attr_accessor :selected

  def verify_precision
    self.bus_fare = FedenaPrecision.set_and_modify_precision self.bus_fare
  end

  def self.human_attribute_name(attr)
    HUMANIZED_ATTRIBUTES[attr.to_sym] || super
  end

  def get_vehicles
    route = self.route
    if route.main_route.nil?
      return route.vehicles
    else
      return route.main_route.vehicles
    end
  end

  def self.single_vehicle_details(parameters)
    sort_order=parameters[:sort_order]|| nil
    vehicle_id=parameters[:vehicle_id]
    receivers=Transport.all(:select=>"transports.*,students.first_name,students.middle_name,students.last_name,students.admission_no,employees.first_name as emp_first_name ,employees.middle_name as emp_middle_name,employees.last_name as emp_last_name,employees.employee_number,routes.destination ,IF(transports.receiver_type='Student',students.first_name,employees.first_name) as receiver_name, r.destination as route_name",:joins=>"LEFT OUTER JOIN `routes` ON `routes`.id = `transports`.route_id LEFT OUTER JOIN routes r on r.id=routes.main_route_id LEFT OUTER JOIN students on students.id=transports.receiver_id LEFT OUTER JOIN employees on employees.id=transports.receiver_id",:conditions=>{:vehicle_id=>vehicle_id},:order=>sort_order)
    data=[]
    col_heads=["#{t('no_text')}","#{t('name')}","#{t('employee_no')} / #{t('admission_no')}","#{t('transport.passenger_type') }","#{t('destination')}","#{t('route') }","#{t('fare')}"]
    data << col_heads
    receivers.each_with_index do |s,i|
      col=[]
      col<< "#{i+1}"
      if s.receiver_type=="Student"
        col<< "#{s.first_name} #{s.middle_name} #{s.last_name}"
        col<< "#{s.admission_no}"
      else
        col<< "#{s.emp_first_name} #{s.emp_middle_name} #{s.emp_last_name}"
        col<< "#{s.employee_number}"
      end
      col<< "#{s.receiver_type}"
      col<< "#{s.destination}"
      col<< "#{s.route_name.nil? ? (s.destination):(s.route_name)}"
      col<< "#{s.bus_fare}"
      col=col.flatten
      data<< col
    end
    return data
  end
  
  def self.students_transport_report(parameters)
    sorting_order=parameters[:sort_order]|| nil
    course = parameters[:course]
    transport_search = parameters[:transport_search]
    if transport_search.nil?
      students = Student.student_transport_details.transport_sort_order(sorting_order)
    else
      if transport_search[:report_type]=="allotted"
        unless transport_search[:batch_ids].nil? and course[:course_id] == ""
          students = Student.student_transport_details.batch_wise_student_transport(transport_search[:batch_ids]).transport_sort_order(sorting_order)
        else
          students = Student.student_transport_details.all_student_transports.transport_sort_order(sorting_order)
        end
      else
        unless transport_search[:batch_ids].nil? and course[:course_id] == ""
          students = Student.student_transport_details.batch_wise_all_students(transport_search[:batch_ids]).transport_sort_order(sorting_order)
        else
          students = Student.student_transport_details.transport_sort_order(sorting_order)
        end
      end
    end
    data=[]
    col_heads=["#{t('no_text')}","#{t('name')}","#{t('admission_no')}","#{t('batch_name') }","#{t('vehicle_no')}","#{t('destination')}","#{t('route') }"]
    col_heads.insert(3, t('roll_no')) if Configuration.enabled_roll_number?
    data << col_heads
    students.each_with_index do |s,i|
      col=[]
      col<< "#{i+1}"
      col<< "#{s.first_name} #{s.middle_name} #{s.last_name}"
      col<< "#{s.admission_no}"
      col<< "#{s.roll_number}" if Configuration.enabled_roll_number?
      col<< "#{s.batch.full_name}"
      col<< "#{s.vehicle_name}"
      col<< "#{s.destination}"
      col<< "#{s.route_name.nil? ? (s.destination):(s.route_name)}"
      col=col.flatten
      data<< col
    end
    return data
  end
  def self.employees_transport_report(parameters)
    sorting_order = parameters[:sort_order]|| nil
    department = parameters[:department]
    transport_search = parameters[:transport_search]
    if transport_search.nil?
      employees = Employee.employee_transport_details.transport_sort_order(sorting_order)
    else
      if transport_search[:report_type]=="allotted"
        unless department[:department_id] == ""
          employees = Employee.employee_transport_details.department_wise_employee_transport(department[:department_id]).transport_sort_order(sorting_order)
        else
          employees = Employee.employee_transport_details.all_employee_transports.transport_sort_order(sorting_order)
        end
      else
        unless department[:department_id] == ""
          employees = Employee.employee_transport_details.department_wise_all_employees(department[:department_id]).transport_sort_order(sorting_order)
        else
          employees = Employee.employee_transport_details.transport_sort_order(sorting_order)
        end
      end
    end
    data=[]
    col_heads=["#{t('no_text')}","#{t('name')}","#{t('employee_number')}","#{t('employee_department') }","#{t('employee_position') }","#{t('vehicle_no')}","#{t('destination')}","#{t('route') }"]
    data << col_heads
    employees.each_with_index do |s,i|
      col=[]
      col<< "#{i+1}"
      col<< "#{s.first_name} #{s.middle_name} #{s.last_name}"
      col<< "#{s.employee_number}"
      col<< "#{s.department_name}"
      col<< "#{s.emp_position}"
      col<< "#{s.vehicle_name}"
      col<< "#{s.destination}"
      col<< "#{s.route_name.nil? ? (s.destination):(s.route_name)}"
      col=col.flatten
      data<< col
    end
    return data
  end
  
  def self.route_transport_report(parameters)
    sorting_order=parameters[:sort_order]|| nil
    main = parameters[:main]
    transport_search = parameters[:transport_search]
    if transport_search.nil?
      transports = Transport.all_transports.sort_order(sorting_order)
    else
      unless transport_search[:type]=="all"
        unless transport_search[:route_ids].nil? and main[:route_id] == ""
          transports = Transport.all_transports.route_and_receiver_type_wise(transport_search[:route_ids],transport_search[:type]).sort_order(sorting_order)
        else
          transports = Transport.all_transports.receiver_type_wise(transport_search[:type]).sort_order(sorting_order)
        end
      else
        unless transport_search[:route_ids].nil? and main[:route_id] == ""
          transports = Transport.all_transports.route_wise(transport_search[:route_ids]).sort_order(sorting_order)
        else
          transports = Transport.all_transports.sort_order(sorting_order)
        end
      end
    end
    data=[]
    col_heads=["#{t('no_text')}","#{t('name')}","#{t('employee_no')} / #{t('admission_no')}","#{t('transport.passenger_type') }","#{t('destination')}","#{t('route') }","#{t('vehicle_no')}"]
    data << col_heads
    transports.each_with_index do |s,i|
      col=[]
      col<< "#{i+1}"
      if s.receiver_type=="Student"
        col<< "#{s.first_name} #{s.middle_name} #{s.last_name}"
        col<< "#{s.admission_no}"
      else
        col<< "#{s.emp_first_name} #{s.emp_middle_name} #{s.emp_last_name}"
        col<< "#{s.employee_number}"
      end
      col<< "#{s.receiver_type}"
      col<< "#{s.destination}"
      col<< "#{s.route_name.nil? ? (s.destination):(s.route_name)}"
      col<< "#{s.vehicle_number}"
      col=col.flatten
      data<< col
    end
    return data
  end

   def delete_existing_records
    if  self.selected == "1"
      if existing=Transport.find_by_receiver_id_and_receiver_type(self.receiver_id,self.receiver_type)
      existing.destroy
    end
  end
   end
end
