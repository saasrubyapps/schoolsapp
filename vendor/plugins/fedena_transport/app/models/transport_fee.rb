
class TransportFee < ActiveRecord::Base


  belongs_to :transport_fee_collection
  belongs_to :groupable, :polymorphic => true
  delegate :name,:to=>:transport_fee_collection,:allow_nil=>true
  belongs_to :receiver, :polymorphic => true
  has_one :finance_transaction, :as => :finance
  has_many :transport_fee_finance_transactions
  has_many :finance_transactions ,:through=>:transport_fee_finance_transactions, :dependent => :destroy, :order => "finance_transactions.id DESC"
  has_many :finance_transactions_with_fine,:through=>:transport_fee_finance_transactions,:source=>:finance_transaction,:conditions=>"finance_transactions.fine_included =1"
  #tax associations
  has_many :tax_collections, :as => :taxable_fee, :dependent => :destroy
  has_many :tax_particulars, :through => :tax_collections, :source => :taxable_entity, :source_type => "TransportFeeCollection"
  
  has_many :tax_payments, :as => :taxed_fee
  has_many :taxed_particulars, :through => :tax_payments, :source => :taxed_entity, :source_type => "TransportFee"
  #invoice associations
  has_many :fee_invoices, :as => :fee
  cattr_accessor :invoice_number_enabled  
  after_create :add_invoice_number, :if => Proc.new { |fee| fee.invoice_number_enabled.present? }
  before_destroy :mark_invoice_number_deleted
  validates_numericality_of :balance,:greater_than_or_equal_to=>0
  validates_uniqueness_of :receiver_id,:scope=>[:transport_fee_collection_id,:receiver_type]
  
  accepts_nested_attributes_for :tax_collections, :allow_destroy => true

  before_save :verify_precision
  validates_uniqueness_of :receiver_id,:scope=>[:transport_fee_collection_id,:receiver_type]
  
  before_validation :set_balance,:if=> Proc.new{|hf| hf.balance.nil?}
  
  named_scope :active , :joins=>[:transport_fee_collection] ,:conditions=>"transport_fee_collections.is_deleted=false and transport_fees.is_active=true", :readonly => false
  named_scope :unpaid ,:conditions=>" balance > 0.0"


  def invoice_no
    fee_invoices.present? ? fee_invoices.try(:first).try(:invoice_number) : ""
  end
  
  def add_invoice_number
    fee_invoices.create
  end
  
  def mark_invoice_number_deleted    
    fee_invoice = fee_invoices.try(:last)
    fee_invoice.mark_deleted if fee_invoice.present?
  end
  
  def verify_precision
    self.bus_fare = FedenaPrecision.set_and_modify_precision self.bus_fare
  end

  def next_user
    next_st =  self.transport_fee_collection.transport_fees.first(:conditions => "id > #{self.id}", :order => "id ASC")
    next_st ||= self.transport_fee_collection.transport_fees.first(:order => "id ASC")
  end

  def previous_user
    prev_st = self.transport_fee_collection.transport_fees.first(:conditions => "id < #{self.id}", :order => "id DESC")
    prev_st ||= self.transport_fee_collection.transport_fees.first(:order => "id DESC")
    prev_st ||= self.first(:order => "id DESC")
  end
  def next_default_user
    next_st =  self.transport_fee_collection.transport_fees.first(:conditions => "id > #{self.id} and transaction_id is null", :order => "id ASC")
    next_st ||= self.transport_fee_collection.transport_fees.first( :conditions=>["transaction_id is null"] , :order => "id ASC")
  end
  
  def previous_default_user
    prev_st = self.transport_fee_collection.transport_fees.first(:conditions => "id < #{self.id} and transaction_id is null", :order => "id DESC")
    prev_st ||= self.transport_fee_collection.transport_fees.first( :conditions=>["transaction_id is null"],:order => "id DESC")
    prev_st ||= self.transport_fee_collection.transport_fees.first( :conditions=>["transaction_id is null"], :order => "id DESC")
  end

  def get_transport_fee_collection(start_date, end_date ,trans_id)
    transport_id = FinanceTransactionCategory.find_by_name('Transport').id
    FinanceTransaction.find(:all,:conditions=>"FIND_IN_SET(id,\"#{trans_id}\") and transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}' and category_id ='#{transport_id}' ")
  end

  def finance_transaction_id
    return self.transaction_id
  end

  def former_student
    return ArchivedStudent.find_by_former_id(self.receiver_id)
  end

  def former_employee
    return ArchivedEmployee.find_by_former_id(self.receiver_id)
  end

  def student_id
    return self.receiver_id if self.receiver_type == 'Student'
  end
  def payee_name
    if receiver.nil?
      if receiver_type=="Student"
        if former_student
          "#{former_student.full_name}(#{former_student.admission_no})"
        else
          "#{t('user_deleted')}"
        end
      else
        if former_employee
          "#{former_employee.full_name}(#{former_employee.employee_number})"
        else
          "#{t('user_deleted')}"
        end
      end
    else
      receiver.full_name
    end
  end

  def  is_paid?
    balance==0
  end

  def fine_amount
    finance_transactions_with_fine.sum(:fine_amount)
  end

  def has_fine
    fine_amount > 0
  end

  private

  def set_balance
    self.balance=self.bus_fare.to_f
    self.balance +=self.tax_amount.to_f if self.tax_enabled?
  end
end
