module FedenaTransport
  module FinanceTransactionTransportExtension

    def self.included(base)
      base.instance_eval do
        has_one :transport_fee_finance_transaction, :dependent => :destroy
        before_save :set_fine_for_transport, :if => Proc.new { |ft| ft.finance_type=='TransportFee' and ft.fine_included }
        after_create :update_transport_fee_data, :if => Proc.new { |ft| ft.finance_type=='TransportFee' }
        after_destroy :delete_transport_fee_data, :if => Proc.new { |ft| ft.finance_type=='TransportFee' }
        validate :check_amount_exceeds_balance_for_transport_fee , :if => Proc.new { |ft| ft.finance_type=='TransportFee' }
      end
    end

    def transport_student
      student = self.finance.receiver
      student ||= ArchivedStudent.find_by_former_id(self.finance.receiver_id)
      "#{student.full_name}- &#x200E;(#{student.batch.full_name})&#x200E;"
    end
    def transport_student_with_out_batch_name
      student = self.finance.receiver
      student ||= ArchivedStudent.find_by_former_id(self.finance.receiver_id)
      student
    end
    def transport_employee
      employee = self.finance.receiver
      employee ||= ArchivedEmployee.find_by_former_id(self.finance.receiver_id)
      employee
    end

    def get_payment_mode
      case self.payment_mode
      when "Online Payment"
        return 'transaction_id'
      when "Cheque"
        return 'cheque_no'
      when "DD"
        return 'dd_no'
      else
        return 'reference_no'
      end
    end

    def previous_fine_transactions_for_transport
      FinanceTransaction.find(:all,:conditions=>"fine_included=true and finance_type='TransportFee' and finance_id=#{self.finance_id} and id <= #{self.id}")
    end

    private

    def check_amount_exceeds_balance_for_transport_fee
      amount_paid = FedenaPrecision.set_and_modify_precision(self.amount.to_f-self.fine_amount.to_f).to_f
      amount_to_pay = FedenaPrecision.set_and_modify_precision(finance.balance.to_f).to_f
      if amount_paid > amount_to_pay
#      if (self.amount.to_f-self.fine_amount.to_f) > finance.balance.to_f
        self.errors.add_to_base(t('finance.flash19'))
      end
    end

    def set_fine_for_transport
      self.fine_amount=[self.amount, self.fine_amount].min
    end

    def update_transport_fee_data
      transport_fee=self.finance
      paying_fee = (self.amount.to_f-self.fine_amount.to_f)
      balance_amount = FedenaPrecision.set_and_modify_precision(transport_fee.balance.to_f).to_f - paying_fee      
      transport_fee.update_attributes(:balance => balance_amount)
      if transport_fee.tax_enabled?        
#        transport_fee=self.finance.reload
        transactions = FinanceTransaction.all(:conditions => 
            {:finance_id => transport_fee.id, :finance_type => "TransportFee" })
        total_paid = transactions.map(&:amount).sum.to_f
        total_fine_paid = transactions.map(&:fine_amount).sum.to_f
        #        total_tax_to_collect = transport_fee.tax_collections.map(&:tax_amount).sum.to_f
        total_tax_to_collect = transport_fee.tax_amount.to_f
        total_tax_paid = transport_fee.tax_payments.map(&:tax_amount).sum.to_f
        is_fee_paid = (total_paid - total_fine_paid - total_tax_paid).to_f >= transport_fee.bus_fare.to_f
        is_tax_paid = transport_fee.tax_amount.to_f <= total_tax_paid
        if is_fee_paid and !is_tax_paid
          balance_tax = (total_tax_to_collect - total_tax_paid).to_f     
          transaction_amount_left = (total_paid - transport_fee.bus_fare - total_fine_paid).to_f
          paying_for_tax = balance_tax - transaction_amount_left > 0 ? transaction_amount_left : balance_tax
          if paying_for_tax.to_f > 0
            # record tax payment
            transport_fee.tax_payments.create({
                :taxed_entity_type => "TransportFee",
                :taxed_entity_id => transport_fee.id,
                :tax_amount => paying_for_tax,
                :finance_transaction_id => self.id
              })
            # update tax amount in finance transaction record
            self.tax_amount = paying_for_tax 
            self.tax_included = true
            self.send(:update_without_callbacks)            
          end
        end
      end
      create_transport_fee_finance_transaction(:transport_fee_id => transport_fee.id)
    end

    def delete_transport_fee_data
      transport_fee=self.finance
      balance_amount=transport_fee.balance.to_f+(self.amount.to_f-self.fine_amount.to_f)
      transport_fee.update_attributes(:balance => balance_amount)
      transport_fee_finance_transaction.destroy
    end
  end
end