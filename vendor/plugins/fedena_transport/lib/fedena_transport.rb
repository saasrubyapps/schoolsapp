require 'dispatcher'
# FedenaTransport
require 'finance_transaction_transport_extension'
module FedenaTransport
  def self.attach_overrides
    Dispatcher.to_prepare :fedena_transport do
      ::Employee.instance_eval { include  EmployeeExtension }
      ::Batch.instance_eval { has_many :transport_fees, :as=>:groupable}
      ::EmployeeDepartment.instance_eval { has_many :transport_fees, :as=>:groupable}
      ::Batch.instance_eval { has_many :transports, :through=>:students }
      ::Batch.class_eval {
        def active_transports
          transports.find(:all,:include => :vehicle, :conditions => ["vehicles.status = ?","Active"])
        end
      }
      ::Student.instance_eval { include  StudentExtension }
      ::FinanceTransaction.instance_eval { include FinanceTransactionTransportExtension }      
      ::TaxSlab.instance_eval { include TaxSlabExtension }
      ::TaxPayment.instance_eval { include TaxPaymentExtension }
    end
  end


  def self.dependency_delete(student)
    student.transport.destroy if student.transport.present?
    student.transport_fees.destroy_all
  end

  def self.dependency_check(record,type)
    if type == "permanant"
      if record.class.to_s == "Student" or record.class.to_s == "Employee"
        return true if record.transport.present?
        return true if record.transport_fees.active.present?
      end
    end
    return false
  end
  def self.student_profile_fees_hook
    "transport_fee/student_profile_fees"
  end
  def self.student_profile_fees_by_batch_hook
    "transport_fee/student_profile_fees"
  end

  def self.mobile_student_profile_fees_hook
    "transport_fee/mobile_student_profile_fees"
  end

  module TaxSlabExtension
    def self.included(base)
      base.instance_eval do 
        #        has_many :transport_fee_collections, :through => :taxable_slabs, :source => :taxable, 
        #          :source_type => 'TransportFeeCollection'
        
        has_many :transport_fee_collections, :through => :collectible_tax_slabs, :source => :collection,
          :source_type => 'TransportFeeCollection'
      end
    end
  end
    
  module TaxPaymentExtension
    def self.included(base)
      base.class_eval do
        def self.transport_fee_tax_payments(start_date, end_date)
          TaxPayment.all(:conditions => ["transaction_date 
            BETWEEN '#{start_date}' AND '#{end_date}' AND finance_type = 'TransportFee'"], 
            :select => "DISTINCT tax_payments.id as tax_payment_id, 
                               tax_payments.tax_amount AS tax_amount, ts.name AS slab_name, 
                               ts.rate AS slab_rate, ts.id AS slab_id, tfc.id AS collection_id, 
                               tfc.name AS collection_name, fts.transaction_date as transaction_date",
            :joins => "INNER JOIN finance_transactions fts 
                                        ON fts.id = tax_payments.finance_transaction_id 
                            INNER JOIN transport_fees tf 
                                        ON tf.id = tax_payments.taxed_fee_id AND 
                                             tax_payments.taxed_fee_type = 'TransportFee' 
                            INNER JOIN transport_fee_collections tfc 
                                        ON tfc.id = tf.transport_fee_collection_id 
                            INNER JOIN collectible_tax_slabs cts 
                                        ON cts.collection_id = tfc.id AND 
                                              cts.collection_type = 'TransportFeeCollection'
                            INNER JOIN tax_slabs ts ON ts.id = cts.tax_slab_id")
        end
      end
    end
  end
  
  module EmployeeExtension
    def self.included(base)
      base.instance_eval do
        has_many :transport_fees, :as => 'receiver'
        has_one:transport, :as => 'receiver', :dependent => :destroy
        accepts_nested_attributes_for :transport_fees
        named_scope :employee_transport_details,:select => "employees.id,first_name,middle_name,last_name,employee_number,employee_departments.name as department_name,employee_positions.name as emp_position,vehicles.vehicle_no as vehicle_name,vehicles.id as v_id, routes.destination as destination, r.destination as route_name", :joins => "INNER JOIN `employee_departments` ON `employee_departments`.id = `employees`.employee_department_id INNER JOIN `employee_positions` ON `employee_positions`.id = `employees`.employee_position_id LEFT OUTER JOIN transports on transports.receiver_id=employees.id AND transports.receiver_type='Employee' LEFT OUTER JOIN vehicles on vehicles.id=transports.vehicle_id LEFT OUTER JOIN routes on routes.id=transports.route_id LEFT OUTER JOIN routes r on r.id=routes.main_route_id", :group => 'employees.id'
        named_scope :transport_sort_order, lambda{|s_order|
          { :order => s_order
          }
        }
        named_scope :department_wise_employee_transport, lambda{|department_ids|
          { :conditions => ['employee_departments.id=? and transports.receiver_id=employees.id and transports.receiver_type="Employee"',department_ids]
          }
        }
        named_scope :all_employee_transports, :conditions => ['transports.receiver_id=employees.id and transports.receiver_type="Employee"']
        named_scope :department_wise_all_employees, lambda{|department_ids|
          { :conditions => ['employee_departments.id=?',department_ids]
          }
        }
      end
    end
  end
  module StudentExtension
    def self.included(base)
      base.instance_eval do
        has_many :transport_fees, :as => 'receiver'
        has_one :transport, :as => 'receiver', :dependent => :destroy
        accepts_nested_attributes_for :transport_fees
        named_scope :batch_wise_student_transport, lambda{|batch_ids|
          { :conditions => ["batches.id IN (?) and transports.receiver_id=students.id and transports.receiver_type='Student'",batch_ids] 
          }
        }
        named_scope :all_student_transports, :conditions => ["transports.receiver_id=students.id and transports.receiver_type='Student'"]
        named_scope :batch_wise_all_students, lambda{|batch_ids|
          { :conditions => ["batches.id IN (?)",batch_ids] 
          }
        }
        named_scope :student_transport_details, :select => "batches.name as batch_name,students.id,first_name,middle_name,last_name,admission_no,roll_number,batches.id as batch_id,vehicles.vehicle_no as vehicle_name,vehicles.id as v_id, routes.destination as destination, routes.destination as destination, r.destination as route_name,courses.code as c_code", :joins => "INNER JOIN `batches` ON `batches`.id = `students`.batch_id INNER JOIN `courses` ON `courses`.id = `batches`.course_id LEFT OUTER JOIN transports on transports.receiver_id=students.id and transports.receiver_type='Student' LEFT OUTER JOIN vehicles on vehicles.id=transports.vehicle_id LEFT OUTER JOIN routes on routes.id=transports.route_id LEFT OUTER JOIN routes r on r.id=routes.main_route_id", :group => 'students.id'
        named_scope :transport_sort_order, lambda{|s_order|
          { :order => s_order
          }
        }
        DependencyHook.make_dependency_hook(:transport_batch_fee, :student, :warning_message=> :transport_fee_are_already_assigned )do
          self.batch_transport_fees_exist
        end
        DependencyHook.make_dependency_hook(:transport_batch_fee_value, :student ) do
          self.transport_fee_collections
        end
        DependencyHook.make_dependency_hook(:fedena_transport_dependency, :student, :warning_message => :transport_present ) do
          self.transport_dependencies
        end
      end
      #TODO implement logic
      def has_pending_transport_fees?
        pending_count=TransportFee.count(:all,
          :joins=>"INNER JOIN students on students.id = transport_fees.receiver_id",
          :conditions=>{
            :transaction_id=>nil,
            :students=>{:id=>self.id}
          }
        )
        pending_count>0
      end
    end

    def transport_fee_collections
      TransportFeeCollection.find(:all,:joins=>'INNER JOIN transport_fees ON transport_fee_collections.id = transport_fees.transport_fee_collection_id',:conditions=>"transport_fees.receiver_id = #{self.id} and transport_fee_collections.is_deleted = 0 and transport_fees.receiver_type='Student' and transport_fees.is_active=1")
    end
    
    def transport_fee_collections_with_dues
      TransportFeeCollection.find(:all,:joins=>'INNER JOIN transport_fees ON transport_fee_collections.id = transport_fees.transport_fee_collection_id',:conditions=>"transport_fees.receiver_id = #{self.id} and transport_fee_collections.is_deleted = 0 and transport_fees.receiver_type='Student' and transport_fees.is_active=1 and transport_fees.balance <> 0")
    end
    
    def transport_dependencies
      return false if self.transport.present? or self.transport_fees.active.present?
      return true
    end

    def transport_fee_balance(fee_collection_id)
      fee_collection= TransportFeeCollection.find(fee_collection_id)
      transportfee = self.transport_fee_transactions(fee_collection)
      return transportfee.balance
    end

    def transport_fee_transactions(fee_collection)
      TransportFee.find_by_transport_fee_collection_id_and_receiver_id(fee_collection.id,self.id)
    end

    def transport_fee_collections_exists
      transport_fee_collections.empty?
    end

    def batch_transport_fees_exist
      transport_fees.select{|t| t.try(:transport_fee_collection).try(:batch_id) == batch_id and !t.try(:transport_fee_collection).try(:is_deleted) }.empty?
    end
    def transport_fees_by_batch(batch_id)
      TransportFee.find_all_by_receiver_id_and_groupable_id_and_groupable_type(self.id,batch_id,'Batch',:order=>"transaction_id",:conditions=>{:is_active=>true})
    end
  end

end
#
