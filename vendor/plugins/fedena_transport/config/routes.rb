ActionController::Routing::Routes.draw do |map|
  
  # transport
  map.resources :vehicles,:collection=>{:add_additional_details => [:get,:post,:put],:edit_additional_details => [:get,:post,:put],:select_passenger=>[:get],:list_batches_by_course=>[:get],:list_students_by_batch=>[:get],:check_passenger=>[:get],:list_employees_by_department=>[:get],:set_fare_value=>[:get],:final_list_for_vehicle=>[:get],:sort_passengers=>[:get],:passengers_list=>[:get]},:member=>{:assign_passengers=>:get}
  map.resources :routes,:collection=>{:add_additional_details => [:get,:post,:put],:edit_additional_details => [:get,:post,:put], :index=>[:get, :post]}

  map.namespace(:api) do |api|
    api.resources :vehicles, :collection => {:vehicle_details => :get, :route_vehicles => :get,:student_vehicle  => :get,:employee_vehicle => :get}
    api.resources :routes,:member=> {:students_route => :get,:employees_route => :get}
    api.resources :transports, :collection => {:students => :get,:vehicle_members => :get}
  end

end
