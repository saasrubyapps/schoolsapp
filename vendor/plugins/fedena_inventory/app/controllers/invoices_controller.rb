class InvoicesController < ApplicationController
  before_filter :login_required
  filter_access_to :all, :except => [:edit, :update, :destroy]
  filter_access_to [:edit, :update, :destroy], :attribute_check=>true

  def index
    @selected_store = (Invoice.last.nil? ? nil : Invoice.last.store.id) || (Store.first.present? ? Store.first.id : "")
  end
  
  def new
    @invoice = Invoice.new
    @stores = Store.find(:all, :joins => :store_items, :conditions => ["store_items.sellable = ? AND stores.is_deleted = ? AND store_items.is_deleted = ?",1, false,false]).uniq
    @sales_user_details = @invoice.sales_user_details.build
    @sold_items = @invoice.sold_items.build
    @discounts = @invoice.discounts.build
    @additional_charges = @invoice.additional_charges.build
    @selected_store = params[:selected_store] || (Store.first.present? ? Store.first.id : nil)
    if @selected_store.present?
      @invoice_no = generate_invoice_no(params[:selected_store])
    else
      flash[:notice] = "No store present"
      redirect_to :action=>:index
    end
  end

  def find_invoice_prefix
    invoice_no = generate_invoice_no(params[:id])
    render :json => {'invoice_no' => invoice_no }
  end

  def edit
    @invoice = Invoice.find(params[:id])
    @stores = Store.find(:all, :joins => :store_items, :conditions => ["store_items.sellable = ? AND stores.is_deleted = ? AND store_items.is_deleted = ?",1, false,false]).uniq
    @discounts = @invoice.discounts.build if @invoice.discounts.empty?
    @additional_charges = @invoice.additional_charges.build if @invoice.additional_charges.empty?
    @username = @invoice.sales_user_details.first.user.username if @invoice.sales_user_details.first.user.present?
  end

  def update
    @invoice = Invoice.find(params[:id])
    if @invoice.update_attributes(params[:invoice])
      if @invoice.is_paid
        payee = @invoice.sales_user_details.first.user
        status=true
        begin
          retries ||= 0
          status = true
          transaction = FinanceTransaction.new
          transaction.title = @invoice.invoice_no
          transaction.category = FinanceTransactionCategory.find_by_name("SalesInventory")
          transaction.finance = @invoice
          transaction.payee = payee
          transaction.transaction_date = @invoice.date
          transaction.amount = params[:invoice][:grandtotal]
          transaction.save
        rescue ActiveRecord::StatementInvalid => er            
          status = false
          retry if (retries += 1) < 2
        rescue Exception => e
          status = false
        end
        unless status
          @invoice.update_attribute('is_paid', false)
        end
      end
      flash[:notice] = "#{t('invoice_update_successfuly')}. <a href ='http://#{request.host_with_port}/invoices/invoice_pdf/#{@invoice.id}' target='_blank'>Print</a>"
      redirect_to :action => "index"
    else
      @stores = Store.find(:all, :joins => :store_items, :conditions => ["store_items.sellable = ? AND stores.is_deleted = ? AND store_items.is_deleted = ?",1, false,false]).uniq
      @discounts = @invoice.discounts.build if @invoice.discounts.empty?
      @additional_charges = @invoice.additional_charges.build if @invoice.additional_charges.empty?
      render :action => "edit", :id => @invoice.id
    end
  end
  
  def create
    @invoice = Invoice.new(params[:invoice])
    @stores = Store.find(:all, :joins => :store_items, :conditions => ["store_items.sellable = ? AND stores.is_deleted = ? AND store_items.is_deleted = ?",1, false,false]).uniq
    @selected_store = params[:invoice][:store_id].to_i || (Store.first.present? ? Store.first.id : "")
    if @invoice.save
      if @invoice.is_paid
        payee = @invoice.sales_user_details.first.user
        status=true
        begin
          retries ||= 0
          status = true
          transaction = FinanceTransaction.new
          transaction.title = @invoice.invoice_no
          transaction.category = FinanceTransactionCategory.find_by_name("SalesInventory")
          transaction.finance = @invoice
          transaction.payee = payee
          transaction.transaction_date = @invoice.date
          transaction.amount = params[:invoice][:grandtotal]
          transaction.save
        rescue ActiveRecord::StatementInvalid => er            
          status = false
          retry if (retries += 1) < 2
        rescue Exception => e
          status = false
        end
        unless status
          @invoice.update_attribute('is_paid', false)
        end
      end
      flash[:notice] = "Invoice #{@invoice.invoice_no} created successfully. <a href ='http://#{request.host_with_port}/invoices/invoice_pdf/#{@invoice.id}' target='_blank'>Print</a>"
      redirect_to :action => "new", :selected_store => params[:invoice][:store_id]
    else
      @discounts = @invoice.discounts.build if @invoice.discounts.empty?
      @additional_charges = @invoice.additional_charges.build if @invoice.additional_charges.empty?
      render :action => 'new', :selected_store => params[:invoice][:store_id]
    end
  end

  def show
    @item=StoreItem.find(params[:item_id]) if params[:item_id].present?
    @invoice = Invoice.find(params[:id])
    @currency = Configuration.find_by_config_key("CurrencyType").config_value
    @start_date = params[:start_date]
    @end_date = params[:end_date]
  end
  
  def destroy
    @invoice = Invoice.find(params[:id])
    @invoice.destroy
    flash[:notice] = "#{t('invoice_deleted_successfully')}"
    respond_to do |format|
      format.html { redirect_to(invoices_url) }
      format.xml  { head :ok }
    end
  end

  def search_code
    item_code = StoreItem.find(:all, :conditions=>[" is_deleted = ? AND code LIKE ? AND store_id = ? AND sellable = ?", false,"%#{params[:query]}%","#{params[:store_id]}", 1])
    render :json=>{'query'=>params["query"],'suggestions'=>item_code.collect{|c| c.code},'data'=>item_code.collect(&:id)  }
  end

  def search_store_item
    unless params[:id].nil?
      store_item = StoreItem.find(params[:id])
      store_item_name = store_item.item_name
      unit_price = store_item.unit_price
      render :json=> {'item_name' => store_item_name, 'unit_price' => unit_price, 'code' => store_item.code}
    else
      store_item = StoreItem.find(:all, :conditions=>[" is_deleted = ? AND item_name LIKE ? AND store_id = ? AND sellable = ?",false, "%#{params[:query]}%","#{params[:store_id]}", 1])
      render :json=> {'query'=>params["query"],'suggestions'=>store_item.collect{|c| c.item_name},'data'=>store_item.collect(&:id)}

    end
    
  end

  def search_username
    user = User.active.find(:all, :conditions=>["username LIKE ?", "%#{params[:query]}%"])
    render :json=>{'query'=>params["query"],'suggestions'=>user.collect{|c| c.username},'data'=>user.collect(&:id)  }
  end

  
  
  def search_user_details
    user = User.find(params[:id])
    first_name = user.first_name
    address = ""
    if user.employee?
      emp = Employee.find_by_user_id(user.id)
      address += emp.home_address_line1 + "\n" unless emp.home_address_line1.nil?
      address += emp.home_address_line2 + "\n" unless emp.home_address_line2.nil?
      address += emp.home_city+ "\n" unless emp.home_city.nil?
      address += emp.home_state + "\n"unless emp.home_state.nil?
      #address += emp.home_country unless emp.home_country_id.nil?
      address += emp.home_pin_code + "\n"unless emp.home_pin_code.nil?
    elsif user.student?
      stud = Student.find_by_user_id(user.id)
      address += stud.address_line1 + "\n" unless stud.address_line1.nil?
      address += stud.address_line2 + "\n" unless stud.address_line2.nil?
      address += stud.city + "\n" unless stud.city.nil?
      address += stud.state + "\n" unless stud.state.nil?
      address += stud.pin_code + "\n" unless stud.pin_code.nil?
      #address += stud.country + "\n" unless stud.country_id.nil?
    end
    render :json => {'name' => first_name, 'address' => address, :user_id => user.id}
  end

  def update_invoice
    @currency = Configuration.find_by_config_key("CurrencyType").config_value
    @invoices = Invoice.paginate(:page => params[:page],:per_page => 10, :conditions => ["invoice_no LIKE ? AND store_id = ? ", "#{params[:query]}%", params[:id] ], :order => 'id desc')  
    render(:update) do|page|
      page.replace_html 'update_invoice', :partial=>'list_invoices'
    end
  end

  def invoice_pdf
    @invoice = Invoice.find(params[:id])
    @store_name = @invoice.store.name
    @user = @invoice.sales_user_details.first.user
    @currency = Configuration.find_by_config_key("CurrencyType").config_value
    if @invoice.is_paid
      transaction = @invoice.finance_transaction
      unless transaction.nil?
        @transaction_date = transaction.transaction_date
        @amount = transaction.amount
        @reciept_no = transaction.receipt_number
      end
    end
    render :pdf => 'invoice_pdf', :show_as_html => false
  end

  def find_item_name
    @store_item = StoreItem.find(params[:id])
    render :json => {:item_name => @store_item.item_name}
  end


  def show_date_filter
    month_date
    @target_action=params[:target_action]
    if request.xhr?
      render(:update) do|page|
        page.replace_html "date_filter", :partial=>"filter_dates"
      end
    end
  end

  def report
    if validate_date
      @target_action="report"
      inventory = FinanceTransactionCategory.find_by_name('SalesInventory').id
      @store_sales=Invoice.find(:all,:joins=>[:finance_transaction,:store],:group=>:store_id,:conditions=>"finance_transactions.category_id=#{inventory} and finance_transactions.transaction_date>='#{@start_date}' and finance_transactions.transaction_date <= '#{@end_date}' and finance_transactions.finance_type='Invoice'",:select=>"stores.name as store_name,sum(finance_transactions.amount) as amount,invoices.store_id as store_id")
      if request.xhr?
        render(:update) do|page|
          page.replace_html "fee_report_div", :partial=>"report"
        end
      end
    else
      render_date_error_partial
    end

  end
  def report_csv
    if date_format_check
      inventory = FinanceTransactionCategory.find_by_name('SalesInventory').id
      store_sales=Invoice.all(:joins=>[:finance_transaction,:store],:group=>:store_id,:conditions=>"finance_transactions.category_id=#{inventory} and finance_transactions.transaction_date>='#{@start_date}' and finance_transactions.transaction_date <= '#{@end_date}' and finance_transactions.finance_type='Invoice'",:select=>"stores.name as store_name,sum(finance_transactions.amount) as amount,invoices.store_id as store_id")
      csv_string=FasterCSV.generate do |csv|
        csv << t('inventory_transaction_report')
        csv << [t('start_date'),format_date(@start_date)]
        csv << [t('end_date'),format_date(@end_date)]
        csv << ""
        csv << [t('store'),t('amount')]
        total=0
        store_sales.each do |t|
          row=[]
          row << t.store_name
          row << precision_label(t.amount)
          total+=t.amount.to_f
          csv << row
        end
        csv << ""
        csv << [t('net_income'),precision_label(total)]
      end
      filename = "#{t('inventory_transaction_report')}-#{format_date(@start_date)} #{t('to')} #{format_date(@end_date)}.csv"
      send_data(csv_string, :type => 'text/csv; charset=utf-8; header=present', :filename => filename)
    end
  end
  
  def sold_items_report
    if validate_date
      @target_action = "sold_items_report"
      @grand_total = SoldItem.sum(:rate, :joins => :invoice,:conditions => "invoices.store_id=#{params[:id]} and  invoices.date BETWEEN '#{@start_date}' AND '#{@end_date}' and invoices.is_paid= true").to_f
      @store_items = StoreItem.paginate(
        :page => params[:page],:per_page=>10,
        :joins =>  {:sold_items=>:invoice },
        :conditions => "store_items.store_id=#{params[:id]} and  invoices.date BETWEEN '#{@start_date}' AND '#{@end_date}' and invoices.is_paid= true",
        :group => "sold_items.store_item_id",
        :select => "item_name,sold_items.store_item_id as item_id,sum(sold_items.rate) as amount,store_items.store_id as store_id"
      )
      if request.xhr?
        render(:update) do|page|
          page.replace_html "fee_report_div", :partial=>"sold_items_report_partial"
        end
      end
    else
      render_date_error_partial
    end
  end
  
  def sold_items_report_csv
    if date_format_check
      store_items = StoreItem.paginate(
        :page => params[:page],:per_page=>10,
        :joins =>  {:sold_items=>:invoice },
        :conditions => "store_items.store_id=#{params[:id]} and  invoices.date BETWEEN '#{@start_date}' AND '#{@end_date}' and invoices.is_paid= true",
        :group => "sold_items.store_item_id",
        :select => "item_name,sold_items.store_item_id as item_id,sum(sold_items.rate) as amount,store_items.store_id as store_id"
      )
      csv_string=FasterCSV.generate do |csv|
        csv << [t('inventory_transaction_report'),t('store_items')]
        csv << [t('start_date'),format_date(@start_date)]
        csv << [t('end_date'),format_date(@end_date)]
        csv << ""
        csv << [t('item'),t('amount')]
        total=0
        store_items.each do |t|
          row=[]
          row << t.item_name
          row << precision_label(t.amount)
          total+=t.amount.to_f
          csv << row
        end
        csv << ""
        csv << [t('net_income'),precision_label(total)]
      end
      filename = "#{t('inventory_transaction_report')}-#{format_date(@start_date)} #{t('to')} #{format_date(@end_date)}.csv"
      send_data(csv_string, :type => 'text/csv; charset=utf-8; header=present', :filename => filename)
    end
  end


  def sold_item_transactions
    if validate_date
      @target_action="sold_item_transactions"
      @item=StoreItem.find(params[:id])
      #      inventory = FinanceTransactionCategory.find_by_name('SalesInventory').id
      #      @grand_total=SoldItem.find(:all,:joins=>"INNER JOIN finance_transactions on finance_transactions.finance_id=sold_items.invoice_id",:conditions=>"sold_items.store_item_id=#{params[:id]} and finance_transactions.category_id =#{inventory} and finance_transactions.transaction_date >= '#{@start_date}' and finance_transactions.transaction_date <= '#{@end_date}' and  finance_transactions.finance_type='Invoice'",:select=>"sum(finance_transactions.amount) as amount").first.amount
      #     @grand_total=SoldItem.paginate(:page => params[:page],:per_page=>10,:joins=>"INNER JOIN finance_transactions on finance_transactions.finance_id=sold_items.invoice_id",:conditions=>"sold_items.store_item_id=#{params[:id]} and finance_transactions.category_id =#{inventory} and finance_transactions.transaction_date >= '#{@start_date}' and finance_transactions.transaction_date <= '#{@end_date}' and  finance_transactions.finance_type='Invoice'",:select=>"sum(finance_transactions.amount) as amount").first.amount
      @inventory_transactions = Invoice.paginate(:page => params[:page],:per_page=>10,
        :joins=>[{:sold_items=>:store_item},{:finance_transaction=>:transaction_ledger}],
        :conditions=>{:is_paid=>true,:date=>@start_date..@end_date,:sold_items=>{:store_item_id=>params[:id]}},
        :group=>"invoices.id",
        :select=>"invoices.invoice_no,sold_items.rate,finance_transactions.finance_type,date,
                         IF(finance_transaction_ledgers.transaction_mode = 'MULTIPLE', 
                         finance_transactions.receipt_no, finance_transaction_ledgers.receipt_no) receipt_no,
                         invoices.id as invoice_id"
      )       
      #      SoldItem.paginate(:page => params[:page],:per_page=>10,:joins=>"INNER JOIN finance_transactions on finance_transactions.finance_id=sold_items.invoice_id",:conditions=>"sold_items.store_item_id=#{params[:id]} and finance_transactions.category_id =#{inventory} and finance_transactions.transaction_date >= '#{@start_date}' and finance_transactions.transaction_date <= '#{@end_date}' and  finance_transactions.finance_type='Invoice'",:select=>"finance_transactions.*")
      if request.xhr?
        render(:update) do|page|
          page.replace_html "fee_report_div", :partial=>"sold_item_transactions_partial"
        end
      end
    else
      render_date_error_partial
    end
  end
  
  def sold_item_transactions_csv
    if date_format_check
      @item=StoreItem.find(params[:id])
      #      inventory = FinanceTransactionCategory.find_by_name('SalesInventory').id
      #      @inventory_transactions=SoldItem.all(:joins=>"INNER JOIN finance_transactions on finance_transactions.finance_id=sold_items.invoice_id",:conditions=>"sold_items.store_item_id=#{params[:id]} and finance_transactions.category_id =#{inventory} and finance_transactions.transaction_date >= '#{@start_date}' and finance_transactions.transaction_date <= '#{@end_date}' and  finance_transactions.finance_type='Invoice'",:select=>"finance_transactions.*")
      @inventory_transactions = Invoice.all(
        :joins=>[{:sold_items=>:store_item},{:finance_transaction => :transaction_ledger}],
        :conditions=>{:is_paid=>true,:date=>@start_date..@end_date,:sold_items=>{:store_item_id=>params[:id]}},
        :group=>"invoices.id",
        :select=>"invoices.invoice_no,sold_items.rate,finance_transactions.finance_type,date,
                        IF(finance_transaction_ledgers.transaction_mode = 'MULTIPLE', 
                         finance_transactions.receipt_no, finance_transaction_ledgers.receipt_no) receipt_no, 
                        invoices.id as invoice_id"
      ) 
      total=0
      csv_string=FasterCSV.generate do |csv|
        csv << [t('inventory_transaction_report'),t('store_items')]
        csv << [t('start_date'),format_date(@start_date)]
        csv << [t('end_date'),format_date(@end_date)]
        csv << ""
        csv << [t('description'),t('date_text'),t('receipt_no'),t('amount')]
        @inventory_transactions.each do |t|
          row=[]
          row << t.finance_type
          row << t.format_date(t.date,:format=>:long_date)
          row << t.receipt_no
          row << precision_label(t.rate)
          total += t.rate.to_f
          csv << row
        end
        csv << ""
        csv << [t('net_income'),"","",precision_label(total)]
      end
      filename = "#{t('inventory_transaction_report')}-#{@start_date} #{t('to')} #{@end_date}.csv"
      send_data(csv_string, :type => 'text/csv; charset=utf-8; header=present', :filename => filename)
    end 
  end
  private

  def generate_invoice_no(store_id)
    prefix = Store.find(store_id).invoice_prefix || "INV"
    last_invoice = Invoice.last(:conditions=> ["store_id = ? and invoice_no LIKE (?)",store_id,"#{prefix}%"])
    unless last_invoice.nil?
      invoice_suffix = last_invoice.invoice_no.scan(/\d+/).first
      invoice_suffix = invoice_suffix.next unless invoice_suffix.nil?
    end
    suffix = invoice_suffix || "001"
    return prefix + suffix
  end
end




