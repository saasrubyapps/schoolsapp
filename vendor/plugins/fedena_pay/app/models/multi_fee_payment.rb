class MultiFeePayment < Payment
  #has many payments for single multi payment
  has_many :finance_payments,:foreign_key=>"payment_id"
  has_one :finance_transactions,:through=> :finance_payments

  #to make finance transaction and payment entry



  def self.create_multi_fees_transactions(params,hostname,identification_token=nil) 
    if params[:return_hash].present?
      @decrypted_hash = PaymentConfiguration.payment_decryption(params[:return_hash])
    end
    if identification_token.present?
      gateway_response = params
      gateway_status = true
      payment_request = PaymentRequest.find_by_identification_token(identification_token)
      @custom_gateway = PaymentConfiguration.active_gateway
    else
      gateway_response = params[:return_hash].present? ? custom_gateway_hash(@decrypted_hash) : custom_gateway_hash(params)
      success_code = custom_gateway.gateway_parameters[:response_parameters][:success_code]
      gateway_status = gateway_response[:transaction_status] == success_code ? true : false
      payment_request = PaymentRequest.find_by_identification_token(params[:identification_token])
    end
    multi_fee_payment = self.new(:payee => payment_request.payee,:gateway_response => gateway_response,:status => gateway_status, :amount => gateway_response[:amount].to_f, :gateway => @active_gateway)
    if multi_fee_payment.save
  		amount_from_gateway = @custom_gateway.present? ?  gateway_response[:amount].to_i : 0 
  		transactions = []
			FinanceTransaction.send_sms=false
      logger = Logger.new("#{RAILS_ROOT}/log/payment_processor_error.log")        
        multi_fees = build_multi_fees_transaction(multi_fee_payment)
  		payment_request.transactions.each do |key,transaction_params|
        begin
          retries ||= 0
          finance_transaction = FinanceTransaction.new(transaction_params.merge(
              {:transaction_ledger_id => multi_fees.id, :transaction_type => multi_fees.transaction_type, 
                :transaction_mode => multi_fees.transaction_mode}))
          finance_transaction.payment_mode = "Online Payment"
          finance_transaction.reference_no = gateway_response[:transaction_reference]
          finance_transaction.transaction_date = Date.today_with_timezone.to_date
          finance_payment = multi_fee_payment.finance_payments.create(:fee_payment => finance_transaction.finance,:fee_collection => finance_transaction.get_collection)
          if amount_from_gateway.to_f > 0.0 and gateway_status and finance_transaction.amount.to_f > 0.0
            finance_transaction.save
            finance_payment.update_attribute("finance_transaction_id",finance_transaction.id)
            transactions << finance_transaction
          end
          # assigning to multifee finance_transaction join table
        rescue ActiveRecord::StatementInvalid => er
          # run code again  to  avoid duplications
          finance_payment.destroy
          retry if (retries += 1) < 4
          logger.info "Error------#{er.message}----for --#{transaction_params}" unless (retries += 1) < 2
        rescue Exception => e
          logger.info "Errror-----#{e.message}------for---#{transaction_params}"
        end
			end
			FinanceTransaction.send_sms=true
			unless transactions.empty?
				@status = MultiFeePayment.payment_status_mapping[:success]
#				multi_fees = build_multi_fees_transaction(multi_fee_payment)
#				multi_fees.finance_transactions = transactions
#				multi_fee_payment.update_attribute("multi_fees_transaction_id",multi_fees.id)
				multi_fees.send_sms
			else
				@status = MultiFeePayment.payment_status_mapping[:failed]
			end

			multi_fee_payment.update_attributes(:status_description => @status)

      user = multi_fee_payment.payee.user
      if multi_fee_payment.payee.is_email_enabled && user.email.present? && gateway_status
        begin
          Delayed::Job.enqueue(OnlinePayment::PaymentMail.new(t('multi_fees'),user.email,user.full_name,@custom_gateway.name,FedenaPrecision.set_and_modify_precision(multi_fee_payment.amount),gateway_response[:transaction_reference],multi_fee_payment.gateway_response,user.school_details,hostname))
        rescue Exception => e
          puts "Error------#{e.message}------#{e.backtrace.inspect}"
          return
        end
      end
    end
    multi_fee_payment
	end


  private

	def self.build_multi_fees_transaction(multi_fee_payment)
#		MultiFeesTransaction.create(
		FinanceTransactionLedger.create(
			:amount=>multi_fee_payment.amount,
			:payment_mode => "Online Payment",
			:transaction_date =>  Date.today_with_timezone.to_date,
			:payee_id => multi_fee_payment.payee_id,
			:payee_type => 'Student',
            :transaction_type => 'MULTIPLE',
            :category_is_income => true
    )
	end
end