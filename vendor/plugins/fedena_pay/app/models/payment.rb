class Payment < ActiveRecord::Base

  attr_accessor :payment_type

  #validation
  validate  :validate_uniqueness_transaction_referense

  #relationships
  belongs_to :payee, :polymorphic => true
  has_many :finance_payments

  serialize :gateway_response
      
      
  def validate_uniqueness_transaction_referense
    errors.add_to_base("It is already done") unless unique_gateway_responses.empty?
  end
  
  def before_create
    if payment_type == "Application"
      if Payment.find_by_payee_id_and_payee_type_and_status(payee_id,'Applicant',1).present?
        false
      else
        true
      end
    end
  end

  def payee_name
    if payee.nil?
      if payee_type == 'Student'
        ArchivedStudent.find_by_former_id(payee_id).try(:full_name) || "NA"
      elsif payee_type == 'Guardian'
        ArchivedGuardian.find_by_former_id(payee_id).try(:full_name) || "NA"
      elsif payee_type == 'Applicant'
        "NA"
      end
    else
      payee.full_name
    end
  end

  def payee_user
    if payee.nil?
      if payee_type == 'Student'
        ArchivedStudent.find_by_former_id(payee_id).try(:admission_no) || "NA"
      elsif payee_type == 'Guardian'
        ArchivedGuardian.find_by_former_id(payee_id).try(:user).try(:username) || "NA"
      elsif payee_type == 'Applicant'
        "NA"
      end
    else
      payee_type == 'Applicant' ? payee.try(:reg_no) : payee.try(:user).try(:username)
    end
  end

  def self.payment_status_mapping
    {
      :success => 1,
      :reverted => 2,
      :failed => 3
    }
  end

  #def to use by child class  STI

  # def self.create_finance_transaction(transaction_attributes) #to create finance_transaction
  #   finance_transaction = FinanceTransaction.create!(transaction_attributes)
  # end

  def self.custom_gateway #fetch custom_gateway
    #@custom_gateway ||= CustomGateway.find(active_gateway)
    @custom_gateway = CustomGateway.find(active_gateway)
  end
  def self.active_gateway #to set active gateway
    #@active_gateway ||= PaymentConfiguration.active_gateway
    @active_gateway = PaymentConfiguration.active_gateway
  end

  def self.custom_gateway_hash(params) #is to map gateway parameters to custom parameters
    custom_gateway.custom_gateway_response(params)
  end

  
  private
  
  def unique_gateway_responses
    Payment.all.select do |payment| 
      payment.gateway_response.to_a == self.gateway_response.to_a
    end
  end
  
end
