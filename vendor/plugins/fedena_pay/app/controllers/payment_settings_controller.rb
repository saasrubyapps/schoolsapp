class PaymentSettingsController < ApplicationController
  require 'will_paginate/array'
  before_filter :login_required, :except=>[:initialize_payment]
  filter_access_to [:index,:transactions,:settings,:show_gateway_fields,:show_transaction_details,:return_to_fedena_pages]

  def index
    
  end

  def transactions
    start_date = params[:start_date].try(:to_date) || FedenaTimeSet.current_time_to_local_time(Time.now).to_date
    end_date = params[:end_date].try(:to_date) || FedenaTimeSet.current_time_to_local_time(Time.now).to_date
    @online_payments = Payment.all(:conditions=>["date(created_at) >= ? and date(created_at) <= ?",start_date,end_date],:order=>"id desc").paginate(:page => params[:page],:per_page => 10)
  end
  
  def settings
    @active_gateway = PaymentConfiguration.config_value("fedena_gateway")
    @gateways = CustomGateway.available_gateways
    #    if @active_gateway == "Webpay" && Country.find(Configuration.find_by_config_key('DefaultCountry').config_value).name != "Nigeria"
    #      @active_gateway = "Paypal"
    #      config = PaymentConfiguration.find_by_config_key('fedena_gateway')
    #      config.update_attributes(:config_value => "")
    #    end
    #    if @active_gateway == "Paypal"
    #      @gateway_fields = FedenaPay::PAYPAL_CONFIG_KEYS
    #    elsif @active_gateway == "Authorize.net"
    #      @gateway_fields = FedenaPay::AUTHORIZENET_CONFIG_KEYS
    #    elsif @active_gateway == "Webpay"
    #      @gateway_fields = FedenaPay::WEBPAY_CONFIG_KEYS
    #    else
    #      @gateway_fields = []
    #    end
    @enabled_fees = PaymentConfiguration.find_by_config_key("enabled_fees").try(:config_value)
    @enable_online_payment = PaymentConfiguration.find_by_config_key("enabled_online_payment").try(:config_value) || "false"
    @enable_partial_payment = PaymentConfiguration.find_by_config_key("enabled_partial_payment").try(:config_value) || "false"
    
    @enabled_fees ||= Array.new
    if request.post?
      payment_settings = Hash.new
      payment_settings = params[:payment_settings] if params[:payment_settings].present?
      enabled_op = params[:payment_settings][:enabled_online_payment] == "true" ? true : false
      if payment_settings.present? and enabled_op
        payment_settings.each_pair do |key,value|
          configuration = PaymentConfiguration.find_or_initialize_by_config_key(key)
          if configuration.update_attributes(:config_value => value)
            flash[:notice] = "Payment setting has been saved successfully."
          else
            flash[:notice] = "#{configuration.errors.full_messages.join("\n")}"
          end
        end
      else
        if payment_settings.present?
          configuration = PaymentConfiguration.find_or_initialize_by_config_key('enabled_online_payment')
          if configuration.update_attributes(:config_value => params[:payment_settings][:enabled_online_payment])
            flash[:notice] = "Payment setting has been saved successfully."
          else
            flash[:notice] = "#{configuration.errors.full_messages.join("\n")}"
          end
        else
          flash[:notice] = "Payment setting has been saved successfully."
        end
      end
      if enabled_op
        configuration = PaymentConfiguration.find_or_initialize_by_config_key("enabled_fees")
        configuration.update_attributes(:config_value => Array.new)
      end unless payment_settings.keys.include? "enabled_fees"
      redirect_to settings_online_payments_path
    end
  end

  def show_gateway_fields
    unless params[:gateway] == "custom"
      @active_gateway = params[:gateway]
      if @active_gateway == "Paypal"
        @gateway_fields = FedenaPay::PAYPAL_CONFIG_KEYS
      elsif @active_gateway == "Authorize.net"
        @gateway_fields = FedenaPay::AUTHORIZENET_CONFIG_KEYS
      elsif @active_gateway == "Webpay"
        @gateway_fields = FedenaPay::WEBPAY_CONFIG_KEYS
      end
    else
      @active_gateway = PaymentConfiguration.config_value("fedena_gateway")
      @gateways = CustomGateway.available_gateways
    end
    render :update do |page|
      if @gateway_fields.present?
        page.replace_html 'gateway_fields',:partial => "gateway_fields"
      else
        if @gateways.present?
          page.replace_html 'gateway_fields',:partial => "custom_gateways"
        else
          page.replace_html 'gateway_fields',:text => ""
        end
      end
    end
  end

  def initialize_payment
    if (FedenaPlugin.can_access_plugin?("fedena_pay") and PaymentConfiguration.config_value("enabled_fees").present? and PaymentConfiguration.op_enabled?)
      active_gateway = PaymentConfiguration.config_value("fedena_gateway")
      @custom_gateway = CustomGateway.find(active_gateway.to_i)
      if request.post?
        @payment_params = params[:online_payment]
      else
        @payment_params = request.query_parameters
      end
      amount_matched = true
      red_url = @payment_params[@custom_gateway.gateway_parameters[:variable_fields][:redirect_url]]
      if red_url.include?("/applicants/registration_return/")
        applicant = Applicant.find(red_url[/applicants\/registration_return\/(.*?)\?/,1])
        sent_amount = @payment_params[@custom_gateway.gateway_parameters[:variable_fields][:amount]]
        amount_matched = false unless sent_amount.to_f == applicant.amount.to_f
      end
      if amount_matched == true
        if PaymentConfiguration.is_encrypted==true
          @encrypted_hash = PaymentConfiguration.payment_encryption(@payment_params,"single")
        end
        render :layout => false
      else
        flash[:notice] = "Online payment could not be processed. Reason : Amount mismatch."
        redirect_to :controller => "user",:action => "dashboard"
      end
    else
      flash[:notice] = t('online_payment_is_currently_disabled')
      redirect_to :controller => "user",:action => "dashboard"
    end    
  end
  
  def complete_payment
    render :layout => false
  end

  def show_transaction_details
    @payment = Payment.find(params[:id])
    @gateway_response = @payment.gateway_response
    respond_to do |format|
      format.html { }
      format.js { render :action => 'show_transaction_details' }
    end
  end

  def return_to_fedena_pages
    @active_gateway = PaymentConfiguration.config_value("fedena_gateway")
    if @active_gateway == "Paypal"
      return_url = OnlinePayment.return_url + {:tx => "#{params[:tx]}",:st => "#{params[:st]}",:amt => "#{params[:amt]}"}.to_param
    else
      return_url = URI.parse(OnlinePayment.return_url)
    end
    redirect_to return_url
    OnlinePayment.return_url = nil
  end

end
