class StudentFeesController < ApplicationController
  include StudentFeesHelper
  skip_before_filter :verify_authenticity_token 

  filter_access_to [:all_fees],:attribute_check=>true, :load_method => lambda { Student.find(params[:id])}

  before_filter :set_student
  before_filter :set_precision

  before_filter :login_required, :except=>[:initialize_all_fees_payment]

  def all_fees
		@current_batch= params[:batch_id].present? ? Batch.find(params[:batch_id]) : @student.batch
		@all_batches= (@student.previous_batches+[@student.batch]).uniq
    @available_gateways = CustomGateway.available_gateways
    active_gateway
   	@finance_fees = payable_fees
    @is_tax_present = @finance_fees.map(&:tax_enabled).include?(true)
    @tax_config = Configuration.get_multiple_configs_as_hash(['FinanceTaxIdentificationLabel',
        'FinanceTaxIdentificationNumber']) if @is_tax_present
    @disabled_fee_ids = FinanceFee.find_all_by_id(@finance_fees.map(&:id), 
      :joins => "INNER JOIN finance_transactions fts 
                         ON fts.trans_type = 'particular_wise' AND 
                            fts.finance_type = 'FinanceFee' AND 
                            fts.finance_id = finance_fees.id" ).map(&:id)    
   	@paid_fees = paid_fees
    @partial_payment_enabled = PaymentConfiguration.is_partial_payment_enabled?
  end

  def initialize_all_fees_payment
    if (FedenaPlugin.can_access_plugin?("fedena_pay") and PaymentConfiguration.config_value("enabled_fees").present? and PaymentConfiguration.op_enabled?)
      @student_payment = build_paymentrequest
      total_amount =  precision_label(params[:transaction_extra][:total_amount].to_f).to_f
      paid_total = 0
      params[:transactions].each do |key,transaction|
        paid_total = paid_total + transaction[:amount].to_f
      end
      paid_total = precision_label(paid_total).to_f
      unless PaymentConfiguration.is_partial_payment_enabled? or (paid_total - total_amount) == 0.0
        flash[:notice] = t('partial_payment_disabled')
        redirect_to :back
      else
        if @student_payment.save
          @custom_gateway = CustomGateway.find(active_gateway)
          if PaymentConfiguration.is_encrypted==true
            hash_for_user_payment = user_payment_hash
            @encrypted_hash = PaymentConfiguration.payment_encryption(hash_for_user_payment,"all")
          end
          render :layout => false
        else
          redirect_to :back
        end
      end
    else
      flash[:notice] = t('online_payment_is_currently_disabled')
      redirect_to :controller => "user",:action => "dashboard"
    end
  end
  

  def procees_pay_all_fees
   	if params[:create_transaction].present?
      hostname = "#{request.protocol}#{request.host_with_port}"
   		multi_fees_transactions = MultiFeePayment.create_multi_fees_transactions(params,hostname)
   		if multi_fees_transactions.status
   			flash[:notice] = "#{t('payment_success')} <br>  #{t('payment_reference')} : #{multi_fees_transactions.gateway_response[:transaction_reference]}"
   		else
        flash[:notice] = "#{t('payment_failed')} <br>  #{t('reason')} : #{multi_fees_transactions.gateway_response[:reason_code]}"
      end
   	else
   		flash[:notice] = t('payment_failed')
   	end
   	redirect_to all_fees_student_fee_path(@student)
  end


  private

  def paid_fees
   	query_object.get_paid_fees
  end

  def payable_fees
   	query_object.fetch_all_fees
  end

  def query_object
    set_current_batch
   	object ||= FinanceQuery.new(@student,@current_batch,PaymentConfiguration.get_assigned_fees)
  end

  def build_paymentrequest
   	student_payment = PaymentRequest.new(
   		:user_id => @student.try(:user_id),
   		:transaction_parameters => wrapp_parameter
    )
  end
  def set_student
   	@student ||= Student.find(params[:id])
  end

  def wrapp_parameter
   	{:multi_fees_transaction => params[:multi_fees_transaction].merge({:transactions=>params[:transactions]})}
  end

	def active_gateway #ToDo code duplication
		@active_gateway = PaymentConfiguration.active_gateway
  end

  def set_current_batch
    @student.batch=@current_batch
  end
end
