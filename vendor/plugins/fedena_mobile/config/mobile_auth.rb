authorization do
  role :student do
    has_permission_on [:timetable],
      :to => [:student_mobile_view,:update_student_mobile_view]
    has_permission_on [:student],
      :to => [:mobile_fee,:mobile_fee_details,:mob_fee_details]
    has_permission_on [:attendance_reports],
      :to => [:student_attendance_view,:update_student_mobile_view] do
      if_attribute :user_id => is {user.id}
    end
#    has_permission_on [:reminder],
#      :to=>[:mobile_index,:mobile_view]
  end
  role :parent do
    has_permission_on [:timetable],
      :to => [:student_mobile_view,:update_student_mobile_view]
    has_permission_on [:student],
      :to => [:mobile_fee,:mobile_fee_details,:mob_fee_details]
    has_permission_on [:attendance_reports],
      :to => [:student_attendance_view,:update_student_mobile_view] do
      if_attribute :user_id => is {user.parent_record.user_id}
    end
#    has_permission_on [:reminder],
#      :to=>[:mobile_index,:mobile_view]
  end
  role :employee do
    has_permission_on [:employee_attendance],
      :to => [
      :mobile_leave,:apply_mobile_leave
    ] do
      if_attribute :id => is {user.id}
    end
    has_permission_on [:timetable],
      :to => [:employee_mobile_view,:update_employee_mobile_view] do
      if_attribute :user_id => is {user.id}
    end
    has_permission_on [:attendances],
      :to=>[:mobile_attendance, :mobile_leave,:load_class_hours] do
      if_attribute :is_allowed_to_mark_attendance? => is {true}
    end
#    has_permission_on [:reminder],
#      :to=>[:mobile_index,:mobile_view]
  end
  role :student_attendance_register do
    has_permission_on [:attendances],
      :to=>[:mobile_attendance,:mobile_leave,:load_class_hours]
  end
  role :admin do
    has_permission_on [:attendances],
      :to=>[:mobile_attendance, :mobile_leave,:load_class_hours]
#    has_permission_on [:reminder],
#      :to=>[:mobile_index,:mobile_view]
  end
end