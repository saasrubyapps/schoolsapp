module FedenaMobile
  def self.attach_overrides
    Dispatcher.to_prepare :fedena_mobile do
      ApplicationController.instance_eval { include FedenaMobile::MobileApplication }
      UserController.instance_eval { include FedenaMobile::MobileUser }
      ReminderController.instance_eval { include FedenaMobile::MobileReminder }
      ReminderHelper.instance_eval { include MobileReminderHelper }
      CalendarController.instance_eval { include FedenaMobile::MobileCalendar }
      TimetableController.instance_eval { include FedenaMobile::MobileTimetable }
      AttendanceReportsController.instance_eval { include FedenaMobile::MobileAttendanceReports }
      EmployeeAttendanceController.instance_eval { include FedenaMobile::MobileEmployeeAttendance }
      AttendancesController.instance_eval { include FedenaMobile::MobileAttendances }
      StudentController.instance_eval { include FedenaMobile::MobileStudent }
    end
  end
end
module MobileReminderHelper
  def self.included(base)
      base.class_eval do
        def reminder_mobile_link(reminder)
          link=""
          if reminder.read?
            link+="<label class='name read'>"
          else
            link+="<label class='subject-unread'>"
          end
          link+= reminder.subject
          link+="</label>"
          link+="<label class='subject read'>"
          link+= reminder.user.try(:first_name)
          link+=", "
          link+= format_date(FedenaTimeSet.current_time_to_local_time(reminder.created_at))
          link+="</label>"
          link+="<span class='attachment_icon_img'> </span>" if reminder.has_attachment?
          return link
        end
      end
    end
end
