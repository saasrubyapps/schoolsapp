module FedenaMobile
  module MobileStudent

    def self.included(base)
      base.instance_eval do
        before_filter :is_mobile_user?
        skip_before_filter :verify_authenticity_token, :only=>[:mobile_fee_details]
        before_filter :mob_fee_details, :only=>[:fee_details]
      end
    end

    def mobile_fee
      @student=Student.find(params[:id])
      @dates = FinanceFeeCollection.find(:all,:joins=>"INNER JOIN fee_collection_batches on fee_collection_batches.finance_fee_collection_id=finance_fee_collections.id INNER JOIN finance_fees on finance_fees.fee_collection_id=finance_fee_collections.id",:conditions=>"finance_fees.student_id='#{@student.id}'  and finance_fee_collections.is_deleted=#{false} and ((finance_fees.is_paid=false and finance_fees.batch_id<>#{@student.batch_id}) or (finance_fees.batch_id=#{@student.batch_id}) )").uniq
      @hostel_dates = []
      @transport_dates = []
      if FedenaPlugin.can_access_plugin?("fedena_hostel")
        @hostel_dates = @student.hostel_fee_collections
      end
      if FedenaPlugin.can_access_plugin?("fedena_transport")
        @transport_dates = @student.transport_fee_collections
      end
      if (@dates.empty? and @hostel_dates.empty? and @transport_dates.empty?)
        flash.now[:notice] = "#{t('no_fee_to_pay')}"
      else
        if FedenaPlugin.can_access_plugin?("fedena_pay")
          active_gateway = PaymentConfiguration.config_value("fedena_gateway")
          if active_gateway.present? and PaymentConfiguration.config_value('enabled_fees').present? and PaymentConfiguration.op_enabled? and (PaymentConfiguration.config_value('enabled_fees') & ["Student Fee","Hostel Fee","Transport Fee"]).present?
            flash.now[:notice] = "#{t('can_pay_fee')}"
          end
        end
      end
      @page_title=t('fees_text')
      render :layout =>"mobile"
    end

    def mob_fee_details
      unless session[:mobile]==true
        select_layout
        if @ret==true
          redirect_to :controller => 'student',:action=>:mobile_fee_details,:id=>params[:id],:id2=>params[:id2]
          return
        end
      end
    end

    def mobile_fee_details
      @page_title=t('fees_text')
      @student = Student.find(params[:id])
      hostname = "#{request.protocol}#{request.host_with_port}"
      current_school_name = Configuration.find_by_config_key('InstitutionName').try(:config_value)
      @date = FinanceFeeCollection.find(params[:id2])
      @financefee = @student.finance_fee_by_date @date
      @particular_wise_paid = @financefee.finance_transactions.map(&:trans_type).include?("particular_wise")
      flash.now[:notice]="#{t('particular_wise_paid_fee_payment_disabled')}" if @particular_wise_paid
      @fee_collection = FinanceFeeCollection.find(params[:id2])
      @due_date = @fee_collection.due_date
      @fee_category = FinanceFeeCategory.find(@fee_collection.fee_category_id,:conditions => ["is_deleted IS NOT NULL"])
      @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
      @categorized_particulars=@fee_particulars.group_by(&:receiver_type)
      @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and ((par.receiver==@financefee.student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) and (par.master_receiver_type!='FinanceFeeParticular' or (par.master_receiver_type=='FinanceFeeParticular' and (par.master_receiver.receiver.present? and @fee_particulars.collect(&:id).include? par.master_receiver_id) and  (par.master_receiver.receiver==@financefee.student or par.master_receiver.receiver==@financefee.student_category or par.master_receiver.receiver==@financefee.batch)))) }
      @categorized_discounts=@discounts.group_by(&:master_receiver_type)
      @total_discount = 0
      @total_payable=@fee_particulars.map{|s| s.amount}.sum.to_f
      @total_discount =@discounts.map { |d| d.master_receiver_type=='FinanceFeeParticular' ? (d.master_receiver.amount * d.discount.to_f/(d.is_amount? ? d.master_receiver.amount : 100)) : @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?
      total_fees = @financefee.balance.to_f+params[:special_fine].to_f
      unless params[:fine].nil?
        total_fees += params[:fine].to_f
      end
      if @financefee.tax_enabled?
        @tax_collections = @financefee.tax_collections.all(:include => :tax_slab)                  
        @total_tax = @tax_collections.map(&:tax_amount).sum.to_f
        #        @tax_slabs = @tax_collections.map {|tax_col| tax_col.tax_slab }.uniq
        @tax_collections = @tax_collections.group_by {|x| x.tax_slab }
      end
      bal=(@total_payable-@total_discount).to_f
      days=(Date.today-@date.due_date.to_date).to_i
      auto_fine=@date.fine
      @fine_amount = 0
      if days > 0 and auto_fine
        @fine_rule=auto_fine.fine_rules.find(:last,:conditions=>["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"],:order=>'fine_days ASC')
        @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule
      end
      @amount = total_fees + @fine_amount
      @paid_fees = @financefee.finance_transactions.all(:include => :transaction_ledger)
      #OnlinePayment.return_url = "http://#{request.host_with_port}/student/fee_details/#{params[:id]}/#{params[:id2]}?create_transaction=1" unless OnlinePayment.return_url.nil?
      total_fees = 0
      total_fees = @fee_collection.student_fee_balance(@student)+params[:special_fine].to_f
      unless @particular_wise_paid
        if FedenaPlugin.can_access_plugin?("fedena_pay")
          if (PaymentConfiguration.config_value("enabled_fees").present? and PaymentConfiguration.is_student_fee_enabled?)
            @active_gateway = PaymentConfiguration.config_value("fedena_gateway")
            if @active_gateway.present?
              @custom_gateway = CustomGateway.find(@active_gateway)
            end

            if params[:create_transaction].present?
              gateway_response = Hash.new
              if @custom_gateway.present?
                if params[:return_hash].present?
                  return_value = params[:return_hash]
                  @decrypted_hash = PaymentConfiguration.payment_decryption(return_value)
                end
                @custom_gateway.gateway_parameters[:response_parameters].each_pair do|k,v|
                  unless k.to_s == "success_code"
                    gateway_response[k.to_sym] = params[:return_hash].present? ? @decrypted_hash[v.to_sym] : params[v.to_sym]
                  end
                end
              end
              @gateway_status = false
              if @custom_gateway.present?
                success_code = @custom_gateway.gateway_parameters[:response_parameters][:success_code]
                @gateway_status = true if gateway_response[:transaction_status] == success_code
              end
            
              tr_status = ""
              tr_ref = ""
              reason = ""
            
              single_payment = SingleFeePayment.new(:payee => @student, :gateway_response => gateway_response, :status => @gateway_status, :amount => gateway_response[:amount].to_f, :gateway => @active_gateway)
              #            payment = Payment.new(:payee => @student,:payment => @financefee,:gateway_response => gateway_response,:status => @gateway_status, :amount => gateway_response[:amount].to_f, :gateway => @active_gateway)
              #            payment.fee_collection = @fee_collection
              if single_payment.save
                finance_payment = FinancePayment.create(:payment_id => single_payment.id, :fee_payment => @financefee, :fee_collection => @financefee.finance_fee_collection)
                unless @financefee.is_paid?
                  amount_from_gateway = 0
                  if @custom_gateway.present?
                    amount_from_gateway = gateway_response[:amount]
                  end
                  unless amount_from_gateway.to_f <= 0.0
                    if @gateway_status == true
                      logger = Logger.new("#{RAILS_ROOT}/log/payment_processor_error.log")
                      pay_status = false
                      begin
                        retries ||= 0
                        pay_status = true
                        transaction = FinanceTransaction.new
                        transaction.title = "#{t('receipt_no')}. F#{@financefee.id}"
                        transaction.category = FinanceTransactionCategory.find_by_name("Fee")
                        transaction.payee = @student
                        transaction.finance = @financefee
                        #transaction.amount = @financefee.balance.to_f + @fine.to_f + @fine_amount.to_f #amount_from_gateway.to_f
                        transaction.amount = amount_from_gateway.to_f
                        transaction.fine_included = (@fine.to_f).zero? ? false : true
                        transaction.fine_amount = @fine.to_f
                        transaction.transaction_date = FedenaTimeSet.current_time_to_local_time(Time.now).to_date
                        transaction.payment_mode = "Online Payment"
                        transaction.reference_no = gateway_response[:transaction_reference]
                        transaction.save
                      rescue ActiveRecord::StatementInvalid => er
                        # run code again  to  avoid duplications
                        pay_status = false
                        retry if (retries += 1) < 2
                        logger.info "Error------#{er.message}----for --#{gateway_response}" unless (retries += 1) < 2
                      rescue Exception => e
                        pay_status = false
                        logger.info "Errror-----#{e.message}------for---#{gateway_response}"
                      end
                    
                      if pay_status
                        finance_payment.update_attribute("finance_transaction_id", transaction.id)
                        unless @financefee.transaction_id.nil?
                          tid = @financefee.transaction_id.to_s + ",#{transaction.id}"
                        else
                          tid=transaction.id
                        end
                        is_paid = (sprintf("%0.2f",total_fees.to_f+@fine.to_f + @fine_amount.to_f).to_f == amount_from_gateway.to_f) ? true : false
                        @financefee.update_attributes(:transaction_id=>tid, :is_paid=>is_paid)

                        @paid_fees = FinanceTransaction.find(:all, :include => :transaction_ledger,
                          :conditions=>"FIND_IN_SET(id,\"#{tid}\")")
                      

                        status = SingleFeePayment.payment_status_mapping[:success]
                        single_payment.update_attributes(:status_description => status)
                        online_transaction_id = single_payment.gateway_response[:transaction_reference]
                        flash[:notice] = "#{t('payment_success')} <br>  #{t('payment_reference')} : #{online_transaction_id}"
                        tr_status = "success"
                        tr_ref = online_transaction_id
                        reason = single_payment.gateway_response[:reason_code]
                      end
                    else
                      status = SingleFeePayment.payment_status_mapping[:failed]
                      single_payment.update_attributes(:status_description => status)
                      flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{single_payment.gateway_response[:reason_code] || 'N/A'} <br> #{t('transaction_id')} : #{single_payment.gateway_response[:transaction_reference] || 'N/A'}"
                      tr_status = "failure"
                      tr_ref = single_payment.gateway_response[:transaction_reference]
                      reason = single_payment.gateway_response[:reason_code]
                    end
                  
                  else
                    status = SingleFeePayment.payment_status_mapping[:failed]
                    single_payment.update_attributes(:status_description => status)
                    flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{single_payment.gateway_response[:reason_code] || 'N/A'} <br> #{t('transaction_id')} : #{single_payment.gateway_response[:transaction_reference] || 'N/A'}"
                    tr_status = "failure"
                    tr_ref = single_payment.gateway_response[:transaction_reference]
                    reason = single_payment.gateway_response[:reason_code]
                  end
                end
                if current_user.parent?
                  user = current_user
                else
                  user = @student.user
                end
                if @student.is_email_enabled && user.email.present? && @gateway_status
                  begin
                    Delayed::Job.enqueue(OnlinePayment::PaymentMail.new(finance_payment.fee_collection.name,user.email,user.full_name,@custom_gateway.name,FedenaPrecision.set_and_modify_precision(single_payment.amount),online_transaction_id,single_payment.gateway_response,user.school_details,hostname))
                  rescue Exception => e
                    puts "Error------#{e.message}------#{e.backtrace.inspect}"
                    return
                  end
                end
              else
                flash[:notice] = "#{t('flash_payed')}"
                tr_status = "failure"
                tr_ref = single_payment.gateway_response[:transaction_reference]
                reason = "#{t('flash_payed')}"
              end
              if session[:mobile] == true
                redirect_to :controller=>"payment_settings", :action=>"complete_payment", :student_id=>@student.id, :fee_collection_id=>@fee_collection.id, :collection_type=>"general", :transaction_status=>tr_status, :reason=>reason, :transaction_id=>tr_ref
              else
                redirect_to :controller => 'student', :action => 'mobile_fee_details', :id => params[:id], :id2 => params[:id2]
              end
            else
              @fine_amount=0 if (@student.finance_fee_by_date @date).is_paid
              render :layout =>"mobile"
            end
          else
            render :layout => "mobile"
            #render 'student/fee_details'
          end
        else
          render :layout => "mobile"
        end
      else
        render :layout => "mobile"
      end
    end

    private

    def is_mobile_user?
      unless FedenaPlugin.can_access_plugin?("fedena_mobile")
        if FedenaMobile::MobileStudent.instance_methods.include?(action_name)
          flash[:notice]=t('flash_msg4')
          redirect_to :controller => 'user', :action => 'dashboard'
        end
      end
    end

    def select_layout
      user_agents=["android","ipod","opera mini","opera mobi","blackberry","palm","hiptop","avantgo","plucker", "xiino","blazer","elaine", "windows ce; ppc;", "windows ce; smartphone;","windows ce; iemobile", "up.browser","up.link","mmp","symbian","smartphone", "midp","wap","vodafone","o2","pocket","kindle", "mobile","pda","psp","treo"]
      @ret=false
      if FedenaPlugin.can_access_plugin?("fedena_mobile")
        user_agents.each do |ua|
          if request.env["HTTP_USER_AGENT"].downcase=~ /#{ua}/i
            @ret=true
            return
          end
        end
      end
    end

  end
end
