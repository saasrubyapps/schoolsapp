module FedenaMobile
  module MobileEmployeeAttendance

    def self.included(base)
      base.instance_eval do
        before_filter :is_mobile_user?
        filter_access_to [:mobile_leave],:attribute_check => true ,:load_method => lambda {Employee.find(params[:id]).user}
      end
    end

    def mobile_leave
      @page_title=t('leave_application')
      @employee = Employee.find(params[:id])
      @leave_types = EmployeeLeaveType.active.all(:joins => :employee_leaves, :conditions=>["employee_leaves.employee_id = ? AND employee_leaves.reset_date IS NOT NULL and employee_leaves.is_active = true",@employee.id])
      @reporting_employees = Employee.find_all_by_reporting_manager_id(@employee.user_id)
      @total_leave_count = 0
      @leave_count = EmployeeLeave.active.find_all_by_employee_id(@employee,:joins=>:employee_leave_type,:conditions=>"creation_status = 2")
      @reporting_employees.each do |e|
        @app_leaves = ApplyLeave.count(:conditions=>["employee_id =? AND viewed_by_manager =?", e.id, false])
        @total_leave_count = @total_leave_count + @app_leaves
      end
      @employee_leaves = EmployeeLeave.active.all(:conditions => ["employee_id = ?", @employee.id])
      @employee_leave_types = EmployeeLeaveType.all_leave_types
      @payroll_group_lop_status = @employee.payroll_group.present? && @employee.payroll_group.enable_lop
      @leave_apply = ApplyLeave.new(params[:leave_apply])
      @leave_apply.viewed_by_manager=false
      @leave_apply.approved = nil
      if request.post? and @leave_apply.save
        flash[:notice]=t('flash5')
        redirect_to :controller => "employee_attendance", :action=> "mobile_leave", :id=>@employee.id
      else
        render :layout =>"mobile"
      end
    end

    private

    def is_mobile_user?
      unless FedenaPlugin.can_access_plugin?("fedena_mobile")
        if FedenaMobile::MobileEmployeeAttendance.instance_methods.include?(action_name)
          flash[:notice]=t('flash_msg4')
          redirect_to :controller => 'user', :action => 'dashboard'
        end
      end
    end
    
  end
end
