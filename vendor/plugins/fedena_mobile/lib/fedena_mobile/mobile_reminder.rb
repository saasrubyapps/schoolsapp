# To change this template, choose Tools | Templates
# and open the template in the editor.

module FedenaMobile
  module MobileReminder

    def self.included(base)
      base.instance_eval do
        before_filter :is_mobile_user?
      end
    end

    def mobile_index
      @user = current_user
      @reminders = Reminder.paginate(:page => params[:page],:per_page=>10, :conditions=>["recipient = '#{@user.id}' and is_deleted_by_recipient = false"], :order=>"created_at DESC")
      @more_reminders_present = 0
      if @reminders.present?
        last_reminder = Reminder.find(:last, :conditions=>["recipient = '#{@user.id}' and is_deleted_by_recipient = false"], :order=>"created_at DESC")
        @more_reminders_present = 1 unless @reminders.last == last_reminder
      end
      if request.xhr?
        render :partial=>'list_reminders', :locals=>{:reminders=>@reminders,:page=>(params[:page].to_i + 1),:more_reminders_present=>@more_reminders_present}
      else
        @page_title=t('messages')
        render :layout =>"mobile"
      end
      
      #      @page = params[:page].to_i
      #      @page||= 1
    end

    def mobile_view
      user = current_user
      @new_reminder = Reminder.find(params[:id2])
      Reminder.update(@new_reminder.id, :is_read => true)
      @sender = @new_reminder.user

      if request.post?
        unless params[:reminder][:body] == "" or params[:recipients] == ""
          Reminder.create(:sender=>user.id, :recipient=>@sender.id, :subject=>params[:reminder][:subject],
            :body=>params[:reminder][:body], :is_read=>false, :is_deleted_by_sender=>false,:is_deleted_by_recipient=>false)
          flash[:notice]="#{t('flash3')}"
          redirect_to :controller=>"reminder", :action=>"mobile_view", :id2=>params[:id2]
        else
          flash[:notice]="<b>ERROR:</b>#{t('flash4')}"
          redirect_to :controller=>"reminder", :action=>"mobile_view",:id2=>params[:id2]
        end
      end
      @page_title=t('messages')
      render :layout =>"mobile"
    end

    private

    def is_mobile_user?
      unless FedenaPlugin.can_access_plugin?("fedena_mobile")
        if FedenaMobile::MobileReminder.instance_methods.include?(action_name)
          flash[:notice]=t('flash_msg4')
          redirect_to :controller => 'user', :action => 'dashboard'
        end
      end
    end

  end
end
