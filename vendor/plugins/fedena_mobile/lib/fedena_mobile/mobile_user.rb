# To change this template, choose Tools | Templates
# and open the template in the editor.

module FedenaMobile
  module MobileUser

    def self.included(base)
      base.instance_eval do
        skip_before_filter :login_required,:only=>[:mobile_login,:mlogin,:mobile_forgot_password,:mob_forgot_pw,:m_reset_pwd,:mobile_reset_password,:mobile_set_new_password]
        before_filter :mobile_login, :only=>[:login]
        prepend_before_filter :mobile_dash, :only=>[:dashboard]
        #before_filter :load_school,:request_scope_validity,:set_title
        before_filter :mob_forgot_pw, :only=>[:forgot_password]
        before_filter :m_reset_pwd, :only=>[:reset_password]
        before_filter :check_if_loggedin, :only => [:mlogin,:login]
        before_filter :is_mobile_user?
        layout :what_layout
      end
    end

    def mobile_login
      select_layout
      if @ret==true
        redirect_to :controller => 'user',:action=>:mlogin
        return
      end
    end
    
    def mob_forgot_pw
      select_layout
      if @ret==true
        redirect_to :controller => 'user',:action=>:mobile_forgot_password
        return
      end
    end

    def mobile_forgot_password
      if request.post? and params[:reset_password]
        if user = User.active.first(:conditions => ["username LIKE BINARY(?)",params[:reset_password][:username]])
          unless user.email.blank?
            user.reset_password_code = Digest::SHA1.hexdigest( "#{user.email}#{Time.now.to_s.split(//).sort_by {rand}.join}" )
            user.reset_password_code_until = 1.day.from_now
            user.role = user.role_name
            user.save(false)
            url = "#{request.protocol}#{request.host_with_port}"
            begin
              UserNotifier.deliver_forgot_password(user,url)
            rescue Exception => e
              puts "Error------#{e.message}------#{e.backtrace.inspect}"
              flash[:notice] = "#{t('flash21')}"
              return
            end
            flash[:notice] = "#{t('flash18')}"
            redirect_to :action => "mobile_dashboard"
          else
            flash[:notice] = "#{t('flash20')}"
            return
          end
        else
          flash[:notice] = "#{t('flash19')} #{params[:reset_password][:username]}"
        end
      end
    end

    def mobile_dash
      load_school
      request_scope_validity
      set_title
      select_layout
      if @ret==true
        redirect_to :controller => 'user',:action=>:mobile_dashboard
        return
      end
    end

    def mlogin
      @institute = Configuration.find_by_config_key("LogoName")
      available_login_authes = FedenaPlugin::AVAILABLE_MODULES.select{|m| m[:name].camelize.constantize.respond_to?("login_hook")}
      selected_login_hook = available_login_authes.first if available_login_authes.count>=1
      if selected_login_hook
        authenticated_user = selected_login_hook[:name].camelize.constantize.send("login_hook",self)
      else
        if request.post? and params[:user]
          @user = User.new(params[:user])
          user = User.active.first(:conditions => ["username LIKE BINARY(?)",@user.username])
          if user.present? and User.authenticate?(@user.username, @user.password)
            authenticated_user = user
          end
        end
      end
      if authenticated_user.present?
        successful_mobile_login(authenticated_user) and return
      elsif authenticated_user.blank? and request.post?
        flash[:notice] = "#{t('login_error_message')}"
      end
    end
    
    def m_reset_pwd
      select_layout
      if @ret==true
        if params[:id]
          redirect_to :controller => 'user',:action=>:mobile_reset_password,:id=>params[:id]
        else
          redirect_to :controller => 'user',:action=>:mobile_reset_password
        end
        return
      end
    end

    def mobile_reset_password
      user = User.active.find_by_reset_password_code(params[:id],:conditions=>"reset_password_code IS NOT NULL")
      if user
        if user.reset_password_code_until > Time.now
          redirect_to :action => 'mobile_set_new_password', :id => user.reset_password_code
        else
          flash[:notice] = "#{t('flash1')}"
          redirect_to :action => 'mobile_dashboard'
        end
      else
        flash[:notice]= "#{t('flash2')}"
        redirect_to :action => 'mobile_dashboard'
      end
    end

    def mobile_set_new_password
      if request.post?
        user = User.active.find_by_reset_password_code(params[:id],:conditions=>"reset_password_code IS NOT NULL")
        if user
          if params[:set_new_password][:new_password]=='' and params[:set_new_password][:confirm_password]==''
            flash[:notice]= "#{t('flash6')}"
            redirect_to :action => 'mobile_set_new_password', :id => user.reset_password_code
          else
            if params[:set_new_password][:new_password] === params[:set_new_password][:confirm_password]
              user.password = params[:set_new_password][:new_password]
              if user.update_attributes(:password => user.password, :reset_password_code => nil, :reset_password_code_until => nil, :role => user.role_name)
                user.clear_menu_cache
                #User.update(user.id, :password => params[:set_new_password][:new_password],
                # :reset_password_code => nil, :reset_password_code_until => nil)
                flash[:notice] = "#{t('flash3')}"
                redirect_to :action => 'mobile_dashboard'
              else
                user.reload
                flash[:notice] = "#{t('user.flash22')}"
                redirect_to :action => 'mobile_set_new_password', :id => user.reset_password_code
              end
            else
              flash[:notice] = "#{t('user.flash4')}"
              redirect_to :action => 'mobile_set_new_password', :id => user.reset_password_code
            end
          end
        else
          flash[:notice] = "#{t('flash5')}"
          redirect_to :action => 'mobile_dashboard'
        end
      end
    end

    def mobile_dashboard
      @user = current_user
      if @user.student? or @user.parent?
        student = @user.student_record if @user.student?
        student = @user.parent_record if @user.parent?
        @pending_fees = student.finance_fees.all(:conditions=>{:is_paid=>false}).count
        if FedenaPlugin.can_access_plugin?("fedena_hostel")
          hostel_fees = student.hostel_fees
          hostel_fees.each do|h|
            unless h.is_paid?
              @pending_fees = @pending_fees + 1
            end
          end
        end
        if FedenaPlugin.can_access_plugin?("fedena_transport")
          transport_fees = student.transport_fees
          transport_fees.each do|t|
            unless t.is_paid?
              @pending_fees = @pending_fees + 1
            end
          end
        end
      end
    end

    def mobile_logout
      available_login_authes = FedenaPlugin::AVAILABLE_MODULES.select{|m| m[:name].camelize.constantize.respond_to?("logout_hook")}
      selected_logout_hook = available_login_authes.first if available_login_authes.count>=1
      if selected_logout_hook
        clear_session
        selected_logout_hook[:name].camelize.constantize.send("logout_hook",self,"/")
      else
        respond_to do |format|
          format.js {
            if Configuration.find_by_config_key('EnableSessionTimeout').config_value == "1"
              clean_session
              flash[:notice] = "#{t('logged_out_due_to_inactivity')}"
              render :js => "window.location = '/'"
            else
              render :nothing => true
            end
          }
          format.html {
            clean_session
            flash[:notice] = "#{t('logged_out')}"
            redirect_to :controller => '/' and return
          }
        end 
      end
    end

    def select_layout
      user_agents=["android","ipod","opera mini","opera mobi","blackberry","palm","hiptop","avantgo","plucker", "xiino","blazer","elaine", "windows ce; ppc;", "windows ce; smartphone;","windows ce; iemobile", "up.browser","up.link","mmp","symbian","smartphone", "midp","wap","vodafone","o2","pocket","kindle", "mobile","pda","psp","treo"]
      @ret=false
      if FedenaPlugin.can_access_plugin?("fedena_mobile")
        user_agents.each do |ua|
          if request.env["HTTP_USER_AGENT"].downcase=~ /#{ua}/i
            @ret=true
            return
          end
        end
      end
    end

    private
    
    def clean_session
      Rails.cache.delete("user_main_menu#{session[:user_id]}")
      Rails.cache.delete("user_autocomplete_menu#{session[:user_id]}")
      current_user.delete_user_menu_caches
      session[:user_id] = nil if session[:user_id]
      session[:language] = nil  
    end

    def what_layout
      select_layout
      return 'login' if action_name == 'login' or action_name == 'set_new_password'
      return 'forgotpw' if action_name == 'forgot_password'
      return 'mobile' if action_name == 'mobile_dashboard'
      return 'mobile_login' if ['mlogin','mobile_forgot_password','mobile_reset_password','mobile_set_new_password'].include?(action_name)
      return 'dashboard' if action_name == 'dashboard'
      return 'mobile'  if @ret==true
      'application'
    end


    def successful_mobile_login(user)
      session[:user_id] = user.id
      flash[:notice] = "#{t('welcome')}, #{user.first_name} #{user.last_name}!"
      redirect_to ((session[:back_url] unless (session[:back_url]) =~ /user\/mobile_logout$/) || {:controller => 'user', :action => 'mobile_dashboard'})
    end

    def is_mobile_user?
      unless FedenaPlugin.can_access_plugin?("fedena_mobile")
        if FedenaMobile::MobileUser.instance_methods.include?(action_name)
          flash[:notice]=t('flash_msg4')
          redirect_to :controller => 'user', :action => 'dashboard'
        end
      end
    end
    
  end
end
