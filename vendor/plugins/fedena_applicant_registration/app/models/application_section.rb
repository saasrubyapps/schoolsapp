class ApplicationSection < ActiveRecord::Base
  
  serialize :section_fields
  
  belongs_to :registration_course

  DEFAULT_FORM = [{:section_name=>"student_personal_details",:applicant_addl_field_group_id=>nil,:section_order=>1,:fields=>[{:field_type=>"default",:show_field=>"default_true",:field_name=>"first_name",:mandatory=>"default_true",:field_order=>1},
        {:field_type=>"default",:show_field=>true,:field_name=>"middle_name",:mandatory=>false,:field_order=>2},{:field_type=>"default",:show_field=>"default_true",:field_name=>"last_name",:mandatory=>true,:field_order=>3},
        {:field_type=>"default",:show_field=>"default_true",:field_name=>"date_of_birth",:mandatory=>"default_true",:field_order=>4},{:field_type=>"default",:show_field=>"default_true",:field_name=>"gender",:mandatory=>"default_true",:field_order=>5},
        {:field_type=>"default",:show_field=>"default_true",:field_name=>"nationality",:mandatory=>"default_true",:field_order=>6},{:field_type=>"default",:show_field=>true,:field_name=>"student_photo",:mandatory=>false,:field_order=>7},{:field_type=>"default",:show_field=>true,:field_name=>"student_category",:mandatory=>false,:field_order=>8},
        {:field_type=>"default",:show_field=>true,:field_name=>"religion",:mandatory=>false,:field_order=>9},{:field_type=>"default",:show_field=>true,:field_name=>"blood_group",:mandatory=>false,:field_order=>10},
        {:field_type=>"default",:show_field=>true,:field_name=>"birth_place",:mandatory=>false,:field_order=>11},{:field_type=>"default",:show_field=>true,:field_name=>"mother_tongue",:mandatory=>false,:field_order=>12}]},
    {:section_name=>"student_communication_details",:applicant_addl_field_group_id=>nil,:section_order=>2,:fields=>[{:field_type=>"default",:show_field=>true,:field_name=>"address_line_1",:mandatory=>false,:field_order=>1},{:field_type=>"default",:show_field=>true,:field_name=>"address_line_2",:mandatory=>false,:field_order=>2},
        {:field_type=>"default",:show_field=>true,:field_name=>"city",:mandatory=>false,:field_order=>3},{:field_type=>"default",:show_field=>true,:field_name=>"state",:mandatory=>false,:field_order=>4},
        {:field_type=>"default",:show_field=>true,:field_name=>"pin_code",:mandatory=>false,:field_order=>5},{:field_type=>"default",:show_field=>true,:field_name=>"country",:mandatory=>false,:field_order=>6},
        {:field_type=>"default",:show_field=>true,:field_name=>"phone",:mandatory=>false,:field_order=>7},{:field_type=>"default",:show_field=>true,:field_name=>"mobile",:mandatory=>false,:field_order=>8},
        {:field_type=>"default",:show_field=>true,:field_name=>"email",:mandatory=>false,:field_order=>9}]},{:section_name=>"elective_subjects",:applicant_addl_field_group_id=>nil,:section_order=>3,:fields=>[
        {:field_type=>"default",:show_field=>true,:field_name=>"choose_electives",:mandatory=>true,:field_order=>1}]},{:section_name=>"previous_institution_details",:applicant_addl_field_group_id=>nil,:section_order=>4,:fields=>[{:field_type=>"default",:show_field=>true,:field_name=>"institution_name",:mandatory=>false,:field_order=>1},
        {:field_type=>"default",:show_field=>true,:field_name=>"qualifying_exam_name",:mandatory=>false,:field_order=>2},{:field_type=>"default",:show_field=>true,:field_name=>"exam_roll_no",:mandatory=>false,:field_order=>3},
        {:field_type=>"default",:show_field=>true,:field_name=>"final_score",:mandatory=>false,:field_order=>4}]},{:section_name=>"guardian_personal_details",:applicant_addl_field_group_id=>nil,:section_order=>5,:fields=>[{:field_type=>"default",:show_field=>"default_true",:field_name=>"first_name",:mandatory=>"default_true",:field_order=>1},
        {:field_type=>"default",:show_field=>true,:field_name=>"last_name",:mandatory=>false,:field_order=>2},{:field_type=>"default",:show_field=>"default_true",:field_name=>"relation",:mandatory=>"default_true",:field_order=>3},{:field_type=>"default",:show_field=>true,:field_name=>"date_of_birth",:mandatory=>false,:field_order=>4},
        {:field_type=>"default",:show_field=>true,:field_name=>"education",:mandatory=>false,:field_order=>5},{:field_type=>"default",:show_field=>true,:field_name=>"occupation",:mandatory=>false,:field_order=>6},
        {:field_type=>"default",:show_field=>true,:field_name=>"income",:mandatory=>false,:field_order=>7}]},{:section_name=>"guardian_contact_details",:applicant_addl_field_group_id=>nil,:section_order=>6,:fields=>[
        {:field_type=>"default",:show_field=>true,:field_name=>"office_address_line1",:mandatory=>false,:field_order=>1},{:field_type=>"default",:show_field=>true,:field_name=>"office_address_line2",:mandatory=>false,:field_order=>2},
        {:field_type=>"default",:show_field=>true,:field_name=>"city",:mandatory=>false,:field_order=>3},{:field_type=>"default",:show_field=>true,:field_name=>"state",:mandatory=>false,:field_order=>4},
        {:field_type=>"default",:show_field=>true,:field_name=>"country",:mandatory=>false,:field_order=>5},{:field_type=>"default",:show_field=>true,:field_name=>"office_phone1",:mandatory=>false,:field_order=>6},{:field_type=>"default",:show_field=>true,:field_name=>"office_phone2",:mandatory=>false,:field_order=>7},
        {:field_type=>"default",:show_field=>true,:field_name=>"mobile",:mandatory=>false,:field_order=>8},{:field_type=>"default",:show_field=>true,:field_name=>"email",:mandatory=>false,:field_order=>9}]},{:section_name=>"attachments",:applicant_addl_field_group_id=>nil,:section_order=>7,:section_description=>"attachment_section_description",:fields=>[]},{:section_name=>"administration_section",:applicant_addl_field_group_id=>nil,:section_order=>8,:section_description=>"admin_section_description",:fields=>[]}]
  
  
  DEFAULT_FIELDS = {
    :student_personal_details=>{:m_name=>"applicants",:fields=>{:first_name=>{:field_type=>"text_field"},:middle_name=>{:field_type=>"text_field"},:last_name=>{:field_type=>"text_field"},
        :date_of_birth=>{:field_type=>"calendar_date_select",:default_value=>"(@applicant.date_of_birth.blank? ?  I18n.l(Date.today-5.years,:format=>:default): I18n.l(@applicant.date_of_birth,:format=>:default))",:year_range=>"72.years.ago..0.years.ago"},
        :gender=>{:field_type=>"radio_button",:field_options=>'[["m","male"],["f","female"]]'},:nationality=>{:field_attr=>"nationality_id",:field_type=>"select",:field_options=>"Country.all.map {|c| [c.full_name, c.id]}",:selected=>"@applicant.nationality_id || Configuration.default_country"},
        :student_photo=>{:field_attr=>"photo",:field_type=>"paperclip_file_field",:size=>"12",:direct=>true},:student_category=>{:field_attr=>"student_category_id",:field_type=>"select",:field_options=>"StudentCategory.active.map {|c| [c.name, c.id]}",:selected=>"@applicant.student_category_id || nil",:prompt=>"t('select_a_category')"},:religion=>{:field_type=>"text_field"},:blood_group=>{:field_type=>"select",:field_options=>"Student::VALID_BLOOD_GROUPS",:selected=>"@applicant.blood_group || nil",:prompt=>"t('unknown')"},
        :birth_place=>{:field_type=>"text_field"},:mother_tongue=>{:field_attr=>"language",:field_type=>"text_field"}}},
    :student_communication_details=>{:m_name=>"applicants",:fields=>{:address_line_1=>{:field_attr=>"address_line1",:field_type=>"text_field"},:address_line_2=>{:field_attr=>"address_line2",:field_type=>"text_field"},
        :city=>{:field_type=>"text_field"},:state=>{:field_type=>"text_field"},:pin_code=>{:field_type=>"text_field"},:country=>{:field_attr=>"country_id",:field_type=>"select",:field_options=>"Country.all.map {|c| [c.full_name, c.id]}",:selected=>"@applicant.country_id || Configuration.default_country"},
        :phone=>{:field_attr=>"phone1",:field_type=>"text_field"},:mobile=>{:field_attr=>"phone2",:field_type=>"text_field"},:email=>{:field_type=>"text_field"}}},
    :elective_subjects=>{:m_name=>"applicants",:fields=>{:choose_electives=>{:field_attr=>"subject_ids"}}},
    :previous_institution_details=>{:m_name=>"applicant_previous_data",:m_build=>"build_applicant_previous_data",:fields=>{:institution_name=>{:field_attr=>"last_attended_school",:field_type=>"text_field"},:qualifying_exam_name=>{:field_attr=>"qualifying_exam",:field_type=>"text_field"},
        :exam_roll_no=>{:field_attr=>"qualifying_exam_roll",:field_type=>"text_field"},:final_score=>{:field_attr=>"qualifying_exam_final_score",:field_type=>"text_field"}}},
    :guardian_personal_details=>{:m_name=>"applicant_guardians",:m_build=>"applicant_guardians.build",:fields=>{:first_name=>{:field_type=>"text_field"},:last_name=>{:field_type=>"text_field"},:relation=>{:field_type=>"select"},
      :date_of_birth=>{:field_attr=>"dob",:field_type=>"calendar_date_select",:default_value=>"",:year_range=>"100.years.ago..20.years.ago"},:education=>{:field_type=>"text_field"},:occupation=>{:field_type=>"text_field"},:income=>{:field_type=>"text_field"}}},
  :guardian_contact_details=>{:m_name=>"applicant_guardians",:m_build=>"applicant_guardians.build",:fields=>{:office_address_line1=>{:field_type=>"text_field"},:office_address_line2=>{:field_type=>"text_field"},
        :city=>{:field_type=>"text_field"},:state=>{:field_type=>"text_field"},:country=>{:field_attr=>"country_id",:field_type=>"select",:field_options=>"Country.all.map {|c| [c.full_name, c.id]}",:selected=>"Configuration.default_country"},:office_phone1=>{:field_type=>"text_field"},
      :office_phone2=>{:field_type=>"text_field"},:mobile=>{:field_attr=>"mobile_phone",:field_type=>"text_field"},:email=>{:field_type=>"text_field"}}}
  }

  before_save :structure_section_fields
  
  def structure_section_fields
    section_hash = {:form_hash=>self.section_fields}
    modified_hash = ApplicationSection.repair_nested_params(section_hash)
    self.section_fields = modified_hash[:form_hash]
  end
  
  def self.repair_nested_params(obj)
    obj.each do |key, value|
      if value.is_a? Hash
        if value.keys.find {|k, _| k =~ /\D/ }
          repair_nested_params(value)
        else
          obj[key] = value.values
          value.values.each {|h| repair_nested_params(h) }
        end
      end
    end
  end

end
