require 'dispatcher'
require "list"
  
module FedenaApplicantRegistration
  def self.attach_overrides
    ActiveRecord::Base.instance_eval { include ActiveRecord::Acts::List }
    Dispatcher.to_prepare :fedena_applicant_registration do
      ::Course.instance_eval { has_one :registration_course }
      FinanceController.send(:include,FedenaApplicantRegistration::ApplicantRegistrationIncomeDetails)
    end
  end

  def self.csv_export_list
    return ["applicant_registration","search_by_registration"]
  end

  def self.csv_export_data(report_type,params)
    case report_type
    when "applicant_registration"
      data = Applicant.applicant_registration_data(params)
    when "search_by_registration"
      data = Applicant.search_by_registration_data(params)
    end
  end
  
  module ApplicantRegistrationIncomeDetails
    def self.included(base)
      base.alias_method_chain :income_details,:applicant_registration
    end
    def income_details_with_applicant_registration
      if date_format_check
        if FedenaPlugin.can_access_plugin?("fedena_applicant_registration")
          @target_action="income_details"
          if validate_date
            if params[:id].present?
              @income_category = FinanceTransactionCategory.find(params[:id])
              @incomes = @income_category.finance_transactions.find(:all, :conditions => ["transaction_date >= '#{@start_date}' and transaction_date <= '#{@end_date}'"])
            else
              @income_category = FinanceTransactionCategory.find_by_name('Applicant Registration')
              @grand_total = @income_category.finance_transactions.sum(:amount, :conditions => ["transaction_date >= '#{@start_date}' and transaction_date <= '#{@end_date}'"]).to_f
              @transactions=@income_category.finance_transactions.paginate(:page => params[:page],:per_page=>10,
                :include => :transaction_ledger,
                :joins=>"INNER JOIN applicants on finance_transactions.payee_id=applicants.id INNER JOIN registration_courses on registration_courses.id=applicants.registration_course_id",
                :select=>"finance_transactions.*,registration_courses.course_id as c_id,applicants.reg_no as applicant_reg_no",
                :conditions=> ["finance_transactions.transaction_date >= '#{@start_date}' and finance_transactions.transaction_date <= '#{@end_date}'"])
              @course_ids=@transactions.group_by(&:c_id)
            end
            if request.xhr?
              render(:update) do|page|
                page.replace_html "fee_report_div", :partial=>"finance_income_details/income_details_partial"
              end
            elsif @income_category.name=='Applicant Registration'
              render  "finance_income_details/income_details"
            else
              render  "finance/income_details"
            end
          else
            render_date_error_partial
          end
        else
          income_details_without_applicant_registration
        end
      end
    end
  end
  
  class ApplicantMail < Struct.new(:applicant_full_name, :email, :reg_no, :reg_course_name, :status_name, :school_details, :hostname)
    def perform
      ApplicantNotifier.deliver_send_update_notification(applicant_full_name, email, reg_no, reg_course_name, status_name, school_details, hostname)
    end
  end
 
  class ApplicantMessageMail < Struct.new(:email_ids, :email_subject, :email_content, :hostname, :school_details)
    def perform
      email_ids.each do |email_id|
        ApplicantMessageNotifier.deliver_send_message_notification(email_id, email_subject, email_content, hostname, school_details)
      end
    end
  end
end