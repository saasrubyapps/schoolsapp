class InstantFeeCategory < ActiveRecord::Base
  validates_presence_of :name
  has_many :instant_fees
  has_many :instant_fee_particulars,:dependent => :destroy
  before_destroy :check_transaction
  def check_transaction
    if instant_fees.present?
      errors.add(:base,:instant_fees_exist)
      return false
    end
  end

end
