class InstantFeeParticular < ActiveRecord::Base
  belongs_to :instant_fee_category
  validates_presence_of :name,:amount,:instant_fee_category_id
  validates_numericality_of :amount,:greater_than => 0
  # tax associations
  has_many :tax_assignments, :as => :taxable
  has_many :tax_slabs, :through => :tax_assignments, :class_name => "TaxSlab"  
  
  has_many :collectible_tax_slabs, :as => :collectible_entity
  has_many :collection_tax_slabs, :through => :collectible_tax_slabs, :class_name => "TaxSlab"
  
  has_many :tax_collections, :as => :taxable_entity, :dependent => :destroy
  has_many :tax_fees, :through => :tax_collections, :source => :taxable_fee, :source_type => "InstantFee"  
  
  cattr_accessor :tax_slab_id
  
  after_create :apply_tax_slab
  before_save :verify_precision

  def verify_precision
    self.amount = FedenaPrecision.set_and_modify_precision self.amount
  end
  
  def apply_tax_slab slab_id = nil
    self.tax_slab_id = slab_id || self.tax_slab_id
    unless self.tax_slab_id.present?
      self.tax_slabs = []
    else
      tax_slab = TaxSlab.find(self.tax_slab_id)    
      self.tax_slabs = [tax_slab] if tax_slab.present?
    end
  end
  
end
