require 'dispatcher'
# FedenaInstantFee
module FedenaInstantFee
  def self.attach_overrides
    Dispatcher.to_prepare :fedena_instant_fee do
      ::Student.instance_eval { has_many :instant_fees, :as => 'payee' }
      ::Employee.instance_eval { has_many :instant_fees, :as => 'payee' }
      ::Student.instance_eval {include StudentExtension}
      ::TaxSlab.instance_eval {include TaxSlabExtension}
      ::TaxPayment.instance_eval {include TaxPaymentExtension}
    end
  end
  def self.student_profile_fees_by_batch_hook
    "instant_fees/student_profile_fees"
  end
  def self.student_profile_fees_hook
    "transport_fee/student_profile_fees"
  end
end
module StudentExtension
  def find_instance_fees_by_batch(batch_id)
    # self.instant_fees.find_all_by_groupable_id(batch_id)
    # workaround for backward compatiability
    self.instant_fees.find_all_by_groupable_id(batch_id,:joins=>:finance_transaction,:select=>"instant_fees.*,transaction_date")
  end
end

module TaxSlabExtension
  def self.included(base)
    base.instance_eval do 
      has_many :instant_fee_particulars, :through => :tax_assignments, :source => :taxable, 
        :source_type => 'InstantFeeParticular'
      
      has_many :tax_assignments, :dependent => :destroy
      has_many :finance_fee_particulars, :through => :tax_assignments, :source => :taxable, 
        :source_type => 'FinanceFeeParticular'
    end
  end
end

module TaxPaymentExtension
  def self.included(base)
    base.class_eval do
      def self.instant_fee_tax_payments(start_date, end_date)
        conds = ["transaction_date 
            BETWEEN '#{start_date}' AND '#{end_date}' AND finance_type = 'InstantFee'"]
        selects = "DISTINCT tax_payments.id as tax_payment_id, 
                             tax_payments.tax_amount AS tax_amount, ts.name AS slab_name, 
                             ts.rate AS slab_rate, ts.id AS slab_id, i_f.id AS collection_id, 
                             'Instant Fee' AS collection_name, fts.transaction_date as transaction_date"
        common_joins = "INNER JOIN finance_transactions fts 
                                      ON fts.id = tax_payments.finance_transaction_id 
                          INNER JOIN instant_fees i_f 
                                      ON i_f.id = tax_payments.taxed_fee_id AND 
                                            tax_payments.taxed_fee_type = 'InstantFee' 
                          INNER JOIN collectible_tax_slabs cts 
                                      ON cts.collection_id = i_f.id AND 
                                            cts.collection_type = 'InstantFee'
                          INNER JOIN tax_slabs ts ON ts.id = cts.tax_slab_id"
        ifp_joins = "INNER JOIN instant_fee_particulars ifp 
                                      ON ifp.id=tax_payments.taxed_entity_id AND 
                                            tax_payments.taxed_entity_type='InstantFeeParticular' AND
                                            cts.collectible_entity_id = ifp.id AND
                                            cts.collectible_entity_type = 'InstantFeeParticular'"
        ifd_joins = "INNER JOIN instant_fee_details ifd 
                                      ON ifd.id=tax_payments.taxed_entity_id AND 
                                            tax_payments.taxed_entity_type='InstantFeeDetail' AND
                                            cts.collectible_entity_id = ifd.id AND
                                            cts.collectible_entity_type = 'InstantFeeDetail'"
        TaxPayment.all(:conditions => conds, :select => "#{selects}", :joins => "#{common_joins} 
                                                                                                                            #{ifp_joins}") + 
          TaxPayment.all(:conditions => conds, :select => "#{selects}", :joins => "#{common_joins} 
                                                                                                                               #{ifd_joins}")
      end
    end
  end
end
