class MiniAudit < ActiveRecord::Base

  after_update :invalidate_cache
  after_destroy :invalidate_cache

  def self.find_from_cache(module_name,action)
    Rails.cache.fetch("mini_audit/#{self.name}/#{module_name}/#{action}"){find_or_create_by_module_and_action(module_name,action)}
  end

  def invalidate_cache
    Rails.cache.delete("mini_audit/#{self.type}/#{self.module}/#{self.action}")
  end
end
