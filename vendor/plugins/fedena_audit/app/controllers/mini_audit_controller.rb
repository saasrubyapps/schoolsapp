class MiniAuditController < ApplicationController

  filter_access_to :all

  def index

  end

  def activity_audit
    @start_date = Date.today
    @end_date = Date.today
    @sort_order = params[:sort_order]
    condition_stmt = "DATE(activity_audit_logs.created_at) BETWEEN ? AND ?"
    condition_stmt += " AND users.school_id = ?" if (MultiSchool rescue nil)
    conditions = [condition_stmt, Date.today,Date.today]
    conditions << MultiSchool.current_school.id  if (MultiSchool rescue nil)
    if @sort_order.nil?
      @logs =  ActivityAudit.paginate(:page => params[:page], :per_page => 10,:joins => "INNER JOIN `activity_audit_logs` ON activity_audit_logs.activity_audit_id = mini_audits.id INNER JOIN users on activity_audit_logs.user_id = users.id",:conditions => conditions, :select => 'mini_audits.id, mini_audits.action as activity, mini_audits.module, count(*) as frequency', :group=> "activity_audit_logs.activity_audit_id")
    else
      @logs =  ActivityAudit.paginate(:page => params[:page], :per_page => 10,:joins => "INNER JOIN `activity_audit_logs` ON activity_audit_logs.activity_audit_id = mini_audits.id INNER JOIN users on activity_audit_logs.user_id = users.id",:conditions => conditions, :select => 'mini_audits.id, mini_audits.action as activity, mini_audits.module, count(*) as frequency', :group=> "activity_audit_logs.activity_audit_id", :order => @sort_order)
      render(:update) do |page|
        page.replace_html 'report', :partial=>'activity_report'
      end
    end
  end

  def user_audit
    @sort_order = params[:sort_order]
    condition_stmt = "DATE(test.created_at) BETWEEN ? AND ?"
    condition_stmt += " AND users.school_id = ?" if (MultiSchool rescue nil)
    conditions = [condition_stmt, Date.today,Date.today]
    conditions << MultiSchool.current_school.id  if (MultiSchool rescue nil)
    if params[:sort_order].nil?
      #@logs = ActivityAudit.paginate(:page => params[:page], :per_page => 10, :joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id', :select => "users.id as user_id,users.username,count(*) as no_of_visits, mini_audits.action as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :order => "activity_audit_logs.id desc",:conditions => ['DATE(activity_audit_logs.created_at) BETWEEN ? AND ?',Date.today,Date.today])
      @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :joins => "inner join (select * from activity_audit_logs order by id desc) as test on mini_audits.id = test.activity_audit_id inner join users on test.user_id = users.id",:conditions => conditions,  :group => "test.user_id", :select => "users.id as user_id,users.username,count(*) as no_of_visits, mini_audits.action as last_activity, MAX(test.created_at) as last_visited")
    else
      @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :joins => "inner join (select * from activity_audit_logs order by id desc) as test on mini_audits.id = test.activity_audit_id inner join users on test.user_id = users.id",:conditions => ["DATE(test.created_at) BETWEEN ? AND ?",Date.today, Date.today],  :group => "test.user_id", :select => "users.id as user_id,users.username,count(*) as no_of_visits, mini_audits.action as last_activity, MAX(test.created_at) as last_visited", :order => @sort_order)
      #@logs = ActivityAudit.paginate(:page => params[:page], :per_page => 10, :joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id', :select => "users.id as user_id,users.username,count(*) as no_of_visits, mini_audits.action as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :order => @sort_order,:conditions => ['DATE(activity_audit_logs.created_at) BETWEEN ? AND ?',Date.today,Date.today] )
      render(:update) do |page|
        page.replace_html 'search_result', :partial=>'search_result'
      end
    end
  end

  def update_course
    @courses = Course.all(:conditions => ["is_deleted = ?", false])
    @batches = Batch.all(:conditions => ["course_id IN (?) AND is_deleted = ?",@courses.collect{|c| c.id}, false])
    @departments = EmployeeDepartment.all
    if params[:search_by] == "Username"
      render(:update) do |page|
        page.replace_html 'user_search', :partial=>'search_box'
        page.replace_html 'course_list', :text=>''
        page.replace_html 'department_list', :text=>''
      end
    elsif params[:search_by] == "Employee"
      render(:update) do |page|
        page.replace_html 'department_list', :partial=>'list_department'
        page.replace_html 'course_list', :text=>''
        page.replace_html 'user_search', :text=>''
      end
    elsif params[:search_by] == ""
      render(:update) do |page|
        page.replace_html 'department_list', :text=>''
        page.replace_html 'course_list', :text=>''
        page.replace_html 'user_search', :text=>''
      end
    else
      render(:update) do |page|
        page.replace_html 'course_list', :partial=>'course_list'
        page.replace_html 'user_search', :text=>''
        page.replace_html 'department_list', :text=>''
      end
    end
  end

  def view_result
    @start_date = params[:from] || Date.today
    @end_date = params[:to] || Date.today
    @sort_order = params[:sort_order]
    if params[:search_by] == ""
      condition_stmt = "DATE(test.created_at) BETWEEN ? AND ?"
      condition_stmt += " AND users.school_id = ?" if (MultiSchool rescue nil)
      conditions = [condition_stmt, params[:from],params[:to]]
      conditions << MultiSchool.current_school.id  if (MultiSchool rescue nil)
      #@logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10,:joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id',:conditions => conditions, :select => "users.id as user_id ,users.username,count(*) as no_of_visits, MAX(mini_audits.action) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :order => "activity_audit_logs.id desc", :order => @sort_order)
      @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :joins => "inner join (select * from activity_audit_logs order by id desc) as test on mini_audits.id = test.activity_audit_id inner join users on test.user_id = users.id",:conditions => conditions,  :group => "test.user_id", :select => "users.id as user_id,users.username,count(*) as no_of_visits, mini_audits.action as last_activity, MAX(test.created_at) as last_visited", :order => @sort_order)
    elsif params[:search_by] == "Student"
      condition_stmt = ""
      condition_stmt = "students.batch_id = ? AND " if (params[:batch_id].present? && params[:batch_id] != "")
      condition_stmt += "DATE(test.created_at) BETWEEN ? AND ? "
      condition_stmt += " AND users.school_id = ?" if (MultiSchool rescue nil)
      conditions = [condition_stmt]
      conditions << params[:batch_id] if (params[:batch_id].present? && params[:batch_id] != "")
      conditions << params[:from] << params[:to]
      conditions << MultiSchool.current_school.id  if (MultiSchool rescue nil)
      join_condition = "INNER JOIN (select * from activity_audit_logs order by id desc) as test on test.activity_audit_id = mini_audits.id INNER JOIN users on test.user_id = users.id AND users.student = true"

      if (params[:course_id].present? && params[:course_id] != "" ) || (params[:batch_id].present? && params[:batch_id] != "")
        join_condition += " INNER JOIN students on students.user_id = test.user_id"
        join_condition += " INNER JOIN batches on batches.id = students.batch_id AND batches.course_id = #{params[:course_id]} INNER JOIN courses on courses.id = batches.course_id"
      end
      #      ActivityAuditLog.find(:all, :joins => "LEFT JOIN users on activity_audit_logs.user_id = users.id LEFT OUTER JOIN students on students.user_id = activity_audit_logs.user_id LEFT JOIN batches on batches.id = students.batch_id LEFT JOIN courses on courses.id = batches.course_id" , :conditions => ["users.student = ? AND students.batch_id = ?", true,1],  :select => "users.username, count(*) as no_of_visits ,MAX(activity_audit_logs.activity_name) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :group => 'activity_audit_logs.user_id')
      @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :order => "activity_audit_logs.id desc",:joins => join_condition , :conditions => conditions ,  :select => "users.id as user_id,users.username, count(*) as no_of_visits ,MAX(mini_audits.action) as last_activity, MAX(test.created_at) AS last_visited", :group => 'test.user_id', :order => @sort_order)
    elsif params[:search_by] == "Employee"
      join_condition = "INNER JOIN activity_audit_logs on activity_audit_logs.activity_audit_id = mini_audits.id INNER JOIN users on activity_audit_logs.user_id = users.id AND (users.employee = true OR users.admin = true) INNER JOIN employees on employees.user_id = users.id"

      join_condition += " AND employees.employee_department_id = #{params[:department_id]}" if params[:department_id].present?
      condition_stmt = "DATE(activity_audit_logs.created_at) BETWEEN ? AND ?"
      condition_stmt += " AND users.school_id = ?" if (MultiSchool rescue nil)
      conditions = [condition_stmt, params[:from], params[:to]]
      conditions << MultiSchool.current_school.id  if (MultiSchool rescue nil)
      @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10,:order => "activity_audit_logs.id desc",:joins => join_condition , :conditions => conditions ,  :select => "users.id as user_id,users.username, count(*) as no_of_visits ,MAX(mini_audits.action) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :group => 'activity_audit_logs.user_id', :order => @sort_order)
    elsif params[:search_by] == "Username"
      conditions = "DATE(activity_audit_logs.created_at) BETWEEN '#{@start_date}' AND '#{@end_date}' AND users.username LIKE '#{params[:query]}%'"
      conditions += " AND users.school_id = #{MultiSchool.current_school.id}" if (MultiSchool rescue nil)
      @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :order => "activity_audit_logs.id desc",:joins => {:activity_audit_logs =>:user},:group => 'activity_audit_logs.user_id',:conditions => conditions, :select => "users.id as user_id,users.username, MAX(mini_audits.action) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited, count(*) as no_of_visits", :order => @sort_order)
    end
    #    @logs = ActivityAudit.find(:all, :order => "activity_audit_logs.id asc",:joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id', :select => "users.username, MAX(activity_audit_logs.activity_name) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :order => "activity_audit_logs.id desc")
    render(:update) do |page|
      page.replace_html 'search_result', :partial=>'search_result'
    end
  end

  def activity_audit_detail
    @start_date = params[:from]
    @end_date = params[:to]
    @activity = ActivityAudit.find(params[:id])
    @sort_order = params[:sort_order]
    #condition_stmt = "mini_audits.action = ? AND DATE(activity_audit_logs.created_at) BETWEEN ? AND ?"
    if (MultiSchool rescue nil)
      condition_stmt = "users.school_id = ?"
      conditions = [condition_stmt]
      conditions << MultiSchool.current_school.id
    end
    join_conditions = "INNER JOIN `activity_audit_logs` ON activity_audit_logs.activity_audit_id = mini_audits.id AND mini_audits.action  = '#{params[:name]}' AND DATE(activity_audit_logs.created_at) BETWEEN '#{params[:from]}' AND '#{params[:to]}' INNER JOIN `users` ON `users`.id = `activity_audit_logs`.user_id "
    if @sort_order.nil?

      @logs = ActivityAudit.paginate(:page => params[:page], :per_page => 10, :joins => join_conditions,:group => 'activity_audit_logs.user_id', :conditions => conditions, :select => "users.id as user_id,users.username, count(*) as no_of_visit, MAX(activity_audit_logs.created_at) AS last_visited, mini_audits.action")
    else
      @logs = ActivityAudit.paginate(:page => params[:page], :per_page => 10, :joins => join_conditions,:group => 'activity_audit_logs.user_id', :conditions => conditions, :select => "users.id as user_id,users.username, count(*) as no_of_visit, MAX(activity_audit_logs.created_at) AS last_visited, mini_audits.action",:order => @sort_order)
      render(:update) do |page|
        page.replace_html 'information', :partial=>'activity_audit_detail_report'
      end
    end
  end

  def user_audit_detail
    @sort_order = params[:sort_order]
    @start_date = params[:from]
    @end_date = params[:to]
    @user = User.find(params[:id])
    condition_stmt = "activity_audit_logs.user_id = ? AND DATE(activity_audit_logs.created_at) BETWEEN ? AND ?"
    condition_stmt += " AND users.school_id = ?" if (MultiSchool rescue nil)
    conditions = [condition_stmt,params[:id],params[:from].to_date,params[:to].to_date]
    conditions << MultiSchool.current_school.id if (MultiSchool rescue nil)
    if @sort_order.nil?
      @logs = ActivityAudit.paginate(:page => params[:page], :per_page => 10, :joins => {:activity_audit_logs => :user},:conditions => conditions, :select => "mini_audits.action,mini_audits.module, count(*) as no_of_visit, MAX(activity_audit_logs.created_at) as last_visited", :group => "activity_audit_logs.activity_audit_id")
    else
      @logs = ActivityAudit.paginate(:page => params[:page], :per_page => 10, :joins => {:activity_audit_logs => :user},:conditions => conditions, :select => "mini_audits.action,mini_audits.module, count(*) as no_of_visit, MAX(activity_audit_logs.created_at) as last_visited", :group => "activity_audit_logs.activity_audit_id", :order => @sort_order)
      render(:update) do |page|
        page.replace_html 'information', :partial=>'user_audit_detail_report'
      end
    end
    @total_visits = @logs.map(&:no_of_visit).map(&:to_i).sum
  end

  def update_activity_report
    @start_date = params[:from]
    @end_date = params[:to]
    @sort_order = params[:sort_order]
    condition_stmt = "DATE(activity_audit_logs.created_at) BETWEEN ? AND ?"
    condition_stmt += " AND activity_audit_logs.school_id = ?" if (MultiSchool rescue nil)
    conditions = [condition_stmt,params[:from].to_date,params[:to].to_date]
    conditions << MultiSchool.current_school.id if (MultiSchool rescue nil)
    if @sort_order.nil?

      @logs =  ActivityAudit.paginate(:page => params[:page],:per_page => 10, :joins => :activity_audit_logs,:conditions => conditions, :select => 'mini_audits.id, mini_audits.action as activity, mini_audits.module, count(*) as frequency', :group=> "activity_audit_logs.activity_audit_id")
    else
      @logs =  ActivityAudit.paginate(:page => params[:page],:per_page => 10, :joins => :activity_audit_logs,:conditions => conditions, :select => 'mini_audits.id, mini_audits.action as activity, mini_audits.module, count(*) as frequency', :group=> "activity_audit_logs.activity_audit_id",  :order => @sort_order)

    end
    render(:update) do |page|
      page.replace_html 'report', :partial=>'activity_report'
    end
  end

  def search_ajax
    @sort_order = params[:sort_order]
    if params[:query] != ""
      #            @logs = ActivityAudit.paginate(:page => params[:page], :per_page => 10, :joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id', :conditions => ['mini_audits.action = ? AND DATE(activity_audit_logs.created_at) BETWEEN ? AND ?',params[:name], params[:from], params[:to]], :select => "users.id as user_id,users.username, count(*) as no_of_visit, MAX(activity_audit_logs.created_at) AS last_visited, mini_audits.action")
      conditions = "users.username LIKE '#{params[:query]}%' AND mini_audits.action = '#{params[:name]}' AND DATE(activity_audit_logs.created_at) BETWEEN '#{params[:from]}' AND '#{params[:to]}'"
      conditions += " AND users.school_id = #{MultiSchool.current_school.id}" if (MultiSchool rescue nil)
      if @sort_order.nil?
        @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :order => "activity_audit_logs.id asc",:conditions => conditions,:joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id', :select => "users.id as user_id,users.username,count(*) as no_of_visits, MAX(mini_audits.action) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :order => "activity_audit_logs.id desc")
      else
        @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :order => "activity_audit_logs.id asc",:conditions => conditions,:joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id', :select => "users.id as user_id,users.username,count(*) as no_of_visits, MAX(mini_audits.action) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :order => @sort_order)
      end
    else
      conditions = "mini_audits.action = '#{params[:name]}' AND DATE(activity_audit_logs.created_at) BETWEEN '#{params[:from]}' AND '#{params[:to]}'"
      conditions += " AND users.school_id = #{MultiSchool.current_school.id}" if (MultiSchool rescue nil)
      if @sort_order.nil?
        @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :order => "activity_audit_logs.id asc",:conditions => conditions,:joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id', :select => "users.id as user_id,users.username,count(*) as no_of_visits, MAX(mini_audits.action) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :order => "activity_audit_logs.id desc")
      else
        @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10, :order => "activity_audit_logs.id asc",:conditions => conditions,:joins => {:activity_audit_logs => :user},:group => 'activity_audit_logs.user_id', :select => "users.id as user_id,users.username,count(*) as no_of_visits, MAX(mini_audits.action) as last_activity, MAX(activity_audit_logs.created_at) AS last_visited", :order => @sort_order)
      end
    end
    render(:update) do |page|
      page.replace_html 'information', :partial=>'search_user_result'
    end
  end

  def activity_user_detailed
    @user = User.find(params[:id])
    @activity = ActivityAudit.find_by_action(params[:name])
    @start_date = params[:from]
    @end_date = params[:to]
    @sort_order = params[:sort_order]
    if @sort_order.nil?
      conditions = "DATE(activity_audit_logs.created_at) BETWEEN '#{params[:from]}' AND '#{params[:to]}' AND activity_audit_logs.user_id = #{params[:id]} AND mini_audits.action = '#{params[:name]}'"
      conditions += " AND activity_audit_logs.school_id = #{MultiSchool.current_school.id}"  if (MultiSchool rescue nil)
      @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10,:joins => :activity_audit_logs, :conditions => conditions, :select => "activity_audit_logs.created_at as time")
    else
      conditions = "DATE(activity_audit_logs.created_at) BETWEEN '#{params[:from]}' AND '#{params[:to]}' AND activity_audit_logs.user_id = #{params[:id]} AND mini_audits.action = '#{params[:name]}'"
      conditions += " AND activity_audit_logs.school_id = #{MultiSchool.current_school.id}"  if (MultiSchool rescue nil)
      @logs = ActivityAudit.paginate(:page => params[:page],:per_page => 10,:joins => :activity_audit_logs, :conditions => conditions, :select => "activity_audit_logs.created_at as time",:order => @sort_order)
      render(:update) do |page|
        page.replace_html 'information', :partial=>'activity_user_detailed_report'
      end
    end
  end

  def data_audit
    @sort_order = params[:sort_order]
    @modules = DataAudit.all(:select => "module", :order => "module").map{|m| m.module}.uniq
    if @sort_order.nil?
      conditions = "DATE(data_audit_logs.created_at) BETWEEN '#{Date.today}' AND '#{Date.today}'"
      conditions += " AND users.school_id = #{MultiSchool.current_school.id}" if (MultiSchool rescue nil)
      @logs = DataAudit.paginate(:page => params[:page], :per_page => 10, :joins => {:data_audit_logs => :user}, :conditions => conditions,:select => "mini_audits.action,data_audit_logs.record_description, users.username, data_audit_logs.created_at")
    else
      conditions = "DATE(data_audit_logs.created_at) BETWEEN '#{Date.today}' AND '#{Date.today}'"
      conditions += " AND users.school_id = #{MultiSchool.current_school.id}" if (MultiSchool rescue nil)
      @logs = DataAudit.paginate(:page => params[:page], :per_page => 10, :joins => {:data_audit_logs => :user}, :conditions => conditions,:select => "mini_audits.action,data_audit_logs.record_description, users.username, data_audit_logs.created_at", :order => @sort_order)
      render(:update) do |page|
        page.replace_html 'search_result', :partial=>'data_audit_report'
      end
    end

  end

 def data_audit_report
    @sort_order = params[:sort_order]
    conditions = "DATE(data_audit_logs.created_at) BETWEEN '#{params[:from] || Date.today}' AND '#{params[:to] || Date.today}'"
    conditions += " AND users.username like '#{params[:username]}%'" if params[:username].present?
    conditions += " AND mini_audits.action like '#{params[:activity]}%' " if params[:activity].present?
    conditions += " AND mini_audits.module like '#{params[:module]}'" if params[:module].present?
    conditions += " AND users.school_id = #{MultiSchool.current_school.id}" if (MultiSchool rescue nil)
    if @sort_order.nil?
      @logs = DataAudit.paginate(:page => params[:page],
                                  :per_page => 10,
                                  :joins => {:data_audit_logs => :user},
                                  :conditions => conditions,
                                  :select => "mini_audits.action,data_audit_logs.record_description, users.username, data_audit_logs.created_at"
                                  )
    else
      @logs = DataAudit.paginate(:page => params[:page], :per_page => 10, :joins => {:data_audit_logs => :user}, :conditions => conditions,  :select => "mini_audits.action,data_audit_logs.record_description, users.username, data_audit_logs.created_at", :order => @sort_order)
    end
    render(:update) do |page|
      page.replace_html 'search_result', :partial=>'data_audit_report'
    end
  end

  def update_activity
    @activities =  DataAudit.all(:select => "action",:conditions => ['module = ?',"#{params[:search_by]}"])
    render(:update) do |page|
      page.replace_html 'activity-select', :partial=>'activity_select'
    end
  end

  def batch_list
    @batches = Batch.all(:conditions => ["course_id = ? ", params[:course_id]])
    render(:update) do |page|
      page.replace_html 'batch_list', :partial=>'batch_list'
    end
  end
end
