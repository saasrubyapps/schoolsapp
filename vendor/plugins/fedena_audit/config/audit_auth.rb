authorization do


  role :admin do
    has_permission_on [:mini_audit],  
      :to => [:index,
              :activity_audit,
              :user_audit,
              :update_course,
              :update_batch,
              :view_result,
              :activity_audit_detail,
              :user_audit_detail,
              :update_activity_report,
              :search_ajax,
              :activity_user_detailed,
              :data_audit,
              :data_audit_report,
              :update_activity,
              :batch_list
              ]
  end

 role :manage_audit do
   has_permission_on [:mini_audit],
      :to => [:index,
              :activity_audit,
              :user_audit,
              :update_course,
              :update_batch,
              :view_result,
              :activity_audit_detail,
              :user_audit_detail,
              :update_activity_report,
              :search_ajax,
              :activity_user_detailed,
              :data_audit,
              :data_audit_report,
              :update_activity,
              :batch_list
              ]
 end
end                                                                                                                                                                                                           