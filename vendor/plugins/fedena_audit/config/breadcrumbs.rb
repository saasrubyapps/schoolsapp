Gretel::Crumbs.layout do
  crumb :mini_audit_index do
    link I18n.t('audit'), {:controller=>"mini_audit", :action=>"index"}
  end

  crumb :mini_audit_user_audit do
    link I18n.t('user_audit'), {:controller=>"mini_audit", :action=>"user_audit"}
    parent :mini_audit_index
  end

  crumb :mini_audit_activity_audit do
    link I18n.t('activity_audit'), {:controller=>"mini_audit", :action=>"activity_audit"}
    parent :mini_audit_index
  end

  crumb :mini_audit_data_audit do
    link I18n.t('data_audit'), {:controller=>"mini_audit", :action=>"data_audit"}
    parent :mini_audit_index
  end
  
 

 

  crumb :mini_audit_user_audit_detail do |params_array|
    link params_array.last.first.full_name, {:controller=>"mini_audit", :action=>"user_audit_detail", :from => params_array.first.first , :to => params_array.first.last, :id => params_array.last.first }
    parent :mini_audit_user_audit
  end






  crumb :mini_audit_activity_audit_detail do |params_array|
    link I18n.t(params_array.first.first.action), {:controller=>"mini_audit", :action=>"activity_audit_detail",:id => params_array.first.first.id,:from=>params_array.last.first,:to=>params_array.last.last,:name=>params_array.first.first.action}
    parent :mini_audit_activity_audit
  end

  crumb :mini_audit_activity_user_detailed do |params_array|
    link params_array.first.last.full_name, {:controller=>"mini_audit", :action=>"activity_user_detailed",  :from => params_array.last.first, :to => params_array.last.last , :name => params_array.first.last}
    parent :mini_audit_activity_audit_detail,params_array
  end
  #   crumb :mini_audit_activity_audit_detail do |activity,start_date,end_date|
  #    link activity.action , {:controller=>"mini_audit", :action=>"activity_audit_detail",  :from => start_date ,:to => end_date , :name => activity.module}
  #    parent :mini_audit_activity_audit
  #  end
  #
  #  crumb :mini_audit_activity_user_detailed do |activity,start_date,end_date|
  #    link "test", {:controller=>"mini_audit", :action=>"activity_user_detailed",  :from => start_date, :to => end_date , :name => activity.action}
  #    parent :mini_audit_activity_audit_detail, activity,start_date,end_date
  #  end
end