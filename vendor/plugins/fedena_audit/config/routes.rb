ActionController::Routing::Routes.draw do |map|
  map.resources :mini_audit , :collection => {:data_audit => [:get, :post],
                                             :data_audit_report => [:get, :post],
                                             :update_activity_report => [:post],
                                             :activity_audit => [:get,:post],
                                             :user_audit => [:get, :post],
                                             :update_course => [:post],
                                             :update_batch => [:get, :post],
                                             :view_result => [:get, :post],
                                             :activity_audit_detail => [:get, :post],
                                             :activity_user_detailed => [:get, :post],
                                             :user_audit_detail => [:get, :post],
                                             :search_ajax => [:get, :post],
                                             :update_activity => [:post]
                                           }, :member => {:search_ajax => [:get]}, :as => 'audit'
##  map.connect 'mini_audit/:action', :controller=>:mini_audit
#  map.connect ':controller/:action/:id'
#  map.connect ':controller/:action'
#  map.connect ':controller/:action/:id/:id2'
#  map.connect ':controller/:action/:id.:format'
end