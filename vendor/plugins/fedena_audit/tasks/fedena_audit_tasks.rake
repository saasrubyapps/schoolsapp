namespace :fedena_audit do
  desc "Install Fedena Audit Module"
  task :install do
    system "rsync -ruv --exclude=.svn vendor/plugins/fedena_audit/public ."
  end
end