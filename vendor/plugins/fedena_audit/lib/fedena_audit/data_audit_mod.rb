  
module FedenaAudit
  module DataAuditMod
    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods

      def audit(method, options = {})
        unless method_defined? :audit_params
          cattr_accessor :audit_params
          self.audit_params = {}
        end
       
        self.audit_params[method] = {}
        self.audit_params[method][:name] = "#{options[:name]}"
        self.audit_params[method][:condition] = options[:condition]

        eval("after_#{method} :audit_data_#{method}")
      end
    end

    private

    def audit_data(method)
      return nil if FedenaAudit.disable_audit
      return nil unless Authorization.current_user.is_a? User

      @activity_name = self.audit_params[method.to_sym][:name] || "#{params[:controller]}_#{params[:action]}"
      @record_description = self.class.audit_description(self,method.to_sym)
      condition = self.audit_params[method.to_sym][:condition].present? ? self.instance_eval(&self.audit_params[method.to_sym][:condition]) : true

      if condition
        case self.audit_params[method.to_sym][:name]
        when "course_edit"
          @activity_name =  "course_deletion" if self.changes.has_key? "is_deleted"
        when "batch_edit"
          @activity_name =  "batch_deletion" if self.changes.has_key? "is_deleted"
        when "fee_collection_edit"
          @activity_name =  "fee_collection_deletion" if self.changes.has_key? "is_deleted"
        end
        return if self.changes.keys.empty? && /^\w+_(edit)/.match(@activity_name.to_s)

        if (method == "update" && changed.reject{|att| att == "updated_at"}.length > 0) or method == "destroy" or method == "create"
          create_audit_log
          create_data_audit_log
        end
      end
    end

    def method_missing(symbol, *args)
      if symbol.to_s =~ /audit_data_(create|update|destroy)/
        audit_data($1) if FedenaPlugin.can_access_plugin?("fedena_audit")
      else
        super
      end
    end

    def create_audit_log
      self.class.superclass == ActiveRecord::Base ? mod_name = self.class.to_s : mod_name = self.class.superclass.to_s
      @audit_log = AuditAdapter.find_or_create(mod_name,@activity_name,'DataAudit')
    end

    def create_data_audit_log
      #DataAuditLog.create({:user_id => @current_user.id,:data_audit_id => @audit_log.id, :record_id => self.id, :record_description => @record_description})
      AuditAdapter.create({'user_id' => Authorization.current_user.id ,'data_audit_id' => @audit_log.to_i, 'record_id' => self.id, 'record_description' => @record_description.gsub(/(?=')/, "\\"),'created_at' => DateTime.now.utc.strftime("%F %T"), 'updated_at' => DateTime.now.utc.strftime("%F %T")},'data_audit_logs','DELAYED')
    end

  end
end