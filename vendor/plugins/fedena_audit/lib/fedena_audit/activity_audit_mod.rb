
module FedenaAudit
  module ActivityAuditMod

    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods
      def audit(action, options = {})
        unless method_defined? :audit_params
          cattr_accessor :audit_params
          self.audit_params = {}
        end

        self.audit_params[action] = {}
        self.audit_params[action][:condition] = options[:condition]
        self.audit_params[action][:method] = options[:method]
        self.audit_params[action][:activity_name] = "#{options[:name]}" || action
        self.instance_eval{ after_filter "audit_activity_#{action}", :only=> action}


      end
    end

    def method_missing name, *arg, &block
      if !!( name.to_s =~ /\Aaudit_activity_\w*/)
         audit_activity if FedenaPlugin.can_access_plugin?("fedena_audit")
      end
    end

    def audit_activity
      @user = @current_user || current_user
      action = params[:action]
      audit_action = self.audit_params[action.to_sym]
      @activity = audit_action[:activity_name]
      @method = audit_action[:method].to_s || ""
      #condition = if audit_action[:condition].nil? && audit_action[:method].nil?  #no options
               #     true
               #   elsif audit_action[:condition].nil? && !audit_action[:method].nil?
               #     audit_action[:method] == request.method.to_s
              #    elsif !audit_action[:condition].nil? &&  audit_action[:method].nil?
              #      self.instance_eval &audit_action[:condition]
              #    else        #both options
              #      (self.instance_eval &audit_action[:condition]) && audit_action[:method] == request.method.to_s
             #     end
             p @method
             p request.method.to_s
      unless @method.empty?
        condition = (@method.to_s == request.method.to_s)
      else
        condition = true
      end
      if condition
        create_audit_log
        create_activity_audit_log
      end
    
    end

    def create_audit_log
      #@audit_log = ActivityAudit.find_from_cache(params[:controller],@activity.strip)
      @audit_log = AuditAdapter.find_or_create(params[:controller],@activity.strip,'ActivityAudit')
    end

    def create_activity_audit_log
      #ActivityAuditLog.create({:user_id => @user.id,:activity_audit_id => @audit_log.id}) if @user.present?
      AuditAdapter.create({'user_id' => @user.id,'activity_audit_id' => @audit_log.to_i, 'created_at' => DateTime.now.utc.strftime("%F %T"), 'updated_at' => DateTime.now.utc.strftime("%F %T")}, 'activity_audit_logs','DELAYED')
    end

  end
end