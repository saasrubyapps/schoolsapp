
      class AuditAdapter

        cattr_accessor :connection
        @@connection = ActiveRecord::Base.connection

        def self.find_or_create(module_name, action, audit_type)
          connect_to_db
          conditions = "module = '#{module_name}' AND action = '#{action}' AND type = '#{audit_type}'"
          conditions += " AND school_id = #{MultiSchool.current_school.id}" if (MultiSchool rescue nil)
          cache_name = "mini_audit/#{audit_type}/#{module_name}/#{action}"
          cache_name +=  "/#{MultiSchool.current_school.id}" if (MultiSchool rescue nil)
          Rails.cache.fetch(cache_name){
            record = @@connection.execute <<-end_sql
                SELECT id,count(*)
                  FROM mini_audits
                WHERE #{conditions}
            end_sql

            result = record.fetch_row
            if result[1].to_i == 0
              fields_and_values = {'module' => module_name, 'action' => action , 'type' => audit_type, 'created_at' => DateTime.now.utc.strftime("%F %T"), 'updated_at' => DateTime.now.utc.strftime("%F %T")}
              create(fields_and_values,'mini_audits','')
            else
              return result[0]
            end
          }
      end

    def self.create(fields_and_values, table_name, delayed)
       connect_to_db
       fields_and_values['school_id'] = MultiSchool.current_school.id if (MultiSchool rescue nil)
       cols = fields_and_values.keys.join(',')
       cols_val = fields_and_values.values.map { |v| "'#{v}'" }.join(',')
       record = @@connection.insert <<-end_sql
         INSERT #{delayed} INTO #{table_name} (#{cols}) values (#{cols_val})
       end_sql
       return record
    end
    
    def self.connect_to_db
      @@connection.reconnect! unless(@@connection.active?)
    end
  end
