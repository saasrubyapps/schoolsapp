require File.join(File.dirname(__FILE__), "fedena_audit", "data_audit_mod")
require File.join(File.dirname(__FILE__), "fedena_audit", "activity_audit_mod")


module FedenaAudit

  mattr_accessor :disable_audit

  def self.attach_overrides
    Dispatcher.to_prepare :fedena_audit do

      ## Model Extensions
      ::Student.instance_eval {
        def audit_description(record, operation)
          desc = "first_name :#{record.first_name}\n adm_no : #{record.admission_no}"

          case operation

          when :create
            return desc

          when :update
            desc += "\n batch_text : #{record.batch.course.course_name} #{record.batch.name}" if record.changes.has_key? "batch_id"
            return desc

          when :destroy
            archived = ArchivedStudent.find_by_former_id(record.id)
            desc += "\n former_id : #{archived.former_id}" unless archived.nil?
            return desc

          end
        end

        audit :create, {:name => :student_creation}
        audit :update, {:name => :student_edit}
        audit :destroy, {:name => :student_deletion}
        
      }

      ::ArchivedStudent.instance_eval {
        def audit_description(record, operation)
          desc = "first_name : #{record.first_name}\n adm_no : #{record.admission_no}"
          case operation
          when :update
            desc += "\n date_of_leaving : #{record.date_of_leaving}"
            return desc
          when :create
            # desc += "\n former_id : #{record.former_id}"
            return desc
          end
        end

        audit :update, {:name => :archived_student_edit}
        audit :create, {:name => :student_archival}
      }

      ::Batch.instance_eval {
        def audit_description(record,operation)
          return "batch_text : #{record.course.course_name} #{record.name}"
        end

        audit :create, {:name => :batch_creation}
        audit :update, {:name => :batch_edit}
      }

      ::Course.instance_eval {
        def audit_description(record,operation)
          return "course_text : #{record.course_name}"
        end

        audit :create, {:name => :course_creation}
        audit :update, {:name => :course_edit}
      }

      ::Employee.instance_eval {
        def audit_description(record,operation)
          desc = "first_name :#{record.first_name}\n emp_no : #{record.employee_number}"
          case operation
          when :create
            return desc
          when :update
            desc += "\n department : #{record.employee_department.name}" if record.changes.has_key? "employee_department_id"
            return desc
          when :destroy
            archived = ArchivedEmployee.find_by_former_id(record.id)
            desc += "\n former_id : #{archived.former_id}" unless archived.nil?
            return desc
          end
        end

        audit :create, {:name => :employee_creation}
        audit :update, {:name => :employee_edit}
        audit :destroy, {:name => :employee_deletion }
      }

      ::FinanceTransaction.instance_eval{

        def audit_description(record,operation)
          return "finance_category : #{record.category.name}\n amount : #{record.amount}"
        end

        audit :create, {:name => :finance_transaction_creation}
        audit :destroy, {:name => :finance_transaction_deletion}
      }


      ::CancelledFinanceTransaction.instance_eval{
        def audit_description(record,operation)
          return "finance_category : #{record.category.name}\n amount : #{record.amount}"
        end

        audit :create, {:name => :revert_finance_transaction}
      }

      ::FinanceFeeCollection.instance_eval{
        def audit_description(record,operation)
          return "fee_collection_name : #{record.name}"
        end

        audit :create, {:name => :fee_collection_creation}
        audit :update, {:name => :fee_collection_edit}
        audit :destroy, {:name => :fee_collection_deletion}
      }


      ::FeeRefund.instance_eval{
        def audit_description(record,operation)
          return "student_name : #{record.finance_fee.student.first_name} \n amount : #{record.amount}\n"
        end

        audit :create, {:name => :fee_refund_creation}
      }

      ::TimetableEntry.instance_eval{        
        def audit_description(record,operation)
          desc = "batch :  #{record.batch.course.course_name} #{record.batch.name}"
          if record.employee_ids.present?
            record.employees.each do |emp|
              desc += "\n employee_text : #{emp.first_name}"
            end
            #          else
            #            desc += "\n employee_text : #{ArchivedEmployee.find_by_former_id(record.employee_id).first_name}"
          end
          desc += "\n subject_text : #{record.entry.name} "
          return desc
        end

        audit :create, {:name => :timetable_entry_creation}
        audit :update, {:name => :timetable_entry_edit}
        audit :destroy, {:name => :timetable_entry_deletion}
      }

      ::EmployeePayslip.instance_eval{
        def audit_description(record,operation)
          desc = "employee_text : #{record.employee.first_name}  \n date_text : #{record.payslips_date_range.date_range}"
          desc += "\n status : Pending" if !record.is_approved && !record.is_rejected
          desc += "\n status : Approved" if record.is_approved
          desc += "\n status : Rejected" if record.is_rejected
          return desc
        end

        audit :create, {:name => :create_employee_payslip}
        audit :update, {:name => :update_employee_payslip, :condition => lambda { audit_check.blank? }}
        audit :destroy, {:name => :destroy_employee_payslip}
      }
      ::IndividualPayslipCategory.instance_eval {
        def audit_description(record,operation)
          employee = record.employee_payslip.employee
          return "employee_text : #{employee.first_name}\n name : #{record.name}\n amount : #{record.amount}"
        end

        audit :create, {:name => :individual_payslip_category_creation}
        audit :update, {:name => :individual_payslip_category_edit}
        audit :destroy, {:name => :individual_payslip_category_deletion}
      }

      ::PayrollGroup.instance_eval {
        def audit_description(record,operation)
          return "payroll_group_name : #{record.name}"
        end

        audit :create, {:name => :payroll_group_creation}
        audit :update, {:name => :payroll_group_updation}
        audit :destroy, {:name => :payroll_group_deletion}
      }

      ::PayrollCategory.instance_eval {
        def audit_description(record,operation)
          return "payroll_category_name : #{record.name}"
        end

        audit :create, {:name => :payroll_category_creation}
        audit :update, {:name => :payroll_category_updation}
        audit :destroy, {:name => :payroll_category_deletion}
      }

      
      ::EmployeeSalaryStructure.instance_eval{
        def audit_description(record,operation)
          employee = record.employee
          return "employee_text : #{employee.first_name}"
        end

        audit :create , {:name => :payroll_creation}
        audit :update , {:name => :payroll_edit}
        audit :destroy, {:name => :payroll_deletion}
      }

      ::Exam.instance_eval{
        def audit_description(record,operation)
          return "exam_group : #{record.exam_group.name}\n batch : #{record.exam_group.batch.name} \n exam_text : #{record.subject.name}"
        end

        audit :create, {:name => :exam_creation}
        audit :update, {:name => :exam_edit}
        audit :destroy, {:name => :exam_deletion}
      }
      ## Controller Extensions
      if(OauthController rescue false)
        #      if defined?(OauthController)
        ::OauthController.instance_eval {
          audit :login, {:name => :login, :method => 'post'}
        }
      end
      
      ::TimetableController.instance_eval {
        audit :update_timetable_view, {:method => 'post', :name => :timetable_view}
        audit :new_timetable, {:method => 'post', :name => :timetable_creation}
        audit :update_student_tt, {:name => :timetable_view}
      }

      ::UserController.instance_eval {
        audit :login, {:name => :login, :method => 'post'}
        audit :logout, {:name => :logout}
      }

      ::AttendancesController.instance_eval {
        audit :daily_register, {:name => :attendances_view}
        audit :subject_wise_register, {:name => :attendances_view}
      }

      ::EventController.instance_eval {
        audit :index, {:method => 'post', :name => :event_creation_text}
      }

      ::FinanceController.instance_eval {
        audit :fee_collection_create, {:name => :fee_collection_creation}
        audit :master_category_create, {:name => :finance_category_create}
        audit :fees_particulars_create, {:name => :fee_particular_creation}
        audit :fees_particulars_new2, {:name => :fee_particular_creation}
        audit :employee_payslip_approve, {:name => :payslip_approval}
        audit :employee_payslip_reject, {:name => :payslip_reject}
        audit :payslip_revert_transaction, {:name => :payslip_revert}
        #        audit :one_click_approve_submit, {:name => :monthly_payslip_approve} #:one_click_payslip_approve}
        #        audit :employee_payslip_approve, {:name => :monthly_payslip_approve}
        #        audit :employee_payslip_reject, {:name => :monthly_payslip_reject}
      }
      #
      #      ::EmployeeController.instance_eval {
      #        audit :create_monthly_payslip, {:name => :monthly_payslip_creation, :method => 'post'}
      #        audit :one_click_payslip_generation, {:name => :monthly_payslip_creation}#:one_click_payslip_generation}
      #        audit :one_click_payslip_revert, {:name => :one_click_payslip_revert}
      #        audit :edit_rejected_payslip, {:name => :rejected_payslip_edit, :method => 'post'}
      #
      #      }

      ::EmployeePayslipsController.instance_eval {
         audit :save_employee_payslip, {:name => :generate_all_payslip}
         audit :create_employee_wise_payslip, {:name => :generate_employee_paylip}
         audit :revert_employee_payslip, {:name => :revert_employee_payslip}
         audit :revert_all_payslips, {:name => :revert_all_payslips}
         audit :update_payslip, {:name => :update_payslip}
      }
    end
  end
end
