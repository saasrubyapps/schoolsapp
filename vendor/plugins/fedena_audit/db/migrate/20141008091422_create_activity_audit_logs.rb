class CreateActivityAuditLogs < ActiveRecord::Migration
  def self.up
    create_table :activity_audit_logs do |t|
      t.integer :user_id
      t.integer :activity_audit_id
      t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :activity_audit_logs
  end
end
