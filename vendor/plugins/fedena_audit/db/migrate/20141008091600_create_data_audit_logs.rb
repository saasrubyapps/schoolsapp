class CreateDataAuditLogs < ActiveRecord::Migration
  def self.up
    create_table :data_audit_logs do |t|
      t.integer :user_id
      t.integer :record_id
      t.text :record_description
      t.integer :data_audit_id
      t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :data_audit_logs
  end
end
