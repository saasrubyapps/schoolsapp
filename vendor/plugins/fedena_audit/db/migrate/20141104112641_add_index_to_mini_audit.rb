class AddIndexToMiniAudit < ActiveRecord::Migration
  def self.up
    add_index :activity_audit_logs, [:activity_audit_id]
    add_index :data_audit_logs, [:data_audit_id]
    add_index :mini_audits, [:school_id]
    add_index :data_audit_logs, [:school_id]
    add_index :activity_audit_logs, [:school_id]
  end

  def self.down
    remove_index :activity_audit_logs, [:activity_audit_id]
    remove_index :data_audit_logs, [:data_audit_id]
    remove_index :mini_audits, [:school_id]
    remove_index :data_audit_logs, [:school_id]
    remove_index :activity_audit_logs, [:school_id]
  end
end

