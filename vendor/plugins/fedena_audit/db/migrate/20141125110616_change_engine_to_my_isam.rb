class ChangeEngineToMyIsam < ActiveRecord::Migration
   def self.table_engine(table, engine='MyISAM')
     execute "ALTER TABLE `#{table}` ENGINE = #{engine}"
   end

   def self.up
     table_engine :activity_audit_logs, 'MyISAM'
     table_engine :data_audit_logs, 'MyISAM'
   end

   def self.down
     table_engine :activity_audit_logs, 'InnoDB'
     table_engine :data_audit_logs, 'InnoDB'
   end
end
  

  
 
