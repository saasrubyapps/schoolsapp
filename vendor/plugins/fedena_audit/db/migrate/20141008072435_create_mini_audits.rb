class CreateMiniAudits < ActiveRecord::Migration
  def self.up
    create_table :mini_audits do |t|
      t.string :module
      t.string :action
      t.string :type
      t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :mini_audits
  end
end
