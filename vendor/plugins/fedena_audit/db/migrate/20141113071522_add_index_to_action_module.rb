class AddIndexToActionModule < ActiveRecord::Migration
  def self.up
    add_index :mini_audits, [:action]
    add_index :mini_audits, [:module, :action]
  end

  def self.down
    remove_index :mini_audits, [:action]
    remove_index :mini_audits, [:module, :action]
  end
end
