cat = MenuLinkCategory.find_by_name("data_and_reports")
unless cat.nil?
  cat.allowed_roles << :manage_audit unless cat.allowed_roles.include?(:manage_audit)
  cat.save

  higher_link=MenuLink.find_or_create_by_name_and_higher_link_id(:name=>'mini_audit_text',:target_controller=>'mini_audit',:target_action=>'index',:higher_link_id=>nil,:icon_class=>'mini_audit-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>cat.id)

  MenuLink.create(:name=>'activity_audit',:target_controller=>'mini_audit',:target_action=>'activity_audit',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>cat.id) unless MenuLink.exists?(:name=>'activity_audit')
  MenuLink.create(:name=>'user_audit',:target_controller=>'mini_audit',:target_action=>'user_audit',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>cat.id) unless MenuLink.exists?(:name=>'user_audit')
  MenuLink.create(:name=>'data_audit',:target_controller=>'mini_audit',:target_action=>'data_audit',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>cat.id) unless MenuLink.exists?(:name=>'data_audit')
end


privilege_tag=PrivilegeTag.find_by_name_tag("administration_operations")
Privilege.find_or_create_by_name :name => "ManageAudit", :description => "manage_audit_privilege", :privilege_tag_id => privilege_tag.id, :priority => 75
