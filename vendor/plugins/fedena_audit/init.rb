require 'dispatcher'
require 'translator'
require File.join(File.dirname(__FILE__), "lib", "fedena_audit")
require File.join(File.dirname(__FILE__), "config", "breadcrumbs")
require File.join(File.dirname(__FILE__), "lib", "audit_adapter")

FedenaPlugin.register = {
  :name=>"fedena_audit",
  :description=>"Fedena Audit Module",
  :auth_file=>"config/audit_auth.rb",
  :multischool_models=>%w{MiniAudit ActivityAuditLog DataAuditLog}
  #:dashboard_menu=>{:title=>"mini_audit_text",:controller => "mini_audit",:action => "index"},
 # :css_overrides=>[{:controller=> "user",:action=>"dashboard"}]
}


Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
  I18n.load_path.unshift(locale)
end

      
ActionController::Base.instance_eval do
  include FedenaAudit::ActivityAuditMod
end

ActiveRecord::Base.instance_eval do
  include FedenaAudit::DataAuditMod
end

FedenaAudit.attach_overrides